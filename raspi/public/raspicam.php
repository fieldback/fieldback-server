<?php
/**
 * recording script for raspberry pi 2 - improved for safety and reliability
 *
 * $_GET variables, tightly coupled to 'raspivid' functions - define the type of 'camera':
 *
 * width
 * height
 * fps
 * awb
 * iso
 * exposure
 * shutter
 *
 * $_POST variables, set by PHP script on server -> always needs to be present (even if this is not a raspberry Pi camera)
 *
 * action
 * camera_identifier (for multi-camera hosts)
 * capture_identifier (unique ID for that capture, saved in database)
 *
 * On install of the Pi, we need to run 'init' once to set up the response_url and the api_key for safety
 *
 */

/********************************************************************************************************/

define("INSTALLED_FILE_NAME", "../installed.php");
// Check if file exist, otherwise try to write with post data and exit
include_once(INSTALLED_FILE_NAME);

/********************************************************************************************************/

require_once("../helper_functions.php");

/********************************************************************************************************/

// Check if INSTA_KEY is valid -> given at install, and also check if other CONSTANTS are set
if(defined('INSTA_KEY') && defined('RESPONSE_URL') && defined('MAX_RECORD')) {
    keyPresent($_POST['insta_key']);
    if($_POST['insta_key'] != INSTA_KEY) {
        set_status_header(403);
        exit("not authorized");
    }

    /********************************************************************************************************/

    // Require the camera specific script -> replace this with an implementation depending on system
    require_once('../camera.php');

    /********************************************************************************************************/

    // Check if action value is valid
    keyPresent($_POST['action']);
    keyCheck($_POST['action'], [
        'start_session',
        'start_capture',
        'stop_capture',
        'stop_session',
        'get_thumbnail',
        'uninstall'
    ]);

    // Check if camera identifier is present
    keyPresent($_GET['camera_identifier']);

    // Create a new camera instance
    $camera = new Camera($_POST, $_GET, INSTA_KEY, RESPONSE_URL, MAX_RECORD);

    // Switch the action to go to right function
    switch($_POST['action']) {
        case 'start_session':
            $camera->start_session();
            break;

        case 'start_capture':
            $camera->start_capture();
            break;

        case 'stop_capture':
            $camera->stop_capture();
            break;

        case 'stop_session':
            $camera->stop_session();
            break;

        case 'get_thumbnail':
            $camera->get_thumbnail();
            break;

        case 'uninstall':
            if(unlink(INSTALLED_FILE_NAME)) {
                exit("uninstalled");
            } else {
                exit("uninstall failed");
            }
            break;
    }

} else {

    echo "INSTALLING";

    // ATTEMPT INSTALL
    keyPresent($_POST['insta_key']);
    keyPresent($_POST['response_url']);
    keyPresent($_POST['max_record']);

    file_put_contents(INSTALLED_FILE_NAME, "<?php
        define('INSTA_KEY', '".$_POST['insta_key']."');
        define('RESPONSE_URL', '".$_POST['response_url']."');
        define('MAX_RECORD', '".$_POST['max_record']."');
    ");
    set_status_header(200);
    exit("installed instant cam");
}