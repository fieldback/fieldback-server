<?php
keyCheck(['action', 'camera_identifier'], $_POST);

switch($_POST['action']) {
    case 'start_session':
        start_session($_POST);
        break;

    case 'start_capture':
        start_capture($_POST);
        break;

    case 'stop_capture':
        stop_capture($_POST);
        break;

    case 'stop_session':
        stop_session($_POST);
        break;

    case 'get_thumbnail':
        get_thumbnail($_POST);
        break;
}

function start_session($post)
{
    echo "RANDOMSTRINGNOTNEEDEDFORNOW";
}

function stop_session($post)
{
    echo "SESSIONIDENTIFIER";
}

function start_capture($post)
{
    keyCheck(['width', 'height', 'fps'], $_GET);
    keyCheck(['target_dir', 'moment_file_id', 'response_url', 'duration'], $post);
    $target_dir = escapeshellcmd($post['target_dir']);
    $moment_file_id = escapeshellcmd($post['moment_file_id']);
    $response_url = escapeshellcmd($post['response_url']);
    $duration = escapeshellcmd($post['duration']) * 1000; // In ms for this type of camera
    $width = escapeshellcmd($_GET['width']);
    $height = escapeshellcmd($_GET['height']);
    $fps = escapeshellcmd($_GET['fps']);
    $awb =  escapeshellcmd($_GET['awb']);
    $iso = escapeshellcmd($_GET['iso']);
    $exposure = escapeshellcmd($_GET['exposure']);
    $shutter = escapeshellcmd($_GET['shutter']);
    $cmd = "./capture.sh $target_dir $moment_file_id $response_url $duration $width $height $fps $awb $iso $exposure $shutter >/dev/null 2> /dev/null & echo $!";
    //echo $cmd;
    $pid = shell_exec($cmd);
    echo $pid;
}

function stop_capture($post)
{
    keyCheck(['process_id'], $post);
    exec("kill " . escapeshellarg($post['process_id']));
    echo "true";
}

function get_thumbnail($post)
{
    $width = 320;
    $height = 240;
    header('Content-type: image/jpeg');
    $cmd = "raspistill -w $width -h $height -t 50 -n -o -";
    passthru($cmd, $img);
    echo $img;
}

function keyCheck($keys, $array) {
    foreach($keys as $key) {
        if(!isset($array[$key]) || empty($array[$key])) die($key . " not set!");
    }
}

class Process{
    private $pid;
    private $command;

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            $this->runCom();
        }
    }
    private function runCom(){
        $command = 'nohup '.$this->command.' > /dev/null 2>&1 & echo $!';
        exec($command ,$op);
        $this->pid = (int)$op[0];
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    public function status(){
        $command = 'ps -p '.$this->pid;
        exec($command,$op);
        if (!isset($op[1]))return false;
        else return true;
    }

    public function start(){
        if ($this->command != '')$this->runCom();
        else return true;
    }

    public function stop(){
        $command = 'kill '.$this->pid;
        exec($command);
        if ($this->status() == false)return true;
        else return false;
    }
}