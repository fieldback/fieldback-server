<?php

class Camera {

    /**
     * Initialize camera specific settings
     */
    function __construct($post, $get, $insta_key, $response_url, $max_record)
    {
        $this->post = $post;
        $this->get = $get;
        $this->insta_key = $insta_key;
        $this->response_url = $response_url;
        $this->max_record = $max_record;
        $this->executable = "/var/www/capture.sh";
    }

    /**
     * Start a session -> the PI does not need active sessions so return random
     * @param $post
     */
    public function start_session()
    {
        set_status_header(200);
        exit();
    }

    /**
     * Stop a session -> the PI does not need active session so return random
     * @param $post
     */
    public function stop_session()
    {
        set_status_header(200);
        exit("sessionidentifier");
    }

    /**
     * Start a capture for x seconds which automatically ends after max_record, but can also be stopped earlier
     */
    public function start_capture()
    {
        // Check the width, height and FPS
        keyPresent($this->get['width']);
        keyPresent($this->get['height']);
        keyPresent($this->get['fps']);

        keyCheck($this->get['width'], [320, 640, 800, 1280, 1920]);
        keyCheck($this->get['height'], [240, 480, 600, 720, 1080]);
        keyCheck($this->get['fps'], [25, 30, 60, 90]);

        // Check duration
        keyPresent($this->post['duration']);
        // Validate duration
        if(($this->post['duration'] < 3) || ($this->post['duration'] > $this->max_record)) {
            set_status_header(400);
            exit("invalid duration");
        }

        // Check unique identifier of this moment_file
        keyPresent($this->post['unique_identifier']);

        // Check white balance present
        keyPresent($this->get['awb']);
        // Validate white balance
        keyCheck($this->get['awb'], array(
            'off',
            'auto',
            'sun',
            'cloud',
            'shade',
            'tungsten',
            'fluorescent',
            'incandescent',
            'flash',
            'horizon'
        ));

        // Check exposure present
        keyPresent($this->get['exposure']);
        // Validate exposure
        keyCheck($this->get['exposure'], array(
            'auto',
            'night',
            'backlight',
            'spotlight',
            'sports',
            'snow',
            'beach'
        ));

        // Escape for shell use, even though they should be all valid
        $response_url = escapeshellcmd($this->response_url);
        $duration = escapeshellcmd($this->post['duration']) * 1000; // ms for this type of camera
        $unique_identifier = escapeshellcmd($this->post['unique_identifier']);

        $width = escapeshellcmd($this->get['width']);
        $height = escapeshellcmd($this->get['height']);
        $fps = escapeshellcmd($this->get['fps']);
        $awb =  escapeshellcmd($this->get['awb']);
        $iso = escapeshellcmd($this->get['iso']);
        $exposure = escapeshellcmd($this->get['exposure']);

        // Check if the executable file is indeed executable
        if(!is_executable($this->executable)) { exit("permission error filesystem"); }

        // Build the command array
        $cmd_array = array(
            $this->executable,
            $width,
            $height,
            $fps,
            $awb,
            $exposure,
            $duration,
            $response_url,
            $unique_identifier,
            ">/dev/null 2> /var/www/logs/capturesh.log & echo $!"
        );
        $cmd = implode($cmd_array, " ");
        $pid = shell_exec($cmd);
        exit($pid);
    }

    public function stop_capture()
    {
        // Check if present
        keyPresent($this->post['process_id']);
        // Check if this process_id is indeed capture.sh or if it is already ended

        // Check if all log files are empty, otherwise send them along to be saved
        $files_to_check = array(
            "../logs/capturesh.log",
            "../logs/raspi.log",
            "../logs/rm.log",
            "../logs/rm_h264.log"
        );
        foreach($files_to_check as $file) {
            if(filesize($file) > 0) { echo $file. "\n" . file_get_contents($file) . "\n\n\n\n"; }
        }
        exec("kill " . escapeshellarg($this->post['process_id']));
        exit();
    }

    public function get_thumbnail()
    {
        $width = 320;
        $height = 240;
        header('Content-type: image/jpeg');
        $cmd = "raspistill -w $width -h $height -t 50 -n -o -";
        passthru($cmd, $img);
        echo $img;
    }
}