#!/usr/bin/env bash

# Send to server once completed or terminated
sendToServer() {
    # if raspivid is still running, kill it! It frees up the .h264 file for conversion with MP4Box
    kill ${RASPID}

    # Set capture LED to off
    /usr/local/bin/gpio write 23 0

    # Set 'Sending' LED to on
    /usr/local/bin/gpio write 25 1

    # Convert video with MP4Box to .mp4 and remove .h264 file as we don't use it anymore
    MP4Box -fps ${FPS} -add ../video/${UNIQUE_ID}.h264 ../video/_${UNIQUE_ID}.mp4
    rm ../video/${UNIQUE_ID}.h264 > /dev/null 2> ../logs/rm_h264.log


    # send back to the server
    curl -F "moment_file=@../video/_$UNIQUE_ID.mp4" -F "unique_identifier=$UNIQUE_ID" -F "timestamp=$STARTED" -F "fps=$FPS" ${URL} > /dev/null 2> ../logs/curl.log
    rm ../video/_${UNIQUE_ID}.mp4 > /dev/null 2> ../logs/rm.log

    # log entry
    echo $(date -u) " - file captured and uploaded with ID: ${UNIQUE_ID}" >> ../logs/uploaded.log

    # Set 'Sending' LED to off
    /usr/local/bin/gpio write 25 0

	exit
}

# Set a trap
trap sendToServer SIGHUP SIGINT SIGTERM

if [ $# -eq 8 ]; then
    WIDTH=$1
    HEIGHT=$2
    FPS=$3
    AWB=$4
    EXPOSURE=$5
    DURATION=$6
    URL=$7
    UNIQUE_ID=$8

    # Set current timestamp to send to server
    STARTED=$(date +"%s%N")

    # INIT LEDS
    /usr/local/bin/gpio mode 23 out # Rood
    /usr/local/bin/gpio mode 25 out # Geel

    # Set capture LED to on
    /usr/local/bin/gpio write 23 1

    # open raspivid and capture output to log files
    raspivid -n -w ${WIDTH} -h ${HEIGHT} -fps ${FPS} -t ${DURATION} -awb ${AWB} -ex ${EXPOSURE} --intra 5 -o ../video/${UNIQUE_ID}.h264 > /dev/null 2> ../logs/raspi.log &
    RASPID=$!
    wait ${RASPID}
    sendToServer
fi

# this is only run when parameters are not correct
echo "error: not enough parameters"
exit