# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.0.95)
# Database: fb_v2
# Generation Time: 2015-05-06 12:57:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL auto_increment,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  PRIMARY KEY  (`camera_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`)
VALUES
	(26,'Axis Camera Q1614','rtsp://192.168.0.94/axis-media/media.amp',1,1,'30','1',1);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cams`;

CREATE TABLE `cams` (
  `cam_id` int(11) NOT NULL auto_increment,
  `stream` varchar(50) NOT NULL default '0',
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `fps` int(11) NOT NULL default '30',
  `constante` int(11) NOT NULL default '1',
  `active` tinyint(4) NOT NULL default '0',
  `copy` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`cam_id`),
  UNIQUE KEY `cam_id` (`cam_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='This table holds the information about the camera''s, and is ';

LOCK TABLES `cams` WRITE;
/*!40000 ALTER TABLE `cams` DISABLE KEYS */;

INSERT INTO `cams` (`cam_id`, `stream`, `name`, `description`, `location`, `fps`, `constante`, `active`, `copy`)
VALUES
	(1,'rtsp://192.168.2.120/stream1','Pelco 1','Camera achter doel','Veld 1, Noord',30,1,0,1),
	(2,'rtsp://192.168.2.121/stream1','Pelco 2','Camera achter doel','Veld 1, Zuid',30,1,1,1),
	(3,'rtsp://192.168.0.84/stream1','Office','Test Cam in the Office','Near the window',30,1,1,1),
	(4,'rtsp://10.0.0.79/stream1','Pelco','Pelco camera HD','In the window',30,1,1,1);

/*!40000 ALTER TABLE `cams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clip_to_part` WRITE;
/*!40000 ALTER TABLE `clip_to_part` DISABLE KEYS */;

INSERT INTO `clip_to_part` (`clip_id`, `part_id`, `cam`)
VALUES
	(1,1,0),
	(2,2,0),
	(3,4,0),
	(4,3,0),
	(5,5,0),
	(6,8,0),
	(7,9,0),
	(8,7,0),
	(9,11,0),
	(10,12,0),
	(11,13,0),
	(12,14,0),
	(13,17,0),
	(14,19,0),
	(15,20,0),
	(16,21,0),
	(17,24,0),
	(18,25,0),
	(19,26,0),
	(20,27,0),
	(21,28,0),
	(22,29,0),
	(23,31,0),
	(24,32,0),
	(25,33,0),
	(26,30,0),
	(27,34,0),
	(28,35,0),
	(29,36,0),
	(30,37,1),
	(31,40,0),
	(32,42,0),
	(33,41,0),
	(34,45,0),
	(35,43,0),
	(36,43,0),
	(37,46,0),
	(38,47,0),
	(39,48,0),
	(40,49,0),
	(41,50,0),
	(42,51,0),
	(43,52,0),
	(44,53,0),
	(45,61,0),
	(46,59,0),
	(47,58,0),
	(48,60,0),
	(49,55,0),
	(50,57,0),
	(51,56,0),
	(52,63,0),
	(53,62,0),
	(54,54,0),
	(55,64,0),
	(56,65,0);

/*!40000 ALTER TABLE `clip_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL auto_increment,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY  (`clip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

LOCK TABLES `clips` WRITE;
/*!40000 ALTER TABLE `clips` DISABLE KEYS */;

INSERT INTO `clips` (`clip_id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `date_created`, `kind`)
VALUES
	(1,'Ajax8.MOV','video/quicktime','/var/www/html/v2/videos/1/1/','/var/www/html/v2/videos/1/1/Ajax8.MOV','Ajax8','Ajax8.MOV','Ajax8.MOV','.MOV','17397.89',1411998849,'upload'),
	(2,'Ajax8.MOV','video/quicktime','/var/www/html/v2/videos/1/2/','/var/www/html/v2/videos/1/2/Ajax8.MOV','Ajax8','Ajax8.MOV','Ajax8.MOV','.MOV','17397.89',1412029920,'upload'),
	(3,'fieldback_system_1280x720.mp4','video/mp4','/var/www/html/v2/videos/2/4/','/var/www/html/v2/videos/2/4/fieldback_system_1280x720.mp4','fieldback_system_1280x720','fieldback_system_1280x720.mp4','fieldback_system_1280x720.mp4','.mp4','15307.52',1412077555,'upload'),
	(4,'fieldback_system_1280x720.mp4','video/mp4','/var/www/html/v2/videos/1/3/','/var/www/html/v2/videos/1/3/fieldback_system_1280x720.mp4','fieldback_system_1280x720','fieldback_system_1280x720.mp4','fieldback_system_1280x720.mp4','.mp4','15307.52',1412077947,'upload'),
	(5,'Frank_de_Boer_kort.mp4','video/mp4','/var/www/html/v2/videos/1/5/','/var/www/html/v2/videos/1/5/Frank_de_Boer_kort.mp4','Frank_de_Boer_kort','Frank_de_Boer_kort.mp4','Frank de Boer kort.mp4','.mp4','20732.8',1412078081,'upload'),
	(6,'PositieSpel_2_teams_individueel_short.mp4','application/octet-stream','/var/www/html/v2/videos/1/8/','/var/www/html/v2/videos/1/8/PositieSpel_2_teams_individueel_short.mp4','PositieSpel_2_teams_individueel_short','PositieSpel_2_teams_individueel_short.mp4','PositieSpel 2 teams individueel short.mp4','.mp4','16349.4',1412079624,'upload'),
	(7,'Deurne_2-NWC2.mp4','application/octet-stream','/var/www/html/v2/videos/4/9/','/var/www/html/v2/videos/4/9/Deurne_2-NWC2.mp4','Deurne_2-NWC2','Deurne_2-NWC2.mp4','Deurne 2-NWC2.mp4','.mp4','1645657.19',1412080674,'upload'),
	(8,'NWC2-BlauwGeel2.mp4','application/octet-stream','/var/www/html/v2/videos/3/7/','/var/www/html/v2/videos/3/7/NWC2-BlauwGeel2.mp4','NWC2-BlauwGeel2','NWC2-BlauwGeel2.mp4','NWC2-BlauwGeel2.mp4','.mp4','1612485.9',1412148839,'upload'),
	(9,'Smartgoals_IC_fin_med.mov','video/quicktime','/var/www/html/v2/videos/5/11/','/var/www/html/v2/videos/5/11/Smartgoals_IC_fin_med.mov','Smartgoals_IC_fin_med','Smartgoals_IC_fin_med.mov','Smartgoals  IC fin med.mov','.mov','330101.73',1412627000,'upload'),
	(10,'000021.mp4','video/mp4','/var/www/html/v2/videos/5/12/','/var/www/html/v2/videos/5/12/000021.mp4','000021','000021.mp4','000021.mp4','.mp4','239323.9',1412683263,'upload'),
	(11,'NWC-Meersen-TweedeHelft.mp4','application/octet-stream','/var/www/html/v2/videos/5/13/','/var/www/html/v2/videos/5/13/NWC-Meersen-TweedeHelft.mp4','NWC-Meersen-TweedeHelft','NWC-Meersen-TweedeHelft.mp4','NWC-Meersen-TweedeHelft.mp4','.mp4','1602534.57',1412696000,'upload'),
	(12,'NWC-Meersen_Eerste_Helft.mp4','application/octet-stream','/var/www/html/v2/videos/5/14/','/var/www/html/v2/videos/5/14/NWC-Meersen_Eerste_Helft.mp4','NWC-Meersen_Eerste_Helft','NWC-Meersen_Eerste_Helft.mp4','NWC-Meersen Eerste Helft.mp4','.mp4','1642642.71',1412752151,'upload'),
	(13,'Rosmalen-Tilburg.wmv','application/octet-stream','/var/www/html/v2/videos/8/17/','/var/www/html/v2/videos/8/17/Rosmalen-Tilburg.wmv','Rosmalen-Tilburg','Rosmalen-Tilburg.wmv','Rosmalen-Tilburg.wmv','.wmv','1995545.21',1412763991,'upload'),
	(14,'Tilburg-Rosmalen.wmv','application/octet-stream','/var/www/html/v2/videos/9/19/','/var/www/html/v2/videos/9/19/Tilburg-Rosmalen.wmv','Tilburg-Rosmalen','Tilburg-Rosmalen.wmv','Tilburg-Rosmalen.wmv','.wmv','1953185.17',1412769094,'upload'),
	(15,'Nuenen-Tilburg.wmv','application/octet-stream','/var/www/html/v2/videos/10/20/','/var/www/html/v2/videos/10/20/Nuenen-Tilburg.wmv','Nuenen-Tilburg','Nuenen-Tilburg.wmv','Nuenen-Tilburg.wmv','.wmv','1981351.05',1412773161,'upload'),
	(16,'00002.MTS','application/octet-stream','/var/www/html/v2/videos/11/21/','/var/www/html/v2/videos/11/21/00002.MTS','00002','00002.MTS','00002.MTS','.MTS','1189056',1414361289,'upload'),
	(17,'NWC-HVCH_Eerstehelft.mp4','application/octet-stream','/var/www/html/v2/videos/11/24/','/var/www/html/v2/videos/11/24/NWC-HVCH_Eerstehelft.mp4','NWC-HVCH_Eerstehelft','NWC-HVCH_Eerstehelft.mp4','NWC-HVCH_Eerstehelft.mp4','.mp4','730906.74',1414398220,'upload'),
	(18,'NWC-HVCH_TweedeHelft.mp4','application/octet-stream','/var/www/html/v2/videos/11/25/','/var/www/html/v2/videos/11/25/NWC-HVCH_TweedeHelft.mp4','NWC-HVCH_TweedeHelft','NWC-HVCH_TweedeHelft.mp4','NWC-HVCH_TweedeHelft.mp4','.mp4','374893.25',1414398263,'upload'),
	(19,'cultentumult.mp4','application/octet-stream','/var/www/html/v2/videos/12/26/','/var/www/html/v2/videos/12/26/cultentumult.mp4','cultentumult','cultentumult.mp4','cultentumult.mp4','.mp4','78123.3',1414658881,'upload'),
	(20,'00004.mp4','video/mp4','/var/www/html/v2/videos/13/27/','/var/www/html/v2/videos/13/27/00004.mp4','00004','00004.mp4','00004.mp4','.mp4','1542.98',1414660115,'upload'),
	(21,'cultentumult.mp4','application/octet-stream','/var/www/html/v2/videos/14/28/','/var/www/html/v2/videos/14/28/cultentumult.mp4','cultentumult','cultentumult.mp4','cultentumult.mp4','.mp4','78123.3',1414660531,'upload'),
	(22,'outro_Fieldback_ASML.mp4','video/mp4','/var/www/html/v2/videos/15/29/','/var/www/html/v2/videos/15/29/outro_Fieldback_ASML.mp4','outro_Fieldback_ASML','outro_Fieldback_ASML.mp4','outro Fieldback ASML.mp4','.mp4','1556.05',1414660582,'upload'),
	(23,'00004.mp4','video/mp4','/var/www/html/v2/videos/16/31/','/var/www/html/v2/videos/16/31/00004.mp4','00004','00004.mp4','00004.mp4','.mp4','1542.98',1414661587,'upload'),
	(24,'S1000003.MP4','video/mp4','/var/www/html/v2/videos/16/32/','/var/www/html/v2/videos/16/32/S1000003.MP4','S1000003','S1000003.MP4','S1000003.MP4','.MP4','9888',1414661643,'upload'),
	(25,'Tilburg-Nijmegen_small.wmv','application/octet-stream','/var/www/html/v2/videos/17/33/','/var/www/html/v2/videos/17/33/Tilburg-Nijmegen_small.wmv','Tilburg-Nijmegen_small','Tilburg-Nijmegen_small.wmv','Tilburg-Nijmegen_small.wmv','.wmv','1899770.22',1414934932,'upload'),
	(26,'capturedvideo.MOV','video/quicktime','/var/www/html/v2/videos/15/30/','/var/www/html/v2/videos/15/30/capturedvideo.MOV','capturedvideo','capturedvideo.MOV','capturedvideo.MOV','.MOV','380.6',1415055104,'upload'),
	(27,'00024.MTS','application/octet-stream','/var/www/html/v2/videos/15/34/','/var/www/html/v2/videos/15/34/00024.MTS','00024','00024.MTS','00024.MTS','.MTS','81468',1415449824,'upload'),
	(28,'Masterprincipe_1_NL.mp4','video/mp4','/var/www/html/v2/videos/15/35/','/var/www/html/v2/videos/15/35/Masterprincipe_1_NL.mp4','Masterprincipe_1_NL','Masterprincipe_1_NL.mp4','Masterprincipe 1 NL.mp4','.mp4','22584.58',1415450512,'upload'),
	(29,'Afstandsbediening_Teams.mp4','video/mp4','/var/www/html/v2/videos/15/36/','/var/www/html/v2/videos/15/36/Afstandsbediening_Teams.mp4','Afstandsbediening_Teams','Afstandsbediening_Teams.mp4','Afstandsbediening_Teams.mp4','.mp4','35673',1415451391,'upload'),
	(30,'rec1-video-H264-1','video/mp4','./videos/15/37/tmp_rec/rec1-video-H264-1','./videos/15/37/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','379825659',1415454006,'recording'),
	(31,'V_tiki_taka.mov','video/quicktime','/var/www/html/v2/videos/18/40/','/var/www/html/v2/videos/18/40/V_tiki_taka.mov','V_tiki_taka','V_tiki_taka.mov','V tiki taka.mov','.mov','135727.52',1415644278,'upload'),
	(32,'Presentatie_DDW_Fast.mp4','video/mp4','/var/www/html/v2/videos/18/42/','/var/www/html/v2/videos/18/42/Presentatie_DDW_Fast.mp4','Presentatie_DDW_Fast','Presentatie_DDW_Fast.mp4','Presentatie_DDW_Fast.mp4','.mp4','38363.1',1415644626,'upload'),
	(33,'00004.mp4','application/octet-stream','/var/www/html/v2/videos/18/41/','/var/www/html/v2/videos/18/41/00004.mp4','00004','00004.mp4','00004.mp4','.mp4','1582120.47',1415645812,'upload'),
	(34,'NWC2-Laar3.mp4','application/octet-stream','/var/www/html/v2/videos/19/45/','/var/www/html/v2/videos/19/45/NWC2-Laar3.mp4','NWC2-Laar3','NWC2-Laar3.mp4','NWC2-Laar3.mp4','.mp4','25692.76',1415646215,'upload'),
	(35,'NWC3-Laar-Deel2.mp4','application/octet-stream','/var/www/html/v2/videos/18/43/','/var/www/html/v2/videos/18/43/NWC3-Laar-Deel2.mp4','NWC3-Laar-Deel2','NWC3-Laar-Deel2.mp4','NWC3-Laar-Deel2.mp4','.mp4','1583709.57',1415646336,'upload'),
	(36,'NWC-Meersen-Helft1.mp4','application/octet-stream','/var/www/html/v2/videos/18/43/','/var/www/html/v2/videos/18/43/NWC-Meersen-Helft1.mp4','NWC-Meersen-Helft1','NWC-Meersen-Helft1.mp4','NWC-Meersen-Helft1.mp4','.mp4','712002.35',1415646446,'upload'),
	(37,'00004.MTS','application/octet-stream','/var/www/html/v2/videos/19/46/','/var/www/html/v2/videos/19/46/00004.MTS','00004','00004.MTS','00004.MTS','.MTS','1740186',1415647853,'upload'),
	(38,'00007.MTS','application/octet-stream','/var/www/html/v2/videos/19/47/','/var/www/html/v2/videos/19/47/00007.MTS','00007','00007.MTS','00007.MTS','.MTS','1739388',1415649396,'upload'),
	(39,'00000.MTS','application/octet-stream','/var/www/html/v2/videos/20/48/','/var/www/html/v2/videos/20/48/00000.MTS','00000','00000.MTS','00000.MTS','.MTS','1813194',1417456420,'upload'),
	(40,'00001.MTS','application/octet-stream','/var/www/html/v2/videos/20/49/','/var/www/html/v2/videos/20/49/00001.MTS','00001','00001.MTS','00001.MTS','.MTS','1781724',1417524592,'upload'),
	(41,'00001.MTS','application/octet-stream','/var/www/html/v2/videos/21/50/','/var/www/html/v2/videos/21/50/00001.MTS','00001','00001.MTS','00001.MTS','.MTS','1187520',1423609146,'upload'),
	(42,'NWC-Susteren_1.mp4','video/mp4','/var/www/html/v2/videos/21/51/','/var/www/html/v2/videos/21/51/NWC-Susteren_1.mp4','NWC-Susteren_1','NWC-Susteren_1.mp4','NWC-Susteren_1.mp4','.mp4','716696.78',1423815523,'upload'),
	(43,'NWC-Susteren_3.mp4','video/mp4','/var/www/html/v2/videos/21/52/','/var/www/html/v2/videos/21/52/NWC-Susteren_3.mp4','NWC-Susteren_3','NWC-Susteren_3.mp4','NWC-Susteren_3.mp4','.mp4','293238.88',1423815529,'upload'),
	(44,'fotoshoot.wmv','video/x-ms-asf','/var/www/html/v2/videos/21/53/','/var/www/html/v2/videos/21/53/fotoshoot.wmv','fotoshoot','fotoshoot.wmv','fotoshoot.wmv','.wmv','3570.28',1423816392,'upload'),
	(45,'00007.MTS','application/octet-stream','/var/www/html/v2/videos/22/61/','/var/www/html/v2/videos/22/61/00007.MTS','00007','00007.MTS','00007.MTS','.MTS','114756',1426202816,'upload'),
	(46,'00005.MTS','application/octet-stream','/var/www/html/v2/videos/22/59/','/var/www/html/v2/videos/22/59/00005.MTS','00005','00005.MTS','00005.MTS','.MTS','282918',1426204605,'upload'),
	(47,'00004.MTS','application/octet-stream','/var/www/html/v2/videos/22/58/','/var/www/html/v2/videos/22/58/00004.MTS','00004','00004.MTS','00004.MTS','.MTS','672024',1426205368,'upload'),
	(48,'00006.MTS','application/octet-stream','/var/www/html/v2/videos/22/60/','/var/www/html/v2/videos/22/60/00006.MTS','00006','00006.MTS','00006.MTS','.MTS','185478',1426235775,'upload'),
	(49,'00000.MTS','application/octet-stream','/var/www/html/v2/videos/22/55/','/var/www/html/v2/videos/22/55/00000.MTS','00000','00000.MTS','00000.MTS','.MTS','1086534',1426238791,'upload'),
	(50,'00003.MTS','application/octet-stream','/var/www/html/v2/videos/22/57/','/var/www/html/v2/videos/22/57/00003.MTS','00003','00003.MTS','00003.MTS','.MTS','917028',1426239072,'upload'),
	(51,'De_Bieb_promotiefilm.wmv','video/x-ms-asf','/var/www/html/v2/videos/22/56/','/var/www/html/v2/videos/22/56/De_Bieb_promotiefilm.wmv','De_Bieb_promotiefilm','De_Bieb_promotiefilm.wmv','De Bieb promotiefilm.wmv','.wmv','39658.72',1426240660,'upload'),
	(52,'00002.MTS','application/octet-stream','/var/www/html/v2/videos/23/63/','/var/www/html/v2/videos/23/63/00002.MTS','00002','00002.MTS','00002.MTS','.MTS','1810344',1427238093,'upload'),
	(53,'00001.MTS','application/octet-stream','/var/www/html/v2/videos/23/62/','/var/www/html/v2/videos/23/62/00001.MTS','00001','00001.MTS','00001.MTS','.MTS','1850418',1427238129,'upload'),
	(54,'capturedvideo.MOV','video/quicktime','/var/www/html/v2/videos/15/54/','/var/www/html/v2/videos/15/54/capturedvideo.MOV','capturedvideo','capturedvideo.MOV','capturedvideo.MOV','.MOV','170.51',1428672817,'upload'),
	(55,'trim.D6CB9C0C-A447-4700-B7A4-D82A015678F2_.MOV','video/quicktime','/var/www/html/v2/videos/15/64/','/var/www/html/v2/videos/15/64/trim.D6CB9C0C-A447-4700-B7A4-D82A015678F2_.MOV','trim.D6CB9C0C-A447-4700-B7A4-D82A015678F2_','trim.D6CB9C0C-A447-4700-B7A4-D82A015678F2_.MOV','trim.D6CB9C0C-A447-4700-B7A4-D82A015678F2_.MOV','.MOV','15468.21',1428673808,'upload'),
	(56,'trim.680B24E2-6D67-4E78-B1AC-F526B5BE1CE8_.MOV','video/quicktime','/var/www/html/v2/videos/15/65/','/var/www/html/v2/videos/15/65/trim.680B24E2-6D67-4E78-B1AC-F526B5BE1CE8_.MOV','trim.680B24E2-6D67-4E78-B1AC-F526B5BE1CE8_','trim.680B24E2-6D67-4E78-B1AC-F526B5BE1CE8_.MOV','trim.680B24E2-6D67-4E78-B1AC-F526B5BE1CE8_.MOV','.MOV','2440.57',1428674218,'upload');

/*!40000 ALTER TABLE `clips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `creator_to_event` WRITE;
/*!40000 ALTER TABLE `creator_to_event` DISABLE KEYS */;

INSERT INTO `creator_to_event` (`creator_id`, `event_id`)
VALUES
	(4,1),
	(4,2),
	(4,3),
	(4,4),
	(4,5),
	(40,9),
	(40,8),
	(40,10),
	(4,11),
	(40,17),
	(4,15),
	(4,19),
	(4,20),
	(4,21),
	(4,22),
	(92,23);

/*!40000 ALTER TABLE `creator_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `event_to_organisation` WRITE;
/*!40000 ALTER TABLE `event_to_organisation` DISABLE KEYS */;

INSERT INTO `event_to_organisation` (`event_id`, `organisation_id`)
VALUES
	(1,1),
	(2,1),
	(3,2),
	(4,2),
	(5,2),
	(9,3),
	(8,3),
	(10,3),
	(11,2),
	(17,3),
	(15,1),
	(19,2),
	(20,2),
	(21,2),
	(22,2),
	(23,2);

/*!40000 ALTER TABLE `event_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL auto_increment,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY  (`event_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`event_id`, `event_name`, `event_description`, `date_created`)
VALUES
	(1,'Je weet','ja toch','1411997829'),
	(2,'Test 2','Hoallo','1412077533'),
	(3,'NWC2 - Blauw Geel2','NWC 2 - Blauw geel 21 sept','1412078713'),
	(4,'Deurne 2 - NWC 2','28 sept','1412079786'),
	(5,'NWC - Meersen','1e Klasse 1-4','1412626430'),
	(9,'Tilburg-Rosmalen','Tilburg-Rosmalen 13 september 2014 (5-0)','1412768090'),
	(8,'Rosmalen-Tilburg','Rosmalen-Tilburg 4 oktober 2014 (1-4)','1412763247'),
	(10,'Nuenen-Tilburg','Nuenen-Tilburg 6 september 2014','1412772418'),
	(11,'NWC2 - HVCH 2','NWC Tegen HVCH','1414360758'),
	(17,'Tilburg-Nijmegen','Tilburg-Nijmegen','1414931790'),
	(15,'test6','hoi','1414660522'),
	(19,'NWC 3 - Laar','1-0 winst','1415645295'),
	(20,'RKMSV 2','NWC3 - RKMSV2 2-0','1417387087'),
	(21,'NWC - Susteren','Susteren Thuis 0-3','1423606718'),
	(22,'Blauw Geel','In het Helmond Sport Stadion','1426201545'),
	(23,'Bekkerveld 1 Thuis','Thuiswedstrijd 1-4','1427232178');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`version`)
VALUES
	(2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_file_to_moment` WRITE;
/*!40000 ALTER TABLE `moment_file_to_moment` DISABLE KEYS */;

INSERT INTO `moment_file_to_moment` (`moment_file_id`, `moment_id`, `cam`)
VALUES
	(1,1,0),
	(2,2,0),
	(3,3,0),
	(44,44,0),
	(173,174,0),
	(166,167,0),
	(7,7,0),
	(182,183,0),
	(9,9,0),
	(80,80,0),
	(83,83,0),
	(82,82,0),
	(40,40,0),
	(45,45,0),
	(41,41,0),
	(42,42,0),
	(43,43,0),
	(46,46,0),
	(47,47,0),
	(48,48,0),
	(49,49,0),
	(50,50,0),
	(51,51,0),
	(52,52,0),
	(53,53,0),
	(54,54,0),
	(55,55,0),
	(56,56,0),
	(57,57,0),
	(58,58,0),
	(85,85,0),
	(60,60,0),
	(62,62,0),
	(79,79,0),
	(76,76,0),
	(66,66,0),
	(67,67,0),
	(84,84,1),
	(87,87,0),
	(88,88,0),
	(89,89,0),
	(77,77,0),
	(90,90,0),
	(92,92,0),
	(93,93,0),
	(94,94,0),
	(95,95,0),
	(96,96,0),
	(97,97,0),
	(98,98,0),
	(99,99,0),
	(100,100,0),
	(101,101,0),
	(102,102,0),
	(103,103,0),
	(104,104,0),
	(105,105,0),
	(106,106,0),
	(107,107,0),
	(121,121,0),
	(109,109,0),
	(110,110,0),
	(111,111,0),
	(112,112,0),
	(113,113,0),
	(114,114,0),
	(115,115,0),
	(116,116,0),
	(117,117,0),
	(118,118,0),
	(119,119,0),
	(120,120,0),
	(122,122,0),
	(167,168,0),
	(169,170,0),
	(175,176,0),
	(159,159,0),
	(155,155,0),
	(168,169,0),
	(184,185,0),
	(177,178,0),
	(157,157,0),
	(171,172,0),
	(172,173,0),
	(170,171,0),
	(181,182,0),
	(183,184,0),
	(185,186,0),
	(186,187,0),
	(187,190,0),
	(188,192,0),
	(189,193,0),
	(190,194,0),
	(191,195,0),
	(192,196,0),
	(212,218,0),
	(194,198,0),
	(195,199,0),
	(196,200,0),
	(211,216,0),
	(198,202,0),
	(199,203,0),
	(200,204,0),
	(201,205,0),
	(202,206,0),
	(203,207,0),
	(210,214,0),
	(206,210,0),
	(213,219,0),
	(214,220,0),
	(215,221,0),
	(216,222,0),
	(217,223,0),
	(218,224,0),
	(219,225,0),
	(220,226,0),
	(221,227,0),
	(222,228,0),
	(223,229,0),
	(224,230,0),
	(225,231,0),
	(226,232,0),
	(227,233,0),
	(228,234,0),
	(230,236,0),
	(231,237,0),
	(232,238,0),
	(233,239,0),
	(234,240,0),
	(235,241,0),
	(241,247,0),
	(242,248,0),
	(239,245,0),
	(240,246,0);

/*!40000 ALTER TABLE `moment_file_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL auto_increment,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  PRIMARY KEY  (`moment_file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;

LOCK TABLES `moment_files` WRITE;
/*!40000 ALTER TABLE `moment_files` DISABLE KEYS */;

INSERT INTO `moment_files` (`moment_file_id`, `filename`, `path`, `cam`)
VALUES
	(1,'1.mp4','videos/1/1/1',0),
	(2,'2.mp4','videos/1/8/2',0),
	(3,'3.mp4','videos/5/12/3',0),
	(40,'40.mp4','videos/4/9/40',0),
	(173,'173.mp4','videos/5/13/174',0),
	(166,'166.mp4','videos/21/52/167',0),
	(7,'7.mp4','videos/5/13/7',0),
	(9,'9.mp4','videos/5/13/9',0),
	(80,'80.mp4','videos/10/20/80',0),
	(83,'83.mp4','videos/10/20/83',0),
	(82,'82.mp4','videos/10/20/82',0),
	(45,'45.mp4','videos/4/9/45',0),
	(41,'41.mp4','videos/4/9/41',0),
	(42,'42.mp4','videos/4/9/42',0),
	(43,'43.mp4','videos/4/9/43',0),
	(44,'44.mp4','videos/4/9/44',0),
	(46,'46.mp4','videos/4/9/46',0),
	(47,'47.mp4','videos/4/9/47',0),
	(48,'48.mp4','videos/4/9/48',0),
	(49,'49.mp4','videos/4/9/49',0),
	(50,'50.mp4','videos/4/9/50',0),
	(51,'51.mp4','videos/4/9/51',0),
	(52,'52.mp4','videos/4/9/52',0),
	(53,'53.mp4','videos/4/9/53',0),
	(54,'54.mp4','videos/4/9/54',0),
	(55,'55.mp4','videos/4/9/55',0),
	(56,'56.mp4','videos/4/9/56',0),
	(57,'57.mp4','videos/4/9/57',0),
	(58,'58.mp4','videos/4/9/58',0),
	(85,'85.mp4','videos/11/25/85',0),
	(60,'60.mp4','videos/4/9/60',0),
	(62,'62.mp4','videos/11/24/62',0),
	(79,'79.mp4','videos/11/25/79',0),
	(76,'76.mp4','videos/15/29/76',0),
	(66,'66.mp4','videos/12/26/66',0),
	(67,'67.mp4','videos/12/26/67',0),
	(90,'90.mp4','videos/11/24/90',0),
	(84,'84.mp4','videos/15/37/84',1),
	(87,'87.mp4','videos/11/24/87',0),
	(88,'88.mp4','videos/11/24/88',0),
	(89,'89.mp4','videos/11/24/89',0),
	(77,'77.mp4','videos/15/29/77',0),
	(93,'93.mp4','videos/11/24/93',0),
	(92,'92.mp4','videos/11/24/92',0),
	(94,'94.mp4','videos/11/24/94',0),
	(95,'95.mp4','videos/11/24/95',0),
	(96,'96.mp4','videos/11/24/96',0),
	(97,'97.mp4','videos/11/24/97',0),
	(98,'98.mp4','videos/11/24/98',0),
	(99,'99.mp4','videos/11/24/99',0),
	(100,'100.mp4','videos/19/46/100',0),
	(101,'101.mp4','videos/19/46/101',0),
	(102,'102.mp4','videos/19/46/102',0),
	(103,'103.mp4','videos/19/46/103',0),
	(104,'104.mp4','videos/19/46/104',0),
	(105,'105.mp4','videos/19/46/105',0),
	(106,'106.mp4','videos/19/46/106',0),
	(107,'107.mp4','videos/19/46/107',0),
	(121,'121.mp4','videos/5/14/121',0),
	(109,'109.mp4','videos/19/46/109',0),
	(110,'110.mp4','videos/19/46/110',0),
	(111,'111.mp4','videos/19/46/111',0),
	(112,'112.mp4','videos/19/46/112',0),
	(113,'113.mp4','videos/19/46/113',0),
	(114,'114.mp4','videos/19/46/114',0),
	(115,'115.mp4','videos/19/46/115',0),
	(116,'116.mp4','videos/19/46/116',0),
	(117,'117.mp4','videos/19/46/117',0),
	(118,'118.mp4','videos/19/46/118',0),
	(119,'119.mp4','videos/11/24/119',0),
	(120,'120.mp4','videos/11/24/120',0),
	(122,'122.mp4','videos/5/13/122',0),
	(172,'172.mp4','videos/21/52/173',0),
	(175,'175.mp4','videos/5/13/176',0),
	(177,'177.mp4','videos/22/57/178',0),
	(155,'155.mp4','videos/21/51/155',0),
	(157,'157.mp4','videos/21/51/157',0),
	(171,'171.mp4','videos/21/52/172',0),
	(159,'159.mp4','videos/21/52/159',0),
	(167,'167.mp4','videos/21/52/168',0),
	(168,'168.mp4','videos/21/52/169',0),
	(169,'169.mp4','videos/21/52/170',0),
	(170,'170.mp4','videos/21/52/171',0),
	(184,'184.mp4','videos/22/57/185',0),
	(181,'181.mp4','videos/22/57/182',0),
	(182,'182.mp4','videos/22/57/183',0),
	(183,'183.mp4','videos/22/57/184',0),
	(185,'185.mp4','videos/22/57/186',0),
	(186,'186.mp4','videos/22/57/187',0),
	(187,'187.mp4','videos/22/57/190',0),
	(188,'188.mp4','videos/22/57/192',0),
	(189,'189.mp4','videos/22/57/193',0),
	(190,'190.mp4','videos/22/57/194',0),
	(191,'191.mp4','videos/22/57/195',0),
	(192,'192.mp4','videos/22/58/196',0),
	(212,'212.mp4','videos/23/63/218',0),
	(194,'194.mp4','videos/22/58/198',0),
	(195,'195.mp4','videos/22/58/199',0),
	(196,'196.mp4','videos/22/58/200',0),
	(211,'211.mp4','videos/23/63/216',0),
	(198,'198.mp4','videos/22/58/202',0),
	(199,'199.mp4','videos/22/58/203',0),
	(200,'200.mp4','videos/22/58/204',0),
	(201,'201.mp4','videos/22/58/205',0),
	(202,'202.mp4','videos/22/58/206',0),
	(203,'203.mp4','videos/22/58/207',0),
	(210,'210.mp4','videos/23/63/214',0),
	(206,'206.mp4','videos/23/62/210',0),
	(213,'213.mp4','videos/23/63/219',0),
	(214,'214.mp4','videos/23/63/220',0),
	(215,'215.mp4','videos/23/63/221',0),
	(216,'216.mp4','videos/23/63/222',0),
	(217,'217.mp4','videos/23/63/223',0),
	(218,'218.mp4','videos/23/63/224',0),
	(219,'219.mp4','videos/23/63/225',0),
	(220,'220.mp4','videos/23/63/226',0),
	(221,'221.mp4','videos/23/63/227',0),
	(222,'222.mp4','videos/23/63/228',0),
	(223,'223.mp4','videos/23/63/229',0),
	(224,'224.mp4','videos/23/63/230',0),
	(225,'225.mp4','videos/23/63/231',0),
	(226,'226.mp4','videos/23/63/232',0),
	(227,'227.mp4','videos/23/63/233',0),
	(228,'228.mp4','videos/23/63/234',0),
	(230,'230.mp4','videos/23/62/236',0),
	(231,'231.mp4','videos/23/62/237',0),
	(232,'232.mp4','videos/23/62/238',0),
	(233,'233.mp4','videos/23/62/239',0),
	(234,'234.mp4','videos/23/62/240',0),
	(235,'235.mp4','videos/15/65/241',0),
	(241,'241.mp4','videos/23/63/247',0),
	(242,'242.mp4','videos/23/63/248',0),
	(239,'239.mp4','videos/23/63/245',0),
	(240,'240.mp4','videos/23/63/246',0);

/*!40000 ALTER TABLE `moment_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_part` WRITE;
/*!40000 ALTER TABLE `moment_to_part` DISABLE KEYS */;

INSERT INTO `moment_to_part` (`moment_id`, `part_id`)
VALUES
	(1,1),
	(2,8),
	(3,12),
	(44,9),
	(174,13),
	(167,52),
	(7,13),
	(183,57),
	(9,13),
	(80,20),
	(83,20),
	(82,20),
	(40,9),
	(45,9),
	(41,9),
	(42,9),
	(43,9),
	(46,9),
	(47,9),
	(48,9),
	(49,9),
	(50,9),
	(51,9),
	(52,9),
	(53,9),
	(54,9),
	(55,9),
	(56,9),
	(57,9),
	(58,9),
	(76,29),
	(60,9),
	(62,24),
	(79,25),
	(85,25),
	(66,26),
	(67,26),
	(84,37),
	(87,24),
	(88,24),
	(89,24),
	(77,29),
	(90,24),
	(92,24),
	(93,24),
	(94,24),
	(95,24),
	(96,24),
	(97,24),
	(98,24),
	(99,24),
	(100,46),
	(101,46),
	(102,46),
	(103,46),
	(104,46),
	(105,46),
	(106,46),
	(107,46),
	(121,14),
	(109,46),
	(110,46),
	(111,46),
	(112,46),
	(113,46),
	(114,46),
	(115,46),
	(116,46),
	(117,46),
	(118,46),
	(119,24),
	(120,24),
	(122,13),
	(168,52),
	(170,52),
	(176,13),
	(159,52),
	(155,51),
	(169,52),
	(185,57),
	(178,57),
	(157,51),
	(172,52),
	(173,52),
	(171,52),
	(182,57),
	(184,57),
	(186,57),
	(187,57),
	(193,57),
	(190,57),
	(192,57),
	(194,57),
	(195,57),
	(196,58),
	(218,63),
	(198,58),
	(199,58),
	(200,58),
	(216,63),
	(202,58),
	(203,58),
	(204,58),
	(205,58),
	(206,58),
	(207,58),
	(214,63),
	(210,62),
	(219,63),
	(220,63),
	(221,63),
	(222,63),
	(223,63),
	(224,63),
	(225,63),
	(226,63),
	(227,63),
	(228,63),
	(229,63),
	(230,63),
	(231,63),
	(232,63),
	(233,63),
	(234,63),
	(236,62),
	(237,62),
	(238,62),
	(239,62),
	(240,62),
	(241,65),
	(247,63),
	(248,63),
	(245,63),
	(246,63);

/*!40000 ALTER TABLE `moment_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL auto_increment,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY  (`moment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;

INSERT INTO `moments` (`moment_id`, `date_created`, `time_in_clip`, `lead`, `lapse`, `comment`)
VALUES
	(1,1411998897,4,3,3,''),
	(2,1412079686,7,5,15,''),
	(3,1412700123,181,15,5,''),
	(40,1414002536,59,5,10,'voor het druk zetten staan we te ver van elkaar en je zal moeten kantelen en door dekken... verste man los laten'),
	(174,1425500852,82,10,2,''),
	(168,1425242284,400,10,2,''),
	(7,1412713395,181,15,5,''),
	(9,1412713843,2181,15,5,'Actie So'),
	(80,1415090207,85,12,3,''),
	(83,1415091326,505,2,12,''),
	(41,1414002981,83,5,10,'deze aktie moet je naar binnen toe met buitenkant voet waarna in de verste hoek met gevoel .. ala robben !'),
	(42,1414003183,103,10,5,''),
	(43,1414003464,129,5,10,''),
	(44,1414003644,169,5,10,''),
	(45,1414003921,204,5,10,''),
	(46,1414004352,272,5,10,'sneller handelen , gaat te traag.\nbal aannemen en tempo - versnellen zodat je de spits voorbij bent !... waarna kruisen dus naar binnen toe dribbelen... dan passen ...'),
	(47,1414004515,283,10,5,''),
	(48,1414004768,374,10,5,''),
	(49,1414005090,474,5,10,''),
	(50,1414005300,542,5,10,'gijs hier doe je sneller handelen en door lopen... alleen dan nog de eind pass strak OVER de grond... is makkelijker aannemen voor je team genoot....   zeikerd he die trainer.. het zijn kleine details om een nog betere voetballer te worden cq te willen zijn !!'),
	(51,1414005480,561,5,10,''),
	(52,1414234706,127,10,5,''),
	(53,1414234749,127,10,5,''),
	(54,1414234841,127,10,5,''),
	(55,1414262474,63,10,5,''),
	(56,1414262507,97,10,5,''),
	(57,1414262543,128,10,5,''),
	(58,1414262577,128,10,5,''),
	(76,1414660613,3,3,3,''),
	(60,1414262811,128,10,5,''),
	(62,1414517774,573,5,18,'Goed stuk! '),
	(87,1415817225,47,10,5,'hoe komt het balverlies van HAN ??\nverklaring ?'),
	(84,1415453595,30,3,3,''),
	(82,1415090355,305,12,3,''),
	(77,1414660663,6,3,3,''),
	(85,1415622654,31,15,5,''),
	(79,1414738862,116,5,3,''),
	(88,1415817525,64,10,5,''),
	(89,1415817888,80,10,5,'mooi balbezit... stom gelijk weer balverlies bij sohail ?\nhoe komt dit ??'),
	(90,1415818118,100,10,5,'mooi bal terug uit... rust .... opbouwen...\nsnel balverlies bij Mark !~\nhoe komt dit ?'),
	(92,1415818498,140,10,10,'hoe zouden we meer uit een vrije trap kunnen halen?\nlet op loop actie van michiel !!'),
	(93,1415818620,161,10,5,'Als je meer wilt scoren is de loop actie van rene dan een goeie ?'),
	(94,1415818816,178,10,5,'Wat valt je op bij het jagen cq druk zetten van Han ?'),
	(95,1415819004,180,10,5,'van de voorhoede het jagen (Han) en wat valt je op bij jagen druk zetten op het middenveld (Gijs) ?'),
	(96,1415819209,229,10,5,'aanval TP , bal bij keeper , snel een lange bal , organisatie bij TP is weg , keeper TP hervat spel ook snel... waar zou je nu op bedacht moeten zijn of moeten proberen?'),
	(97,1415819437,286,10,10,''),
	(98,1415819831,386,10,5,'TP trap de bal uit op sportieve basis.... wij nemen uitbal ... trappen hem naar de keeper...\nals je nu brutaal en lef hebt .. een winnaars mentaliteit ipv .. lief terug trappen....  wat zou je anders kunnen doen en toch sportief te blijven ??'),
	(99,1415820272,514,10,5,'wat kunnen we doen met deze vrijtrap , als je  weet dat we bij corners ook afspraken hebben ?  nu komt er helemaal geen gevaar uit zo\'n vrijetrap !!'),
	(100,1416253628,237,10,10,'Wie is er nu te laat Sam?\nHoe had deze persoon het eerder kunnen zien dat hij snellere moest reageren?'),
	(101,1416253797,356,10,10,'Moet Ronald druk gaan zetten?\nGeef eens aan waarom wel of waarom niet Bas?'),
	(102,1416253883,405,10,10,'Waar moet deze bal komen Stefan?\nHoe staan we als team? Volgens afspraak..?\nHoe zouden we moeten staan..?'),
	(103,1416254090,600,10,10,'Waar laat Aaron deze corner vallen? Allan\nHoe kunnen we hier meer op trainen..... Aaron en Ronald..?'),
	(104,1416254148,618,10,10,'Iedereen op tijd en we weten wat we moeten doen. Super'),
	(105,1416254256,670,10,10,'Het is moeilijk te zien........\nWat had Jerri hier beter moeten doen Tim?'),
	(106,1416254349,756,10,10,'Druk zetten? Hoe doen we dat?\nWat moet je doen in een situatie waar een diepe bal komt? Zoals hier het geval is...?'),
	(107,1416254495,890,10,10,'Wat zou Stefan in deze actie beter hebben moeten doen? Jerri'),
	(109,1416254879,1230,10,10,'Wat doet Aaron hier fout?\nWat zou Aaron in deze situatie beter kunnen doen Martijn?'),
	(110,1416254991,1267,15,15,'Waar had Martijn de bal beter neer kunnen leggen?\nWaar ligt de ruimte...?'),
	(111,1416255059,1309,10,10,'Goeie corner Ronald en prime terug gelegd Stefan. Goede loopacties en we komen vrij.\nStefan probeer de bal niet naar achter terug te leggen.'),
	(112,1416255421,1665,15,15,'Waar moet de bal van de corner komen Ronald?\n\nWat is er in het 2e deel wel goed? \n'),
	(113,1416255453,1683,10,10,'Wat deed Aaron hier verkeerd? uuh Aaron\n\nWat valt hier nog meer op?'),
	(114,1416255767,1861,10,10,'Welke 2 zaken gaan er hier fout Geert?'),
	(115,1416255922,1974,15,15,'Waar ligt de ruimte Teun?\nBal over 3 meter moet goed zijn.!!!!\nWaar is onze back en half wanneer we balverlies hebben?'),
	(116,1416256166,2177,10,10,'Een bal over 7 meter moet gewoon goed zijn, dat is concentratie verlies.'),
	(117,1416256577,2574,10,10,'Wat kan Daniël hier beter doen?'),
	(118,1416256649,2633,15,15,'Wat moet Aaron in deze situatie doen?'),
	(119,1416336204,5,10,5,''),
	(120,1416336223,5,10,5,''),
	(167,1425242260,377,10,2,'alles half'),
	(121,1421334383,458,5,5,''),
	(122,1421335077,1673,15,5,''),
	(173,1425243325,1078,10,2,''),
	(183,1426961789,116,10,2,''),
	(172,1425242980,832,10,2,'Balverlies Luuk'),
	(155,1424975352,587,10,2,''),
	(157,1424975785,626,10,2,'Martijn aanname,omschakeling'),
	(171,1425242823,713,10,2,''),
	(159,1424976868,122,10,2,'vrije trap in 2'),
	(169,1425242536,622,10,2,''),
	(170,1425242740,677,10,2,''),
	(176,1425501054,227,10,2,''),
	(182,1426961756,83,10,2,''),
	(178,1426885581,49,10,2,'Inspeelbal met speler in de rug.\nvoorbakte maken.'),
	(185,1426962450,464,10,2,''),
	(184,1426961814,141,10,2,'Rudi op 80% te laat uit rug'),
	(186,1426962508,522,10,2,''),
	(187,1426962655,640,10,2,''),
	(193,1426963978,1239,10,2,''),
	(190,1426963622,1164,10,2,''),
	(192,1426963914,1175,10,2,''),
	(194,1426964187,1288,10,5,''),
	(195,1426964325,1426,10,2,''),
	(196,1426964831,271,10,2,''),
	(198,1426964944,385,10,2,''),
	(199,1426965072,513,10,2,''),
	(200,1426965166,607,10,2,''),
	(218,1427748092,265,10,2,''),
	(202,1426965301,742,10,2,''),
	(203,1426965436,877,10,2,''),
	(204,1426965507,948,10,2,''),
	(205,1426965552,993,10,2,''),
	(206,1426965582,1023,10,2,''),
	(207,1426965598,1039,10,2,''),
	(216,1427747627,194,10,2,''),
	(210,1427472486,785,3,3,''),
	(214,1427742516,125,10,2,'ingooi'),
	(219,1427748134,307,10,2,''),
	(220,1427748164,337,10,2,''),
	(221,1427748204,376,10,2,''),
	(222,1427748304,477,10,2,''),
	(223,1427748485,527,10,2,''),
	(224,1427748937,573,10,2,''),
	(225,1427748955,592,10,2,''),
	(226,1427749599,617,10,2,''),
	(227,1427749642,639,10,2,''),
	(228,1427749920,1016,10,2,''),
	(229,1427750012,1108,10,2,''),
	(230,1427750331,1130,10,2,''),
	(231,1427750442,1600,10,2,''),
	(232,1427750522,1445,10,2,''),
	(233,1427750561,1484,10,2,''),
	(234,1427750627,2295,10,2,''),
	(236,1427831010,180,10,2,''),
	(237,1427832099,869,10,2,''),
	(238,1427832364,1055,10,2,''),
	(239,1427832500,1107,10,2,''),
	(240,1427832721,1223,10,2,''),
	(241,1428674256,2,3,3,''),
	(247,1429560512,657,10,2,''),
	(245,1429559702,113,10,2,''),
	(246,1429560065,656,10,2,''),
	(248,1429560527,671,10,2,'');

/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL auto_increment,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY  (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL auto_increment,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY  (`organisation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Test organisatie'),
	(3,'Beta'),
	(2,'NWC');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `part_to_event` WRITE;
/*!40000 ALTER TABLE `part_to_event` DISABLE KEYS */;

INSERT INTO `part_to_event` (`part_id`, `event_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,2),
	(5,1),
	(6,2),
	(7,3),
	(8,1),
	(9,4),
	(14,5),
	(30,15),
	(13,5),
	(17,8),
	(19,9),
	(20,10),
	(24,11),
	(25,11),
	(29,15),
	(34,15),
	(33,17),
	(35,15),
	(36,15),
	(37,15),
	(47,19),
	(46,19),
	(45,19),
	(48,20),
	(49,20),
	(50,21),
	(51,21),
	(52,21),
	(54,15),
	(55,22),
	(62,23),
	(57,22),
	(58,22),
	(59,22),
	(60,22),
	(61,22),
	(63,23),
	(64,15),
	(65,15);

/*!40000 ALTER TABLE `part_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL auto_increment,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  PRIMARY KEY  (`part_id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`part_id`, `part_name`, `date_created`)
VALUES
	(1,'no part name','1411997832'),
	(2,'Test','1412029365'),
	(3,'no part name','1412030548'),
	(4,'no part name','1412077537'),
	(5,'no part name','1412077988'),
	(6,'no part name','1412078237'),
	(7,'Hele Wedstrijd','1412078715'),
	(8,'no part name','1412079388'),
	(9,'Hele Wedstrijd','1412079792'),
	(14,'1ste Helft High res','1412752043'),
	(29,'no part name','1414660548'),
	(13,'2de Helft High Res','1412695906'),
	(17,'Full video','1412763252'),
	(19,'Full video','1412768231'),
	(20,'Full video','1412772442'),
	(30,'no part name','1414661487'),
	(24,'Eerste Helft','1414398182'),
	(25,'Tweede Helft','1414398232'),
	(34,'no part name','1415449774'),
	(33,'Full video','1414931799'),
	(35,'no part name','1415450492'),
	(36,'no part name','1415451379'),
	(37,'no part name','1415452730'),
	(47,'Tweede Helft','1415648684'),
	(46,'Eerste Helft','1415646762'),
	(45,'1ste deeltje','1415645302'),
	(48,'Eerste helft','1417387090'),
	(49,'Tweede helft','1417388270'),
	(50,'1ste Helft Deel 2 (30  min)','1423606720'),
	(51,'1ste helft','1423611337'),
	(52,'2de Helft Deel 1','1423611434'),
	(54,'no part name','1424539956'),
	(55,'2de helft 4/4','1426201547'),
	(62,'1ste helft','1427232181'),
	(57,'1ste helft 1/2','1426201685'),
	(58,'1ste helft 2/2','1426201735'),
	(59,'2de helft 1/4','1426201766'),
	(60,'2d helft 2/4','1426201806'),
	(61,'2de helft 3/4','1426201841'),
	(63,'2de helft','1427232237'),
	(64,'no part name','1428673785'),
	(65,'no part name','1428674191');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL auto_increment,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

LOCK TABLES `player_to_moment` WRITE;
/*!40000 ALTER TABLE `player_to_moment` DISABLE KEYS */;

INSERT INTO `player_to_moment` (`id`, `player_id`, `moment_id`, `user_id`)
VALUES
	(1,4,1,4),
	(2,4,2,4),
	(5,33,30,44),
	(11,33,59,44),
	(13,82,61,4),
	(14,82,62,4),
	(15,4,63,4),
	(16,55,64,4),
	(17,4,64,4),
	(18,4,74,4),
	(19,75,75,4),
	(20,4,76,4),
	(21,4,78,4),
	(22,55,78,4),
	(23,4,79,4),
	(24,4,84,4),
	(25,88,85,4),
	(26,75,85,4),
	(29,61,208,4),
	(31,103,210,94),
	(32,115,206,94),
	(33,115,205,94),
	(34,108,203,94),
	(35,104,122,94),
	(36,106,207,94),
	(37,111,196,94),
	(38,106,202,94),
	(39,106,200,94),
	(40,109,198,94),
	(41,115,182,94),
	(42,115,185,94),
	(43,115,195,94),
	(44,115,190,94),
	(87,109,211,94),
	(86,109,155,94),
	(85,106,155,94),
	(89,109,213,94),
	(88,109,212,94),
	(50,111,199,94),
	(51,109,199,94),
	(52,106,193,94),
	(53,115,184,94),
	(54,61,173,94),
	(55,105,173,94),
	(56,61,171,94),
	(57,61,169,94),
	(112,110,223,94),
	(59,61,157,94),
	(60,61,9,94),
	(61,105,172,94),
	(62,61,170,94),
	(63,109,170,94),
	(65,109,168,94),
	(66,109,159,94),
	(67,111,121,94),
	(69,61,7,94),
	(70,115,192,94),
	(71,105,192,94),
	(72,106,187,94),
	(73,116,174,94),
	(74,111,174,94),
	(75,106,204,94),
	(76,109,194,94),
	(79,114,186,94),
	(80,106,183,94),
	(81,105,183,94),
	(82,115,178,94),
	(83,61,176,94),
	(84,105,122,94),
	(90,111,214,94),
	(91,61,216,94),
	(95,115,219,94),
	(94,110,218,94),
	(96,115,220,94),
	(97,110,221,94),
	(98,103,222,94),
	(99,116,223,94),
	(100,111,224,94),
	(101,61,225,94),
	(102,116,226,94),
	(103,61,227,94),
	(104,115,228,94),
	(105,105,229,94),
	(106,105,230,94),
	(107,105,231,94),
	(108,61,232,94),
	(109,61,233,94),
	(110,61,234,94),
	(111,109,167,94),
	(113,109,236,94),
	(114,111,236,94),
	(115,110,237,94),
	(116,111,238,94),
	(117,109,239,94),
	(118,116,240,94),
	(119,4,241,4),
	(121,116,244,94),
	(125,116,246,94),
	(124,116,245,94),
	(126,116,247,94),
	(127,116,248,94);

/*!40000 ALTER TABLE `player_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `player_to_team` WRITE;
/*!40000 ALTER TABLE `player_to_team` DISABLE KEYS */;

INSERT INTO `player_to_team` (`player_id`, `team_id`)
VALUES
	(88,20),
	(7,4),
	(3,7),
	(2,7),
	(2,1),
	(4,1),
	(19,17),
	(22,1),
	(30,21),
	(79,20),
	(38,20),
	(61,22),
	(74,20),
	(75,20),
	(78,20),
	(55,20),
	(80,20),
	(81,20),
	(83,20),
	(82,20),
	(4,20),
	(86,20),
	(90,20),
	(91,20),
	(100,22),
	(101,22),
	(102,22),
	(103,22),
	(104,22),
	(105,22),
	(106,22),
	(107,22),
	(108,22),
	(109,22),
	(110,22),
	(111,22),
	(112,22),
	(113,22),
	(114,22),
	(115,22),
	(116,22);

/*!40000 ALTER TABLE `player_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL auto_increment,
  `cam_number` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY  (`recording_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`)
VALUES
	(1,'Fieldback Beta V2','Fieldback Beta v2','/usr/local/bin/ffmpeg','openRTSP',120,'sjeffransen@gmail.com');

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_moment` WRITE;
/*!40000 ALTER TABLE `tag_to_moment` DISABLE KEYS */;

INSERT INTO `tag_to_moment` (`tag_id`, `moment_id`, `user_id`, `tag_name`)
VALUES
	(11,1,4,'Schakelen'),
	(11,2,4,'Schakelen'),
	(4,2,4,'Doelpunt tegen'),
	(12,2,4,'Balbezit -> Balverlies'),
	(3,3,4,'Doelpunt voor'),
	(11,3,4,'Schakelen'),
	(12,168,94,'Balbezit -> Balverlies'),
	(3,7,4,'Doelpunt voor'),
	(12,174,94,'Balbezit -> Balverlies'),
	(40,80,40,'Doelpunt Voor'),
	(12,84,4,'Balbezit -> Balverlies'),
	(41,82,40,'Doelpunt Tegen'),
	(28,42,44,'Om van te leren'),
	(22,41,44,'Linksvoor'),
	(28,40,44,'Om van te leren'),
	(28,43,44,'Om van te leren'),
	(49,44,44,'Hoekschop tegen'),
	(28,45,44,'Om van te leren'),
	(10,46,44,'Balverlies'),
	(22,47,44,'Linksvoor'),
	(22,48,44,'Linksvoor'),
	(22,49,44,'Linksvoor'),
	(28,50,44,'Om van te leren'),
	(22,51,44,'Linksvoor'),
	(11,45,44,'Schakelen'),
	(10,52,44,'Balverlies'),
	(10,53,44,'Balverlies'),
	(10,56,44,'Balverlies'),
	(10,57,44,'Balverlies'),
	(10,58,44,'Balverlies'),
	(4,77,4,'Doelpunt tegen'),
	(10,60,44,'Balverlies'),
	(12,85,4,'Balbezit -> Balverlies'),
	(3,62,4,'Doelpunt voor'),
	(7,77,4,'Vrije trap voor'),
	(29,66,87,'Achter'),
	(29,67,87,'Achter'),
	(30,67,87,'Midden'),
	(11,85,4,'Schakelen'),
	(13,79,4,'Balverlies -> Balbezit'),
	(39,83,40,'Strafcorner Tegen'),
	(31,87,44,'Voor'),
	(29,88,44,'Achter'),
	(13,89,44,'Balverlies -> Balbezit'),
	(12,90,44,'Balbezit -> Balverlies'),
	(7,92,44,'Vrije trap voor'),
	(31,93,44,'Voor'),
	(31,94,44,'Voor'),
	(30,95,44,'Midden'),
	(28,96,44,'Om van te leren'),
	(49,97,44,'Hoekschop tegen'),
	(28,98,44,'Om van te leren'),
	(7,99,44,'Vrije trap voor'),
	(50,100,85,'Druk zetten'),
	(50,101,85,'Druk zetten'),
	(27,102,85,'Slecht moment'),
	(27,103,85,'Slecht moment'),
	(26,104,85,'Goed moment'),
	(10,105,85,'Balverlies'),
	(50,106,85,'Druk zetten'),
	(27,107,85,'Slecht moment'),
	(9,121,43,'Balbezit'),
	(27,109,85,'Slecht moment'),
	(28,110,85,'Om van te leren'),
	(26,111,85,'Goed moment'),
	(28,112,85,'Om van te leren'),
	(10,113,85,'Balverlies'),
	(27,114,85,'Slecht moment'),
	(28,115,85,'Om van te leren'),
	(10,116,85,'Balverlies'),
	(10,117,85,'Balverlies'),
	(28,118,85,'Om van te leren'),
	(10,122,43,'Balverlies'),
	(12,184,94,'Balbezit -> Balverlies'),
	(12,183,94,'Balbezit -> Balverlies'),
	(12,173,94,'Balbezit -> Balverlies'),
	(12,155,94,'Balbezit -> Balverlies'),
	(12,157,94,'Balbezit -> Balverlies'),
	(12,159,94,'Balbezit -> Balverlies'),
	(12,172,94,'Balbezit -> Balverlies'),
	(12,171,94,'Balbezit -> Balverlies'),
	(12,169,94,'Balbezit -> Balverlies'),
	(12,170,94,'Balbezit -> Balverlies'),
	(12,182,94,'Balbezit -> Balverlies'),
	(50,187,94,'Druk zetten'),
	(50,176,94,'Druk zetten'),
	(12,167,94,'Balbezit -> Balverlies'),
	(12,196,94,'Balbezit -> Balverlies'),
	(12,186,4,'Balbezit -> Balverlies'),
	(12,185,94,'Balbezit -> Balverlies'),
	(12,178,94,'Balbezit -> Balverlies'),
	(50,192,94,'Druk zetten'),
	(12,193,94,'Balbezit -> Balverlies'),
	(3,194,94,'Doelpunt voor'),
	(12,195,94,'Balbezit -> Balverlies'),
	(49,210,94,'Hoekschop tegen'),
	(12,198,94,'Balbezit -> Balverlies'),
	(3,199,94,'Doelpunt voor'),
	(12,200,94,'Balbezit -> Balverlies'),
	(50,190,94,'Druk zetten'),
	(12,202,94,'Balbezit -> Balverlies'),
	(12,203,94,'Balbezit -> Balverlies'),
	(50,204,94,'Druk zetten'),
	(12,205,94,'Balbezit -> Balverlies'),
	(50,206,94,'Druk zetten'),
	(12,207,94,'Balbezit -> Balverlies'),
	(12,9,94,'Balbezit -> Balverlies'),
	(12,238,94,'Balbezit -> Balverlies'),
	(12,214,94,'Balbezit -> Balverlies'),
	(50,237,94,'Druk zetten'),
	(12,216,94,'Balbezit -> Balverlies'),
	(12,218,94,'Balbezit -> Balverlies'),
	(12,219,94,'Balbezit -> Balverlies'),
	(50,220,94,'Druk zetten'),
	(12,221,94,'Balbezit -> Balverlies'),
	(4,222,94,'Doelpunt tegen'),
	(12,223,94,'Balbezit -> Balverlies'),
	(12,224,94,'Balbezit -> Balverlies'),
	(12,225,94,'Balbezit -> Balverlies'),
	(50,226,94,'Druk zetten'),
	(12,227,94,'Balbezit -> Balverlies'),
	(49,229,94,'Hoekschop tegen'),
	(50,228,94,'Druk zetten'),
	(49,230,94,'Hoekschop tegen'),
	(49,231,94,'Hoekschop tegen'),
	(12,232,94,'Balbezit -> Balverlies'),
	(12,233,94,'Balbezit -> Balverlies'),
	(12,234,94,'Balbezit -> Balverlies'),
	(12,236,94,'Balbezit -> Balverlies'),
	(12,239,94,'Balbezit -> Balverlies'),
	(50,240,94,'Druk zetten'),
	(13,245,94,'Balverlies -> Balbezit'),
	(50,246,94,'Druk zetten'),
	(50,247,94,'Druk zetten'),
	(50,248,94,'Druk zetten');

/*!40000 ALTER TABLE `tag_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_organisation` WRITE;
/*!40000 ALTER TABLE `tag_to_organisation` DISABLE KEYS */;

INSERT INTO `tag_to_organisation` (`tag_id`, `organisation_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(3,2),
	(4,2),
	(5,2),
	(6,2),
	(7,2),
	(8,2),
	(9,2),
	(10,2),
	(11,2),
	(12,2),
	(13,2),
	(14,2),
	(15,2),
	(16,2),
	(17,2),
	(18,2),
	(19,2),
	(20,2),
	(21,2),
	(22,2),
	(23,2),
	(24,2),
	(25,2),
	(26,2),
	(27,2),
	(28,2),
	(29,2),
	(30,2),
	(31,2),
	(32,1),
	(38,3),
	(39,3),
	(40,3),
	(41,3),
	(42,3),
	(43,3),
	(44,3),
	(45,3),
	(46,3),
	(47,3),
	(48,2),
	(49,2),
	(50,2);

/*!40000 ALTER TABLE `tag_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `tag_to_tagfield` (`tag_id`, `tagfield_id`, `left`, `top`, `label`, `original_tag_id`)
VALUES
	(0,5,7,9,'Test voor',1),
	(0,1,57,68,'Balverlies -> Balbezit',13),
	(0,1,40,9,'Schakelen',11),
	(0,1,40,21,'Balbezit',9),
	(0,1,40,32,'Balverlies',10),
	(0,1,76,32,'Vrije slag tegen',8),
	(0,1,5,31,'Vrije slag voor',7),
	(0,1,75,19,'Strafcorner tegen',6),
	(0,1,5,19,'Strafcorner voor',5),
	(0,1,75,8,'Doelpunt tegen',4),
	(0,1,5,7,'Doelpunt voor',3),
	(0,6,75,17,'Strafcorner tegen',6),
	(0,6,4,17,'Strafcorner voor',5),
	(0,6,4,27,'Vrije slag voor',7),
	(0,6,75,28,'Vrije slag tegen',8),
	(0,6,40,27,'Balbezit',9),
	(0,6,40,36,'Balverlies',10),
	(0,6,39,58,'Balbezit -> Balverlies',12),
	(0,6,39,65,'Balverlies -> Balbezit',13),
	(0,6,40,46,'Schakelen',11),
	(0,6,5,85,'Achter',29),
	(0,6,74,85,'Voor',31),
	(0,6,41,86,'Midden',30),
	(0,6,39,2,'Om van te leren',28),
	(0,6,75,7,'Doelpunt tegen',4),
	(0,6,29,14,'Goed moment',26),
	(0,43,5,48,'Goal For',35),
	(0,43,6,15,'Goal Against',36),
	(0,38,30,54,'Onze keeper',14),
	(0,36,48,76,'Centrumvoor',23),
	(0,36,70,7,'Slecht moment',27),
	(0,36,64,66,'Goed moment',26),
	(0,36,59,61,'Cirkel',25),
	(0,36,19,69,'Balverlies -> Balbezit',13),
	(0,9,9,46,'Goed moment',26),
	(0,9,55,26,'Strafcorner tegen',6),
	(0,9,7,27,'Strafcorner voor',5),
	(0,9,56,11,'Doelpunt tegen',4),
	(0,9,6,10,'Doelpunt voor',3),
	(0,9,55,45,'Slecht moment',27),
	(0,10,7,11,'Doelpunt voor',3),
	(0,10,58,13,'Doelpunt tegen',4),
	(0,10,58,31,'Strafcorner tegen',6),
	(0,10,7,31,'Strafcorner voor',5),
	(0,10,8,57,'Goed moment',26),
	(0,10,59,54,'Om van te leren',28),
	(0,36,9,34,'Strafcorner voor',5),
	(0,36,23,17,'Doelpunt voor',3),
	(0,1,23,68,'Balbezit -> Balverlies',12),
	(0,12,5,35,'Goed moment',26),
	(0,12,76,24,'Strafcorner tegen',6),
	(0,12,4,21,'Strafcorner voor',5),
	(0,12,76,8,'Doelpunt tegen',4),
	(0,12,5,7,'Doelpunt voor',3),
	(0,13,76,63,'Goed moment',26),
	(0,13,47,61,'Om van te leren',28),
	(0,13,18,60,'Doelpunt voor',3),
	(0,13,49,31,'Doelpunt tegen',4),
	(0,12,76,39,'Slecht moment',27),
	(0,12,39,50,'Om van te leren',28),
	(0,18,57,63,'Vrije trap tegen',8),
	(0,18,6,15,'Doelpunt voor',3),
	(0,18,72,14,'Doelpunt tegen',4),
	(0,36,22,12,'Doelpunt tegen',4),
	(0,20,14,14,'Doelpunt tegen',4),
	(0,20,69,25,'Strafcorner tegen',6),
	(0,20,27,76,'Balbezit',9),
	(0,6,4,7,'Doelpunt voor',3),
	(0,36,7,31,'Strafcorner tegen',6),
	(0,36,42,34,'Onze keeper',14),
	(0,36,18,57,'Balbezit -> Balverlies',12),
	(0,18,46,15,'Schakelen',11),
	(0,18,11,34,'Balverlies -> Balbezit',13),
	(0,18,67,34,'Balbezit -> Balverlies',12),
	(0,6,52,14,'Slecht moment',27),
	(0,20,50,32,'Vrije slag tegen',8),
	(0,44,1,11,'Doelpunt tegen',4),
	(0,44,2,2,'Doelpunt voor',3),
	(0,18,28,63,'Vrije trap voor',7),
	(0,44,21,47,'Balbezit -> Balverlies',12),
	(0,44,64,47,'Balverlies -> Balbezit',13),
	(0,44,21,11,'Balbezit',9),
	(0,44,64,4,'Balverlies',10),
	(0,44,3,61,'Vrije trap tegen',8),
	(0,44,3,45,'Vrije trap voor',7),
	(0,44,3,77,'Onze keeper',14),
	(0,44,40,70,'Goed moment',26),
	(0,44,57,69,'Slecht moment',27),
	(0,56,75,34,'Vrije slag tegen',43),
	(0,56,6,20,'Doelpunt Voor',40),
	(0,56,6,6,'Strafcorner Voor',38),
	(0,56,75,5,'Strafcorner Tegen',39),
	(0,62,27,52,'Om van te leren',28),
	(0,62,1,65,'Hoekschop voor',25),
	(0,62,55,8,'Keeper tegenpartij',15),
	(0,62,55,1,'Onze keeper',14),
	(0,62,27,40,'Balverlies -> Balbezit',13),
	(0,62,27,33,'Balbezit -> Balverlies',12),
	(0,62,1,10,'Doelpunt tegen',4),
	(0,62,1,3,'Doelpunt voor',3),
	(0,62,1,23,'Penalty Voor',5),
	(0,62,1,44,'Vrije trap tegen',8),
	(0,62,1,30,'Penalty Tegen',6),
	(0,62,1,51,'Vrije trap voor',7),
	(0,62,27,2,'Balbezit',9),
	(0,62,27,10,'Balverlies',10),
	(0,62,27,21,'Schakelen',11),
	(0,62,1,72,'Hoekschop tegen',49),
	(0,62,85,2,'Linksvoor',22),
	(0,62,55,19,'Achter',29),
	(0,62,55,27,'Midden',30),
	(0,62,55,34,'Voor',31),
	(0,64,41,16,'Achter',29),
	(0,64,41,35,'Midden',30),
	(0,64,43,57,'Voor',31),
	(0,64,21,35,'Balbezit',9),
	(0,64,63,36,'Balverlies',10),
	(0,56,75,19,'Doelpunt Tegen',41),
	(0,56,6,36,'Vrije Slag voor',42),
	(0,70,28,7,'Doelpunt voor',3),
	(0,70,37,21,'Doelpunt tegen',4),
	(0,70,17,18,'Balbezit',9),
	(0,70,63,14,'Balverlies',10),
	(0,70,50,1,'Schakelen',11),
	(0,70,52,38,'Druk zetten',50),
	(0,70,22,39,'Goed moment',26),
	(0,70,72,28,'Slecht moment',27),
	(0,70,44,56,'Om van te leren',28),
	(0,71,51,59,'Onze keeper',14),
	(0,71,7,12,'Druk zetten',50),
	(0,71,6,4,'Balbezit -> Balverlies',12),
	(0,71,7,20,'Balverlies -> Balbezit',13),
	(0,71,68,18,'Doelpunt voor',3),
	(0,71,69,32,'Doelpunt tegen',4),
	(0,71,43,38,'Centrummidden',20),
	(0,71,28,76,'Hoekschop tegen',49);

/*!40000 ALTER TABLE `tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL auto_increment,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY  (`tagfield_id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

LOCK TABLES `tagfields` WRITE;
/*!40000 ALTER TABLE `tagfields` DISABLE KEYS */;

INSERT INTO `tagfields` (`tagfield_id`, `tagfield_name`, `show_players`, `date_saved`)
VALUES
	(64,'Tim\'s tagfield',0,1414659318),
	(63,'',0,1413998296),
	(62,'Wedstrijd Simpel',1,1414002286),
	(61,'',0,1413836914),
	(60,'',0,1413836121),
	(18,'Simple ',1,1417530196),
	(56,'Basis Tags',0,1415091624),
	(74,'Uitverdedigen',0,1429343367),
	(70,'Jan 1',0,1416256868),
	(71,'Eenvoudig Tagfield',1,1424979689),
	(65,'',0,1415090652),
	(73,'verdedigen',0,1424779885),
	(72,'doelpunten',0,1424542462),
	(44,'Basis Codeer',1,1412781450);

/*!40000 ALTER TABLE `tagfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL auto_increment,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY  (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`tag_id`, `tag_name`)
VALUES
	(1,'Test voor'),
	(2,'Test tegen'),
	(3,'Doelpunt voor'),
	(4,'Doelpunt tegen'),
	(5,'Penalty Voor'),
	(6,'Penalty Tegen'),
	(7,'Vrije trap voor'),
	(8,'Vrije trap tegen'),
	(9,'Balbezit'),
	(10,'Balverlies'),
	(11,'Schakelen'),
	(12,'Balbezit -> Balverlies'),
	(13,'Balverlies -> Balbezit'),
	(14,'Onze keeper'),
	(15,'Keeper tegenpartij'),
	(16,'Linksachter'),
	(17,'Centrumachter'),
	(18,'Rechtsachter'),
	(19,'Linksmidden'),
	(20,'Centrummidden'),
	(21,'Rechtsmidden'),
	(22,'Linksvoor'),
	(23,'Centrumvoor'),
	(24,'Rechtsvoor'),
	(25,'Hoekschop voor'),
	(26,'Goed moment'),
	(27,'Slecht moment'),
	(28,'Om van te leren'),
	(29,'Achter'),
	(30,'Midden'),
	(31,'Voor'),
	(32,'edwin plus'),
	(33,'testetetetet'),
	(34,'testtag'),
	(35,'Goal For'),
	(36,'Goal Against'),
	(37,'Hoekschop tegen'),
	(38,'Strafcorner Voor'),
	(39,'Strafcorner Tegen'),
	(40,'Doelpunt Voor'),
	(41,'Doelpunt Tegen'),
	(42,'Vrije Slag voor'),
	(43,'Vrije slag tegen'),
	(44,'Om van te leren'),
	(45,'Goed moment'),
	(46,'Balverlies -> Balbezit'),
	(47,'Balbezit -> Balverlies'),
	(48,'Hoekschop voor'),
	(49,'Hoekschop tegen'),
	(50,'Druk zetten');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_event` WRITE;
/*!40000 ALTER TABLE `team_to_event` DISABLE KEYS */;

INSERT INTO `team_to_event` (`team_id`, `event_id`)
VALUES
	(1,1),
	(1,2),
	(20,3),
	(20,4),
	(22,5),
	(21,9),
	(21,8),
	(21,10),
	(20,11),
	(21,17),
	(1,15),
	(23,19),
	(23,20),
	(22,21),
	(22,22),
	(22,23);

/*!40000 ALTER TABLE `team_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(1,1),
	(2,2),
	(3,2),
	(4,2),
	(5,1),
	(6,2),
	(7,1),
	(8,2),
	(9,2),
	(10,2),
	(11,2),
	(12,2),
	(13,2),
	(14,2),
	(15,2),
	(16,2),
	(17,2),
	(18,3),
	(19,1),
	(20,2),
	(21,3),
	(22,2),
	(23,2);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL auto_increment,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY  (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL auto_increment,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(1,'Test team'),
	(23,'NWC3'),
	(22,'NWC'),
	(21,'Tilburg'),
	(20,'NWC2');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=372 DEFAULT CHARSET=latin1;

LOCK TABLES `track_login` WRITE;
/*!40000 ALTER TABLE `track_login` DISABLE KEYS */;

INSERT INTO `track_login` (`id`, `user_id`, `username`, `email`, `active`, `ip_address`)
VALUES
	(1,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(2,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(3,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(4,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(5,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(6,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(7,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(8,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(9,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(10,32,'bert@sjefdesign.nl','bert@sjefdesign.nl',1,'192.168.0.1'),
	(11,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(115,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(13,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(114,87,'tim@lessormore.nl','tim@lessormore.nl',1,'192.168.0.1'),
	(15,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(16,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(17,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(18,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(19,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(20,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(21,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(22,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(23,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(24,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(25,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(26,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(27,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(28,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(29,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(30,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(31,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(32,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(33,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(34,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(35,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(36,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(37,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(38,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(39,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(40,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(41,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(42,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(43,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(44,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(45,70,'e.oppermanjurrius@ziggo.nl','e.oppermanjurrius@ziggo.nl',1,'82.95.51.122'),
	(46,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(47,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(48,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(49,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(50,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(51,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(52,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(53,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(54,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(55,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(56,61,'mcf.mulder@gmail.com','mcf.mulder@gmail.com',1,'82.95.51.122'),
	(57,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(58,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(59,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(60,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(61,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(62,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(63,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(64,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(65,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(66,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(67,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(68,76,'Arnevangeffen@hotmail.com','Arnevangeffen@hotmail.com',1,'145.131.157.29'),
	(69,38,'jasper@baa.nl','jasper@baa.nl',1,'192.168.0.1'),
	(70,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(71,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(72,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(73,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(74,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(75,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(76,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(77,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(78,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(79,75,'michiel_reijnders@hotmail.com','michiel_reijnders@hotmail.com',1,'145.131.157.29'),
	(80,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(81,78,'mister_shervin@hotmail.com','mister_shervin@hotmail.com',1,'192.168.0.1'),
	(82,55,'markus.halma@gmail.com','markus.halma@gmail.com',1,'82.95.51.122'),
	(83,75,'michiel_reijnders@hotmail.com','michiel_reijnders@hotmail.com',1,'145.131.157.29'),
	(84,78,'mister_shervin@hotmail.com','mister_shervin@hotmail.com',1,'192.168.0.1'),
	(85,38,'jasper@baa.nl','jasper@baa.nl',1,'192.168.0.1'),
	(86,79,'bankers91@hotmail.com','bankers91@hotmail.com',1,'88.159.82.158'),
	(87,80,'stefan_vaas@live.nl','stefan_vaas@live.nl',1,'88.159.82.158'),
	(88,38,'jasper@baa.nl','jasper@baa.nl',1,'192.168.0.1'),
	(89,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(90,79,'bankers91@hotmail.com','bankers91@hotmail.com',1,'88.159.82.158'),
	(91,82,'gijskuijp@hotmail.com','gijskuijp@hotmail.com',1,'88.159.82.158'),
	(92,83,'hakimi_sohail@hotmail.com','hakimi_sohail@hotmail.com',1,'88.159.82.158'),
	(93,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(94,82,'gijskuijp@hotmail.com','gijskuijp@hotmail.com',1,'88.159.82.158'),
	(95,82,'gijskuijp@hotmail.com','gijskuijp@hotmail.com',1,'88.159.82.158'),
	(96,79,'bankers91@hotmail.com','bankers91@hotmail.com',1,'88.159.82.158'),
	(97,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(98,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(99,75,'michiel_reijnders@hotmail.com','michiel_reijnders@hotmail.com',1,'145.131.157.29'),
	(100,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(101,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(102,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(103,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(104,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(105,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(106,38,'jasper@baa.nl','jasper@baa.nl',1,'192.168.0.1'),
	(107,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(108,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(109,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(110,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(111,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(112,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(113,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(116,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(117,88,'jurianthorst@hotmail.com','jurianthorst@hotmail.com',1,'192.168.0.1'),
	(118,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(119,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(120,55,'markus.halma@gmail.com','markus.halma@gmail.com',1,'82.95.51.122'),
	(121,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(122,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(123,86,'dennis_kuypers@hotmail.com','dennis_kuypers@hotmail.com',1,'92.110.33.134'),
	(124,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(125,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(126,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(127,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(128,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(129,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(130,40,'hans@sjefdesign.nl','hans@sjefdesign.nl',1,'192.168.0.1'),
	(131,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(132,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(133,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(134,74,'peters__tom@hotmail.com','peters__tom@hotmail.com',1,'145.131.157.29'),
	(135,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(136,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(137,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(138,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(139,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(140,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(141,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(142,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(143,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(144,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(145,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(146,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(147,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(148,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(149,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(150,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(151,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(152,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(153,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(154,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(155,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(156,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(157,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(158,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(159,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(160,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(161,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(162,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(163,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(164,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(165,82,'gijskuijp@hotmail.com','gijskuijp@hotmail.com',1,'88.159.82.158'),
	(166,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(167,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(168,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(169,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(170,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(171,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(172,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(173,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(174,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(175,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(176,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(177,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(178,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(179,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(180,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(181,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(182,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(183,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(184,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(185,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(186,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(187,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(188,81,'bartvanrooij@outlook.com','bartvanrooij@outlook.com',1,'88.159.82.158'),
	(189,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(190,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(191,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(192,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(193,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(194,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(195,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(196,91,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'192.168.0.1'),
	(197,91,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'192.168.0.1'),
	(198,91,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'192.168.0.1'),
	(199,91,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'192.168.0.1'),
	(200,85,'Jvgrotel@hotmail.com','Jvgrotel@hotmail.com',1,'145.131.157.29'),
	(201,91,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'192.168.0.1'),
	(202,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(203,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(204,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(205,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(206,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(207,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(208,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(209,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(210,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(211,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(212,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(213,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(214,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(215,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(216,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(217,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(218,92,'sjeffransen+test@gmail.com','sjeffransen+test@gmail.com',1,'192.168.0.1'),
	(219,92,'sjeffransen+test@gmail.com','sjeffransen+test@gmail.com',1,'192.168.0.1'),
	(220,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(221,92,'sjeffransen+test@gmail.com','sjeffransen+test@gmail.com',1,'192.168.0.1'),
	(222,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(223,92,'sjeffransen+test@gmail.com','sjeffransen+test@gmail.com',1,'192.168.0.1'),
	(224,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(225,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(226,92,'sjeffransen+test@gmail.com','sjeffransen+test@gmail.com',1,'192.168.0.1'),
	(227,43,'alebeekhpam@hetnet.nl','alebeekhpam@hetnet.nl',1,'92.110.33.134'),
	(228,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(229,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(230,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(231,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(232,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(233,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(234,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(235,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(236,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(237,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(238,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(239,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(240,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(241,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(242,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(243,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(244,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(245,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(246,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(247,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(248,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(249,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(250,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(251,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(252,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(253,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(254,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(255,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(256,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(257,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(258,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(259,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(260,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(261,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(262,44,'cooij060@planet.nl','cooij060@planet.nl',1,'88.159.82.158'),
	(263,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(264,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(265,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(266,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(267,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(268,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(269,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(270,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(271,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(272,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(273,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(274,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(275,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(276,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(277,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(278,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(279,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(280,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(281,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(282,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(283,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(284,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(285,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(286,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(287,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(288,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(289,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(290,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(291,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(292,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(293,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(294,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(295,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(306,110,'R.Jacobs.AA@gmail.com','R.Jacobs.AA@gmail.com',1,'192.168.0.1'),
	(297,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(299,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(305,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(301,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(313,114,'ralf_van_oosterhout@hotmail.com','ralf_van_oosterhout@hotmail.com',1,'192.168.0.1'),
	(303,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(307,104,'tomvanbussel1991@hotmail.com','tomvanbussel1991@hotmail.com',1,'192.168.0.1'),
	(308,104,'tomvanbussel1991@hotmail.com','tomvanbussel1991@hotmail.com',1,'192.168.0.1'),
	(309,111,'roel_jansen14@hotmail.com','roel_jansen14@hotmail.com',1,'192.168.0.1'),
	(310,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(311,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(312,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(314,115,'Strauss.rudi@gmail.com','Strauss.rudi@gmail.com',1,'192.168.0.1'),
	(315,115,'Strauss.rudi@gmail.com','Strauss.rudi@gmail.com',1,'192.168.0.1'),
	(316,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(317,106,'Jeroen_van_deursen@hotmail.com','Jeroen_van_deursen@hotmail.com',1,'192.168.0.1'),
	(318,106,'Jeroen_van_deursen@hotmail.com','Jeroen_van_deursen@hotmail.com',1,'192.168.0.1'),
	(319,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(320,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(321,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(322,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(323,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(324,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(325,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(326,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(327,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(328,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(329,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(330,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(331,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(332,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(333,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(334,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(335,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(336,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(337,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(338,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(339,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(340,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(341,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(342,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(343,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(344,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(345,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(346,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(347,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(348,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(349,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(350,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(351,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(352,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(353,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(354,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(355,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(356,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(357,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(358,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(359,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(360,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(361,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(362,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(363,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(364,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(365,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(366,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(367,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(368,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(369,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(370,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134'),
	(371,94,'cvdakker@home.nl','cvdakker@home.nl',1,'92.110.33.134');

/*!40000 ALTER TABLE `track_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) default NULL,
  `lead` int(11) default NULL,
  `lapse` int(11) default NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `trigger_tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `trigger_tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `trigger_tag_to_tagfield` (`trigger_tag_id`, `tagfield_id`, `name`, `lead`, `lapse`)
VALUES
	(0,1,'Hier ook',10,10),
	(0,5,'test',2,2),
	(0,1,'10 voor 10 na',10,10),
	(0,1,'5 voor 5 na',5,5),
	(0,6,'10 om 10',10,10),
	(0,6,'5 om 10',5,10),
	(0,6,'10 om 5',10,5),
	(0,6,'5 om 5',5,5),
	(0,7,'140224_Test',3,10),
	(0,10,'Voor',3,15),
	(0,10,'Na',20,5),
	(0,7,'voor',15,3),
	(0,7,'Na',3,15),
	(0,5,'Frans111',3,5),
	(0,12,'Eerder',15,3),
	(0,13,'Later',3,15),
	(0,13,'Eerder',15,3),
	(0,12,'Later',3,15),
	(0,13,'',0,0),
	(0,18,'Before',15,5),
	(0,18,'After',5,15),
	(0,19,'Before',20,2),
	(0,20,'test',5,5),
	(0,21,'140721Test1',10,5),
	(0,21,'140721Test2',5,10),
	(0,21,'',0,0),
	(0,5,'Hier moet nog iets!',3,3),
	(0,1,'Frans',3,5),
	(0,18,'Short',3,3),
	(0,35,'renee',5,5),
	(0,35,'kom',11,4),
	(0,38,'voor',12,3),
	(0,36,'rol 3',4,4),
	(0,36,'kom',1,14),
	(0,36,'corner',1,8),
	(0,36,'kans',4,12),
	(0,38,'na',3,12),
	(0,43,'Before',20,5),
	(0,43,'After',3,15),
	(0,20,'before',10,0),
	(0,44,'Kort',5,5),
	(0,44,'na',5,15),
	(0,44,'voor',15,5),
	(0,70,'Druk zetten',10,10),
	(0,62,'Voor',10,5),
	(0,62,'Na',5,10),
	(0,62,'Kort',3,3),
	(0,64,'Momentje',3,3),
	(0,64,'Moment',6,6),
	(0,56,'Doelpunt tegen',12,3),
	(0,56,'Doelpunt voor',12,3),
	(0,56,'Strafcorner tegen',2,12),
	(0,56,'Strafcorner voor',2,12),
	(0,70,'Om van te leren',15,15),
	(0,70,'Doelpunt voor',5,15),
	(0,70,'Schakelen',10,10),
	(0,70,'Doelpunt tegen',15,5),
	(0,70,'Balverlies',10,10),
	(0,70,'Balbezit',10,10),
	(0,70,'Goed moment',10,10),
	(0,70,'Slecht moment',10,10),
	(0,71,'Na',3,10),
	(0,71,'Voor',10,2);

/*!40000 ALTER TABLE `trigger_tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL auto_increment,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY  (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL auto_increment,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY  (`user_level_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY  (`user_profile_id`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(88,'Jurian','Ter horst','jurianthorst@hotmail.com'),
	(2,'Frans','de Jong','info@fdejong.com'),
	(3,'Dennis','Slade','D.G.Slade@Massey.ac.nz'),
	(4,'Sjef','Fransen','sjef@spinnov.nl'),
	(5,'Moniek','Bazuin','mail@upc.nl'),
	(6,'Jeannie','Coopmans','jn.coopmans@chello.nl'),
	(7,'MA2','Speler','ma3@hcas.nl'),
	(8,'Geert','Berkvens','geert.berkvens@gmail.com'),
	(9,'Wil','Hoebergen','wil@baa.nl'),
	(10,'Tom','Verlijsdonk','tom.verlijsdonk@hotmail.com'),
	(11,'Joep','Peters','joep@hcas.nl'),
	(12,'Jaap','Habets','j.habets10@chello.nl'),
	(13,'Edwin','Meijne','edwinmeijne@gmail.com'),
	(14,'Sjors','de Jong','sdejong1988@live.nl'),
	(15,'Simon','van de Loo','simonvdloo@gmail.com'),
	(16,'Niek','Pauwels','hockeyniek@hotmail.com'),
	(17,'Rik','Pijnenburg','rikpijnen@hotmail.com'),
	(18,'Guido','Schakenraad','schakenraad@me.com'),
	(19,'Test','User','info@testuser.nl'),
	(20,'David','Mike','kangaroo@coolindian.com'),
	(21,'Sjef','Franen','info@sjefdesign.nl'),
	(22,'Test','test','drumkruk@gmail.com'),
	(23,'Amber','Beijers','amberbeijers@hotmail.com'),
	(24,'Iris','Berkvens','irisberkvens@gmail.com'),
	(25,'Jesse','Cortooms','jesse.cortooms@gmail.com'),
	(26,'Mirthe','Deimann','deima043@planet.nl'),
	(27,'Julia','Driessen','juliadriessen186@hotmail.com'),
	(28,'Mitchell','Cronin','mitchellcronin16@gmail.com'),
	(29,'Karel','Klaasen','sjeffransen@gmail.com'),
	(30,'test','Van Sjef','info@sjefdesign.nl'),
	(31,'Hans','Smits','j.c.smits@gmail.com'),
	(32,'sje','hoi','bert@sjefdesign.nl'),
	(91,'Edwin','TestAccount','edwinmeijne@gmail.com'),
	(34,'Michiel','Reijnders','michiel_reijnders@hotmail.com'),
	(35,'Jip','de Jong','jdejong1985.jdj@gmail.com'),
	(36,'Rene','Peters','r_peters17@hotmail.com'),
	(37,'Willem','van der zanden','willemvanderzanden@hotmail.com'),
	(38,'Jasper','Werts','jasper@baa.nl'),
	(39,'Hans','Smits','j.c.smits@gmail.com'),
	(40,'Hans','Smits','hans@sjefdesign.nl'),
	(41,'Sj','Fr','in.rob@sjefdesign.nl'),
	(42,'Hans','Smits','j.c.smits@gmail.com'),
	(43,'Henry','van Alebeek','alebeekhpam@hetnet.nl'),
	(44,'Wim','Cooijman','cooij060@planet.nl'),
	(111,'Roel','Jansen','roel_jansen14@hotmail.com'),
	(112,'Han','Kersten','kerstenhan@hotmail.com'),
	(106,'Jeroen','van Deursen','Jeroen_van_deursen@hotmail.com'),
	(107,'Sander','van den Eijnde','sandereijnde@hotmail.com'),
	(104,'Tom','van Bussel','tomvanbussel1991@hotmail.com'),
	(105,'Luuk','Castelijns','luukcastelijns_voetbal@hotmail.com'),
	(100,'Abdou','Il ah Bazi','abdou-bazi@hotmail.com'),
	(101,'Itay','Blaycher','itay_.99@hotmail.com'),
	(55,'Mark','Halma','markus.halma@gmail.com'),
	(110,'Rob','Jacobs','R.Jacobs.AA@gmail.com'),
	(109,'Martijn','Ghielen','martijnghielen2@hotmail.com'),
	(59,'Gijs','Kuijpers','Gijskuijp@hotmail.com'),
	(61,'Martijn','Mulder','mcf.mulder@gmail.com'),
	(63,'Sohail','Rahimi','hakimi_sohail@hotmail.com'),
	(65,'Stefan','Vaas','stefan_vaas@live.nl'),
	(102,'Leon','van den Bosch','leon_nwc@hotmail.com'),
	(94,'Chris','van den Akker','cvdakker@home.nl'),
	(103,'Gijs','Brüsewitz','gijs_brusewitz@hotmail.com'),
	(119,'slja','ljoija','Ditiseentestvoor@hotmail.com'),
	(70,'Eef','Opperman','e.oppermanjurrius@ziggo.nl'),
	(71,'Theo','Lankhorst','theo@fysiotherapie-asten.nl'),
	(108,'Bas','van Geffen','basvangeffen@hotmail.com'),
	(74,'Tom','Peters','peters__tom@hotmail.com'),
	(75,'Michiel','Reijnders','michiel_reijnders@hotmail.com'),
	(76,'Arne','Van Geffen','Arnevangeffen@hotmail.com'),
	(77,'René','Peters','renepeters11@gmail.com'),
	(78,'Shervin','Sousaffian','mister_shervin@hotmail.com'),
	(79,'Randy','Bankers','bankers91@hotmail.com'),
	(80,'Stefan','Vaas','stefan_vaas@live.nl'),
	(81,'Bart','van Rooij','bartvanrooij@outlook.com'),
	(82,'Gijs','Kuijpers','gijskuijp@hotmail.com'),
	(83,'Sohail','Rahimi','hakimi_sohail@hotmail.com'),
	(90,'Rene','Peters','renepeters11@gmail.com'),
	(85,'Jan','Van Grotel','Jvgrotel@hotmail.com'),
	(86,'Dennis','Kuypers','dennis_kuypers@hotmail.com'),
	(87,'tim','scholten','tim@lessormore.nl'),
	(92,'Sjef','Fransen','sjeffransen+test@gmail.com'),
	(113,'Ruud','Leenen','ruud.leenen@hotmail.com'),
	(114,'Ralf','van Oosterhout','ralf_van_oosterhout@hotmail.com'),
	(115,'Rudi','Strauss','Strauss.rudi@gmail.com'),
	(116,'Juul','Verbugt','juul_verbugt_ajax@hotmail.com');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_clip` WRITE;
/*!40000 ALTER TABLE `user_to_clip` DISABLE KEYS */;

INSERT INTO `user_to_clip` (`user_id`, `clip_id`)
VALUES
	(4,1),
	(4,2),
	(4,3),
	(4,4),
	(4,5),
	(4,6),
	(4,7),
	(4,8),
	(4,9),
	(4,10),
	(4,11),
	(4,12),
	(40,13),
	(40,14),
	(40,15),
	(4,16),
	(4,17),
	(4,18),
	(87,19),
	(4,20),
	(87,21),
	(4,22),
	(4,23),
	(4,24),
	(40,25),
	(4,26),
	(4,27),
	(4,28),
	(4,29),
	(4,30),
	(4,31),
	(4,32),
	(4,33),
	(4,34),
	(4,35),
	(4,36),
	(4,37),
	(4,38),
	(4,39),
	(4,40),
	(4,41),
	(4,42),
	(4,43),
	(4,44),
	(4,45),
	(4,46),
	(4,47),
	(4,48),
	(4,49),
	(4,50),
	(4,51),
	(4,52),
	(4,53),
	(4,54),
	(4,55),
	(4,56);

/*!40000 ALTER TABLE `user_to_clip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1),
	(55,2),
	(54,2),
	(53,2),
	(52,2),
	(51,2),
	(50,2),
	(49,2),
	(48,2),
	(47,2),
	(46,2),
	(45,2),
	(40,3),
	(20,2),
	(44,2),
	(42,3),
	(41,3),
	(43,2),
	(39,3),
	(38,2),
	(37,2),
	(36,2),
	(4,2),
	(35,2),
	(34,2),
	(33,2),
	(32,3),
	(31,3),
	(30,3),
	(4,3),
	(1,3),
	(1,2),
	(4,1),
	(56,2),
	(57,2),
	(58,2),
	(59,2),
	(60,2),
	(61,2),
	(62,2),
	(63,2),
	(64,2),
	(65,2),
	(66,2),
	(67,2),
	(68,2),
	(69,2),
	(70,2),
	(71,2),
	(72,2),
	(73,2),
	(74,2),
	(75,2),
	(76,2),
	(77,2),
	(78,2),
	(79,2),
	(80,2),
	(81,2),
	(82,2),
	(83,2),
	(84,2),
	(85,2),
	(86,2),
	(87,2),
	(88,2),
	(89,2),
	(90,2),
	(91,2),
	(92,2),
	(93,2),
	(94,2),
	(95,2),
	(96,2),
	(97,2),
	(98,2),
	(99,2),
	(100,2),
	(101,2),
	(102,2),
	(103,2),
	(104,2),
	(105,2),
	(106,2),
	(107,2),
	(108,2),
	(109,2),
	(110,2),
	(111,2),
	(112,2),
	(113,2),
	(114,2),
	(115,2),
	(116,2),
	(117,2),
	(118,2),
	(119,2);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_tagfield` WRITE;
/*!40000 ALTER TABLE `user_to_tagfield` DISABLE KEYS */;

INSERT INTO `user_to_tagfield` (`user_id`, `tagfield_id`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(1,4),
	(1,5),
	(2,6),
	(2,7),
	(2,8),
	(8,6),
	(8,7),
	(2,9),
	(2,10),
	(2,11),
	(17,12),
	(17,13),
	(2,14),
	(13,15),
	(13,16),
	(13,17),
	(4,18),
	(4,19),
	(13,20),
	(2,21),
	(2,22),
	(2,23),
	(12,24),
	(12,25),
	(2,26),
	(12,27),
	(12,28),
	(12,29),
	(2,30),
	(12,31),
	(12,32),
	(12,33),
	(12,34),
	(12,35),
	(12,36),
	(12,37),
	(12,38),
	(12,39),
	(8,40),
	(8,41),
	(2,42),
	(2,43),
	(43,44),
	(40,45),
	(40,46),
	(40,47),
	(40,48),
	(40,49),
	(40,50),
	(40,51),
	(40,52),
	(40,53),
	(40,54),
	(40,55),
	(40,56),
	(40,57),
	(40,58),
	(40,59),
	(44,60),
	(44,63),
	(44,62),
	(87,64),
	(40,65),
	(4,66),
	(85,67),
	(85,68),
	(85,69),
	(85,70),
	(94,71),
	(94,72),
	(94,73),
	(94,74);

/*!40000 ALTER TABLE `user_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(85,23),
	(2,2),
	(2,3),
	(2,1),
	(2,4),
	(2,5),
	(6,4),
	(1,6),
	(2,6),
	(20,6),
	(1,3),
	(11,3),
	(12,3),
	(1,2),
	(9,2),
	(20,2),
	(13,17),
	(1,4),
	(1,5),
	(1,7),
	(8,8),
	(1,8),
	(12,9),
	(1,9),
	(12,10),
	(1,10),
	(14,11),
	(1,11),
	(14,10),
	(14,12),
	(1,12),
	(1,13),
	(15,13),
	(15,14),
	(1,14),
	(1,15),
	(16,15),
	(1,16),
	(17,16),
	(18,13),
	(3,1),
	(4,1),
	(2,17),
	(4,18),
	(2,19),
	(8,10),
	(4,20),
	(30,21),
	(4,22),
	(40,21),
	(43,22),
	(70,22),
	(4,21),
	(44,20),
	(76,20),
	(87,20),
	(4,23),
	(92,22),
	(94,22);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(88,3),
	(2,1),
	(3,3),
	(4,1),
	(5,3),
	(6,3),
	(7,3),
	(8,3),
	(9,3),
	(10,3),
	(11,3),
	(12,3),
	(13,2),
	(14,3),
	(15,3),
	(16,3),
	(17,3),
	(18,3),
	(19,4),
	(20,3),
	(21,3),
	(22,3),
	(23,3),
	(24,3),
	(25,3),
	(26,3),
	(27,3),
	(28,3),
	(29,3),
	(30,3),
	(31,3),
	(32,3),
	(92,3),
	(34,3),
	(35,3),
	(36,3),
	(37,3),
	(38,3),
	(39,3),
	(40,3),
	(41,3),
	(42,3),
	(43,3),
	(44,3),
	(114,3),
	(107,3),
	(108,3),
	(116,3),
	(105,3),
	(106,3),
	(101,3),
	(115,3),
	(55,3),
	(113,3),
	(112,3),
	(111,3),
	(59,3),
	(100,3),
	(61,3),
	(102,3),
	(63,3),
	(110,3),
	(65,3),
	(103,3),
	(94,3),
	(104,3),
	(119,3),
	(70,3),
	(71,3),
	(109,3),
	(91,3),
	(74,3),
	(75,3),
	(76,3),
	(77,3),
	(78,3),
	(79,3),
	(80,3),
	(81,3),
	(82,3),
	(83,3),
	(90,3),
	(85,3),
	(86,3),
	(87,1);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(88,88,0),
	(2,2,0),
	(3,3,0),
	(4,4,0),
	(5,5,0),
	(6,6,0),
	(7,7,0),
	(8,8,0),
	(9,9,0),
	(10,10,0),
	(11,11,0),
	(12,12,0),
	(13,13,0),
	(14,14,0),
	(15,15,0),
	(16,16,0),
	(17,17,0),
	(18,18,0),
	(19,19,0),
	(20,20,0),
	(21,21,0),
	(22,22,0),
	(23,23,0),
	(24,24,0),
	(25,25,0),
	(26,26,0),
	(27,27,0),
	(28,28,0),
	(29,29,0),
	(30,30,0),
	(31,31,0),
	(32,32,0),
	(92,92,0),
	(34,34,0),
	(35,35,0),
	(36,36,0),
	(37,37,0),
	(38,38,0),
	(39,39,0),
	(40,40,0),
	(41,41,0),
	(42,42,0),
	(43,43,0),
	(44,44,0),
	(114,114,0),
	(107,107,0),
	(108,108,0),
	(116,116,0),
	(105,105,0),
	(106,106,0),
	(101,101,0),
	(115,115,0),
	(55,55,0),
	(113,113,0),
	(112,112,0),
	(111,111,0),
	(59,59,0),
	(100,100,0),
	(61,61,0),
	(102,102,0),
	(63,63,0),
	(110,110,0),
	(65,65,0),
	(103,103,0),
	(94,94,0),
	(104,104,0),
	(119,119,0),
	(70,70,0),
	(71,71,0),
	(109,109,0),
	(91,91,0),
	(74,74,0),
	(75,75,0),
	(76,76,0),
	(77,77,0),
	(78,78,0),
	(79,79,0),
	(80,80,0),
	(81,81,0),
	(82,82,0),
	(83,83,0),
	(90,90,0),
	(85,85,0),
	(86,86,0),
	(87,87,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(1) NOT NULL default '1',
  `api_key` varchar(64) default NULL,
  `selected_organisation` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`, `selected_organisation`)
VALUES
	(88,X'3139322E3136382E302E31','jurianthorst@hotmail.com','gz3QW/ifcMtbt5w9eWTw7u64a6b7cc0626e3c5b9','','jurianthorst@hotmail.com','','',0,'',1414671236,1414672021,1,'','',0,NULL,NULL),
	(40,X'3139322E3136382E302E31','hans@sjefdesign.nl','M4VTFa9uRigddx8G6CPHs.7a8f52d5f73ae9b10b','','hans@sjefdesign.nl','','',0,'',1412352134,1415091412,1,'','',0,NULL,NULL),
	(42,X'39322E3131302E33332E313334','j.c.smits@gmail.com','0GQ43ZxluflowlMoVz0yC.b0561ed74e085621c5','','j.c.smits@gmail.com','','NckIxRJW4h5bo7gNtKOique0b6995a3eddaf9771',1412763620,'',1412627828,1412627828,1,'','',0,NULL,NULL),
	(32,X'3139322E3136382E302E31','bert@sjefdesign.nl','Ca8/NykBdSXdOn1iqiGJBe347d961178ee30e0c3','','bert@sjefdesign.nl','4dbc3ae7e56d77caf2899138dae2d92179aa0622','',0,'',1412153319,1412154572,0,'','',0,NULL,NULL),
	(4,X'30','sjef@smartgoals.nl','xvOX7QEXMRKmTXmIIXhwlee2a18862d020f6945a','','sjef@smartgoals.nl','','',0,'',0,1429615117,1,'','',0,NULL,1),
	(112,X'3139322E3136382E302E31','kerstenhan@hotmail.com','cH9j2Wa55WWP7u7KU8OQ1O1c9df55f014f8c824c','','kerstenhan@hotmail.com','95d39e4d7f9a7500e5503d2b4582f59526edd1b2','',0,'',1427454150,1427454150,0,'','',1,NULL,NULL),
	(106,X'3139322E3136382E302E31','Jeroen_van_deursen@hotmail.com','X4pKFJWJ4RUogxwNBTVF2uaed3dd66877f1c8244','','Jeroen_van_deursen@hotmail.com','','',0,'',1427454040,1427482510,1,'','',0,NULL,2),
	(107,X'3139322E3136382E302E31','sandereijnde@hotmail.com','p3LfRjGyy41WhtYs3zIGke8ae703d05780dbfe4f','','sandereijnde@hotmail.com','137f3249efdc252614d70da2d5e5e0497859b566','',0,'',1427454059,1427454059,0,'','',1,NULL,NULL),
	(108,X'3139322E3136382E302E31','basvangeffen@hotmail.com','1sF8DNdJWgwkRjyS4pgIae3842ac2e8be54bbcc6','','basvangeffen@hotmail.com','9ac654cf64852fd04b247ee7374ea3f23e825bdf','',0,'',1427454075,1427454075,0,'','',1,NULL,NULL),
	(109,X'3139322E3136382E302E31','martijnghielen2@hotmail.com','2LW8G1QA2rxRLDW7Bxi1Eebc590a95f707574dce','','martijnghielen2@hotmail.com','753525200890dd5a047dbc7a08acdd11ad0788a6','',0,'',1427454091,1427454091,0,'','',1,NULL,NULL),
	(110,X'3139322E3136382E302E31','R.Jacobs.AA@gmail.com','qpZ//M134RMWKFrzCI8V4u97fb857de2e760e623','','R.Jacobs.AA@gmail.com','','',0,'',1427454108,1427454627,1,'','',0,NULL,2),
	(111,X'3139322E3136382E302E31','roel_jansen14@hotmail.com','F6dcDUCPWkllm9EanHMdDedbde649b1e9fab3187','','roel_jansen14@hotmail.com','','',0,'',1427454126,1427457449,1,'','',0,NULL,2),
	(44,X'38382E3135392E38322E313538','cooij060@planet.nl','2jCticjEzwVfFjJfbNi5P.ad473c0cd6ba8ae644','','cooij060@planet.nl','','Un5YdeN0qMVwnGXjA8km5.2719096ba39e29fd14',1415649335,'FHUsMaTIrfw/R8KD9S6wCO',1412688406,1425577126,1,'','',0,NULL,2),
	(74,X'3134352E3133312E3135372E3239','peters__tom@hotmail.com','U.qNbsWoOxJCSE5yLVg7ke38ba70fd033aeb03f9','','peters__tom@hotmail.com','','',0,'',1414101376,1415310683,1,'','',0,NULL,NULL),
	(43,X'39322E3131302E33332E313334','alebeekhpam@hetnet.nl','eTxFGjis/mMh.2ID/wK3g.dea39e8d3888792fde','','alebeekhpam@hetnet.nl','','',0,'',1412628672,1424193395,1,'','',0,NULL,2),
	(41,X'3139322E3136382E302E31','in.rob@sjefdesign.nl','QJD09vCVcDyQX75QdpK/M.2c9146c389dceb93dc','','in.rob@sjefdesign.nl','72be9a327624b22209f41fd416d45ee826f0274c','',0,'',1412423433,1412423433,0,'','',0,NULL,NULL),
	(38,X'3139322E3136382E302E31','jasper@baa.nl','izZtgziUcTCsHGNe9LTaUeca0bd29b3b204133b8','','jasper@baa.nl','','',0,'',1412175789,1414584157,1,'','',0,NULL,NULL),
	(37,X'3139322E3136382E302E31','willemvanderzanden@hotmail.com','8w/m6vTqaakT2aDhK76TPO1da3fd6a88870d69b9','','willemvanderzanden@hotmail.com','502d26e79d77e51b083111188dfa866d08ebef1e','',0,'',1412175774,1412175774,0,'','',0,NULL,NULL),
	(90,X'38382E3135392E38322E313538','renepeters11@gmail.com','cjqHFiGNmIKpRb5D4PyzLe9dd947bd32cbaaf4ee','','renepeters11@gmail.com','','SxbQ8Ov80Dcn9lwpFilMIe0a9ba09beedf36ae19',1420577315,'',1416926903,1416926903,1,'','',1,NULL,NULL),
	(75,X'3134352E3133312E3135372E3239','michiel_reijnders@hotmail.com','5nB1HEZido0u3hRvBscnRu33a1cb307466665edd','','michiel_reijnders@hotmail.com','','',0,'',1414101579,1414521715,1,'','',0,NULL,NULL),
	(35,X'3139322E3136382E302E31','jdejong1985.jdj@gmail.com','rJd7YknqVn08wqrqshH9c.2814d703d2875323b4','','jdejong1985.jdj@gmail.com','8ee42badedb2f3c29eca3642360ede0849738643','',0,'',1412175714,1412175714,0,'','',0,NULL,NULL),
	(30,X'3139322E3136382E302E31','info@sjefdesign.nl','ruWqQCE4ZJs7VZ3k9Frx5u8302d4ff26ca93b2d7','','info@sjefdesign.nl','51f2f9ba92edcfdb4048d6af6ac8d07db0bf617b','MJWZaq4T3GwFwfD1ts5SK.6e488792804904763e',1412081343,'',1412078620,1412078620,0,'','',0,NULL,NULL),
	(101,X'3139322E3136382E302E31','itay_.99@hotmail.com','6unNM1Wj.Y50zfeB/wkgbO27c3d4154b6d8e59f1','','itay_.99@hotmail.com','2f8cd6bfaad71be82282fa482e952eddd40a89bf','',0,'',1427453949,1427453949,0,'','',1,NULL,NULL),
	(102,X'3139322E3136382E302E31','leon_nwc@hotmail.com','PNZ6vPRYXGpf5dKTQ0EzdOd8b2c762f3fd918e3d','','leon_nwc@hotmail.com','09b62b0bc989dbd714c0d998dd060ea383cad7e8','',0,'',1427453969,1427453969,0,'','',1,NULL,NULL),
	(55,X'38322E39352E35312E313232','markus.halma@gmail.com','JZxqo.S60Dgrd/K3x5PRfu4c0420f0008abeed9d','','markus.halma@gmail.com','','',0,'',1412781621,1414754021,1,'','',0,NULL,NULL),
	(116,X'3139322E3136382E302E31','juul_verbugt_ajax@hotmail.com','5MZTEB7D.Oq1Sn8BZUYwC.21175246db7c06ceb5','','juul_verbugt_ajax@hotmail.com','381ad01fe00867bf5b9b95f2595daa73c72044d6','',0,'',1427454226,1427454226,0,'','',1,NULL,NULL),
	(115,X'3139322E3136382E302E31','Strauss.rudi@gmail.com','Kn6ZoFRMF2ZMI4uJosu/Tu835822b2efc3b87aa3','','Strauss.rudi@gmail.com','','',0,'',1427454207,1427478457,1,'','',0,NULL,2),
	(82,X'38382E3135392E38322E313538','gijskuijp@hotmail.com','J0yXgXG4q1IjC/K5Po6HIO9b719dbd90115a5f72','','gijskuijp@hotmail.com','','',0,'prD8MDjod7FI42OFXMt5Pu',1414495887,1416483166,1,'','',0,NULL,NULL),
	(100,X'3139322E3136382E302E31','abdou-bazi@hotmail.com','qu.Xg/NxqDOVr8NZqpwJ6.fa96644a2a84859b7d','','abdou-bazi@hotmail.com','569f90ada199a3f7128ded12ae6ec891c65e24ce','',0,'',1427453920,1427453920,0,'','',1,NULL,NULL),
	(61,X'38322E39352E35312E313232','mcf.mulder@gmail.com','HHC9sU5iW0DrLK4GDY4v.O3733f676b9ec41ed1a','','mcf.mulder@gmail.com','','',0,'',1412781763,1413194085,1,'','',0,NULL,NULL),
	(103,X'3139322E3136382E302E31','gijs_brusewitz@hotmail.com','8hWuM/Nsj.wMcC68kOtX0.fdfce7041681fbf482','','gijs_brusewitz@hotmail.com','10a66791f81858416d5acec975b50ab90a4ec149','',0,'',1427453993,1427453993,0,'','',1,NULL,NULL),
	(83,X'38382E3135392E38322E313538','hakimi_sohail@hotmail.com','O7U3xht8lBOq1Z76yysKUu1e224597829e66fceb','','hakimi_sohail@hotmail.com','','',0,'',1414495967,1414499025,1,'','',0,NULL,NULL),
	(114,X'3139322E3136382E302E31','ralf_van_oosterhout@hotmail.com','4.4clLm3AnVbiBgFt2986ec2378059e7b6c51469','','ralf_van_oosterhout@hotmail.com','','',0,'',1427454192,1427477697,1,'','',0,NULL,2),
	(104,X'3139322E3136382E302E31','tomvanbussel1991@hotmail.com','9B9.J3ycCmDOiIIdk4Eoj.bd73ce8e206b7433af','','tomvanbussel1991@hotmail.com','','',0,'bzfk8VJ.yGLFbAtZM9RWRe',1427454009,1427456780,1,'','',0,NULL,2),
	(105,X'3139322E3136382E302E31','luukcastelijns_voetbal@hotmail.com','E8xQycR/lr5Z3SO.u1h8XO301321e086a7991e93','','luukcastelijns_voetbal@hotmail.com','606821cebbffadfbad40d95bf264c3295126a9e1','',0,'',1427454023,1427454023,0,'','',1,NULL,NULL),
	(119,X'3139322E3136382E302E31','Ditiseentestvoor@hotmail.com','QgKfyh3EqooEoMNhHlhb2u5e2bc2ce3dba41315e','','Ditiseentestvoor@hotmail.com','9cb0edaeb6f4a8cdf315d19df7016be0f380743c','',0,'',1427480363,1427480363,0,'','',1,NULL,NULL),
	(70,X'38322E39352E35312E313232','e.oppermanjurrius@ziggo.nl','lxcfydUN5VqwNyh/qsx.Ce3a59eec15fd4254ddc','','e.oppermanjurrius@ziggo.nl','','',0,'',1412781987,1412794085,1,'','',0,NULL,NULL),
	(71,X'38322E39352E35312E313232','theo@fysiotherapie-asten.nl','S/zxM.9hSR.oBWhuozvMpu8ddc384d09ce1c87e3','','theo@fysiotherapie-asten.nl','60bc671d5012a881209423fc15cbe7d578cd3e4a','',0,'',1412782006,1412782006,0,'','',0,NULL,NULL),
	(113,X'3139322E3136382E302E31','ruud.leenen@hotmail.com','DET1.xY12jdOV3HYSvNJS.8601bb7524dd265df0','','ruud.leenen@hotmail.com','1d7943d92fb4778e705478ca1f05737f7941ff8c','',0,'',1427454172,1427454172,0,'','',1,NULL,NULL),
	(76,X'3134352E3133312E3135372E3239','Arnevangeffen@hotmail.com','MJ/5T2zK59y8pYPyXri73u106260ea75abdb0be6','','Arnevangeffen@hotmail.com','','',0,'',1414101620,1414142808,1,'','',0,NULL,NULL),
	(78,X'3139322E3136382E302E31','mister_shervin@hotmail.com','I/3G9mwPeEiMUgz/Qc1HCef5aa3a9071adb20bbb','','mister_shervin@hotmail.com','','',0,'',1414430863,1414493020,1,'','',0,NULL,NULL),
	(80,X'38382E3135392E38322E313538','stefan_vaas@live.nl','O3r/pLD8oW67W/eCVKynmu97e83b251f604afa95','','stefan_vaas@live.nl','','',0,'dNMdnxgimuHIr4tPB3K5F.',1414495765,1414513301,1,'','',0,NULL,NULL),
	(79,X'38382E3135392E38322E313538','bankers91@hotmail.com','eFThVXI6VX69LK0.gyLBEea4eee68f0682a0dc91','','bankers91@hotmail.com','','',0,'',1414495699,1414511727,1,'','',0,NULL,NULL),
	(81,X'38382E3135392E38322E313538','bartvanrooij@outlook.com','kcynijoE42zSXnG5rTc3uuc2bfe9ce20340233ab','','bartvanrooij@outlook.com','','',0,'H5KcBh0YgCqrPQnhzLieCu',1414495790,1417875610,1,'','',0,NULL,NULL),
	(85,X'3134352E3133312E3135372E3239','Jvgrotel@hotmail.com','OIkAg0b9HfjEJO09Oqiunec03d6ab583f8a1de81','','Jvgrotel@hotmail.com','','',0,'',1414531028,1418724371,1,'','',0,NULL,NULL),
	(86,X'39322E3131302E33332E313334','dennis_kuypers@hotmail.com','IAQeTI8e16Ok2bbMmLKTgu0374d216b4b22af302','','dennis_kuypers@hotmail.com','','',0,'',1414571400,1414870148,1,'','',0,NULL,NULL),
	(87,X'3139322E3136382E302E31','tim@lessormore.nl','FjHlLjREoFxMmyJBWgWTm.32ed1748e08521aded','','tim@lessormore.nl','','',0,'',1414658662,1414658706,1,'','',0,NULL,NULL),
	(91,X'3139322E3136382E302E31','edwinmeijne@gmail.com','TD61QTK7yiUG3ffpoZ.2eu35edbab90518d6b8bb','','edwinmeijne@gmail.com','','',0,'',1418385509,1418735975,1,'','',0,NULL,NULL),
	(92,X'3139322E3136382E302E31','sjeffransen+test@gmail.com','Favx8tzcZyHLHj3toArJM.3bca771b46c360ad37','','sjeffransen+test@gmail.com','','',0,'',1423816647,1423844509,1,'','',0,NULL,2),
	(94,X'39322E3131302E33332E313334','cvdakker@home.nl','gILp0xwg88/g.gbOg/lvkOc01c0623a4855aec44','','cvdakker@home.nl','','2B.lsDK4PMSnZpHI6SrXmu725da02d553aed145a',1424328818,'',1424296773,1430256342,1,'','',0,NULL,2);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(2,2,1),
	(3,4,1),
	(4,19,2),
	(5,20,2),
	(6,21,2),
	(7,22,2),
	(8,23,2),
	(9,24,2),
	(10,25,2),
	(11,26,2),
	(12,27,2),
	(13,28,2),
	(14,29,2),
	(15,30,2),
	(16,31,2),
	(17,32,2),
	(18,33,2),
	(19,34,2),
	(20,35,2),
	(21,36,2),
	(22,37,2),
	(23,38,2),
	(24,39,2),
	(25,40,2),
	(26,41,2),
	(27,42,2),
	(28,43,2),
	(29,44,2),
	(30,45,2),
	(31,46,2),
	(32,47,2),
	(33,48,2),
	(34,49,2),
	(35,50,2),
	(36,51,2),
	(37,52,2),
	(38,53,2),
	(39,54,2),
	(40,55,2),
	(41,56,2),
	(42,57,2),
	(43,58,2),
	(44,59,2),
	(45,60,2),
	(46,61,2),
	(47,62,2),
	(48,63,2),
	(49,64,2),
	(50,65,2),
	(51,66,2),
	(52,67,2),
	(53,68,2),
	(54,69,2),
	(55,70,2),
	(56,71,2),
	(57,72,2),
	(58,73,2),
	(59,74,2),
	(60,75,2),
	(61,76,2),
	(62,77,2),
	(63,78,2),
	(64,79,2),
	(65,80,2),
	(66,81,2),
	(67,82,2),
	(68,83,2),
	(69,84,2),
	(70,85,2),
	(71,86,2),
	(72,87,2),
	(73,88,2),
	(74,89,2),
	(75,90,2),
	(76,91,2),
	(77,92,2),
	(78,93,2),
	(79,94,2),
	(80,95,2),
	(81,96,2),
	(82,97,2),
	(83,98,2),
	(84,99,2),
	(85,100,2),
	(86,101,2),
	(87,102,2),
	(88,103,2),
	(89,104,2),
	(90,105,2),
	(91,106,2),
	(92,107,2),
	(93,108,2),
	(94,109,2),
	(95,110,2),
	(96,111,2),
	(97,112,2),
	(98,113,2),
	(99,114,2),
	(100,115,2),
	(101,116,2),
	(102,117,2),
	(103,118,2),
	(104,119,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
