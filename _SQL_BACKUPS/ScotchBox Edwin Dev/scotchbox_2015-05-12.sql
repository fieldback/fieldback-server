# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.43-0ubuntu0.12.04.1)
# Database: scotchbox
# Generation Time: 2015-05-12 07:23:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `stream_live` varchar(256) NOT NULL DEFAULT '',
  `stream_live_preview` varchar(256) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  `live_enabled` int(1) NOT NULL DEFAULT '0',
  `is_instacam` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`camera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `stream_live`, `stream_live_preview`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`, `live_enabled`, `is_instacam`)
VALUES
	(1,'TestCam 1','http://192.168.0.62/recording.php?width=640&height=480&fps=90','','',1,1,'90','cam_1',1,0,1);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clip_to_part` WRITE;
/*!40000 ALTER TABLE `clip_to_part` DISABLE KEYS */;

INSERT INTO `clip_to_part` (`clip_id`, `part_id`, `cam`)
VALUES
	(1,1,1),
	(2,2,1),
	(3,3,1),
	(4,4,1),
	(5,5,1),
	(6,6,1),
	(7,7,1),
	(8,9,1),
	(9,10,1),
	(10,11,1),
	(11,13,1),
	(12,14,1),
	(13,15,1),
	(14,16,1),
	(15,17,1),
	(16,18,1),
	(17,19,1),
	(18,20,1),
	(19,21,1),
	(20,22,1),
	(21,23,1),
	(22,24,1),
	(23,25,1),
	(24,26,1),
	(25,27,1),
	(26,28,1),
	(27,29,1),
	(28,30,1),
	(29,31,1),
	(30,32,1),
	(31,33,1),
	(32,34,1),
	(33,35,1),
	(34,36,1),
	(35,37,1),
	(36,38,1),
	(37,39,1),
	(38,40,1),
	(39,41,1),
	(40,42,1),
	(41,43,1);

/*!40000 ALTER TABLE `clip_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clips` WRITE;
/*!40000 ALTER TABLE `clips` DISABLE KEYS */;

INSERT INTO `clips` (`clip_id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `date_created`, `kind`)
VALUES
	(1,'','','','','','','','','',1430471995,'instacording'),
	(2,'','','','','','','','','',1430474131,'instacording'),
	(3,'','','','','','','','','',1430492058,'instacording'),
	(4,'','','','','','','','','',1430494767,'instacording'),
	(5,'','','','','','','','','',1430494897,'instacording'),
	(6,'','','','','','','','','',1430496383,'instacording'),
	(7,'','','','','','','','','',1430731467,'instacording'),
	(8,'','','','','','','','','',1430733081,'instacording'),
	(9,'','','','','','','','','',1430733133,'instacording'),
	(10,'','','','','','','','','',1430743134,'instacording'),
	(11,'','','','','','','','','',1430749290,'instacording'),
	(12,'','','','','','','','','',1430750015,'instacording'),
	(13,'','','','','','','','','',1430750358,'instacording'),
	(14,'','','','','','','','','',1430750455,'instacording'),
	(15,'','','','','','','','','',1430750521,'instacording'),
	(16,'','','','','','','','','',1430774639,'instacording'),
	(17,'','','','','','','','','',1430818303,'instacording'),
	(18,'','','','','','','','','',1430818603,'instacording'),
	(19,'','','','','','','','','',1430819976,'instacording'),
	(20,'','','','','','','','','',1430824510,'instacording'),
	(21,'','','','','','','','','',1430827797,'instacording'),
	(22,'','','','','','','','','',1430827817,'instacording'),
	(23,'','','','','','','','','',1430828696,'instacording'),
	(24,'','','','','','','','','',1430831258,'instacording'),
	(25,'','','','','','','','','',1430831779,'instacording'),
	(26,'','','','','','','','','',1430832259,'instacording'),
	(27,'','','','','','','','','',1430832300,'instacording'),
	(28,'','','','','','','','','',1430832699,'instacording'),
	(29,'','','','','','','','','',1430833350,'instacording'),
	(30,'','','','','','','','','',1430833875,'instacording'),
	(31,'','','','','','','','','',1430838005,'instacording'),
	(32,'','','','','','','','','',1430839042,'instacording'),
	(33,'','','','','','','','','',1430840647,'instacording'),
	(34,'','','','','','','','','',1430842340,'instacording'),
	(35,'','','','','','','','','',1430843452,'instacording'),
	(36,'','','','','','','','','',1430845024,'instacording'),
	(37,'','','','','','','','','',1430845225,'instacording'),
	(38,'','','','','','','','','',1430845933,'instacording'),
	(39,'','','','','','','','','',1430908246,'instacording'),
	(40,'','','','','','','','','',1430908939,'instacording'),
	(41,'','','','','','','','','',1430915685,'instacording');

/*!40000 ALTER TABLE `clips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `creator_to_event` WRITE;
/*!40000 ALTER TABLE `creator_to_event` DISABLE KEYS */;

INSERT INTO `creator_to_event` (`creator_id`, `event_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `creator_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `event_to_organisation` WRITE;
/*!40000 ALTER TABLE `event_to_organisation` DISABLE KEYS */;

INSERT INTO `event_to_organisation` (`event_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `event_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`event_id`, `event_name`, `event_description`, `date_created`)
VALUES
	(1,'test','test','1430471053');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table instacordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `instacordings`;

CREATE TABLE `instacordings` (
  `instacording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(200) NOT NULL DEFAULT '0',
  `camera_identifier` varchar(30) NOT NULL DEFAULT '0' COMMENT 'matches the cam_constante from cameras table',
  `event_id` int(11) unsigned NOT NULL DEFAULT '0',
  `part_id` int(11) unsigned NOT NULL DEFAULT '0',
  `cam` int(11) NOT NULL DEFAULT '0',
  `cam_description` varchar(50) DEFAULT NULL,
  `session_start` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instacording_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_file_to_moment` WRITE;
/*!40000 ALTER TABLE `moment_file_to_moment` DISABLE KEYS */;

INSERT INTO `moment_file_to_moment` (`moment_file_id`, `moment_id`, `cam`)
VALUES
	(1,1,1),
	(2,2,1),
	(3,3,1),
	(4,4,1),
	(5,5,1),
	(6,6,1),
	(7,7,1),
	(8,8,1),
	(9,9,1),
	(10,10,1),
	(11,11,1),
	(12,12,1),
	(13,13,1),
	(14,14,1),
	(15,15,1),
	(16,16,1),
	(17,17,1),
	(18,18,1),
	(19,19,1),
	(20,20,1),
	(21,21,1),
	(22,22,1),
	(23,23,1),
	(24,24,1),
	(25,25,1),
	(26,26,1),
	(27,27,1),
	(28,28,1),
	(29,29,1),
	(30,30,1),
	(31,31,1),
	(32,32,1),
	(33,33,1),
	(34,34,1),
	(35,35,1),
	(36,36,1),
	(37,37,1),
	(38,38,1),
	(39,39,1),
	(40,40,1),
	(41,41,1),
	(42,42,1),
	(43,43,1),
	(44,44,1),
	(45,45,1),
	(46,46,1),
	(47,47,1),
	(48,48,1),
	(49,49,1),
	(50,50,1),
	(51,51,1),
	(52,52,1),
	(53,53,1),
	(54,54,1),
	(55,55,1),
	(56,56,1),
	(57,57,1),
	(58,58,1),
	(59,59,1),
	(60,60,1),
	(61,61,1),
	(62,62,1),
	(63,63,1),
	(64,64,1),
	(65,65,1),
	(66,66,1),
	(67,67,1),
	(68,68,1),
	(69,69,1),
	(70,70,1),
	(71,71,1),
	(72,72,1),
	(73,73,1),
	(74,74,1),
	(75,75,1),
	(76,76,1),
	(77,77,1),
	(78,78,1),
	(79,79,1),
	(80,80,1),
	(81,81,1),
	(82,82,1),
	(83,83,1),
	(84,84,1),
	(85,85,1),
	(86,86,1),
	(87,87,1),
	(88,88,1),
	(89,89,1),
	(90,90,1),
	(91,91,1),
	(92,92,1),
	(93,93,1),
	(94,94,1),
	(95,95,1),
	(96,96,1),
	(97,97,1),
	(98,98,1),
	(99,99,1),
	(100,100,1),
	(101,101,1),
	(102,102,1),
	(103,103,1),
	(104,104,1),
	(105,105,1),
	(106,106,1),
	(107,107,1),
	(108,108,1),
	(109,109,1),
	(110,110,1),
	(111,111,1),
	(112,112,1),
	(113,113,1),
	(114,114,1),
	(115,115,1),
	(116,116,1),
	(117,117,1),
	(118,118,1),
	(119,119,1),
	(120,120,1),
	(121,121,1),
	(122,122,1),
	(123,123,1),
	(124,124,1),
	(125,125,1),
	(126,126,1),
	(127,127,1),
	(128,128,1),
	(129,129,1),
	(130,130,1),
	(131,131,1),
	(132,132,1),
	(133,133,1),
	(134,134,1),
	(137,137,1),
	(138,138,1),
	(139,139,1),
	(140,140,1),
	(141,141,1),
	(142,142,1),
	(143,143,1),
	(144,144,1),
	(145,145,1),
	(146,146,1),
	(147,147,1),
	(148,148,1),
	(153,153,1),
	(152,152,1),
	(154,154,1),
	(155,155,1),
	(156,156,1),
	(157,157,1),
	(158,158,1),
	(159,159,1),
	(160,160,1),
	(161,161,1),
	(162,162,1),
	(164,164,1),
	(166,166,1),
	(167,167,1),
	(168,168,1),
	(169,169,1),
	(170,170,1),
	(171,171,1),
	(172,172,1),
	(173,173,1),
	(174,174,1),
	(175,175,1),
	(176,176,1),
	(177,177,1),
	(178,178,1),
	(179,179,1),
	(180,180,1),
	(181,181,1),
	(182,182,1),
	(184,184,1),
	(185,185,1),
	(186,186,1),
	(187,187,1),
	(188,188,1),
	(189,189,1),
	(190,190,1),
	(191,191,1),
	(192,192,1),
	(193,193,1),
	(194,194,1),
	(195,195,1),
	(196,196,1),
	(197,197,1),
	(198,198,1),
	(199,199,1),
	(200,200,1),
	(201,201,1),
	(202,202,1),
	(203,203,1),
	(204,204,1),
	(205,205,1),
	(206,206,1),
	(207,207,1),
	(208,208,1),
	(209,209,1),
	(210,210,1),
	(211,211,1),
	(212,212,1),
	(213,213,1),
	(214,214,1),
	(215,215,1),
	(216,216,1),
	(217,217,1),
	(218,218,1),
	(223,223,1),
	(220,220,1),
	(222,222,1);

/*!40000 ALTER TABLE `moment_file_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`moment_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_files` WRITE;
/*!40000 ALTER TABLE `moment_files` DISABLE KEYS */;

INSERT INTO `moment_files` (`moment_file_id`, `filename`, `path`, `cam`, `cam_description`)
VALUES
	(1,'1.mp4','videos/1/1/1',1,'TestCam 1'),
	(2,'2.mp4','videos/1/1/2',1,'TestCam 1'),
	(3,'3.mp4','videos/1/1/3',1,'TestCam 1'),
	(4,'4.mp4','videos/1/1/4',1,'TestCam 1'),
	(5,'5.mp4','videos/1/1/5',1,'TestCam 1'),
	(6,'6.mp4','videos/1/2/6',1,'TestCam 1'),
	(7,'7.mp4','videos/1/2/7',1,'TestCam 1'),
	(8,'8.mp4','videos/1/2/8',1,'TestCam 1'),
	(9,'9.mp4','videos/1/2/9',1,'TestCam 1'),
	(10,'10.mp4','videos/1/2/10',1,'TestCam 1'),
	(11,'11.mp4','videos/1/2/11',1,'TestCam 1'),
	(12,'12.mp4','videos/1/2/12',1,'TestCam 1'),
	(13,'13.mp4','videos/1/2/13',1,'TestCam 1'),
	(14,'14.mp4','videos/1/3/14',1,'TestCam 1'),
	(15,'15.mp4','videos/1/3/15',1,'TestCam 1'),
	(16,'16.mp4','videos/1/3/16',1,'TestCam 1'),
	(17,'17.mp4','videos/1/3/17',1,'TestCam 1'),
	(18,'18.mp4','videos/1/3/18',1,'TestCam 1'),
	(19,'19.mp4','videos/1/3/19',1,'TestCam 1'),
	(20,'20.mp4','videos/1/3/20',1,'TestCam 1'),
	(21,'21.mp4','videos/1/4/21',1,'TestCam 1'),
	(22,'22.mp4','videos/1/4/22',1,'TestCam 1'),
	(23,'23.mp4','videos/1/4/23',1,'TestCam 1'),
	(24,'24.mp4','videos/1/4/24',1,'TestCam 1'),
	(25,'25.mp4','videos/1/4/25',1,'TestCam 1'),
	(26,'26.mp4','videos/1/5/26',1,'TestCam 1'),
	(27,'27.mp4','videos/1/5/27',1,'TestCam 1'),
	(28,'28.mp4','videos/1/5/28',1,'TestCam 1'),
	(29,'29.mp4','videos/1/5/29',1,'TestCam 1'),
	(30,'30.mp4','videos/1/5/30',1,'TestCam 1'),
	(31,'31.mp4','videos/1/5/31',1,'TestCam 1'),
	(32,'32.mp4','videos/1/5/32',1,'TestCam 1'),
	(33,'33.mp4','videos/1/5/33',1,'TestCam 1'),
	(34,'34.mp4','videos/1/6/34',1,'TestCam 1'),
	(35,'35.mp4','videos/1/6/35',1,'TestCam 1'),
	(36,'36.mp4','videos/1/6/36',1,'TestCam 1'),
	(37,'37.mp4','videos/1/7/37',1,'TestCam 1'),
	(38,'38.mp4','videos/1/7/38',1,'TestCam 1'),
	(39,'39.mp4','videos/1/7/39',1,'TestCam 1'),
	(40,'40.mp4','videos/1/7/40',1,'TestCam 1'),
	(41,'41.mp4','videos/1/7/41',1,'TestCam 1'),
	(42,'42.mp4','videos/1/7/42',1,'TestCam 1'),
	(43,'43.mp4','videos/1/7/43',1,'TestCam 1'),
	(44,'44.mp4','videos/1/7/44',1,'TestCam 1'),
	(45,'45.mp4','videos/1/9/45',1,'TestCam 1'),
	(46,'46.mp4','videos/1/10/46',1,'TestCam 1'),
	(47,'47.mp4','videos/1/10/47',1,'TestCam 1'),
	(48,'48.mp4','videos/1/10/48',1,'TestCam 1'),
	(49,'49.mp4','videos/1/10/49',1,'TestCam 1'),
	(50,'50.mp4','videos/1/10/50',1,'TestCam 1'),
	(51,'51.mp4','videos/1/10/51',1,'TestCam 1'),
	(52,'52.mp4','videos/1/10/52',1,'TestCam 1'),
	(53,'53.mp4','videos/1/10/53',1,'TestCam 1'),
	(54,'54.mp4','videos/1/10/54',1,'TestCam 1'),
	(55,'55.mp4','videos/1/10/55',1,'TestCam 1'),
	(56,'56.mp4','videos/1/10/56',1,'TestCam 1'),
	(57,'57.mp4','videos/1/13/57',1,'TestCam 1'),
	(58,'58.mp4','videos/1/13/58',1,'TestCam 1'),
	(59,'59.mp4','videos/1/17/59',1,'TestCam 1'),
	(60,'60.mp4','videos/1/17/60',1,'TestCam 1'),
	(61,'61.mp4','videos/1/17/61',1,'TestCam 1'),
	(62,'62.mp4','videos/1/17/62',1,'TestCam 1'),
	(63,'63.mp4','videos/1/17/63',1,'TestCam 1'),
	(64,'64.mp4','videos/1/17/64',1,'TestCam 1'),
	(65,'65.mp4','videos/1/17/65',1,'TestCam 1'),
	(66,'66.mp4','videos/1/17/66',1,'TestCam 1'),
	(67,'67.mp4','videos/1/17/67',1,'TestCam 1'),
	(68,'68.mp4','videos/1/17/68',1,'TestCam 1'),
	(69,'69.mp4','videos/1/17/69',1,'TestCam 1'),
	(70,'70.mp4','videos/1/17/70',1,'TestCam 1'),
	(71,'71.mp4','videos/1/17/71',1,'TestCam 1'),
	(72,'72.mp4','videos/1/17/72',1,'TestCam 1'),
	(73,'73.mp4','videos/1/17/73',1,'TestCam 1'),
	(74,'74.mp4','videos/1/17/74',1,'TestCam 1'),
	(75,'75.mp4','videos/1/17/75',1,'TestCam 1'),
	(76,'76.mp4','videos/1/17/76',1,'TestCam 1'),
	(77,'77.mp4','videos/1/17/77',1,'TestCam 1'),
	(78,'78.mp4','videos/1/18/78',1,'TestCam Local'),
	(79,'79.mp4','videos/1/18/79',1,'TestCam Local'),
	(80,'80.mp4','videos/1/19/80',1,'TestCam 1'),
	(81,'81.mp4','videos/1/19/81',1,'TestCam 1'),
	(82,'82.mp4','videos/1/20/82',1,'TestCam 1'),
	(83,'83.mp4','videos/1/21/83',1,'TestCam 1'),
	(84,'84.mp4','videos/1/21/84',1,'TestCam 1'),
	(85,'85.mp4','videos/1/21/85',1,'TestCam 1'),
	(86,'86.mp4','videos/1/21/86',1,'TestCam 1'),
	(87,'87.mp4','videos/1/21/87',1,'TestCam 1'),
	(88,'88.mp4','videos/1/21/88',1,'TestCam 1'),
	(89,'89.mp4','videos/1/21/89',1,'TestCam 1'),
	(90,'90.mp4','videos/1/21/90',1,'TestCam 1'),
	(91,'91.mp4','videos/1/21/91',1,'TestCam 1'),
	(92,'92.mp4','videos/1/21/92',1,'TestCam 1'),
	(93,'93.mp4','videos/1/21/93',1,'TestCam 1'),
	(94,'94.mp4','videos/1/22/94',1,'TestCam 1'),
	(95,'95.mp4','videos/1/24/95',1,'TestCam 1'),
	(96,'96.mp4','videos/1/24/96',1,'TestCam 1'),
	(97,'97.mp4','videos/1/24/97',1,'TestCam 1'),
	(98,'98.mp4','videos/1/24/98',1,'TestCam 1'),
	(99,'99.mp4','videos/1/24/99',1,'TestCam 1'),
	(100,'100.mp4','videos/1/24/100',1,'TestCam 1'),
	(101,'101.mp4','videos/1/24/101',1,'TestCam 1'),
	(102,'102.mp4','videos/1/25/102',1,'TestCam 1'),
	(103,'103.mp4','videos/1/25/103',1,'TestCam 1'),
	(104,'104.mp4','./videos/1/25/104',1,'TestCam 1'),
	(105,'105.mp4','./videos/1/25/105',1,'TestCam 1'),
	(106,'106.mp4','./videos/1/25/106',1,'TestCam 1'),
	(107,'107.mp4','./videos/1/25/107',1,'TestCam 1'),
	(108,'108.mp4','./videos/1/25/108',1,'TestCam 1'),
	(109,'109.mp4','./videos/1/25/109',1,'TestCam 1'),
	(110,'110.mp4','./videos/1/25/110',1,'TestCam 1'),
	(111,'111.mp4','./videos/1/26/111',1,'TestCam 1'),
	(112,'112.mp4','./videos/1/26/112',1,'TestCam 1'),
	(113,'113.mp4','./videos/1/26/113',1,'TestCam 1'),
	(114,'114.mp4','./videos/1/26/114',1,'TestCam 1'),
	(115,'115.mp4','./videos/1/26/115',1,'TestCam 1'),
	(116,'116.mp4','./videos/1/26/116',1,'TestCam 1'),
	(117,'117.mp4','./videos/1/26/117',1,'TestCam 1'),
	(118,'118.mp4','./videos/1/27/118',1,'TestCam 1'),
	(119,'119.mp4','./videos/1/27/119',1,'TestCam 1'),
	(120,'120.mp4','./videos/1/27/120',1,'TestCam 1'),
	(121,'121.mp4','./videos/1/27/121',1,'TestCam 1'),
	(122,'122.mp4','./videos/1/27/122',1,'TestCam 1'),
	(123,'123.mp4','./videos/1/28/123',1,'TestCam 1'),
	(124,'124.mp4','./videos/1/29/124',1,'TestCam 1'),
	(125,'125.mp4','./videos/1/30/125',1,'TestCam 1'),
	(126,'126.mp4','./videos/1/30/126',1,'TestCam 1'),
	(127,'127.mp4','./videos/1/30/127',1,'TestCam 1'),
	(128,'128.mp4','./videos/1/30/128',1,'TestCam 1'),
	(129,'129.mp4','./videos/1/30/129',1,'TestCam 1'),
	(130,'130.mp4','./videos/1/31/130',1,'TestCam 1'),
	(131,'131.mp4','./videos/1/32/131',1,'TestCam 1'),
	(132,'132.mp4','./videos/1/33/132',1,'TestCam 1'),
	(133,'133.mp4','./videos/1/33/133',1,'TestCam 1'),
	(134,'134.mp4','./videos/1/33/134',1,'TestCam 1'),
	(137,'137.mp4','./videos/1/34/137',1,'TestCam 1'),
	(138,'138.mp4','./videos/1/34/138',1,'TestCam 1'),
	(139,'139.mp4','./videos/1/34/139',1,'TestCam 1'),
	(140,'140.mp4','./videos/1/34/140',1,'TestCam 1'),
	(141,'141.mp4','./videos/1/34/141',1,'TestCam 1'),
	(142,'142.mp4','./videos/1/35/142',1,'TestCam 1'),
	(143,'143.mp4','./videos/1/35/143',1,'TestCam 1'),
	(144,'144.mp4','./videos/1/35/144',1,'TestCam 1'),
	(145,'145.mp4','./videos/1/35/145',1,'TestCam 1'),
	(146,'146.mp4','./videos/1/35/146',1,'TestCam 1'),
	(147,'147.mp4','./videos/1/35/147',1,'TestCam 1'),
	(148,'148.mp4','./videos/1/35/148',1,'TestCam 1'),
	(153,'153.mp4','./videos/1/36/153',1,'TestCam 1'),
	(152,'152.mp4','./videos/1/36/152',1,'TestCam 1'),
	(154,'154.mp4','./videos/1/36/154',1,'TestCam 1'),
	(155,'155.mp4','./videos/1/36/155',1,'TestCam 1'),
	(156,'156.mp4','./videos/1/36/156',1,'TestCam 1'),
	(157,'157.mp4','./videos/1/36/157',1,'TestCam 1'),
	(158,'158.mp4','./videos/1/36/158',1,'TestCam 1'),
	(159,'159.mp4','./videos/1/36/159',1,'TestCam 1'),
	(160,'160.mp4','./videos/1/37/160',1,'TestCam 1'),
	(161,'161.mp4','./videos/1/37/161',1,'TestCam 1'),
	(162,'162.mp4','./videos/1/37/162',1,'TestCam 1'),
	(164,'164.mp4','./videos/1/37/164',1,'TestCam 1'),
	(166,'166.mp4','./videos/1/37/166',1,'TestCam 1'),
	(167,'167.mp4','./videos/1/37/167',1,'TestCam 1'),
	(168,'168.mp4','./videos/1/37/168',1,'TestCam 1'),
	(169,'169.mp4','./videos/1/37/169',1,'TestCam 1'),
	(170,'170.mp4','./videos/1/37/170',1,'TestCam 1'),
	(171,'171.mp4','./videos/1/37/171',1,'TestCam 1'),
	(172,'172.mp4','./videos/1/37/172',1,'TestCam 1'),
	(173,'173.mp4','./videos/1/37/173',1,'TestCam 1'),
	(174,'174.mp4','./videos/1/37/174',1,'TestCam 1'),
	(175,'175.mp4','./videos/1/37/175',1,'TestCam 1'),
	(176,'176.mp4','./videos/1/37/176',1,'TestCam 1'),
	(177,'177.mp4','./videos/1/37/177',1,'TestCam 1'),
	(178,'178.mp4','./videos/1/37/178',1,'TestCam 1'),
	(179,'179.mp4','./videos/1/37/179',1,'TestCam 1'),
	(180,'180.mp4','./videos/1/37/180',1,'TestCam 1'),
	(181,'181.mp4','./videos/1/37/181',1,'TestCam 1'),
	(182,'182.mp4','./videos/1/37/182',1,'TestCam 1'),
	(184,'184.mp4','./videos/1/37/184',1,'TestCam 1'),
	(185,'185.mp4','./videos/1/38/185',1,'TestCam 1'),
	(186,'186.mp4','./videos/1/38/186',1,'TestCam 1'),
	(187,'187.mp4','./videos/1/38/187',1,'TestCam 1'),
	(188,'188.mp4','./videos/1/39/188',1,'TestCam 1'),
	(189,'189.mp4','./videos/1/39/189',1,'TestCam 1'),
	(190,'190.mp4','./videos/1/39/190',1,'TestCam 1'),
	(191,'191.mp4','./videos/1/40/191',1,'TestCam 1'),
	(192,'192.mp4','./videos/1/40/192',1,'TestCam 1'),
	(193,'193.mp4','./videos/1/40/193',1,'TestCam 1'),
	(194,'194.mp4','./videos/1/40/194',1,'TestCam 1'),
	(195,'195.mp4','./videos/1/40/195',1,'TestCam 1'),
	(196,'196.mp4','./videos/1/40/196',1,'TestCam 1'),
	(197,'197.mp4','./videos/1/40/197',1,'TestCam 1'),
	(198,'198.mp4','./videos/1/41/198',1,'TestCam 1'),
	(199,'199.mp4','./videos/1/41/199',1,'TestCam 1'),
	(200,'200.mp4','./videos/1/41/200',1,'TestCam 1'),
	(201,'201.mp4','./videos/1/41/201',1,'TestCam 1'),
	(202,'202.mp4','./videos/1/41/202',1,'TestCam 1'),
	(203,'203.mp4','./videos/1/41/203',1,'TestCam 1'),
	(204,'204.mp4','./videos/1/41/204',1,'TestCam 1'),
	(205,'205.mp4','./videos/1/42/205',1,'TestCam 1'),
	(206,'206.mp4','./videos/1/42/206',1,'TestCam 1'),
	(207,'207.mp4','./videos/1/42/207',1,'TestCam 1'),
	(208,'208.mp4','./videos/1/42/208',1,'TestCam 1'),
	(209,'209.mp4','./videos/1/42/209',1,'TestCam 1'),
	(210,'210.mp4','./videos/1/42/210',1,'TestCam 1'),
	(211,'211.mp4','./videos/1/42/211',1,'TestCam 1'),
	(212,'212.mp4','./videos/1/42/212',1,'TestCam 1'),
	(213,'213.mp4','./videos/1/42/213',1,'TestCam 1'),
	(214,'214.mp4','./videos/1/42/214',1,'TestCam 1'),
	(215,'215.mp4','./videos/1/42/215',1,'TestCam 1'),
	(216,'216.mp4','./videos/1/42/216',1,'TestCam 1'),
	(217,'217.mp4','./videos/1/43/217',1,'TestCam 1'),
	(218,'218.mp4','./videos/1/43/218',1,'TestCam 1'),
	(223,'223.mp4','./videos/1/43/223',1,'TestCam 1'),
	(220,'220.mp4','./videos/1/43/220',1,'TestCam 1'),
	(222,'222.mp4','./videos/1/43/222',1,'TestCam 1');

/*!40000 ALTER TABLE `moment_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_part` WRITE;
/*!40000 ALTER TABLE `moment_to_part` DISABLE KEYS */;

INSERT INTO `moment_to_part` (`moment_id`, `part_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,2),
	(7,2),
	(8,2),
	(9,2),
	(10,2),
	(11,2),
	(12,2),
	(13,2),
	(14,3),
	(15,3),
	(16,3),
	(17,3),
	(18,3),
	(19,3),
	(20,3),
	(21,4),
	(22,4),
	(23,4),
	(24,4),
	(25,4),
	(26,5),
	(27,5),
	(28,5),
	(29,5),
	(30,5),
	(31,5),
	(32,5),
	(33,5),
	(34,6),
	(35,6),
	(36,6),
	(37,7),
	(38,7),
	(39,7),
	(40,7),
	(41,7),
	(42,7),
	(43,7),
	(44,7),
	(45,9),
	(46,10),
	(47,10),
	(48,10),
	(49,10),
	(50,10),
	(51,10),
	(52,10),
	(53,10),
	(54,10),
	(55,10),
	(56,10),
	(57,13),
	(58,13),
	(59,17),
	(60,17),
	(61,17),
	(62,17),
	(63,17),
	(64,17),
	(65,17),
	(66,17),
	(67,17),
	(68,17),
	(69,17),
	(70,17),
	(71,17),
	(72,17),
	(73,17),
	(74,17),
	(75,17),
	(76,17),
	(77,17),
	(78,18),
	(79,18),
	(80,19),
	(81,19),
	(82,20),
	(83,21),
	(84,21),
	(85,21),
	(86,21),
	(87,21),
	(88,21),
	(89,21),
	(90,21),
	(91,21),
	(92,21),
	(93,21),
	(94,22),
	(95,24),
	(96,24),
	(97,24),
	(98,24),
	(99,24),
	(100,24),
	(101,24),
	(102,25),
	(103,25),
	(104,25),
	(105,25),
	(106,25),
	(107,25),
	(108,25),
	(109,25),
	(110,25),
	(111,26),
	(112,26),
	(113,26),
	(114,26),
	(115,26),
	(116,26),
	(117,26),
	(118,27),
	(119,27),
	(120,27),
	(121,27),
	(122,27),
	(123,28),
	(124,29),
	(125,30),
	(126,30),
	(127,30),
	(128,30),
	(129,30),
	(130,31),
	(131,32),
	(132,33),
	(133,33),
	(134,33),
	(137,34),
	(138,34),
	(139,34),
	(140,34),
	(141,34),
	(142,35),
	(143,35),
	(144,35),
	(145,35),
	(146,35),
	(147,35),
	(148,35),
	(153,36),
	(152,36),
	(154,36),
	(155,36),
	(156,36),
	(157,36),
	(158,36),
	(159,36),
	(160,37),
	(161,37),
	(162,37),
	(164,37),
	(166,37),
	(167,37),
	(168,37),
	(169,37),
	(170,37),
	(171,37),
	(172,37),
	(173,37),
	(174,37),
	(175,37),
	(176,37),
	(177,37),
	(178,37),
	(179,37),
	(180,37),
	(181,37),
	(182,37),
	(184,37),
	(185,38),
	(186,38),
	(187,38),
	(188,39),
	(189,39),
	(190,39),
	(191,40),
	(192,40),
	(193,40),
	(194,40),
	(195,40),
	(196,40),
	(197,40),
	(198,41),
	(199,41),
	(200,41),
	(201,41),
	(202,41),
	(203,41),
	(204,41),
	(205,42),
	(206,42),
	(207,42),
	(208,42),
	(209,42),
	(210,42),
	(211,42),
	(212,42),
	(213,42),
	(214,42),
	(215,42),
	(216,42),
	(217,43),
	(218,43),
	(223,43),
	(220,43),
	(222,43);

/*!40000 ALTER TABLE `moment_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_playlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_playlist`;

CREATE TABLE `moment_to_playlist` (
  `playlist_id` int(11) unsigned NOT NULL,
  `moment_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `playlist_id` (`playlist_id`,`moment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_playlist` WRITE;
/*!40000 ALTER TABLE `moment_to_playlist` DISABLE KEYS */;

INSERT INTO `moment_to_playlist` (`playlist_id`, `moment_id`)
VALUES
	(1,1),
	(1,3),
	(1,13),
	(2,4);

/*!40000 ALTER TABLE `moment_to_playlist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;

INSERT INTO `moments` (`moment_id`, `date_created`, `time_in_clip`, `lead`, `lapse`, `comment`)
VALUES
	(1,1430472093,98,0,-1,''),
	(2,1430472308,313,0,-1,''),
	(3,1430472356,361,0,-1,''),
	(4,1430472393,398,0,-1,''),
	(5,1430472519,524,0,-1,''),
	(6,1430474132,1,0,-1,''),
	(7,1430474134,3,0,-1,''),
	(8,1430474370,239,0,-1,''),
	(9,1430490555,16424,0,-1,''),
	(10,1430491666,17535,0,-1,''),
	(11,1430491676,17545,0,-1,''),
	(12,1430491937,17806,0,-1,''),
	(13,1430491952,17821,0,-1,''),
	(14,1430492060,2,0,-1,''),
	(15,1430492227,169,0,-1,''),
	(16,1430492250,192,0,-1,''),
	(17,1430493384,1326,0,-1,''),
	(18,1430493598,1540,0,-1,''),
	(19,1430493609,1551,0,-1,''),
	(20,1430494655,2597,0,-1,''),
	(21,1430494768,1,0,-1,''),
	(22,1430494791,24,0,-1,''),
	(23,1430494801,34,0,-1,''),
	(24,1430494816,49,0,-1,''),
	(25,1430494887,120,0,-1,''),
	(26,1430494898,1,0,-1,''),
	(27,1430494909,12,0,-1,''),
	(28,1430494925,28,0,-1,''),
	(29,1430495040,143,0,-1,''),
	(30,1430495564,667,0,-1,''),
	(31,1430495602,705,0,-1,''),
	(32,1430495707,810,0,-1,''),
	(33,1430496367,1470,0,-1,''),
	(34,1430496384,1,0,-1,''),
	(35,1430496527,144,0,-1,''),
	(36,1430497559,1176,0,-1,''),
	(37,1430731578,111,0,-1,''),
	(38,1430731803,336,0,-1,''),
	(39,1430731845,378,0,-1,''),
	(40,1430731891,424,0,-1,''),
	(41,1430731898,431,0,-1,''),
	(42,1430731927,460,0,-1,''),
	(43,1430731949,482,0,-1,''),
	(44,1430732059,592,0,-1,''),
	(45,1430733083,2,0,-1,''),
	(46,1430733144,11,0,-1,''),
	(47,1430733207,74,0,-1,''),
	(48,1430733231,98,0,-1,''),
	(49,1430733276,143,0,-1,''),
	(50,1430733424,291,0,-1,''),
	(51,1430733444,311,0,-1,''),
	(52,1430733532,399,0,-1,''),
	(53,1430733558,425,0,-1,''),
	(54,1430733565,432,0,-1,''),
	(55,1430733583,450,0,-1,''),
	(56,1430734873,1740,0,-1,''),
	(57,1430749291,1,0,-1,''),
	(58,1430749297,7,0,-1,''),
	(59,1430751529,1008,0,-1,''),
	(60,1430751543,1022,0,-1,''),
	(61,1430751609,1088,0,-1,''),
	(62,1430751678,1157,0,-1,''),
	(63,1430751730,1209,0,-1,''),
	(64,1430751744,1223,0,-1,''),
	(65,1430751770,1249,0,-1,''),
	(66,1430751890,1369,0,-1,''),
	(67,1430752291,1770,0,-1,''),
	(68,1430752362,1841,0,-1,''),
	(69,1430752695,2174,0,-1,''),
	(70,1430753123,2602,0,-1,''),
	(71,1430753199,2678,0,-1,''),
	(72,1430753792,3271,0,-1,''),
	(73,1430773680,23159,0,-1,''),
	(74,1430773690,23169,0,-1,''),
	(75,1430773717,23196,0,-1,''),
	(76,1430773793,23272,0,-1,''),
	(77,1430773904,23383,0,-1,''),
	(78,1430774641,2,0,-1,''),
	(79,1430774791,152,0,-1,''),
	(80,1430818304,1,0,-1,''),
	(81,1430818537,234,0,-1,''),
	(82,1430818856,253,0,-1,''),
	(83,1430819979,3,0,-1,''),
	(84,1430820041,65,0,-1,''),
	(85,1430820063,87,0,-1,''),
	(86,1430820083,107,0,-1,''),
	(87,1430820115,139,0,-1,''),
	(88,1430820439,463,0,-1,''),
	(89,1430820483,507,0,-1,''),
	(90,1430820502,526,0,-1,''),
	(91,1430821431,1455,0,-1,''),
	(92,1430824359,4383,0,-1,''),
	(93,1430824399,4423,0,-1,''),
	(94,1430824532,22,0,-1,''),
	(95,1430827826,9,0,-1,''),
	(96,1430827837,20,0,-1,''),
	(97,1430828035,218,0,-1,''),
	(98,1430828115,298,0,-1,''),
	(99,1430828122,305,0,-1,''),
	(100,1430828131,314,0,-1,''),
	(101,1430828133,316,0,-1,''),
	(102,1430828708,12,0,-1,''),
	(103,1430829237,541,0,-1,''),
	(104,1430829505,809,0,-1,''),
	(105,1430829597,901,0,-1,''),
	(106,1430829655,959,0,-1,''),
	(107,1430829870,1174,0,-1,''),
	(108,1430830265,1569,0,-1,''),
	(109,1430830528,1832,0,-1,''),
	(110,1430830613,1917,0,-1,''),
	(111,1430831260,2,0,-1,''),
	(112,1430831265,7,0,-1,''),
	(113,1430831277,19,0,-1,''),
	(114,1430831327,69,0,-1,''),
	(115,1430831342,84,0,-1,''),
	(116,1430831614,356,0,-1,''),
	(117,1430831665,407,0,-1,''),
	(118,1430831781,2,0,-1,''),
	(119,1430831824,45,0,-1,''),
	(120,1430831930,151,0,-1,''),
	(121,1430832084,305,0,-1,''),
	(122,1430832137,358,0,-1,''),
	(123,1430832260,1,0,-1,''),
	(124,1430832302,2,0,-1,''),
	(125,1430832701,2,0,-1,''),
	(126,1430832721,22,0,-1,''),
	(127,1430832877,178,0,-1,''),
	(128,1430832905,206,0,-1,''),
	(129,1430833115,416,0,-1,''),
	(130,1430833351,1,0,-1,''),
	(131,1430833877,2,0,-1,''),
	(132,1430838006,1,0,-1,''),
	(133,1430838289,284,0,-1,''),
	(134,1430838429,424,0,-1,''),
	(137,1430839176,134,0,-1,''),
	(138,1430840086,1044,0,-1,''),
	(139,1430840108,1066,0,-1,''),
	(140,1430840194,1152,0,-1,''),
	(141,1430840315,1273,0,-1,''),
	(142,1430840650,3,0,-1,''),
	(143,1430840755,108,0,-1,''),
	(144,1430841232,585,0,-1,''),
	(145,1430841254,607,0,-1,''),
	(146,1430841293,646,0,-1,''),
	(147,1430841333,686,0,-1,''),
	(148,1430841382,735,0,-1,''),
	(153,1430842896,556,0,-1,''),
	(152,1430842444,104,0,-1,''),
	(154,1430842942,602,0,-1,''),
	(155,1430842969,629,0,-1,''),
	(156,1430843105,765,0,-1,''),
	(157,1430843238,898,0,-1,''),
	(158,1430843290,950,0,-1,''),
	(159,1430843389,1049,0,-1,''),
	(160,1430843464,12,0,-1,''),
	(161,1430843625,173,0,-1,''),
	(162,1430843710,258,0,-1,''),
	(164,1430843878,426,0,-1,''),
	(166,1430843928,476,0,-1,''),
	(167,1430843976,524,0,-1,''),
	(168,1430843983,531,0,-1,''),
	(169,1430844301,849,0,-1,''),
	(170,1430844381,929,0,-1,''),
	(171,1430844403,951,0,-1,''),
	(172,1430844435,983,0,-1,''),
	(173,1430844457,1005,0,-1,''),
	(174,1430844524,1072,0,-1,''),
	(175,1430844596,1144,0,-1,''),
	(176,1430844667,1215,0,-1,''),
	(177,1430844744,1292,0,-1,''),
	(178,1430844802,1350,0,-1,''),
	(179,1430844820,1368,0,-1,''),
	(180,1430844839,1387,0,-1,''),
	(181,1430844880,1428,0,-1,''),
	(182,1430844906,1454,0,-1,''),
	(184,1430844930,1478,0,-1,''),
	(185,1430845032,8,0,-1,''),
	(186,1430845087,63,0,-1,''),
	(187,1430845142,118,0,-1,''),
	(188,1430845238,13,0,-1,''),
	(189,1430845250,25,0,-1,''),
	(190,1430845534,309,0,-1,''),
	(191,1430845953,20,0,-1,''),
	(192,1430845982,49,0,-1,''),
	(193,1430846059,126,0,-1,''),
	(194,1430846116,183,0,-1,''),
	(195,1430846198,265,0,-1,''),
	(196,1430846206,273,0,-1,''),
	(197,1430846222,289,0,-1,''),
	(198,1430908248,2,0,-1,''),
	(199,1430908277,31,0,-1,''),
	(200,1430908321,75,0,-1,''),
	(201,1430908618,372,0,-1,''),
	(202,1430908640,394,0,-1,''),
	(203,1430908819,573,0,-1,''),
	(204,1430908826,580,0,-1,''),
	(205,1430908942,3,0,-1,''),
	(206,1430908984,45,0,-1,''),
	(207,1430909077,138,0,-1,''),
	(208,1430909179,240,0,-1,''),
	(209,1430909429,490,0,-1,''),
	(210,1430909494,555,0,-1,''),
	(211,1430909843,904,0,-1,''),
	(212,1430910531,1592,0,-1,''),
	(213,1430910589,1650,0,-1,''),
	(214,1430910728,1789,0,-1,''),
	(215,1430910817,1878,0,-1,''),
	(216,1430910834,1895,0,-1,''),
	(217,1430915702,17,0,-1,''),
	(218,1430915747,62,0,-1,''),
	(223,1430915965,280,0,-1,''),
	(220,1430915838,153,0,-1,''),
	(222,1430915896,211,0,-1,'');

/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Local testing');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `part_to_event` WRITE;
/*!40000 ALTER TABLE `part_to_event` DISABLE KEYS */;

INSERT INTO `part_to_event` (`part_id`, `event_id`)
VALUES
	(36,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1);

/*!40000 ALTER TABLE `part_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  `type` varchar(11) NOT NULL DEFAULT 'streaming',
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`part_id`, `part_name`, `date_created`, `type`)
VALUES
	(36,'Part 1','1430842335','instant'),
	(38,'Part 3','1430845018','instant'),
	(39,'Part 4','1430845221','instant'),
	(40,'Part 5','1430845925','instant'),
	(41,'Part 6','1430908239','instant'),
	(42,'Part 7','1430908928','instant'),
	(43,'Part 8','1430911020','instant');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `player_to_team` WRITE;
/*!40000 ALTER TABLE `player_to_team` DISABLE KEYS */;

INSERT INTO `player_to_team` (`player_id`, `team_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `player_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table playlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlists`;

CREATE TABLE `playlists` (
  `playlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;

INSERT INTO `playlists` (`playlist_id`, `owner_id`, `title`)
VALUES
	(1,1,'test'),
	(2,1,'lol');

/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cam_number` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `cam_description` varchar(256) DEFAULT '',
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table shares
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shares`;

CREATE TABLE `shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `share_token` varchar(254) NOT NULL DEFAULT '',
  `visits` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `type` varchar(254) NOT NULL DEFAULT 'moment',
  `id` int(11) NOT NULL,
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_token` (`share_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;

INSERT INTO `shares` (`share_id`, `share_token`, `visits`, `creator_id`, `type`, `id`)
VALUES
	(1,'7YnPWvbgTj',0,1,'moment',220),
	(2,'MK8faAecFT',0,1,'moment',222);

/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  `is_online` int(1) NOT NULL DEFAULT '0',
  `system_from_email` varchar(128) NOT NULL DEFAULT 'fieldback@fieldback.net',
  `support_email` varchar(128) DEFAULT 'support@fieldback.net',
  `downloads` int(1) NOT NULL DEFAULT '0',
  `max_instacord_seconds` int(11) NOT NULL DEFAULT '10',
  `insta_url` varchar(500) DEFAULT NULL,
  `shares` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`, `is_online`, `system_from_email`, `support_email`, `downloads`, `max_instacord_seconds`, `insta_url`, `shares`)
VALUES
	(1,'LOCAL','LOCAL version','/usr/bin/ffmpeg','/usr/bin/openRTSP',120,'tim@lessormore.nl',0,'fieldback@fieldback.net','support@fieldback.net',0,15,'http://192.168.0.198/insta/moment_file',1);

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_organisation` WRITE;
/*!40000 ALTER TABLE `tag_to_organisation` DISABLE KEYS */;

INSERT INTO `tag_to_organisation` (`tag_id`, `organisation_id`)
VALUES
	(49,1),
	(50,1);

/*!40000 ALTER TABLE `tag_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tagfields` WRITE;
/*!40000 ALTER TABLE `tagfields` DISABLE KEYS */;

INSERT INTO `tagfields` (`tagfield_id`, `tagfield_name`, `show_players`, `date_saved`)
VALUES
	(1,'Test',0,1430468832);

/*!40000 ALTER TABLE `tagfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`tag_id`, `tag_name`)
VALUES
	(48,'Goed moment'),
	(47,'Slecht moment'),
	(46,'Aanval'),
	(45,'Verdediging'),
	(44,'Balbezit'),
	(43,'Links'),
	(42,'Rechts'),
	(41,'Voor'),
	(40,'Midden'),
	(39,'Achter'),
	(49,'test'),
	(50,'test2');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_event` WRITE;
/*!40000 ALTER TABLE `team_to_event` DISABLE KEYS */;

INSERT INTO `team_to_event` (`team_id`, `event_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `team_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(1,'Test Team');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `track_login` WRITE;
/*!40000 ALTER TABLE `track_login` DISABLE KEYS */;

INSERT INTO `track_login` (`id`, `user_id`, `username`, `email`, `active`, `ip_address`)
VALUES
	(245,1,'admin','tim@lessormore.nl',1,'0'),
	(246,1,'admin','admin@fieldback.net',1,'0'),
	(247,1,'admin','tim@lessormore.nl',1,'0'),
	(248,1,'admin','admin@fieldback.net',1,'0'),
	(249,1,'admin','admin@fieldback.net',1,'0'),
	(250,1,'admin','admin@fieldback.net',1,'0'),
	(251,1,'admin','admin@fieldback.net',1,'0'),
	(252,1,'admin','admin@fieldback.net',1,'0'),
	(253,1,'admin','admin@fieldback.net',1,'0'),
	(254,1,'admin','admin@fieldback.net',1,'0'),
	(255,1,'admin','admin@fieldback.net',1,'0'),
	(256,1,'admin','admin@fieldback.net',1,'0'),
	(257,1,'admin','admin@fieldback.net',1,'0'),
	(258,1,'admin','admin@fieldback.net',1,'0'),
	(259,1,'admin','admin@fieldback.net',1,'0'),
	(260,1,'admin','admin@fieldback.net',1,'0'),
	(261,1,'admin','admin@fieldback.net',1,'0'),
	(262,1,'admin','admin@fieldback.net',1,'0'),
	(263,1,'admin','admin@fieldback.net',1,'0'),
	(264,1,'admin','admin@fieldback.net',1,'0'),
	(265,1,'admin','admin@fieldback.net',1,'0');

/*!40000 ALTER TABLE `track_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lead` int(11) DEFAULT NULL,
  `lapse` int(11) DEFAULT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(1,'Admin','Admin','admin@fieldback.net');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_clip` WRITE;
/*!40000 ALTER TABLE `user_to_clip` DISABLE KEYS */;

INSERT INTO `user_to_clip` (`user_id`, `clip_id`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(1,4),
	(1,5),
	(1,6),
	(1,7),
	(1,8),
	(1,9),
	(1,10),
	(1,11),
	(1,12),
	(1,13),
	(1,14),
	(1,15),
	(1,16),
	(1,17),
	(1,18),
	(1,19),
	(1,20),
	(1,21),
	(1,22),
	(1,23),
	(1,24),
	(1,25),
	(1,26),
	(1,27),
	(1,28),
	(1,29),
	(1,30),
	(1,31),
	(1,32),
	(1,33),
	(1,34),
	(1,35),
	(1,36),
	(1,37),
	(1,38),
	(1,39),
	(1,40),
	(1,41);

/*!40000 ALTER TABLE `user_to_clip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_tagfield` WRITE;
/*!40000 ALTER TABLE `user_to_tagfield` DISABLE KEYS */;

INSERT INTO `user_to_tagfield` (`user_id`, `tagfield_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(1,1,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(1) NOT NULL DEFAULT '1',
  `api_key` varchar(64) DEFAULT NULL,
  `selected_organisation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`, `selected_organisation`)
VALUES
	(1,X'30','admin','$2y$08$Ur3gT56i0f3v658/IDZePeAgehbErEIB32gVcHrTowkBLLBcvIu3.','','admin@fieldback.net','','',0,'',0,1431412777,1,'','',0,'007',1);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
