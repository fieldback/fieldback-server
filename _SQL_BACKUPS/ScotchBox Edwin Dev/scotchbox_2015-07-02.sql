# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.43-0ubuntu0.12.04.1)
# Database: scotchbox
# Generation Time: 2015-07-02 12:11:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `stream_live` varchar(256) NOT NULL DEFAULT '',
  `stream_live_preview` varchar(256) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  `live_enabled` int(1) NOT NULL DEFAULT '0',
  `is_instacam` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`camera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `stream_live`, `stream_live_preview`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`, `live_enabled`, `is_instacam`)
VALUES
	(1,'TestCam 1','http://192.168.0.68/raspicam.php?width=640&height=480&fps=90&awb=cloud&iso=200&exposure=sports&camera_identifier=1','http://192.168.0.68/mjpeg.php','http://192.168.0.68/mjpeg.php',1,1,'90','cam_1',1,0,1),
	(5,'TestCam 2','http://192.168.178.31/raspicam.php?width=640&height=480&fps=90&awb=cloud&iso=200&exposure=sports&camera_identifier=1','http://192.168.178.31/mjpeg.php','http://192.168.178.31/mjpeg.php',2,0,'90','cam_2',1,0,1),
	(6,'TestCam 3','http://192.168.178.32/raspicam.php?width=640&height=480&fps=90&awb=cloud&iso=200&exposure=sports&camera_identifier=1','http://192.168.178.32/mjpeg.php','http://192.168.178.32/mjpeg.php',3,0,'90','cam_3',1,0,1),
	(9,'720p2','http://192.168.178.31/raspicam.php?width=1280&height=720&fps=60&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',4,0,'60','cam_720p2',1,0,1),
	(8,'720p1','http://192.168.178.27/raspicam.php?width=1280&height=720&fps=60&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',5,0,'60','cam_720p1',1,0,1),
	(10,'720p3','http://192.168.178.32/raspicam.php?width=1280&height=720&fps=60&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',6,0,'60','cam_720p3',1,0,1),
	(11,'1080p1','http://192.168.178.31/raspicam.php?width=1920&height=1080&fps=30&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',7,0,'30','cam_1080p1',1,0,1),
	(12,'1080p2','http://192.168.178.27/raspicam.php?width=1920&height=1080&fps=30&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',8,0,'30','cam_1080p2',1,0,1),
	(13,'1080p3','http://192.168.178.32/raspicam.php?width=1920&height=1080&fps=30&awb=cloud&iso=200&exposure=sports&camera_identifier=1','','',9,0,'30','cam_1080p3',1,0,1);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clip_to_part` WRITE;
/*!40000 ALTER TABLE `clip_to_part` DISABLE KEYS */;

INSERT INTO `clip_to_part` (`clip_id`, `part_id`, `cam`)
VALUES
	(1,1,1),
	(2,2,1),
	(3,3,1),
	(4,4,1),
	(5,5,1),
	(6,6,1),
	(7,7,1),
	(8,9,1),
	(9,10,1),
	(10,11,1),
	(11,13,1),
	(12,14,1),
	(13,15,1),
	(14,16,1),
	(15,17,1),
	(16,18,1),
	(17,19,1),
	(18,20,1),
	(19,21,1),
	(20,22,1),
	(21,23,1),
	(22,24,1),
	(23,25,1),
	(24,26,1),
	(25,27,1),
	(26,28,1),
	(27,29,1),
	(28,30,1),
	(29,31,1),
	(30,32,1),
	(31,33,1),
	(32,34,1),
	(33,35,1),
	(54,57,1),
	(35,37,1),
	(53,56,1),
	(52,55,2),
	(38,40,1),
	(51,55,1),
	(50,54,2),
	(49,54,1),
	(42,46,1),
	(43,47,1),
	(44,48,1),
	(48,53,1),
	(47,52,1),
	(55,57,2),
	(153,114,1),
	(152,113,2),
	(155,115,1),
	(154,114,2),
	(151,113,1),
	(150,112,2),
	(62,61,1),
	(63,62,1),
	(64,63,1),
	(65,65,1),
	(66,66,1),
	(67,67,1),
	(68,68,1),
	(69,69,1),
	(70,70,1),
	(71,70,1),
	(72,71,1),
	(139,107,2),
	(149,112,1),
	(148,111,2),
	(147,111,1),
	(146,110,1),
	(145,109,2),
	(142,108,2),
	(141,108,1),
	(140,107,3),
	(159,116,2),
	(158,116,1),
	(157,115,3),
	(156,115,2),
	(144,109,1),
	(143,108,3),
	(138,107,1),
	(137,106,3),
	(136,106,2),
	(135,106,1),
	(134,105,3),
	(133,105,2),
	(132,105,1),
	(131,104,2),
	(96,85,1),
	(129,103,2),
	(130,104,1),
	(128,103,1),
	(127,102,2),
	(126,102,1),
	(102,89,1),
	(103,89,2),
	(104,89,3),
	(105,90,1),
	(106,90,2),
	(107,90,3),
	(124,101,1),
	(123,100,1),
	(122,99,1),
	(121,98,1),
	(120,97,2),
	(119,97,1),
	(125,101,2),
	(115,94,1),
	(116,95,1),
	(117,96,1),
	(118,96,2),
	(160,116,3),
	(161,117,1),
	(162,117,2),
	(163,118,1),
	(164,118,2),
	(165,118,3),
	(166,119,1),
	(167,119,2),
	(168,119,3),
	(169,120,1),
	(170,120,2),
	(171,120,3),
	(172,121,1),
	(173,121,2),
	(174,121,3),
	(175,122,1),
	(176,122,2),
	(177,123,1),
	(178,124,1),
	(179,124,2),
	(180,124,3),
	(181,125,1),
	(182,125,2),
	(183,125,3),
	(184,126,1),
	(185,126,2),
	(186,126,3),
	(187,127,1),
	(188,128,1),
	(189,129,1),
	(190,130,1);

/*!40000 ALTER TABLE `clip_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clips` WRITE;
/*!40000 ALTER TABLE `clips` DISABLE KEYS */;

INSERT INTO `clips` (`clip_id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `date_created`, `kind`)
VALUES
	(1,'','','','','','','','','',1430471995,'instacording'),
	(2,'','','','','','','','','',1430474131,'instacording'),
	(3,'','','','','','','','','',1430492058,'instacording'),
	(4,'','','','','','','','','',1430494767,'instacording'),
	(5,'','','','','','','','','',1430494897,'instacording'),
	(6,'','','','','','','','','',1430496383,'instacording'),
	(7,'','','','','','','','','',1430731467,'instacording'),
	(8,'','','','','','','','','',1430733081,'instacording'),
	(9,'','','','','','','','','',1430733133,'instacording'),
	(10,'','','','','','','','','',1430743134,'instacording'),
	(11,'','','','','','','','','',1430749290,'instacording'),
	(12,'','','','','','','','','',1430750015,'instacording'),
	(13,'','','','','','','','','',1430750358,'instacording'),
	(14,'','','','','','','','','',1430750455,'instacording'),
	(15,'','','','','','','','','',1430750521,'instacording'),
	(16,'','','','','','','','','',1430774639,'instacording'),
	(17,'','','','','','','','','',1430818303,'instacording'),
	(18,'','','','','','','','','',1430818603,'instacording'),
	(19,'','','','','','','','','',1430819976,'instacording'),
	(20,'','','','','','','','','',1430824510,'instacording'),
	(21,'','','','','','','','','',1430827797,'instacording'),
	(22,'','','','','','','','','',1430827817,'instacording'),
	(23,'','','','','','','','','',1430828696,'instacording'),
	(24,'','','','','','','','','',1430831258,'instacording'),
	(25,'','','','','','','','','',1430831779,'instacording'),
	(26,'','','','','','','','','',1430832259,'instacording'),
	(27,'','','','','','','','','',1430832300,'instacording'),
	(28,'','','','','','','','','',1430832699,'instacording'),
	(29,'','','','','','','','','',1430833350,'instacording'),
	(30,'','','','','','','','','',1430833875,'instacording'),
	(31,'','','','','','','','','',1430838005,'instacording'),
	(32,'','','','','','','','','',1430839042,'instacording'),
	(33,'','','','','','','','','',1430840647,'instacording'),
	(54,'','','','','','','','','',1432627831,'instacording'),
	(35,'','','','','','','','','',1430843452,'instacording'),
	(53,'','','','','','','','','',1432627647,'instacording'),
	(52,'','','','','','','','','',1432627549,'instacording'),
	(38,'','','','','','','','','',1430845933,'instacording'),
	(51,'','','','','','','','','',1432627548,'instacording'),
	(50,'','','','','','','','','',1432627260,'instacording'),
	(49,'','','','','','','','','',1432627260,'instacording'),
	(42,'','','','','','','','','',1432110172,'instacording'),
	(43,'','','','','','','','','',1432110636,'instacording'),
	(44,'','','','','','','','','',1432114501,'instacording'),
	(48,'','','','','','','','','',1432625371,'instacording'),
	(47,'','','','','','','','','',1432201777,'instacording'),
	(55,'','','','','','','','','',1432627832,'instacording'),
	(150,'','','','','','','','','',1434096348,'instacording'),
	(149,'','','','','','','','','',1434096346,'instacording'),
	(151,'','','','','','','','','',1434096656,'instacording'),
	(148,'','','','','','','','','',1434095356,'instacording'),
	(147,'','','','','','','','','',1434095355,'instacording'),
	(62,'','','','','','','','','',1433251850,'instacording'),
	(63,'','','','','','','','','',1433251977,'instacording'),
	(64,'','','','','','','','','',1433252158,'instacording'),
	(65,'','','','','','','','','',1433340910,'instacording'),
	(66,'','','','','','','','','',1433340987,'instacording'),
	(67,'','','','','','','','','',1433345414,'instacording'),
	(68,'','','','','','','','','',1433358665,'instacording'),
	(69,'','','','','','','','','',1433358753,'instacording'),
	(70,'','','','','','','','','',1433358933,'instacording'),
	(71,'','','','','','','','','',1433358934,'instacording'),
	(72,'','','','','','','','','',1433406846,'instacording'),
	(137,'','','','','','','','','',1434019240,'instacording'),
	(146,'','','','','','','','','',1434029743,'instacording'),
	(145,'','','','','','','','','',1434020210,'instacording'),
	(144,'','','','','','','','','',1434020209,'instacording'),
	(143,'','','','','','','','','',1434020124,'instacording'),
	(140,'','','','','','','','','',1434019901,'instacording'),
	(139,'','','','','','','','','',1434019900,'instacording'),
	(138,'','','','','','','','','',1434019899,'instacording'),
	(154,'','','','','','','','','',1434096797,'instacording'),
	(153,'','','','','','','','','',1434096797,'instacording'),
	(152,'','','','','','','','','',1434096657,'instacording'),
	(142,'','','','','','','','','',1434020123,'instacording'),
	(141,'','','','','','','','','',1434020122,'instacording'),
	(136,'','','','','','','','','',1434019239,'instacording'),
	(135,'','','','','','','','','',1434019238,'instacording'),
	(134,'','','','','','','','','',1434018975,'instacording'),
	(133,'','','','','','','','','',1434018974,'instacording'),
	(132,'','','','','','','','','',1434018973,'instacording'),
	(131,'','','','','','','','','',1434018708,'instacording'),
	(130,'','','','','','','','','',1434018707,'instacording'),
	(129,'','','','','','','','','',1434017999,'instacording'),
	(96,'','','','','','','','','',1433756752,'instacording'),
	(128,'','','','','','','','','',1434017998,'instacording'),
	(127,'','','','','','','','','',1434017693,'instacording'),
	(126,'','','','','','','','','',1434017692,'instacording'),
	(125,'','','','','','','','','',1434017294,'instacording'),
	(102,'','','','','','','','','',1433844190,'instacording'),
	(103,'','','','','','','','','',1433844191,'instacording'),
	(104,'','','','','','','','','',1433844192,'instacording'),
	(105,'','','','','','','','','',1433849664,'instacording'),
	(106,'','','','','','','','','',1433849666,'instacording'),
	(107,'','','','','','','','','',1433849667,'instacording'),
	(124,'','','','','','','','','',1434017293,'instacording'),
	(123,'','','','','','','','','',1434016709,'instacording'),
	(122,'','','','','','','','','',1434015809,'instacording'),
	(121,'','','','','','','','','',1434015278,'instacording'),
	(120,'','','','','','','','','',1434012587,'instacording'),
	(119,'','','','','','','','','',1434012585,'instacording'),
	(115,'','','','','','','','','',1433936818,'instacording'),
	(116,'','','','','','','','','',1434008309,'instacording'),
	(117,'','','','','','','','','',1434008932,'instacording'),
	(118,'','','','','','','','','',1434008933,'instacording'),
	(155,'','','','','','','','','',1434096865,'instacording'),
	(156,'','','','','','','','','',1434096867,'instacording'),
	(157,'','','','','','','','','',1434096867,'instacording'),
	(158,'','','','','','','','','',1434098099,'instacording'),
	(159,'','','','','','','','','',1434098100,'instacording'),
	(160,'','','','','','','','','',1434098101,'instacording'),
	(161,'','','','','','','','','',1434098907,'instacording'),
	(162,'','','','','','','','','',1434098908,'instacording'),
	(163,'','','','','','','','','',1434099219,'instacording'),
	(164,'','','','','','','','','',1434099220,'instacording'),
	(165,'','','','','','','','','',1434099221,'instacording'),
	(166,'','','','','','','','','',1434099707,'instacording'),
	(167,'','','','','','','','','',1434099708,'instacording'),
	(168,'','','','','','','','','',1434099709,'instacording'),
	(169,'','','','','','','','','',1434100935,'instacording'),
	(170,'','','','','','','','','',1434100936,'instacording'),
	(171,'','','','','','','','','',1434100936,'instacording'),
	(172,'','','','','','','','','',1434101735,'instacording'),
	(173,'','','','','','','','','',1434101735,'instacording'),
	(174,'','','','','','','','','',1434101736,'instacording'),
	(175,'','','','','','','','','',1434102449,'instacording'),
	(176,'','','','','','','','','',1434102450,'instacording'),
	(177,'','','','','','','','','',1434102503,'instacording'),
	(178,'','','','','','','','','',1434104607,'instacording'),
	(179,'','','','','','','','','',1434104608,'instacording'),
	(180,'','','','','','','','','',1434104609,'instacording'),
	(181,'','','','','','','','','',1434104649,'instacording'),
	(182,'','','','','','','','','',1434104650,'instacording'),
	(183,'','','','','','','','','',1434104651,'instacording'),
	(184,'','','','','','','','','',1434105058,'instacording'),
	(185,'','','','','','','','','',1434105059,'instacording'),
	(186,'','','','','','','','','',1434105060,'instacording'),
	(187,'','','','','','','','','',1435648761,'instacording'),
	(188,'','','','','','','','','',1435653320,'instacording'),
	(189,'','','','','','','','','',1435662903,'instacording'),
	(190,'','','','','','','','','',1435671444,'instacording');

/*!40000 ALTER TABLE `clips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `creator_to_event` WRITE;
/*!40000 ALTER TABLE `creator_to_event` DISABLE KEYS */;

INSERT INTO `creator_to_event` (`creator_id`, `event_id`)
VALUES
	(1,5),
	(1,4);

/*!40000 ALTER TABLE `creator_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `event_to_organisation` WRITE;
/*!40000 ALTER TABLE `event_to_organisation` DISABLE KEYS */;

INSERT INTO `event_to_organisation` (`event_id`, `organisation_id`)
VALUES
	(5,1),
	(4,1);

/*!40000 ALTER TABLE `event_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`event_id`, `event_name`, `event_description`, `date_created`)
VALUES
	(5,'PH Vught Test','Test with raspicams','1434094926'),
	(4,'Playmode testing','Simple playmode link added','1433936718');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table instacordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `instacordings`;

CREATE TABLE `instacordings` (
  `instacording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(200) NOT NULL DEFAULT '0',
  `camera_identifier` varchar(30) NOT NULL DEFAULT '0' COMMENT 'matches the cam_constante from cameras table',
  `event_id` int(11) unsigned NOT NULL DEFAULT '0',
  `part_id` int(11) unsigned NOT NULL DEFAULT '0',
  `cam` int(11) NOT NULL DEFAULT '0',
  `cam_description` varchar(50) DEFAULT NULL,
  `session_start` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instacording_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_file_to_moment` WRITE;
/*!40000 ALTER TABLE `moment_file_to_moment` DISABLE KEYS */;

INSERT INTO `moment_file_to_moment` (`moment_file_id`, `moment_id`, `cam`)
VALUES
	(1,1,1),
	(2,2,1),
	(3,3,1),
	(4,4,1),
	(5,5,1),
	(6,6,1),
	(7,7,1),
	(8,8,1),
	(9,9,1),
	(10,10,1),
	(11,11,1),
	(12,12,1),
	(13,13,1),
	(14,14,1),
	(15,15,1),
	(16,16,1),
	(17,17,1),
	(18,18,1),
	(19,19,1),
	(20,20,1),
	(21,21,1),
	(22,22,1),
	(23,23,1),
	(24,24,1),
	(25,25,1),
	(26,26,1),
	(27,27,1),
	(28,28,1),
	(29,29,1),
	(30,30,1),
	(31,31,1),
	(32,32,1),
	(33,33,1),
	(34,34,1),
	(35,35,1),
	(36,36,1),
	(37,37,1),
	(38,38,1),
	(39,39,1),
	(40,40,1),
	(41,41,1),
	(42,42,1),
	(43,43,1),
	(44,44,1),
	(45,45,1),
	(46,46,1),
	(47,47,1),
	(48,48,1),
	(49,49,1),
	(50,50,1),
	(51,51,1),
	(52,52,1),
	(53,53,1),
	(54,54,1),
	(55,55,1),
	(56,56,1),
	(57,57,1),
	(58,58,1),
	(59,59,1),
	(60,60,1),
	(61,61,1),
	(62,62,1),
	(63,63,1),
	(64,64,1),
	(65,65,1),
	(66,66,1),
	(67,67,1),
	(68,68,1),
	(69,69,1),
	(70,70,1),
	(71,71,1),
	(72,72,1),
	(73,73,1),
	(74,74,1),
	(75,75,1),
	(76,76,1),
	(77,77,1),
	(78,78,1),
	(79,79,1),
	(80,80,1),
	(81,81,1),
	(82,82,1),
	(83,83,1),
	(84,84,1),
	(85,85,1),
	(86,86,1),
	(87,87,1),
	(88,88,1),
	(89,89,1),
	(90,90,1),
	(91,91,1),
	(92,92,1),
	(93,93,1),
	(94,94,1),
	(95,95,1),
	(96,96,1),
	(97,97,1),
	(98,98,1),
	(99,99,1),
	(100,100,1),
	(101,101,1),
	(102,102,1),
	(103,103,1),
	(104,104,1),
	(105,105,1),
	(106,106,1),
	(107,107,1),
	(108,108,1),
	(109,109,1),
	(110,110,1),
	(111,111,1),
	(112,112,1),
	(113,113,1),
	(114,114,1),
	(115,115,1),
	(116,116,1),
	(117,117,1),
	(118,118,1),
	(119,119,1),
	(120,120,1),
	(121,121,1),
	(122,122,1),
	(123,123,1),
	(124,124,1),
	(125,125,1),
	(126,126,1),
	(127,127,1),
	(128,128,1),
	(129,129,1),
	(130,130,1),
	(131,131,1),
	(132,132,1),
	(133,133,1),
	(134,134,1),
	(137,137,1),
	(138,138,1),
	(139,139,1),
	(140,140,1),
	(141,141,1),
	(142,142,1),
	(143,143,1),
	(144,144,1),
	(145,145,1),
	(146,146,1),
	(147,147,1),
	(148,148,1),
	(160,160,1),
	(161,161,1),
	(162,162,1),
	(225,225,1),
	(164,164,1),
	(226,226,1),
	(166,166,1),
	(167,167,1),
	(168,168,1),
	(169,169,1),
	(170,170,1),
	(171,171,1),
	(172,172,1),
	(173,173,1),
	(174,174,1),
	(175,175,1),
	(176,176,1),
	(177,177,1),
	(178,178,1),
	(179,179,1),
	(180,180,1),
	(181,181,1),
	(182,182,1),
	(224,224,1),
	(184,184,1),
	(191,191,1),
	(192,192,1),
	(193,193,1),
	(194,194,1),
	(195,195,1),
	(196,196,1),
	(197,197,1),
	(277,272,2),
	(276,272,1),
	(275,271,1),
	(274,270,2),
	(273,270,1),
	(272,269,2),
	(271,269,1),
	(270,268,2),
	(269,268,1),
	(268,267,2),
	(267,267,1),
	(265,265,1),
	(264,264,1),
	(263,263,1),
	(262,262,1),
	(266,266,1),
	(260,260,1),
	(259,259,1),
	(258,258,1),
	(257,257,1),
	(256,256,1),
	(255,255,1),
	(254,254,1),
	(227,227,1),
	(251,251,1),
	(228,228,1),
	(229,229,1),
	(230,230,1),
	(250,250,1),
	(249,249,1),
	(248,248,1),
	(247,247,1),
	(246,245,1),
	(245,245,1),
	(317,295,1),
	(316,294,1),
	(314,292,1),
	(318,296,1),
	(319,297,1),
	(320,298,1),
	(321,299,1),
	(322,300,1),
	(323,301,1),
	(324,302,1),
	(325,303,1),
	(326,303,1),
	(327,304,1),
	(328,304,1),
	(329,305,1),
	(330,305,1),
	(331,306,1),
	(332,306,1),
	(333,307,1),
	(334,307,1),
	(335,308,1),
	(336,308,1),
	(337,309,1),
	(338,309,1),
	(339,310,1),
	(340,310,1),
	(341,311,1),
	(342,311,1),
	(343,312,1),
	(344,312,1),
	(345,313,1),
	(346,314,1),
	(347,315,1),
	(348,316,1),
	(349,317,1),
	(350,318,1),
	(351,319,1),
	(352,320,1),
	(353,321,1),
	(354,322,1),
	(355,323,1),
	(356,324,1),
	(357,325,1),
	(358,326,1),
	(359,327,1),
	(360,328,1),
	(361,329,1),
	(805,569,1),
	(363,331,1),
	(364,332,1),
	(365,333,1),
	(367,335,1),
	(368,336,1),
	(369,337,1),
	(869,597,1),
	(868,596,1),
	(867,595,1),
	(866,594,1),
	(865,593,1),
	(862,590,1),
	(860,588,3),
	(859,588,2),
	(858,588,1),
	(857,587,3),
	(856,587,2),
	(855,587,1),
	(854,586,3),
	(853,586,2),
	(852,586,1),
	(851,585,3),
	(850,585,2),
	(849,585,1),
	(848,584,3),
	(847,584,2),
	(846,584,1),
	(845,583,1),
	(844,582,2),
	(843,582,1),
	(842,581,3),
	(841,581,2),
	(840,581,1),
	(839,580,3),
	(838,580,2),
	(837,580,1),
	(836,579,3),
	(835,579,2),
	(834,579,1),
	(833,578,3),
	(832,578,2),
	(831,578,1),
	(830,577,3),
	(829,577,2),
	(828,577,1),
	(827,576,3),
	(826,576,2),
	(825,576,1),
	(824,575,3),
	(823,575,2),
	(822,575,1),
	(821,574,3),
	(820,574,2),
	(815,572,3),
	(814,572,2),
	(813,572,1),
	(812,571,3),
	(811,571,2),
	(810,571,1),
	(809,570,3),
	(808,570,2),
	(807,570,1),
	(806,569,2),
	(819,574,1),
	(818,573,3),
	(817,573,2),
	(816,573,1),
	(804,568,3),
	(803,568,2),
	(802,568,1),
	(801,567,3),
	(800,567,2),
	(799,567,1),
	(798,566,3),
	(797,566,2),
	(796,566,1),
	(795,565,3),
	(794,565,2),
	(793,565,1),
	(792,564,3),
	(791,564,2),
	(790,564,1),
	(789,563,3),
	(788,563,2),
	(787,563,1),
	(786,562,3),
	(785,562,2),
	(784,562,1),
	(783,561,3),
	(782,561,2),
	(781,561,1),
	(564,442,1),
	(565,443,1),
	(566,444,1),
	(567,445,1),
	(568,446,1),
	(569,447,1),
	(570,448,1),
	(571,449,1),
	(572,450,1),
	(573,451,1),
	(574,452,1),
	(575,453,1),
	(576,454,1),
	(577,455,1),
	(779,560,2),
	(778,560,1),
	(777,559,3),
	(776,559,2),
	(775,559,1),
	(774,558,3),
	(773,558,2),
	(772,558,1),
	(771,557,3),
	(770,557,2),
	(769,557,1),
	(768,556,3),
	(767,556,2),
	(766,556,1),
	(765,555,3),
	(764,555,2),
	(763,555,1),
	(762,554,3),
	(759,553,2),
	(780,560,3),
	(758,553,1),
	(761,554,2),
	(760,554,1),
	(755,551,1),
	(754,550,2),
	(753,550,1),
	(752,549,2),
	(751,549,1),
	(750,548,2),
	(749,548,1),
	(748,547,2),
	(747,547,1),
	(746,546,3),
	(745,546,2),
	(744,546,1),
	(743,545,3),
	(742,545,2),
	(741,545,1),
	(740,544,1),
	(739,543,3),
	(738,543,2),
	(737,543,1),
	(736,542,3),
	(735,542,2),
	(734,542,1),
	(733,541,3),
	(732,541,2),
	(731,541,1),
	(730,540,3),
	(729,540,2),
	(728,540,1),
	(727,539,3),
	(726,539,2),
	(725,539,1),
	(724,538,3),
	(723,538,2),
	(722,538,1),
	(721,537,3),
	(720,537,2),
	(719,537,1),
	(718,536,2),
	(717,536,1),
	(716,535,2),
	(715,535,1),
	(714,534,2),
	(713,534,1),
	(712,533,2),
	(711,533,1),
	(710,532,2),
	(709,532,1),
	(708,531,2),
	(652,496,1),
	(653,496,2),
	(654,496,3),
	(703,529,1),
	(702,528,2),
	(701,528,1),
	(864,592,1),
	(863,591,1),
	(698,526,1),
	(697,525,1),
	(696,524,1),
	(695,523,1),
	(694,522,1),
	(693,521,1),
	(692,520,1),
	(707,531,1),
	(706,530,2),
	(705,530,1),
	(704,529,2),
	(672,506,1),
	(673,507,1),
	(674,508,1),
	(675,509,1),
	(690,518,2),
	(677,511,1),
	(678,512,1),
	(689,518,1),
	(688,517,2),
	(687,517,1);

/*!40000 ALTER TABLE `moment_file_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_description` varchar(256) NOT NULL DEFAULT '',
  `started_timestamp` bigint(20) DEFAULT NULL,
  `is_uploaded` tinyint(1) NOT NULL DEFAULT '1',
  `fps` int(11) DEFAULT NULL,
  PRIMARY KEY (`moment_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_files` WRITE;
/*!40000 ALTER TABLE `moment_files` DISABLE KEYS */;

INSERT INTO `moment_files` (`moment_file_id`, `filename`, `path`, `cam`, `cam_description`, `started_timestamp`, `is_uploaded`, `fps`)
VALUES
	(1,'1.mp4','videos/1/1/1',1,'TestCam 1',NULL,1,NULL),
	(2,'2.mp4','videos/1/1/2',1,'TestCam 1',NULL,1,NULL),
	(3,'3.mp4','videos/1/1/3',1,'TestCam 1',NULL,1,NULL),
	(4,'4.mp4','videos/1/1/4',1,'TestCam 1',NULL,1,NULL),
	(5,'5.mp4','videos/1/1/5',1,'TestCam 1',NULL,1,NULL),
	(6,'6.mp4','videos/1/2/6',1,'TestCam 1',NULL,1,NULL),
	(7,'7.mp4','videos/1/2/7',1,'TestCam 1',NULL,1,NULL),
	(8,'8.mp4','videos/1/2/8',1,'TestCam 1',NULL,1,NULL),
	(9,'9.mp4','videos/1/2/9',1,'TestCam 1',NULL,1,NULL),
	(10,'10.mp4','videos/1/2/10',1,'TestCam 1',NULL,1,NULL),
	(11,'11.mp4','videos/1/2/11',1,'TestCam 1',NULL,1,NULL),
	(12,'12.mp4','videos/1/2/12',1,'TestCam 1',NULL,1,NULL),
	(13,'13.mp4','videos/1/2/13',1,'TestCam 1',NULL,1,NULL),
	(14,'14.mp4','videos/1/3/14',1,'TestCam 1',NULL,1,NULL),
	(15,'15.mp4','videos/1/3/15',1,'TestCam 1',NULL,1,NULL),
	(16,'16.mp4','videos/1/3/16',1,'TestCam 1',NULL,1,NULL),
	(17,'17.mp4','videos/1/3/17',1,'TestCam 1',NULL,1,NULL),
	(18,'18.mp4','videos/1/3/18',1,'TestCam 1',NULL,1,NULL),
	(19,'19.mp4','videos/1/3/19',1,'TestCam 1',NULL,1,NULL),
	(20,'20.mp4','videos/1/3/20',1,'TestCam 1',NULL,1,NULL),
	(21,'21.mp4','videos/1/4/21',1,'TestCam 1',NULL,1,NULL),
	(22,'22.mp4','videos/1/4/22',1,'TestCam 1',NULL,1,NULL),
	(23,'23.mp4','videos/1/4/23',1,'TestCam 1',NULL,1,NULL),
	(24,'24.mp4','videos/1/4/24',1,'TestCam 1',NULL,1,NULL),
	(25,'25.mp4','videos/1/4/25',1,'TestCam 1',NULL,1,NULL),
	(26,'26.mp4','videos/1/5/26',1,'TestCam 1',NULL,1,NULL),
	(27,'27.mp4','videos/1/5/27',1,'TestCam 1',NULL,1,NULL),
	(28,'28.mp4','videos/1/5/28',1,'TestCam 1',NULL,1,NULL),
	(29,'29.mp4','videos/1/5/29',1,'TestCam 1',NULL,1,NULL),
	(30,'30.mp4','videos/1/5/30',1,'TestCam 1',NULL,1,NULL),
	(31,'31.mp4','videos/1/5/31',1,'TestCam 1',NULL,1,NULL),
	(32,'32.mp4','videos/1/5/32',1,'TestCam 1',NULL,1,NULL),
	(33,'33.mp4','videos/1/5/33',1,'TestCam 1',NULL,1,NULL),
	(34,'34.mp4','videos/1/6/34',1,'TestCam 1',NULL,1,NULL),
	(35,'35.mp4','videos/1/6/35',1,'TestCam 1',NULL,1,NULL),
	(36,'36.mp4','videos/1/6/36',1,'TestCam 1',NULL,1,NULL),
	(37,'37.mp4','videos/1/7/37',1,'TestCam 1',NULL,1,NULL),
	(38,'38.mp4','videos/1/7/38',1,'TestCam 1',NULL,1,NULL),
	(39,'39.mp4','videos/1/7/39',1,'TestCam 1',NULL,1,NULL),
	(40,'40.mp4','videos/1/7/40',1,'TestCam 1',NULL,1,NULL),
	(41,'41.mp4','videos/1/7/41',1,'TestCam 1',NULL,1,NULL),
	(42,'42.mp4','videos/1/7/42',1,'TestCam 1',NULL,1,NULL),
	(43,'43.mp4','videos/1/7/43',1,'TestCam 1',NULL,1,NULL),
	(44,'44.mp4','videos/1/7/44',1,'TestCam 1',NULL,1,NULL),
	(45,'45.mp4','videos/1/9/45',1,'TestCam 1',NULL,1,NULL),
	(46,'46.mp4','videos/1/10/46',1,'TestCam 1',NULL,1,NULL),
	(47,'47.mp4','videos/1/10/47',1,'TestCam 1',NULL,1,NULL),
	(48,'48.mp4','videos/1/10/48',1,'TestCam 1',NULL,1,NULL),
	(49,'49.mp4','videos/1/10/49',1,'TestCam 1',NULL,1,NULL),
	(50,'50.mp4','videos/1/10/50',1,'TestCam 1',NULL,1,NULL),
	(51,'51.mp4','videos/1/10/51',1,'TestCam 1',NULL,1,NULL),
	(52,'52.mp4','videos/1/10/52',1,'TestCam 1',NULL,1,NULL),
	(53,'53.mp4','videos/1/10/53',1,'TestCam 1',NULL,1,NULL),
	(54,'54.mp4','videos/1/10/54',1,'TestCam 1',NULL,1,NULL),
	(55,'55.mp4','videos/1/10/55',1,'TestCam 1',NULL,1,NULL),
	(56,'56.mp4','videos/1/10/56',1,'TestCam 1',NULL,1,NULL),
	(57,'57.mp4','videos/1/13/57',1,'TestCam 1',NULL,1,NULL),
	(58,'58.mp4','videos/1/13/58',1,'TestCam 1',NULL,1,NULL),
	(59,'59.mp4','videos/1/17/59',1,'TestCam 1',NULL,1,NULL),
	(60,'60.mp4','videos/1/17/60',1,'TestCam 1',NULL,1,NULL),
	(61,'61.mp4','videos/1/17/61',1,'TestCam 1',NULL,1,NULL),
	(62,'62.mp4','videos/1/17/62',1,'TestCam 1',NULL,1,NULL),
	(63,'63.mp4','videos/1/17/63',1,'TestCam 1',NULL,1,NULL),
	(64,'64.mp4','videos/1/17/64',1,'TestCam 1',NULL,1,NULL),
	(65,'65.mp4','videos/1/17/65',1,'TestCam 1',NULL,1,NULL),
	(66,'66.mp4','videos/1/17/66',1,'TestCam 1',NULL,1,NULL),
	(67,'67.mp4','videos/1/17/67',1,'TestCam 1',NULL,1,NULL),
	(68,'68.mp4','videos/1/17/68',1,'TestCam 1',NULL,1,NULL),
	(69,'69.mp4','videos/1/17/69',1,'TestCam 1',NULL,1,NULL),
	(70,'70.mp4','videos/1/17/70',1,'TestCam 1',NULL,1,NULL),
	(71,'71.mp4','videos/1/17/71',1,'TestCam 1',NULL,1,NULL),
	(72,'72.mp4','videos/1/17/72',1,'TestCam 1',NULL,1,NULL),
	(73,'73.mp4','videos/1/17/73',1,'TestCam 1',NULL,1,NULL),
	(74,'74.mp4','videos/1/17/74',1,'TestCam 1',NULL,1,NULL),
	(75,'75.mp4','videos/1/17/75',1,'TestCam 1',NULL,1,NULL),
	(76,'76.mp4','videos/1/17/76',1,'TestCam 1',NULL,1,NULL),
	(77,'77.mp4','videos/1/17/77',1,'TestCam 1',NULL,1,NULL),
	(78,'78.mp4','videos/1/18/78',1,'TestCam Local',NULL,1,NULL),
	(79,'79.mp4','videos/1/18/79',1,'TestCam Local',NULL,1,NULL),
	(80,'80.mp4','videos/1/19/80',1,'TestCam 1',NULL,1,NULL),
	(81,'81.mp4','videos/1/19/81',1,'TestCam 1',NULL,1,NULL),
	(82,'82.mp4','videos/1/20/82',1,'TestCam 1',NULL,1,NULL),
	(83,'83.mp4','videos/1/21/83',1,'TestCam 1',NULL,1,NULL),
	(84,'84.mp4','videos/1/21/84',1,'TestCam 1',NULL,1,NULL),
	(85,'85.mp4','videos/1/21/85',1,'TestCam 1',NULL,1,NULL),
	(86,'86.mp4','videos/1/21/86',1,'TestCam 1',NULL,1,NULL),
	(87,'87.mp4','videos/1/21/87',1,'TestCam 1',NULL,1,NULL),
	(88,'88.mp4','videos/1/21/88',1,'TestCam 1',NULL,1,NULL),
	(89,'89.mp4','videos/1/21/89',1,'TestCam 1',NULL,1,NULL),
	(90,'90.mp4','videos/1/21/90',1,'TestCam 1',NULL,1,NULL),
	(91,'91.mp4','videos/1/21/91',1,'TestCam 1',NULL,1,NULL),
	(92,'92.mp4','videos/1/21/92',1,'TestCam 1',NULL,1,NULL),
	(93,'93.mp4','videos/1/21/93',1,'TestCam 1',NULL,1,NULL),
	(94,'94.mp4','videos/1/22/94',1,'TestCam 1',NULL,1,NULL),
	(95,'95.mp4','videos/1/24/95',1,'TestCam 1',NULL,1,NULL),
	(96,'96.mp4','videos/1/24/96',1,'TestCam 1',NULL,1,NULL),
	(97,'97.mp4','videos/1/24/97',1,'TestCam 1',NULL,1,NULL),
	(98,'98.mp4','videos/1/24/98',1,'TestCam 1',NULL,1,NULL),
	(99,'99.mp4','videos/1/24/99',1,'TestCam 1',NULL,1,NULL),
	(100,'100.mp4','videos/1/24/100',1,'TestCam 1',NULL,1,NULL),
	(101,'101.mp4','videos/1/24/101',1,'TestCam 1',NULL,1,NULL),
	(102,'102.mp4','videos/1/25/102',1,'TestCam 1',NULL,1,NULL),
	(103,'103.mp4','videos/1/25/103',1,'TestCam 1',NULL,1,NULL),
	(104,'104.mp4','./videos/1/25/104',1,'TestCam 1',NULL,1,NULL),
	(105,'105.mp4','./videos/1/25/105',1,'TestCam 1',NULL,1,NULL),
	(106,'106.mp4','./videos/1/25/106',1,'TestCam 1',NULL,1,NULL),
	(107,'107.mp4','./videos/1/25/107',1,'TestCam 1',NULL,1,NULL),
	(108,'108.mp4','./videos/1/25/108',1,'TestCam 1',NULL,1,NULL),
	(109,'109.mp4','./videos/1/25/109',1,'TestCam 1',NULL,1,NULL),
	(110,'110.mp4','./videos/1/25/110',1,'TestCam 1',NULL,1,NULL),
	(111,'111.mp4','./videos/1/26/111',1,'TestCam 1',NULL,1,NULL),
	(112,'112.mp4','./videos/1/26/112',1,'TestCam 1',NULL,1,NULL),
	(113,'113.mp4','./videos/1/26/113',1,'TestCam 1',NULL,1,NULL),
	(114,'114.mp4','./videos/1/26/114',1,'TestCam 1',NULL,1,NULL),
	(115,'115.mp4','./videos/1/26/115',1,'TestCam 1',NULL,1,NULL),
	(116,'116.mp4','./videos/1/26/116',1,'TestCam 1',NULL,1,NULL),
	(117,'117.mp4','./videos/1/26/117',1,'TestCam 1',NULL,1,NULL),
	(118,'118.mp4','./videos/1/27/118',1,'TestCam 1',NULL,1,NULL),
	(119,'119.mp4','./videos/1/27/119',1,'TestCam 1',NULL,1,NULL),
	(120,'120.mp4','./videos/1/27/120',1,'TestCam 1',NULL,1,NULL),
	(121,'121.mp4','./videos/1/27/121',1,'TestCam 1',NULL,1,NULL),
	(122,'122.mp4','./videos/1/27/122',1,'TestCam 1',NULL,1,NULL),
	(123,'123.mp4','./videos/1/28/123',1,'TestCam 1',NULL,1,NULL),
	(124,'124.mp4','./videos/1/29/124',1,'TestCam 1',NULL,1,NULL),
	(125,'125.mp4','./videos/1/30/125',1,'TestCam 1',NULL,1,NULL),
	(126,'126.mp4','./videos/1/30/126',1,'TestCam 1',NULL,1,NULL),
	(127,'127.mp4','./videos/1/30/127',1,'TestCam 1',NULL,1,NULL),
	(128,'128.mp4','./videos/1/30/128',1,'TestCam 1',NULL,1,NULL),
	(129,'129.mp4','./videos/1/30/129',1,'TestCam 1',NULL,1,NULL),
	(130,'130.mp4','./videos/1/31/130',1,'TestCam 1',NULL,1,NULL),
	(131,'131.mp4','./videos/1/32/131',1,'TestCam 1',NULL,1,NULL),
	(132,'132.mp4','./videos/1/33/132',1,'TestCam 1',NULL,1,NULL),
	(133,'133.mp4','./videos/1/33/133',1,'TestCam 1',NULL,1,NULL),
	(134,'134.mp4','./videos/1/33/134',1,'TestCam 1',NULL,1,NULL),
	(137,'137.mp4','./videos/1/34/137',1,'TestCam 1',NULL,1,NULL),
	(138,'138.mp4','./videos/1/34/138',1,'TestCam 1',NULL,1,NULL),
	(139,'139.mp4','./videos/1/34/139',1,'TestCam 1',NULL,1,NULL),
	(140,'140.mp4','./videos/1/34/140',1,'TestCam 1',NULL,1,NULL),
	(141,'141.mp4','./videos/1/34/141',1,'TestCam 1',NULL,1,NULL),
	(142,'142.mp4','./videos/1/35/142',1,'TestCam 1',NULL,1,NULL),
	(143,'143.mp4','./videos/1/35/143',1,'TestCam 1',NULL,1,NULL),
	(144,'144.mp4','./videos/1/35/144',1,'TestCam 1',NULL,1,NULL),
	(145,'145.mp4','./videos/1/35/145',1,'TestCam 1',NULL,1,NULL),
	(146,'146.mp4','./videos/1/35/146',1,'TestCam 1',NULL,1,NULL),
	(147,'147.mp4','./videos/1/35/147',1,'TestCam 1',NULL,1,NULL),
	(148,'148.mp4','./videos/1/35/148',1,'TestCam 1',NULL,1,NULL),
	(828,'828.mp4','./videos/5/120/577',1,'720p2',1434100963402,1,60),
	(829,'829.mp4','./videos/5/120/577',2,'720p1',1434100963451,1,60),
	(830,'830.mp4','./videos/5/120/577',3,'720p3',1434100963427,1,60),
	(831,'831.mp4','./videos/5/121/578',1,'1080p1',1434101746402,1,30),
	(832,'832.mp4','./videos/5/121/578',2,'1080p2',1434101746436,1,30),
	(160,'160.mp4','./videos/1/37/160',1,'TestCam 1',NULL,1,NULL),
	(161,'161.mp4','./videos/1/37/161',1,'TestCam 1',NULL,1,NULL),
	(162,'162.mp4','./videos/1/37/162',1,'TestCam 1',NULL,1,NULL),
	(225,'225.mp4','./videos/1/47/225',1,'TestCam 1',NULL,1,NULL),
	(164,'164.mp4','./videos/1/37/164',1,'TestCam 1',NULL,1,NULL),
	(226,'226.mp4','./videos/1/47/226',1,'TestCam 1',NULL,1,NULL),
	(166,'166.mp4','./videos/1/37/166',1,'TestCam 1',NULL,1,NULL),
	(167,'167.mp4','./videos/1/37/167',1,'TestCam 1',NULL,1,NULL),
	(168,'168.mp4','./videos/1/37/168',1,'TestCam 1',NULL,1,NULL),
	(169,'169.mp4','./videos/1/37/169',1,'TestCam 1',NULL,1,NULL),
	(170,'170.mp4','./videos/1/37/170',1,'TestCam 1',NULL,1,NULL),
	(171,'171.mp4','./videos/1/37/171',1,'TestCam 1',NULL,1,NULL),
	(172,'172.mp4','./videos/1/37/172',1,'TestCam 1',NULL,1,NULL),
	(173,'173.mp4','./videos/1/37/173',1,'TestCam 1',NULL,1,NULL),
	(174,'174.mp4','./videos/1/37/174',1,'TestCam 1',NULL,1,NULL),
	(175,'175.mp4','./videos/1/37/175',1,'TestCam 1',NULL,1,NULL),
	(176,'176.mp4','./videos/1/37/176',1,'TestCam 1',NULL,1,NULL),
	(177,'177.mp4','./videos/1/37/177',1,'TestCam 1',NULL,1,NULL),
	(178,'178.mp4','./videos/1/37/178',1,'TestCam 1',NULL,1,NULL),
	(179,'179.mp4','./videos/1/37/179',1,'TestCam 1',NULL,1,NULL),
	(180,'180.mp4','./videos/1/37/180',1,'TestCam 1',NULL,1,NULL),
	(181,'181.mp4','./videos/1/37/181',1,'TestCam 1',NULL,1,NULL),
	(182,'182.mp4','./videos/1/37/182',1,'TestCam 1',NULL,1,NULL),
	(224,'224.mp4','./videos/1/46/224',1,'TestCam 1',NULL,1,NULL),
	(184,'184.mp4','./videos/1/37/184',1,'TestCam 1',NULL,1,NULL),
	(841,'841.mp4','./videos/5/121/581',2,'1080p2',1434102216600,1,30),
	(840,'840.mp4','./videos/5/121/581',1,'1080p1',1434102216566,1,30),
	(191,'191.mp4','./videos/1/40/191',1,'TestCam 1',NULL,1,NULL),
	(192,'192.mp4','./videos/1/40/192',1,'TestCam 1',NULL,1,NULL),
	(193,'193.mp4','./videos/1/40/193',1,'TestCam 1',NULL,1,NULL),
	(194,'194.mp4','./videos/1/40/194',1,'TestCam 1',NULL,1,NULL),
	(195,'195.mp4','./videos/1/40/195',1,'TestCam 1',NULL,1,NULL),
	(196,'196.mp4','./videos/1/40/196',1,'TestCam 1',NULL,1,NULL),
	(197,'197.mp4','./videos/1/40/197',1,'TestCam 1',NULL,1,NULL),
	(277,'277.mp4','./videos/3/57/272/272',2,'TestCam 2',NULL,1,NULL),
	(276,'276.mp4','./videos/3/57/272',1,'TestCam 1',NULL,1,NULL),
	(275,'275.mp4','./videos/3/56/271',1,'TestCam 2',NULL,1,NULL),
	(274,'274.mp4','./videos/3/55/270/270',2,'TestCam 2',NULL,1,NULL),
	(273,'273.mp4','./videos/3/55/270',1,'TestCam 1',NULL,1,NULL),
	(272,'272.mp4','./videos/3/55/269/269',2,'TestCam 2',NULL,1,NULL),
	(271,'271.mp4','./videos/3/55/269',1,'TestCam 1',NULL,1,NULL),
	(270,'270.mp4','./videos/3/54/268/268',2,'TestCam 2',NULL,1,NULL),
	(269,'269.mp4','./videos/3/54/268',1,'TestCam 1',NULL,1,NULL),
	(268,'268.mp4','./videos/3/54/267/267',2,'TestCam 2',NULL,1,NULL),
	(267,'267.mp4','./videos/3/54/267',1,'TestCam 1',NULL,1,NULL),
	(265,'265.mp4','./videos/3/52/265',1,'TestCam 1',NULL,1,NULL),
	(264,'264.mp4','./videos/3/52/264',1,'TestCam 1',NULL,1,NULL),
	(263,'263.mp4','./videos/3/52/263',1,'TestCam 1',NULL,1,NULL),
	(262,'262.mp4','./videos/3/52/262',1,'TestCam 1',NULL,1,NULL),
	(266,'266.mp4','./videos/3/53/266',1,'TestCam 1',NULL,1,NULL),
	(260,'260.mp4','./videos/3/52/260',1,'TestCam 1',NULL,1,NULL),
	(259,'259.mp4','./videos/3/52/259',1,'TestCam 1',NULL,1,NULL),
	(258,'258.mp4','./videos/3/52/258',1,'TestCam 1',NULL,1,NULL),
	(227,'227.mp4','./videos/1/47/227',1,'TestCam 1',NULL,1,NULL),
	(257,'257.mp4','./videos/3/52/257',1,'TestCam 1',NULL,1,NULL),
	(228,'228.mp4','./videos/1/47/228',1,'TestCam 1',NULL,1,NULL),
	(229,'229.mp4','./videos/1/47/229',1,'TestCam 1',NULL,1,NULL),
	(230,'230.mp4','./videos/1/47/230',1,'TestCam 1',NULL,1,NULL),
	(833,'833.mp4','./videos/5/121/578',3,'1080p3',1434101746428,1,30),
	(250,'250.mp4','./videos/3/52/250',1,'TestCam 1',NULL,1,NULL),
	(251,'251.mp4','./videos/3/52/251',1,'TestCam 1',NULL,1,NULL),
	(254,'254.mp4','./videos/3/52/254',1,'TestCam 1',NULL,1,NULL),
	(255,'255.mp4','./videos/3/52/255',1,'TestCam 1',NULL,1,NULL),
	(256,'256.mp4','./videos/3/52/256',1,'TestCam 1',NULL,1,NULL),
	(248,'248.mp4','./videos/3/52/248',1,'TestCam 1',NULL,1,NULL),
	(249,'249.mp4','./videos/3/52/249',1,'TestCam 1',NULL,1,NULL),
	(247,'247.mp4','./videos/3/52/247',1,'TestCam 1',NULL,1,NULL),
	(246,'246.mp4','./videos/3/52/246',1,'TestCam 1',NULL,1,NULL),
	(245,'245.mp4','./videos/3/52/245',1,'TestCam 1',NULL,1,NULL),
	(839,'839.mp4','./videos/5/121/580',3,'1080p3',1434101950435,1,30),
	(834,'834.mp4','./videos/5/121/579',1,'1080p1',1434101788765,1,30),
	(835,'835.mp4','./videos/5/121/579',2,'1080p2',1434101788800,1,30),
	(836,'836.mp4','./videos/5/121/579',3,'1080p3',1434101788792,1,30),
	(837,'837.mp4','./videos/5/121/580',1,'1080p1',1434101950402,1,30),
	(838,'838.mp4','./videos/5/121/580',2,'1080p2',1434101950435,1,30),
	(317,'317.mp4','./videos/3/65/295',1,'TestCam 1',NULL,1,NULL),
	(316,'316.mp4','./videos/3/65/294',1,'TestCam 1',NULL,1,NULL),
	(314,'314.mp4','./videos/3/62/292',1,'TestCam 1',NULL,1,NULL),
	(318,'318.mp4','./videos/3/66/296',1,'TestCam 1',NULL,1,NULL),
	(319,'319.mp4','./videos/3/66/297',1,'TestCam 1',NULL,1,NULL),
	(320,'320.mp4','./videos/3/67/298',1,'TestCam 1',NULL,1,NULL),
	(321,'321.mp4','./videos/3/67/299',1,'TestCam 1',NULL,1,NULL),
	(322,'322.mp4','./videos/3/67/300',1,'TestCam 1',NULL,1,NULL),
	(323,'323.mp4','./videos/3/68/301',1,'TestCam 1',NULL,1,NULL),
	(324,'324.mp4','./videos/3/69/302',1,'TestCam 1',NULL,1,NULL),
	(325,'325.mp4','./videos/3/70/303',1,'TestCam 1',NULL,1,NULL),
	(326,'326.mp4','./videos/3/70/303',1,'TestCam 1',NULL,1,NULL),
	(327,'327.mp4','./videos/3/70/304',1,'TestCam 1',NULL,1,NULL),
	(328,'328.mp4','./videos/3/70/304',1,'TestCam 1',NULL,1,NULL),
	(329,'329.mp4','./videos/3/70/305',1,'TestCam 1',NULL,1,NULL),
	(330,'330.mp4','./videos/3/70/305',1,'TestCam 1',NULL,1,NULL),
	(331,'331.mp4','./videos/3/70/306',1,'TestCam 1',NULL,1,NULL),
	(332,'332.mp4','./videos/3/70/306',1,'TestCam 1',NULL,1,NULL),
	(333,'333.mp4','./videos/3/70/307',1,'TestCam 1',NULL,1,NULL),
	(334,'334.mp4','./videos/3/70/307',1,'TestCam 1',NULL,1,NULL),
	(335,'335.mp4','./videos/3/70/308',1,'TestCam 1',NULL,1,NULL),
	(336,'336.mp4','./videos/3/70/308',1,'TestCam 1',NULL,1,NULL),
	(337,'337.mp4','./videos/3/70/309',1,'TestCam 1',NULL,1,NULL),
	(338,'338.mp4','./videos/3/70/309',1,'TestCam 1',NULL,1,NULL),
	(339,'339.mp4','./videos/3/70/310',1,'TestCam 1',NULL,1,NULL),
	(340,'340.mp4','./videos/3/70/310',1,'TestCam 1',NULL,1,NULL),
	(341,'341.mp4','./videos/3/70/311',1,'TestCam 1',NULL,1,NULL),
	(342,'342.mp4','./videos/3/70/311',1,'TestCam 1',NULL,1,NULL),
	(343,'343.mp4','./videos/3/70/312',1,'TestCam 1',NULL,1,NULL),
	(344,'344.mp4','./videos/3/70/312',1,'TestCam 1',NULL,1,NULL),
	(345,'345.mp4','./videos/3/71/313',1,'TestCam 1',NULL,1,NULL),
	(346,'346.mp4','./videos/3/71/314',1,'TestCam 1',NULL,1,NULL),
	(347,'347.mp4','./videos/3/71/315',1,'TestCam 1',NULL,1,NULL),
	(348,'348.mp4','./videos/3/71/316',1,'TestCam 1',NULL,1,NULL),
	(349,'349.mp4','./videos/3/71/317',1,'TestCam 1',NULL,1,NULL),
	(350,'350.mp4','./videos/3/71/318',1,'TestCam 1',NULL,1,NULL),
	(351,'351.mp4','./videos/3/71/319',1,'TestCam 1',NULL,1,NULL),
	(352,'352.mp4','./videos/3/71/320',1,'TestCam 1',NULL,1,NULL),
	(353,'353.mp4','./videos/3/71/321',1,'TestCam 1',NULL,1,NULL),
	(354,'354.mp4','./videos/3/71/322',1,'TestCam 1',NULL,1,NULL),
	(355,'355.mp4','./videos/3/71/323',1,'TestCam 1',NULL,1,NULL),
	(356,'356.mp4','./videos/3/71/324',1,'TestCam 1',NULL,1,NULL),
	(357,'357.mp4','./videos/3/71/325',1,'TestCam 1',NULL,1,NULL),
	(358,'358.mp4','./videos/3/71/326',1,'TestCam 1',NULL,1,NULL),
	(359,'359.mp4','./videos/3/71/327',1,'TestCam 1',NULL,1,NULL),
	(360,'360.mp4','./videos/3/71/328',1,'TestCam 1',NULL,1,NULL),
	(361,'361.mp4','./videos/3/71/329',1,'TestCam 1',NULL,1,NULL),
	(363,'363.mp4','./videos/3/71/331',1,'TestCam 1',NULL,1,NULL),
	(364,'364.mp4','./videos/3/71/332',1,'TestCam 1',NULL,1,NULL),
	(365,'365.mp4','./videos/3/71/333',1,'TestCam 1',NULL,1,NULL),
	(367,'367.mp4','./videos/3/71/335',1,'TestCam 1',NULL,1,NULL),
	(368,'368.mp4','./videos/3/71/336',1,'TestCam 1',NULL,1,NULL),
	(369,'369.mp4','./videos/3/71/337',1,'TestCam 1',NULL,1,NULL),
	(827,'827.mp4','./videos/5/119/576',3,'TestCam 3',1434100651217,1,90),
	(826,'826.mp4','./videos/5/119/576',2,'TestCam 2',1434100651212,1,90),
	(825,'825.mp4','./videos/5/119/576',1,'TestCam 1',1434100651190,1,90),
	(824,'824.mp4','./videos/5/119/575',3,'TestCam 3',1434099727770,1,90),
	(823,'823.mp4','./videos/5/119/575',2,'TestCam 2',1434099727768,1,90),
	(822,'822.mp4','./videos/5/119/575',1,'TestCam 1',1434099727798,1,90),
	(821,'821.mp4','./videos/5/118/574',3,'720p3',1434099542047,1,60),
	(820,'820.mp4','./videos/5/118/574',2,'720p1',1434099542097,1,60),
	(819,'819.mp4','./videos/5/118/574',1,'720p2',1434099542018,1,60),
	(818,'818.mp4','./videos/5/118/573',3,'720p3',1434099511632,1,60),
	(817,'817.mp4','./videos/5/118/573',2,'720p1',1434099511683,1,60),
	(816,'816.mp4','./videos/5/118/573',1,'720p2',1434099511610,1,60),
	(815,'815.mp4','./videos/5/118/572',3,'720p3',1434099322581,1,60),
	(814,'814.mp4','./videos/5/118/572',2,'720p1',1434099322632,1,60),
	(813,'813.mp4','./videos/5/118/572',1,'720p2',1434099322555,1,60),
	(812,'812.mp4','./videos/5/118/571',3,'720p3',1434099281322,1,60),
	(811,'811.mp4','./videos/5/118/571',2,'720p1',1434099281371,1,60),
	(810,'810.mp4','./videos/5/118/571',1,'720p2',1434099281296,1,60),
	(809,'809.mp4','./videos/5/118/570',3,'720p3',1434099254795,1,60),
	(808,'808.mp4','./videos/5/118/570',2,'720p1',1434099254839,1,60),
	(807,'807.mp4','./videos/5/118/570',1,'720p2',1434099254769,1,60),
	(806,'806.mp4','./videos/5/117/569',2,'720p1',1434098917087,1,60),
	(805,'805.mp4','./videos/5/117/569',1,'720p2',1434098916729,1,60),
	(804,'804.mp4','./videos/5/116/568',3,'TestCam 3',1434098614475,1,90),
	(803,'803.mp4','./videos/5/116/568',2,'TestCam 2',1434098614445,1,90),
	(802,'802.mp4','./videos/5/116/568',1,'TestCam 1',1434098614859,1,90),
	(801,'801.mp4','./videos/5/116/567',3,'TestCam 3',1434098596890,1,90),
	(800,'800.mp4','./videos/5/116/567',2,'TestCam 2',1434098596860,1,90),
	(799,'799.mp4','./videos/5/116/567',1,'TestCam 1',1434098597278,1,90),
	(798,'798.mp4','./videos/5/116/566',3,'TestCam 3',1434098581748,1,90),
	(797,'797.mp4','./videos/5/116/566',2,'TestCam 2',1434098581723,1,90),
	(793,'793.mp4','./videos/5/116/565',1,'TestCam 1',1434098491933,1,90),
	(792,'792.mp4','./videos/5/116/564',3,'TestCam 3',1434098441048,1,90),
	(791,'791.mp4','./videos/5/116/564',2,'TestCam 2',1434098441026,1,90),
	(790,'790.mp4','./videos/5/116/564',1,'TestCam 1',1434098441389,1,90),
	(787,'787.mp4','./videos/5/116/563',1,'TestCam 1',1434098415819,1,90),
	(788,'788.mp4','./videos/5/116/563',2,'TestCam 2',1434098415458,1,90),
	(789,'789.mp4','./videos/5/116/563',3,'TestCam 3',1434098415481,1,90),
	(869,'869.mp4','videos/5/130/597',1,'TestCam 1',1434130063687,1,90),
	(868,'868.mp4','videos/5/130/596',1,'TestCam 1',1434130031432,1,90),
	(867,'867.mp4','videos/5/129/595',1,'TestCam 1',1434121524227,1,90),
	(866,'866.mp4','videos/5/129/594',1,'TestCam 1',1434121495991,1,90),
	(865,'865.mp4','videos/5/129/593',1,'TestCam 1',1434121489441,1,90),
	(862,'862.mp4','./videos/5/127/590',1,'TestCam 1',1434107434506,1,90),
	(860,'860.mp4','./videos/5/126/588',3,'TestCam 3',1434105316600,1,90),
	(859,'859.mp4','./videos/5/126/588',2,'TestCam 2',1434105316596,1,90),
	(858,'858.mp4','./videos/5/126/588',1,'TestCam 1',1434105316562,1,90),
	(857,'857.mp4','./videos/5/126/587',3,'TestCam 3',1434105224442,1,90),
	(856,'856.mp4','./videos/5/126/587',2,'TestCam 2',1434105224438,1,90),
	(855,'855.mp4','./videos/5/126/587',1,'TestCam 1',1434105224402,1,90),
	(854,'854.mp4','./videos/5/125/586',3,'TestCam 3',1434105030829,1,90),
	(853,'853.mp4','./videos/5/125/586',2,'TestCam 2',1434105030833,1,90),
	(852,'852.mp4','./videos/5/125/586',1,'TestCam 1',1434105030784,1,90),
	(851,'851.mp4','./videos/5/125/585',3,'TestCam 3',1434104898098,1,90),
	(850,'850.mp4','./videos/5/125/585',2,'TestCam 2',1434104898095,1,90),
	(849,'849.mp4','./videos/5/125/585',1,'TestCam 1',1434104898045,1,90),
	(848,'848.mp4','./videos/5/125/584',3,'TestCam 3',1434104819175,1,90),
	(847,'847.mp4','./videos/5/125/584',2,'TestCam 2',1434104819171,1,90),
	(846,'846.mp4','./videos/5/125/584',1,'TestCam 1',1434104819128,1,90),
	(845,'845.mp4','./videos/5/123/583',1,'1080p1',1434102508243,1,30),
	(844,'844.mp4','./videos/5/122/582',2,'1080p3',1434102453758,1,30),
	(843,'843.mp4','./videos/5/122/582',1,'1080p1',1434102453758,1,30),
	(842,'842.mp4','./videos/5/121/581',3,'1080p3',1434102216594,1,30),
	(796,'796.mp4','./videos/5/116/566',1,'TestCam 1',1434098582126,1,90),
	(795,'795.mp4','./videos/5/116/565',3,'TestCam 3',1434098491576,1,90),
	(794,'794.mp4','./videos/5/116/565',2,'TestCam 2',1434098491549,1,90),
	(786,'786.mp4','./videos/5/116/562',3,'TestCam 3',1434098219551,1,90),
	(785,'785.mp4','./videos/5/116/562',2,'TestCam 2',1434098219535,1,90),
	(784,'784.mp4','./videos/5/116/562',1,'TestCam 1',1434098219832,1,90),
	(783,'783.mp4','./videos/5/116/561',3,'TestCam 3',1434098197382,1,90),
	(782,'782.mp4','./videos/5/116/561',2,'TestCam 2',1434098197367,1,90),
	(781,'781.mp4','./videos/5/116/561',1,'TestCam 1',1434098197656,1,90),
	(780,'780.mp4','./videos/5/116/560',3,'TestCam 3',1434098114093,1,90),
	(779,'779.mp4','./videos/5/116/560',2,'TestCam 2',1434098114084,1,90),
	(778,'778.mp4','./videos/5/116/560',1,'TestCam 1',1434098114347,1,90),
	(777,'777.mp4','./videos/5/115/559',3,'TestCam 3',1434097769005,1,90),
	(776,'776.mp4','./videos/5/115/559',2,'TestCam 2',1434097769057,1,90),
	(775,'775.mp4','./videos/5/115/559',1,'TestCam 1',1434097769041,1,90),
	(773,'773.mp4','./videos/5/115/558',2,'TestCam 2',1434097666442,1,90),
	(774,'774.mp4','./videos/5/115/558',3,'TestCam 3',1434097666394,1,90),
	(772,'772.mp4','./videos/5/115/558',1,'TestCam 1',1434097666422,1,90),
	(771,'771.mp4','./videos/5/115/557',3,'TestCam 3',1434097547715,1,90),
	(770,'770.mp4','./videos/5/115/557',2,'TestCam 2',1434097547759,1,90),
	(769,'769.mp4','./videos/5/115/557',1,'TestCam 1',1434097547728,1,90),
	(768,'768.mp4','./videos/5/115/556',3,'TestCam 3',1434097257612,1,90),
	(564,'564.mp4','./videos/3/85/442',1,'TestCam 2',NULL,0,NULL),
	(565,'565.mp4','./videos/3/85/443',1,'TestCam 2',NULL,0,NULL),
	(566,'566.mp4','./videos/3/85/444',1,'TestCam 2',NULL,0,NULL),
	(567,'567.mp4','./videos/3/85/445',1,'TestCam 2',NULL,0,NULL),
	(568,'568.mp4','./videos/3/85/446',1,'TestCam 2',NULL,0,NULL),
	(569,'569.mp4','./videos/3/85/447',1,'TestCam 2',NULL,0,NULL),
	(570,'570.mp4','./videos/3/85/448',1,'TestCam 2',NULL,0,NULL),
	(571,'571.mp4','./videos/3/85/449',1,'TestCam 2',NULL,0,NULL),
	(572,'572.mp4','./videos/3/85/450',1,'TestCam 2',NULL,0,NULL),
	(573,'573.mp4','./videos/3/85/451',1,'TestCam 2',NULL,0,NULL),
	(574,'574.mp4','./videos/3/85/452',1,'TestCam 2',NULL,0,NULL),
	(575,'575.mp4','./videos/3/85/453',1,'TestCam 2',NULL,0,NULL),
	(576,'576.mp4','./videos/3/85/454',1,'TestCam 2',NULL,0,NULL),
	(577,'577.mp4','./videos/3/85/455',1,'TestCam 2',1433760608169,1,90),
	(767,'767.mp4','./videos/5/115/556',2,'TestCam 2',1434097257644,1,90),
	(766,'766.mp4','./videos/5/115/556',1,'TestCam 1',1434097257595,1,90),
	(765,'765.mp4','./videos/5/115/555',3,'TestCam 3',1434097167707,1,90),
	(764,'764.mp4','./videos/5/115/555',2,'TestCam 2',1434097167736,1,90),
	(763,'763.mp4','./videos/5/115/555',1,'TestCam 1',1434097167685,1,90),
	(759,'759.mp4','./videos/5/113/553',2,'TestCam 3',1434096658955,1,90),
	(758,'758.mp4','./videos/5/113/553',1,'TestCam 1',1434096658973,1,90),
	(760,'760.mp4','./videos/5/115/554',1,'TestCam 1',1434097011742,1,90),
	(761,'761.mp4','./videos/5/115/554',2,'TestCam 2',1434097011784,1,90),
	(762,'762.mp4','./videos/5/115/554',3,'TestCam 3',1434097011761,1,90),
	(755,'755.mp4','./videos/4/110/551',1,'TestCam 4',1434029744549,1,90),
	(754,'754.mp4','./videos/4/109/550',2,'TestCam 3',1434020677725,1,90),
	(753,'753.mp4','./videos/4/109/550',1,'TestCam 2',1434020677644,1,90),
	(752,'752.mp4','./videos/4/109/549',2,'TestCam 3',1434020424576,1,90),
	(751,'751.mp4','./videos/4/109/549',1,'TestCam 2',1434020424510,1,90),
	(749,'749.mp4','./videos/4/109/548',1,'TestCam 2',1434020333251,1,90),
	(750,'750.mp4','./videos/4/109/548',2,'TestCam 3',1434020333315,1,90),
	(748,'748.mp4','./videos/4/109/547',2,'TestCam 3',1434020252194,1,90),
	(747,'747.mp4','./videos/4/109/547',1,'TestCam 2',1434020252131,1,90),
	(746,'746.mp4','./videos/4/108/546',3,'TestCam 3',1434020134660,1,90),
	(745,'745.mp4','./videos/4/108/546',2,'TestCam 2',1434020134590,1,90),
	(744,'744.mp4','./videos/4/108/546',1,'TestCam 1',1434020134642,1,90),
	(743,'743.mp4','./videos/4/107/545',3,'TestCam 3',1434019908962,1,90),
	(742,'742.mp4','./videos/4/107/545',2,'TestCam 2',1434019908885,1,90),
	(741,'741.mp4','./videos/4/107/545',1,'TestCam 1',1434019908882,1,90),
	(740,'740.mp4','./videos/4/106/544',1,'TestCam 1',NULL,0,NULL),
	(739,'739.mp4','./videos/4/106/543',3,'TestCam 3',1434019755535,1,90),
	(738,'738.mp4','./videos/4/106/543',2,'TestCam 2',1434019755478,1,90),
	(737,'737.mp4','./videos/4/106/543',1,'TestCam 1',1434019755482,1,90),
	(736,'736.mp4','./videos/4/106/542',3,'TestCam 3',1434019667576,1,90),
	(734,'734.mp4','./videos/4/106/542',1,'TestCam 1',1434019667546,1,90),
	(735,'735.mp4','./videos/4/106/542',2,'TestCam 2',1434019667507,1,90),
	(732,'732.mp4','./videos/4/106/541',2,'TestCam 2',1434019442517,1,90),
	(733,'733.mp4','./videos/4/106/541',3,'TestCam 3',1434019442587,1,90),
	(731,'731.mp4','./videos/4/106/541',1,'TestCam 1',1434019442560,1,90),
	(730,'730.mp4','./videos/4/106/540',3,'TestCam 3',1434019405306,1,90),
	(729,'729.mp4','./videos/4/106/540',2,'TestCam 2',1434019405236,1,90),
	(728,'728.mp4','./videos/4/106/540',1,'TestCam 1',1434019405283,1,90),
	(727,'727.mp4','./videos/4/106/539',3,'TestCam 3',1434019384095,1,90),
	(726,'726.mp4','./videos/4/106/539',2,'TestCam 2',1434019384025,1,90),
	(725,'725.mp4','./videos/4/106/539',1,'TestCam 1',1434019384075,1,90),
	(724,'724.mp4','./videos/4/106/538',3,'TestCam 3',1434019250898,1,90),
	(723,'723.mp4','./videos/4/106/538',2,'TestCam 2',1434019250803,1,90),
	(722,'722.mp4','./videos/4/106/538',1,'TestCam 1',1434019250858,1,90),
	(721,'721.mp4','./videos/4/105/537',3,'TestCam 3',1434018986829,1,90),
	(719,'719.mp4','./videos/4/105/537',1,'TestCam 1',1434018986767,1,90),
	(720,'720.mp4','./videos/4/105/537',2,'TestCam 2',1434018986772,1,90),
	(718,'718.mp4','./videos/4/104/536',2,'TestCam 3',1434018787309,1,90),
	(717,'717.mp4','./videos/4/104/536',1,'TestCam 2',1434018787245,1,90),
	(716,'716.mp4','./videos/4/104/535',2,'TestCam 3',1434018763982,1,90),
	(715,'715.mp4','./videos/4/104/535',1,'TestCam 2',1434018763920,1,90),
	(714,'714.mp4','./videos/4/104/534',2,'TestCam 3',1434018755572,1,90),
	(713,'713.mp4','./videos/4/104/534',1,'TestCam 2',1434018755511,1,90),
	(712,'712.mp4','./videos/4/104/533',2,'TestCam 3',1434018747701,1,90),
	(711,'711.mp4','./videos/4/104/533',1,'TestCam 2',1434018747643,1,90),
	(710,'710.mp4','./videos/4/104/532',2,'TestCam 3',1434018740398,1,90),
	(709,'709.mp4','./videos/4/104/532',1,'TestCam 2',1434018740343,1,90),
	(708,'708.mp4','./videos/4/104/531',2,'TestCam 3',1434018716410,1,90),
	(707,'707.mp4','./videos/4/104/531',1,'TestCam 2',1434018716348,1,90),
	(652,'652.mp4','./videos/3/89/496',1,'TestCam 1',1433844259597,1,90),
	(653,'653.mp4','./videos/3/89/496',2,'TestCam 2',1433844259698,1,90),
	(654,'654.mp4','./videos/3/89/496',3,'TestCam 3',1433844259780,1,90),
	(706,'706.mp4','./videos/4/103/530',2,'TestCam 3',1434018023697,1,90),
	(705,'705.mp4','./videos/4/103/530',1,'TestCam 2',1434018023631,1,90),
	(704,'704.mp4','./videos/4/103/529',2,'TestCam 3',1434018006219,1,90),
	(703,'703.mp4','./videos/4/103/529',1,'TestCam 2',1434018006153,1,90),
	(702,'702.mp4','./videos/4/102/528',2,'TestCam 3',1434017701577,1,90),
	(701,'701.mp4','./videos/4/102/528',1,'TestCam 2',1434017701497,1,90),
	(863,'863.mp4','./videos/5/128/591',1,'TestCam 1',1434111913121,1,90),
	(864,'864.mp4','./videos/5/128/592',1,'TestCam 1',1434112253967,1,90),
	(698,'698.mp4','./videos/4/100/526',1,'TestCam 3',1434016777149,1,90),
	(697,'697.mp4','./videos/4/100/525',1,'TestCam 3',1434016752366,1,90),
	(693,'693.mp4','./videos/4/99/521',1,'TestCam 3',1434015896991,1,90),
	(694,'694.mp4','./videos/4/99/522',1,'TestCam 3',1434016172839,1,90),
	(695,'695.mp4','./videos/4/99/523',1,'TestCam 3',1434016236047,1,90),
	(696,'696.mp4','./videos/4/100/524',1,'TestCam 3',1434016719317,1,90),
	(672,'672.mp4','./videos/4/94/506',1,'TestCam 1',1433937355052,1,90),
	(673,'673.mp4','./videos/4/94/507',1,'TestCam 1',1433937383865,1,90),
	(674,'674.mp4','./videos/4/94/508',1,'TestCam 1',1433947601126,1,90),
	(675,'675.mp4','./videos/4/94/509',1,'TestCam 1',1433964850505,1,90),
	(689,'689.mp4','./videos/4/97/518',1,'TestCam 2',NULL,0,NULL),
	(677,'677.mp4','./videos/4/95/511',1,'TestCam 2',1433965970373,1,90),
	(678,'678.mp4','./videos/4/95/512',1,'TestCam 2',1433966000614,1,90),
	(692,'692.mp4','./videos/4/99/520',1,'TestCam 3',1434015812120,1,90),
	(690,'690.mp4','./videos/4/97/518',2,'TestCam 3',NULL,0,NULL),
	(688,'688.mp4','./videos/4/97/517',2,'TestCam 3',1434012589680,1,90),
	(687,'687.mp4','./videos/4/97/517',1,'TestCam 2',1433970058374,1,90);

/*!40000 ALTER TABLE `moment_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_part` WRITE;
/*!40000 ALTER TABLE `moment_to_part` DISABLE KEYS */;

INSERT INTO `moment_to_part` (`moment_id`, `part_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,2),
	(7,2),
	(8,2),
	(9,2),
	(10,2),
	(11,2),
	(12,2),
	(13,2),
	(14,3),
	(15,3),
	(16,3),
	(17,3),
	(18,3),
	(19,3),
	(20,3),
	(21,4),
	(22,4),
	(23,4),
	(24,4),
	(25,4),
	(26,5),
	(27,5),
	(28,5),
	(29,5),
	(30,5),
	(31,5),
	(32,5),
	(33,5),
	(34,6),
	(35,6),
	(36,6),
	(37,7),
	(38,7),
	(39,7),
	(40,7),
	(41,7),
	(42,7),
	(43,7),
	(44,7),
	(45,9),
	(46,10),
	(47,10),
	(48,10),
	(49,10),
	(50,10),
	(51,10),
	(52,10),
	(53,10),
	(54,10),
	(55,10),
	(56,10),
	(57,13),
	(58,13),
	(59,17),
	(60,17),
	(61,17),
	(62,17),
	(63,17),
	(64,17),
	(65,17),
	(66,17),
	(67,17),
	(68,17),
	(69,17),
	(70,17),
	(71,17),
	(72,17),
	(73,17),
	(74,17),
	(75,17),
	(76,17),
	(77,17),
	(78,18),
	(79,18),
	(80,19),
	(81,19),
	(82,20),
	(83,21),
	(84,21),
	(85,21),
	(86,21),
	(87,21),
	(88,21),
	(89,21),
	(90,21),
	(91,21),
	(92,21),
	(93,21),
	(94,22),
	(95,24),
	(96,24),
	(97,24),
	(98,24),
	(99,24),
	(100,24),
	(101,24),
	(102,25),
	(103,25),
	(104,25),
	(105,25),
	(106,25),
	(107,25),
	(108,25),
	(109,25),
	(110,25),
	(111,26),
	(112,26),
	(113,26),
	(114,26),
	(115,26),
	(116,26),
	(117,26),
	(118,27),
	(119,27),
	(120,27),
	(121,27),
	(122,27),
	(123,28),
	(124,29),
	(125,30),
	(126,30),
	(127,30),
	(128,30),
	(129,30),
	(130,31),
	(131,32),
	(132,33),
	(133,33),
	(134,33),
	(137,34),
	(138,34),
	(139,34),
	(140,34),
	(141,34),
	(142,35),
	(143,35),
	(144,35),
	(145,35),
	(146,35),
	(147,35),
	(148,35),
	(300,67),
	(299,67),
	(298,67),
	(297,66),
	(296,66),
	(292,62),
	(294,65),
	(295,65),
	(160,37),
	(161,37),
	(162,37),
	(225,47),
	(164,37),
	(226,47),
	(166,37),
	(167,37),
	(168,37),
	(169,37),
	(170,37),
	(171,37),
	(172,37),
	(173,37),
	(174,37),
	(175,37),
	(176,37),
	(177,37),
	(178,37),
	(179,37),
	(180,37),
	(181,37),
	(182,37),
	(224,46),
	(184,37),
	(191,40),
	(192,40),
	(193,40),
	(194,40),
	(195,40),
	(196,40),
	(197,40),
	(272,57),
	(271,56),
	(270,55),
	(269,55),
	(268,54),
	(265,52),
	(264,52),
	(263,52),
	(262,52),
	(267,54),
	(260,52),
	(259,52),
	(258,52),
	(257,52),
	(256,52),
	(255,52),
	(254,52),
	(227,47),
	(251,52),
	(228,47),
	(229,47),
	(230,47),
	(301,68),
	(250,52),
	(302,69),
	(303,70),
	(306,70),
	(304,70),
	(305,70),
	(249,52),
	(248,52),
	(307,70),
	(247,52),
	(266,53),
	(245,52),
	(308,70),
	(309,70),
	(310,70),
	(311,70),
	(312,70),
	(313,71),
	(314,71),
	(315,71),
	(316,71),
	(317,71),
	(318,71),
	(319,71),
	(320,71),
	(321,71),
	(322,71),
	(323,71),
	(324,71),
	(325,71),
	(326,71),
	(327,71),
	(328,71),
	(329,71),
	(575,119),
	(331,71),
	(332,71),
	(333,71),
	(335,71),
	(336,71),
	(337,71),
	(597,130),
	(596,130),
	(595,129),
	(594,129),
	(593,129),
	(592,128),
	(590,127),
	(584,125),
	(583,123),
	(582,122),
	(581,121),
	(580,121),
	(579,121),
	(578,121),
	(577,120),
	(576,119),
	(588,126),
	(587,126),
	(586,125),
	(585,125),
	(574,118),
	(573,118),
	(572,118),
	(571,118),
	(570,118),
	(569,117),
	(568,116),
	(567,116),
	(566,116),
	(442,85),
	(443,85),
	(444,85),
	(445,85),
	(446,85),
	(447,85),
	(448,85),
	(449,85),
	(450,85),
	(451,85),
	(452,85),
	(453,85),
	(454,85),
	(455,85),
	(564,116),
	(563,116),
	(562,116),
	(561,116),
	(560,116),
	(559,115),
	(558,115),
	(557,115),
	(556,115),
	(555,115),
	(553,113),
	(554,115),
	(551,110),
	(550,109),
	(549,109),
	(548,109),
	(547,109),
	(546,108),
	(545,107),
	(565,116),
	(544,106),
	(543,106),
	(542,106),
	(541,106),
	(540,106),
	(539,106),
	(538,106),
	(537,105),
	(536,104),
	(535,104),
	(534,104),
	(533,104),
	(532,104),
	(531,104),
	(530,103),
	(529,103),
	(528,102),
	(496,89),
	(523,99),
	(522,99),
	(521,99),
	(520,99),
	(591,128),
	(526,100),
	(525,100),
	(524,100),
	(506,94),
	(507,94),
	(508,94),
	(509,94),
	(518,97),
	(511,95),
	(512,95),
	(517,97);

/*!40000 ALTER TABLE `moment_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_playlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_playlist`;

CREATE TABLE `moment_to_playlist` (
  `playlist_id` int(11) unsigned NOT NULL,
  `moment_id` int(11) unsigned NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `playlist_id` (`playlist_id`,`moment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_playlist` WRITE;
/*!40000 ALTER TABLE `moment_to_playlist` DISABLE KEYS */;

INSERT INTO `moment_to_playlist` (`playlist_id`, `moment_id`, `order`)
VALUES
	(21,480,4),
	(21,493,6);

/*!40000 ALTER TABLE `moment_to_playlist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;

INSERT INTO `moments` (`moment_id`, `date_created`, `time_in_clip`, `lead`, `lapse`, `comment`)
VALUES
	(1,1430472093,98,0,-1,''),
	(2,1430472308,313,0,-1,''),
	(3,1430472356,361,0,-1,''),
	(4,1430472393,398,0,-1,''),
	(5,1430472519,524,0,-1,''),
	(6,1430474132,1,0,-1,''),
	(7,1430474134,3,0,-1,''),
	(8,1430474370,239,0,-1,''),
	(9,1430490555,16424,0,-1,''),
	(10,1430491666,17535,0,-1,''),
	(11,1430491676,17545,0,-1,''),
	(12,1430491937,17806,0,-1,''),
	(13,1430491952,17821,0,-1,''),
	(14,1430492060,2,0,-1,''),
	(15,1430492227,169,0,-1,''),
	(16,1430492250,192,0,-1,''),
	(17,1430493384,1326,0,-1,''),
	(18,1430493598,1540,0,-1,''),
	(19,1430493609,1551,0,-1,''),
	(20,1430494655,2597,0,-1,''),
	(21,1430494768,1,0,-1,''),
	(22,1430494791,24,0,-1,''),
	(23,1430494801,34,0,-1,''),
	(24,1430494816,49,0,-1,''),
	(25,1430494887,120,0,-1,''),
	(26,1430494898,1,0,-1,''),
	(27,1430494909,12,0,-1,''),
	(28,1430494925,28,0,-1,''),
	(29,1430495040,143,0,-1,''),
	(30,1430495564,667,0,-1,''),
	(31,1430495602,705,0,-1,''),
	(32,1430495707,810,0,-1,''),
	(33,1430496367,1470,0,-1,''),
	(34,1430496384,1,0,-1,''),
	(35,1430496527,144,0,-1,''),
	(36,1430497559,1176,0,-1,''),
	(37,1430731578,111,0,-1,''),
	(38,1430731803,336,0,-1,''),
	(39,1430731845,378,0,-1,''),
	(40,1430731891,424,0,-1,''),
	(41,1430731898,431,0,-1,''),
	(42,1430731927,460,0,-1,''),
	(43,1430731949,482,0,-1,''),
	(44,1430732059,592,0,-1,''),
	(45,1430733083,2,0,-1,''),
	(46,1430733144,11,0,-1,''),
	(47,1430733207,74,0,-1,''),
	(48,1430733231,98,0,-1,''),
	(49,1430733276,143,0,-1,''),
	(50,1430733424,291,0,-1,''),
	(51,1430733444,311,0,-1,''),
	(52,1430733532,399,0,-1,''),
	(53,1430733558,425,0,-1,''),
	(54,1430733565,432,0,-1,''),
	(55,1430733583,450,0,-1,''),
	(56,1430734873,1740,0,-1,''),
	(57,1430749291,1,0,-1,''),
	(58,1430749297,7,0,-1,''),
	(59,1430751529,1008,0,-1,''),
	(60,1430751543,1022,0,-1,''),
	(61,1430751609,1088,0,-1,''),
	(62,1430751678,1157,0,-1,''),
	(63,1430751730,1209,0,-1,''),
	(64,1430751744,1223,0,-1,''),
	(65,1430751770,1249,0,-1,''),
	(66,1430751890,1369,0,-1,''),
	(67,1430752291,1770,0,-1,''),
	(68,1430752362,1841,0,-1,''),
	(69,1430752695,2174,0,-1,''),
	(70,1430753123,2602,0,-1,''),
	(71,1430753199,2678,0,-1,''),
	(72,1430753792,3271,0,-1,''),
	(73,1430773680,23159,0,-1,''),
	(74,1430773690,23169,0,-1,''),
	(75,1430773717,23196,0,-1,''),
	(76,1430773793,23272,0,-1,''),
	(77,1430773904,23383,0,-1,''),
	(78,1430774641,2,0,-1,''),
	(79,1430774791,152,0,-1,''),
	(80,1430818304,1,0,-1,''),
	(81,1430818537,234,0,-1,''),
	(82,1430818856,253,0,-1,''),
	(83,1430819979,3,0,-1,''),
	(84,1430820041,65,0,-1,''),
	(85,1430820063,87,0,-1,''),
	(86,1430820083,107,0,-1,''),
	(87,1430820115,139,0,-1,''),
	(88,1430820439,463,0,-1,''),
	(89,1430820483,507,0,-1,''),
	(90,1430820502,526,0,-1,''),
	(91,1430821431,1455,0,-1,''),
	(92,1430824359,4383,0,-1,''),
	(93,1430824399,4423,0,-1,''),
	(94,1430824532,22,0,-1,''),
	(95,1430827826,9,0,-1,''),
	(96,1430827837,20,0,-1,''),
	(97,1430828035,218,0,-1,''),
	(98,1430828115,298,0,-1,''),
	(99,1430828122,305,0,-1,''),
	(100,1430828131,314,0,-1,''),
	(101,1430828133,316,0,-1,''),
	(102,1430828708,12,0,-1,''),
	(103,1430829237,541,0,-1,''),
	(104,1430829505,809,0,-1,''),
	(105,1430829597,901,0,-1,''),
	(106,1430829655,959,0,-1,''),
	(107,1430829870,1174,0,-1,''),
	(108,1430830265,1569,0,-1,''),
	(109,1430830528,1832,0,-1,''),
	(110,1430830613,1917,0,-1,''),
	(111,1430831260,2,0,-1,''),
	(112,1430831265,7,0,-1,''),
	(113,1430831277,19,0,-1,''),
	(114,1430831327,69,0,-1,''),
	(115,1430831342,84,0,-1,''),
	(116,1430831614,356,0,-1,''),
	(117,1430831665,407,0,-1,''),
	(118,1430831781,2,0,-1,''),
	(119,1430831824,45,0,-1,''),
	(120,1430831930,151,0,-1,''),
	(121,1430832084,305,0,-1,''),
	(122,1430832137,358,0,-1,''),
	(123,1430832260,1,0,-1,''),
	(124,1430832302,2,0,-1,''),
	(125,1430832701,2,0,-1,''),
	(126,1430832721,22,0,-1,''),
	(127,1430832877,178,0,-1,''),
	(128,1430832905,206,0,-1,''),
	(129,1430833115,416,0,-1,''),
	(130,1430833351,1,0,-1,''),
	(131,1430833877,2,0,-1,''),
	(132,1430838006,1,0,-1,''),
	(133,1430838289,284,0,-1,''),
	(134,1430838429,424,0,-1,''),
	(137,1430839176,134,0,-1,''),
	(138,1430840086,1044,0,-1,''),
	(139,1430840108,1066,0,-1,''),
	(140,1430840194,1152,0,-1,''),
	(141,1430840315,1273,0,-1,''),
	(142,1430840650,3,0,-1,''),
	(143,1430840755,108,0,-1,''),
	(144,1430841232,585,0,-1,''),
	(145,1430841254,607,0,-1,''),
	(146,1430841293,646,0,-1,''),
	(147,1430841333,686,0,-1,''),
	(148,1430841382,735,0,-1,''),
	(299,1433345423,9,0,-1,''),
	(298,1433345415,1,0,-1,''),
	(297,1433341320,333,0,-1,''),
	(296,1433340989,2,0,-1,''),
	(295,1433340923,13,0,-1,''),
	(292,1433251978,1,0,-1,''),
	(294,1433340911,1,0,-1,''),
	(160,1430843464,12,0,-1,''),
	(161,1430843625,173,0,-1,''),
	(162,1430843710,258,0,-1,''),
	(225,1432110637,1,0,-1,''),
	(164,1430843878,426,0,-1,''),
	(226,1432110655,19,0,-1,''),
	(166,1430843928,476,0,-1,''),
	(167,1430843976,524,0,-1,''),
	(168,1430843983,531,0,-1,''),
	(169,1430844301,849,0,-1,''),
	(170,1430844381,929,0,-1,''),
	(171,1430844403,951,0,-1,''),
	(172,1430844435,983,0,-1,''),
	(173,1430844457,1005,0,-1,''),
	(174,1430844524,1072,0,-1,''),
	(175,1430844596,1144,0,-1,''),
	(176,1430844667,1215,0,-1,''),
	(177,1430844744,1292,0,-1,''),
	(178,1430844802,1350,0,-1,''),
	(179,1430844820,1368,0,-1,''),
	(180,1430844839,1387,0,-1,''),
	(181,1430844880,1428,0,-1,''),
	(182,1430844906,1454,0,-1,''),
	(224,1432110173,1,0,-1,''),
	(184,1430844930,1478,0,-1,''),
	(191,1430845953,20,0,-1,''),
	(192,1430845982,49,0,-1,''),
	(193,1430846059,126,0,-1,''),
	(194,1430846116,183,0,-1,''),
	(195,1430846198,265,0,-1,''),
	(196,1430846206,273,0,-1,''),
	(197,1430846222,289,0,-1,''),
	(272,1432627833,2,0,-1,''),
	(271,1432627648,1,0,-1,''),
	(270,1432627592,44,0,-1,''),
	(269,1432627551,3,0,-1,''),
	(268,1432627335,75,0,-1,''),
	(265,1432202674,897,0,-1,''),
	(264,1432202623,846,0,-1,''),
	(263,1432202534,757,0,-1,''),
	(262,1432202506,729,0,-1,''),
	(267,1432627262,2,0,-1,''),
	(260,1432202477,700,0,-1,''),
	(259,1432202422,645,0,-1,''),
	(258,1432202368,591,0,-1,''),
	(257,1432202349,572,0,-1,''),
	(256,1432202339,562,0,-1,''),
	(227,1432110660,24,0,-1,''),
	(255,1432202308,531,0,-1,''),
	(228,1432110743,107,0,-1,''),
	(229,1432111291,655,0,-1,''),
	(230,1432111446,810,0,-1,''),
	(300,1433345428,14,0,-1,''),
	(250,1432201916,139,0,-1,''),
	(251,1432202003,226,0,-1,''),
	(254,1432202293,516,0,-1,''),
	(249,1432201899,122,0,-1,''),
	(248,1432201859,82,0,-1,''),
	(247,1432201850,73,0,-1,''),
	(266,1432625372,1,0,-1,''),
	(245,1432201779,2,0,-1,''),
	(301,1433358666,1,0,-1,''),
	(302,1433358755,2,0,-1,''),
	(303,1433358934,1,0,-1,''),
	(304,1433360614,1681,0,-1,''),
	(305,1433360623,1690,0,-1,''),
	(306,1433360777,1844,0,-1,''),
	(307,1433360844,1911,0,-1,''),
	(308,1433361059,2126,0,-1,''),
	(309,1433361160,2227,0,-1,''),
	(310,1433361356,2423,0,-1,''),
	(311,1433361381,2448,0,-1,''),
	(312,1433361458,2525,0,-1,''),
	(313,1433406847,1,0,-1,''),
	(314,1433406854,8,0,-1,''),
	(315,1433406860,14,0,-1,''),
	(316,1433406862,16,0,-1,''),
	(317,1433407211,365,0,-1,''),
	(318,1433407221,375,0,-1,''),
	(319,1433407234,388,0,-1,''),
	(320,1433407339,493,0,-1,''),
	(321,1433407345,499,0,-1,''),
	(322,1433407382,536,0,-1,''),
	(323,1433407408,562,0,-1,''),
	(324,1433407431,585,0,-1,''),
	(325,1433407508,662,0,-1,''),
	(326,1433407515,669,0,-1,''),
	(327,1433407526,680,0,-1,''),
	(328,1433407533,687,0,-1,''),
	(329,1433407543,697,0,-1,''),
	(572,1434099322,103,0,-1,''),
	(331,1433408861,2015,0,-1,''),
	(332,1433409019,2173,0,-1,''),
	(333,1433409223,2377,0,-1,''),
	(335,1433410022,3176,0,-1,''),
	(336,1433410034,3188,0,-1,''),
	(337,1433410149,3303,0,-1,''),
	(597,1435671479,35,0,-1,''),
	(596,1435671446,2,0,-1,''),
	(595,1435662939,36,0,-1,''),
	(594,1435662911,8,0,-1,''),
	(593,1435662904,1,0,-1,''),
	(592,1435653669,349,0,-1,''),
	(590,1435648860,99,0,-1,''),
	(588,1434105157,99,0,-1,''),
	(587,1434105065,7,0,-1,''),
	(586,1434104871,222,0,-1,''),
	(581,1434102216,481,0,-1,''),
	(580,1434101950,215,0,-1,''),
	(579,1434101788,53,0,-1,''),
	(578,1434101746,11,0,-1,''),
	(577,1434100963,28,0,-1,''),
	(576,1434100651,944,0,-1,''),
	(575,1434099727,20,0,-1,''),
	(574,1434099541,322,0,-1,''),
	(573,1434099511,292,0,-1,''),
	(585,1434104738,89,0,-1,''),
	(584,1434104659,10,0,-1,''),
	(583,1434102508,5,0,-1,''),
	(582,1434102453,4,0,-1,''),
	(571,1434099281,62,0,-1,''),
	(570,1434099254,35,0,-1,''),
	(569,1434098916,9,0,-1,''),
	(568,1434098614,515,0,-1,''),
	(567,1434098596,497,0,-1,''),
	(566,1434098581,482,0,-1,''),
	(565,1434098491,392,0,-1,''),
	(564,1434098441,342,0,-1,''),
	(563,1434098415,316,0,-1,''),
	(442,1433756754,2,0,-1,''),
	(443,1433756769,17,0,-1,''),
	(444,1433757272,520,0,-1,''),
	(445,1433757280,528,0,-1,''),
	(446,1433757765,1013,0,-1,''),
	(447,1433757833,1081,0,-1,''),
	(448,1433757860,1108,0,-1,''),
	(449,1433758216,1464,0,-1,''),
	(450,1433758828,2076,0,-1,''),
	(451,1433758857,2105,0,-1,''),
	(452,1433759969,3217,0,-1,''),
	(453,1433760685,3933,0,-1,''),
	(454,1433760695,3943,0,-1,''),
	(455,1433760717,3965,0,-1,''),
	(562,1434098219,120,0,-1,''),
	(561,1434098197,98,0,-1,''),
	(560,1434098114,15,0,-1,''),
	(559,1434097768,903,0,-1,''),
	(558,1434097665,800,0,-1,''),
	(557,1434097547,682,0,-1,''),
	(556,1434097257,392,0,-1,''),
	(555,1434097167,302,0,-1,''),
	(553,1434096658,2,0,-1,''),
	(554,1434097011,146,0,-1,''),
	(551,1434029744,1,0,-1,''),
	(550,1434020677,468,0,-1,''),
	(549,1434020424,215,0,-1,''),
	(548,1434020333,124,0,-1,''),
	(547,1434020252,43,0,-1,''),
	(546,1434020134,12,0,-1,''),
	(545,1434019908,9,0,-1,''),
	(544,1434019832,594,0,-1,''),
	(543,1434019755,517,0,-1,''),
	(542,1434019667,429,0,-1,''),
	(541,1434019442,204,0,-1,''),
	(540,1434019405,167,0,-1,''),
	(539,1434019383,145,0,-1,''),
	(538,1434019250,12,0,-1,''),
	(537,1434018986,13,0,-1,''),
	(536,1434018787,80,0,-1,''),
	(535,1434018763,56,0,-1,''),
	(534,1434018755,48,0,-1,''),
	(533,1434018747,40,0,-1,''),
	(532,1434018740,33,0,-1,''),
	(531,1434018716,9,0,-1,''),
	(530,1434018023,25,0,-1,''),
	(529,1434018006,8,0,-1,''),
	(528,1434017701,9,0,-1,''),
	(591,1435653328,8,0,-1,''),
	(526,1434016777,68,0,-1,''),
	(496,1433844259,69,0,-1,''),
	(523,1434016235,426,0,-1,''),
	(522,1434016172,363,0,-1,''),
	(521,1434015896,87,0,-1,''),
	(520,1434015812,3,0,-1,''),
	(525,1434016752,43,0,-1,''),
	(524,1434016719,10,0,-1,''),
	(506,1433937355,537,0,-1,''),
	(507,1433937383,565,0,-1,''),
	(508,1433947619,10801,0,-1,''),
	(509,1433964850,28032,0,-1,''),
	(518,1434015194,2609,0,-1,''),
	(511,1434008501,192,0,-1,''),
	(512,1434008532,223,0,-1,''),
	(517,1434012589,4,0,-1,'');

/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Local testing');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `part_to_event` WRITE;
/*!40000 ALTER TABLE `part_to_event` DISABLE KEYS */;

INSERT INTO `part_to_event` (`part_id`, `event_id`)
VALUES
	(123,5),
	(122,5),
	(121,5),
	(120,5),
	(116,5),
	(117,5),
	(118,5),
	(119,5),
	(110,4),
	(109,4),
	(108,4),
	(107,4),
	(106,4),
	(105,4),
	(104,4),
	(103,4),
	(102,4),
	(101,4),
	(100,4),
	(99,4),
	(94,4),
	(98,4),
	(97,4),
	(95,4),
	(96,4),
	(124,5),
	(125,5),
	(126,5),
	(127,5),
	(128,5),
	(129,5),
	(130,5);

/*!40000 ALTER TABLE `part_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  `type` varchar(11) NOT NULL DEFAULT 'streaming',
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`part_id`, `part_name`, `date_created`, `type`)
VALUES
	(123,'Part 8','1434102494','instant'),
	(102,'Part 9','1434017680','instant'),
	(103,'Part 10','1434017991','instant'),
	(104,'Part 11','1434018698','instant'),
	(105,'Part 12','1434018954','instant'),
	(106,'Part 13','1434019222','instant'),
	(107,'Part 14','1434019874','instant'),
	(108,'Part 15','1434020105','instant'),
	(109,'Part 16','1434020201','instant'),
	(110,'Part 17','1434029735','instant'),
	(122,'Part 7','1434102436','instant'),
	(121,'30 fps','1434101719','instant'),
	(120,'60 fps','1434100924','instant'),
	(116,'Part 1','1434098081','instant'),
	(117,'Part 2','1434098900','instant'),
	(118,'Part 3','1434099183','instant'),
	(119,'90 fps','1434099695','instant'),
	(101,'Part 8','1434017276','instant'),
	(99,'Part 6','1434015802','instant'),
	(100,'Part 7','1434016702','instant'),
	(98,'Part 5','1434015214','instant'),
	(97,'Part 4','1434012571','instant'),
	(94,'Part 1','1433936718','instant'),
	(95,'Part 2','1434008302','instant'),
	(96,'Part 3','1434008922','instant'),
	(124,'Part 9','1434102644','instant'),
	(125,'Part 10','1434104640','instant'),
	(126,'Part 11','1434105046','instant'),
	(127,'Part 12','1435648755','instant'),
	(128,'Part 13','1435653272','instant'),
	(129,'Part 14','1435662897','instant'),
	(130,'Part 15','1435671429','instant');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `player_to_moment` WRITE;
/*!40000 ALTER TABLE `player_to_moment` DISABLE KEYS */;

INSERT INTO `player_to_moment` (`id`, `player_id`, `moment_id`, `user_id`)
VALUES
	(6,1,245,1),
	(5,1,265,1),
	(3,1,234,1),
	(4,1,233,1),
	(9,1,576,1);

/*!40000 ALTER TABLE `player_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `player_to_team` WRITE;
/*!40000 ALTER TABLE `player_to_team` DISABLE KEYS */;

INSERT INTO `player_to_team` (`player_id`, `team_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `player_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table playlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlists`;

CREATE TABLE `playlists` (
  `playlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;

INSERT INTO `playlists` (`playlist_id`, `owner_id`, `title`)
VALUES
	(21,1,'test Lijst');

/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cam_number` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `cam_description` varchar(256) DEFAULT '',
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table shares
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shares`;

CREATE TABLE `shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `share_token` varchar(254) NOT NULL DEFAULT '',
  `visits` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `type` varchar(254) NOT NULL DEFAULT 'moment',
  `id` int(11) NOT NULL,
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_token` (`share_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;

INSERT INTO `shares` (`share_id`, `share_token`, `visits`, `creator_id`, `type`, `id`)
VALUES
	(1,'7YnPWvbgTj',0,1,'moment',220),
	(2,'MK8faAecFT',0,1,'moment',222),
	(3,'hYqmA2tgDT',0,1,'moment',223),
	(4,'xUBJDyKjlA',0,1,'moment',203),
	(5,'UyajYjpxUT',0,1,'moment',265);

/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  `is_online` int(1) NOT NULL DEFAULT '0',
  `system_from_email` varchar(128) NOT NULL DEFAULT 'fieldback@fieldback.net',
  `support_email` varchar(128) DEFAULT 'support@fieldback.net',
  `downloads` int(1) NOT NULL DEFAULT '0',
  `max_instacord_seconds` int(11) NOT NULL DEFAULT '10',
  `insta_url` varchar(500) DEFAULT NULL,
  `shares` tinyint(1) NOT NULL DEFAULT '0',
  `insta_key` varchar(200) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`, `is_online`, `system_from_email`, `support_email`, `downloads`, `max_instacord_seconds`, `insta_url`, `shares`, `insta_key`)
VALUES
	(1,'LOCAL','LOCAL version','/usr/bin/ffmpeg','/usr/bin/openRTSP',120,'tim@lessormore.nl',0,'fieldback@fieldback.net','support@fieldback.net',1,30,'http://10.0.0.47/insta/moment_file',1,'SUPERSECRET');

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_moment` WRITE;
/*!40000 ALTER TABLE `tag_to_moment` DISABLE KEYS */;

INSERT INTO `tag_to_moment` (`tag_id`, `moment_id`, `user_id`, `tag_name`)
VALUES
	(49,265,1,'test'),
	(49,245,1,'test'),
	(56,245,1,'sa'),
	(58,245,1,'f'),
	(53,245,1,'asdf'),
	(54,245,1,'fas'),
	(57,245,1,'sadf'),
	(51,245,1,'asdf'),
	(50,245,1,'test2'),
	(52,245,1,'asd'),
	(56,577,1,'sa'),
	(56,576,1,'sa');

/*!40000 ALTER TABLE `tag_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_organisation` WRITE;
/*!40000 ALTER TABLE `tag_to_organisation` DISABLE KEYS */;

INSERT INTO `tag_to_organisation` (`tag_id`, `organisation_id`)
VALUES
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1),
	(55,1),
	(56,1),
	(57,1),
	(58,1);

/*!40000 ALTER TABLE `tag_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `tag_to_tagfield` (`tag_id`, `tagfield_id`, `left`, `top`, `label`, `original_tag_id`)
VALUES
	(0,1,20,21,'test',49),
	(0,1,9,45,'asd',52),
	(0,1,32,50,'asdf',53),
	(0,1,71,16,'asdf',51),
	(0,1,49,33,'f',58),
	(0,1,52,75,'fas',54),
	(0,1,74,54,'sadf',57),
	(0,1,46,14,'sa',56),
	(0,1,26,80,'test2',50);

/*!40000 ALTER TABLE `tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tagfields` WRITE;
/*!40000 ALTER TABLE `tagfields` DISABLE KEYS */;

INSERT INTO `tagfields` (`tagfield_id`, `tagfield_name`, `show_players`, `date_saved`)
VALUES
	(1,'Test',1,1432547841);

/*!40000 ALTER TABLE `tagfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`tag_id`, `tag_name`)
VALUES
	(48,'Goed moment'),
	(47,'Slecht moment'),
	(46,'Aanval'),
	(45,'Verdediging'),
	(44,'Balbezit'),
	(43,'Links'),
	(42,'Rechts'),
	(41,'Voor'),
	(40,'Midden'),
	(39,'Achter'),
	(49,'test'),
	(50,'test2'),
	(51,'asdf'),
	(52,'asd'),
	(53,'asdf'),
	(54,'fas'),
	(55,'asdf'),
	(56,'sa'),
	(57,'sadf'),
	(58,'f');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_event` WRITE;
/*!40000 ALTER TABLE `team_to_event` DISABLE KEYS */;

INSERT INTO `team_to_event` (`team_id`, `event_id`)
VALUES
	(1,5),
	(1,4);

/*!40000 ALTER TABLE `team_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(1,'Test Team');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `track_login` WRITE;
/*!40000 ALTER TABLE `track_login` DISABLE KEYS */;

INSERT INTO `track_login` (`id`, `user_id`, `username`, `email`, `active`, `ip_address`)
VALUES
	(245,1,'admin','tim@lessormore.nl',1,'0'),
	(246,1,'admin','admin@fieldback.net',1,'0'),
	(247,1,'admin','tim@lessormore.nl',1,'0'),
	(248,1,'admin','admin@fieldback.net',1,'0'),
	(249,1,'admin','admin@fieldback.net',1,'0'),
	(250,1,'admin','admin@fieldback.net',1,'0'),
	(251,1,'admin','admin@fieldback.net',1,'0'),
	(252,1,'admin','admin@fieldback.net',1,'0'),
	(253,1,'admin','admin@fieldback.net',1,'0'),
	(254,1,'admin','admin@fieldback.net',1,'0'),
	(255,1,'admin','admin@fieldback.net',1,'0'),
	(256,1,'admin','admin@fieldback.net',1,'0'),
	(257,1,'admin','admin@fieldback.net',1,'0'),
	(258,1,'admin','admin@fieldback.net',1,'0'),
	(259,1,'admin','admin@fieldback.net',1,'0'),
	(260,1,'admin','admin@fieldback.net',1,'0'),
	(261,1,'admin','admin@fieldback.net',1,'0'),
	(262,1,'admin','admin@fieldback.net',1,'0'),
	(263,1,'admin','admin@fieldback.net',1,'0'),
	(264,1,'admin','admin@fieldback.net',1,'0'),
	(265,1,'admin','admin@fieldback.net',1,'0'),
	(266,1,'admin','admin@fieldback.net',1,'0'),
	(267,1,'admin','admin@fieldback.net',1,'0'),
	(268,1,'admin','admin@fieldback.net',1,'0'),
	(269,1,'admin','admin@fieldback.net',1,'0'),
	(270,1,'admin','admin@fieldback.net',1,'0'),
	(271,1,'admin','admin@fieldback.net',1,'0'),
	(272,1,'admin','admin@fieldback.net',1,'0'),
	(273,1,'admin','admin@fieldback.net',1,'0'),
	(274,1,'admin','admin@fieldback.net',1,'0'),
	(275,1,'admin','admin@fieldback.net',1,'0'),
	(276,1,'admin','admin@fieldback.net',1,'0'),
	(277,1,'admin','admin@fieldback.net',1,'0'),
	(278,1,'admin','admin@fieldback.net',1,'0'),
	(279,1,'admin','admin@fieldback.net',1,'0'),
	(280,1,'admin','admin@fieldback.net',1,'0'),
	(281,1,'admin','admin@fieldback.net',1,'0'),
	(282,1,'admin','admin@fieldback.net',1,'0'),
	(283,1,'admin','admin@fieldback.net',1,'0'),
	(284,1,'admin','admin@fieldback.net',1,'0'),
	(285,1,'admin','admin@fieldback.net',1,'0'),
	(286,1,'admin','admin@fieldback.net',1,'0'),
	(287,1,'admin','admin@fieldback.net',1,'0'),
	(288,1,'admin','admin@fieldback.net',1,'0'),
	(289,1,'admin','admin@fieldback.net',1,'0'),
	(290,1,'admin','admin@fieldback.net',1,'0'),
	(291,1,'admin','admin@fieldback.net',1,'0'),
	(292,1,'admin','admin@fieldback.net',1,'0'),
	(293,1,'admin','admin@fieldback.net',1,'0'),
	(294,1,'admin','admin@fieldback.net',1,'0'),
	(295,1,'admin','admin@fieldback.net',1,'0'),
	(296,1,'admin','admin@fieldback.net',1,'0'),
	(297,1,'admin','admin@fieldback.net',1,'0'),
	(298,1,'admin','admin@fieldback.net',1,'0'),
	(299,1,'admin','admin@fieldback.net',1,'0'),
	(300,1,'admin','admin@fieldback.net',1,'0'),
	(301,1,'admin','admin@fieldback.net',1,'0'),
	(302,1,'admin','admin@fieldback.net',1,'0'),
	(303,1,'admin','admin@fieldback.net',1,'0'),
	(304,1,'admin','admin@fieldback.net',1,'0'),
	(305,1,'admin','admin@fieldback.net',1,'0'),
	(306,1,'admin','admin@fieldback.net',1,'0'),
	(307,1,'admin','admin@fieldback.net',1,'0'),
	(308,1,'admin','admin@fieldback.net',1,'0'),
	(309,1,'admin','admin@fieldback.net',1,'0'),
	(310,1,'admin','admin@fieldback.net',1,'0'),
	(311,1,'admin','admin@fieldback.net',1,'0'),
	(312,1,'admin','admin@fieldback.net',1,'0'),
	(313,1,'admin','admin@fieldback.net',1,'0'),
	(314,1,'admin','admin@fieldback.net',1,'0'),
	(315,1,'admin','admin@fieldback.net',1,'0');

/*!40000 ALTER TABLE `track_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lead` int(11) DEFAULT NULL,
  `lapse` int(11) DEFAULT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `trigger_tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `trigger_tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `trigger_tag_to_tagfield` (`trigger_tag_id`, `tagfield_id`, `name`, `lead`, `lapse`)
VALUES
	(0,1,'asdf',1,1);

/*!40000 ALTER TABLE `trigger_tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(1,'Admin','Admin','admin@fieldback.net');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_clip` WRITE;
/*!40000 ALTER TABLE `user_to_clip` DISABLE KEYS */;

INSERT INTO `user_to_clip` (`user_id`, `clip_id`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(1,4),
	(1,5),
	(1,6),
	(1,7),
	(1,8),
	(1,9),
	(1,10),
	(1,11),
	(1,12),
	(1,13),
	(1,14),
	(1,15),
	(1,16),
	(1,17),
	(1,18),
	(1,19),
	(1,20),
	(1,21),
	(1,22),
	(1,23),
	(1,24),
	(1,25),
	(1,26),
	(1,27),
	(1,28),
	(1,29),
	(1,30),
	(1,31),
	(1,32),
	(1,33),
	(1,54),
	(1,35),
	(1,53),
	(1,52),
	(1,38),
	(1,51),
	(1,50),
	(1,49),
	(1,42),
	(1,43),
	(1,44),
	(1,48),
	(1,47),
	(1,55),
	(1,153),
	(1,152),
	(1,155),
	(1,154),
	(1,151),
	(1,150),
	(1,62),
	(1,63),
	(1,64),
	(1,65),
	(1,66),
	(1,67),
	(1,68),
	(1,69),
	(1,70),
	(1,71),
	(1,72),
	(1,139),
	(1,149),
	(1,148),
	(1,147),
	(1,146),
	(1,145),
	(1,142),
	(1,141),
	(1,140),
	(1,159),
	(1,158),
	(1,157),
	(1,156),
	(1,144),
	(1,143),
	(1,138),
	(1,137),
	(1,136),
	(1,135),
	(1,134),
	(1,133),
	(1,132),
	(1,131),
	(1,96),
	(1,129),
	(1,130),
	(1,128),
	(1,127),
	(1,126),
	(1,102),
	(1,103),
	(1,104),
	(1,105),
	(1,106),
	(1,107),
	(1,124),
	(1,123),
	(1,122),
	(1,121),
	(1,120),
	(1,119),
	(1,125),
	(1,115),
	(1,116),
	(1,117),
	(1,118),
	(1,160),
	(1,161),
	(1,162),
	(1,163),
	(1,164),
	(1,165),
	(1,166),
	(1,167),
	(1,168),
	(1,169),
	(1,170),
	(1,171),
	(1,172),
	(1,173),
	(1,174),
	(1,175),
	(1,176),
	(1,177),
	(1,178),
	(1,179),
	(1,180),
	(1,181),
	(1,182),
	(1,183),
	(1,184),
	(1,185),
	(1,186),
	(1,187),
	(1,188),
	(1,189),
	(1,190);

/*!40000 ALTER TABLE `user_to_clip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_tagfield` WRITE;
/*!40000 ALTER TABLE `user_to_tagfield` DISABLE KEYS */;

INSERT INTO `user_to_tagfield` (`user_id`, `tagfield_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(1,1,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(1) NOT NULL DEFAULT '1',
  `api_key` varchar(64) DEFAULT NULL,
  `selected_organisation` int(11) DEFAULT NULL,
  `selected_tagfield` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`, `selected_organisation`, `selected_tagfield`)
VALUES
	(1,X'30','admin','$2y$08$Ur3gT56i0f3v658/IDZePeAgehbErEIB32gVcHrTowkBLLBcvIu3.','','admin@fieldback.net','','',0,'0qKv5FJevkmxrDTVynki8O',0,1435673563,1,'','',0,'007',1,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
