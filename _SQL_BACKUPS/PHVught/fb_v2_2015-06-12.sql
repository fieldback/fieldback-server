# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.66)
# Database: fb_v2
# Generation Time: 2015-06-12 07:03:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  PRIMARY KEY (`camera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`)
VALUES
	(53,'Pelco 1 Achterzijde','rtsp://192.168.178.6/stream1',1,1,'30','1',1),
	(54,'Pelco 2 Zijkant','rtsp://192.168.178.7/stream1',2,1,'30','1',1);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_id` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clip_to_part` WRITE;
/*!40000 ALTER TABLE `clip_to_part` DISABLE KEYS */;

INSERT INTO `clip_to_part` (`clip_id`, `part_id`, `cam`, `cam_id`)
VALUES
	(16,9,1,42),
	(15,9,2,43),
	(14,8,1,42),
	(13,8,2,43),
	(12,7,1,42),
	(11,7,2,43),
	(10,6,1,42),
	(9,6,2,43),
	(17,12,0,0),
	(18,13,2,43),
	(19,13,1,42),
	(20,14,0,0),
	(21,15,0,0),
	(22,15,0,0),
	(23,16,0,0),
	(24,10,1,42),
	(25,17,1,42),
	(26,18,1,42),
	(27,19,2,43),
	(28,19,1,42),
	(29,20,2,43),
	(30,20,1,42),
	(31,22,1,42),
	(32,23,1,42),
	(33,24,2,43),
	(34,26,1,46),
	(35,27,0,0),
	(36,28,0,0),
	(37,29,0,0),
	(38,30,0,0),
	(39,31,0,0),
	(40,32,0,0),
	(41,34,0,0);

/*!40000 ALTER TABLE `clip_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clips` WRITE;
/*!40000 ALTER TABLE `clips` DISABLE KEYS */;

INSERT INTO `clips` (`clip_id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `date_created`, `kind`)
VALUES
	(16,'rec1.mp4','video/mp4','./videos/5/9/tmp_rec/rec1.mp4','./videos/5/9/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','721132103',1424455163,'recording'),
	(15,'rec2.mp4','video/mp4','./videos/5/9/tmp_rec/rec2.mp4','./videos/5/9/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','4139935458',1424463801,'recording'),
	(14,'rec1.mp4','video/mp4','./videos/4/8/tmp_rec/rec1.mp4','./videos/4/8/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','420898306',1424450818,'recording'),
	(13,'rec2.mp4','video/mp4','./videos/4/8/tmp_rec/rec2.mp4','./videos/4/8/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','420308244',1424450818,'recording'),
	(12,'rec1.mp4','video/mp4','./videos/4/7/tmp_rec/rec1.mp4','./videos/4/7/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','723311417',1424449688,'recording'),
	(11,'rec2.mp4','video/mp4','./videos/4/7/tmp_rec/rec2.mp4','./videos/4/7/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','745255949',1424449754,'recording'),
	(10,'rec1.mp4','video/mp4','./videos/4/6/tmp_rec/rec1.mp4','./videos/4/6/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','26748183',1424445579,'recording'),
	(9,'rec2.mp4','video/mp4','./videos/4/6/tmp_rec/rec2.mp4','./videos/4/6/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','26869307',1424445579,'recording'),
	(17,'20150220_18-25-Test_Camera_hoeken-Demo_Deel_3-nr_21-Axis_F1005.mp4','video/mp4','/var/www/html/videos/5/12/','/var/www/html/videos/5/12/20150220_18-25-Test_Camera_hoeken-Demo_Deel_3-nr_21-Axis_F1005.mp4','20150220_18-25-Test_Camera_hoeken-Demo_Deel_3-nr_21-Axis_F1005','20150220_18-25-Test_Camera_hoeken-Demo_Deel_3-nr_21-Axis_F1005.mp4','20150220 18-25-Test Camera hoeken-Demo Deel 3-nr 21-Axis F1005.mp4','.mp4','1199.03',1424465701,'upload'),
	(18,'rec2.mp4','video/mp4','./videos/5/13/tmp_rec/rec2.mp4','./videos/5/13/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','135746807',1424538844,'recording'),
	(19,'rec1.mp4','video/mp4','./videos/5/13/tmp_rec/rec1.mp4','./videos/5/13/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','134963399',1424538844,'recording'),
	(20,'5.mp4','video/mp4','/var/www/html/videos/5/14/','/var/www/html/videos/5/14/5.mp4','5','5.mp4','5.mp4','.mp4','594046.21',1424538991,'upload'),
	(21,'4.mp4','video/mp4','/var/www/html/videos/5/15/','/var/www/html/videos/5/15/4.mp4','4','4.mp4','4.mp4','.mp4','263595.75',1424698311,'upload'),
	(22,'41.mp4','video/mp4','/var/www/html/videos/5/15/','/var/www/html/videos/5/15/41.mp4','41','4.mp4','4.mp4','.mp4','263595.75',1424698312,'upload'),
	(23,'4.mp4','video/mp4','/var/www/html/videos/5/16/','/var/www/html/videos/5/16/4.mp4','4','4.mp4','4.mp4','.mp4','263595.75',1424698686,'upload'),
	(24,'rec1.mp4','video/mp4','./videos/4/10/tmp_rec/rec1.mp4','./videos/4/10/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','18649344',1424703741,'recording'),
	(25,'rec1.mp4','video/mp4','./videos/4/17/tmp_rec/rec1.mp4','./videos/4/17/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','31926182',1424703844,'recording'),
	(26,'rec1.mp4','video/mp4','./videos/4/18/tmp_rec/rec1.mp4','./videos/4/18/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','47618032',1424703980,'recording'),
	(27,'rec2.mp4','video/mp4','./videos/5/19/tmp_rec/rec2.mp4','./videos/5/19/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','1865083057',1424709177,'recording'),
	(28,'rec1.mp4','video/mp4','./videos/5/19/tmp_rec/rec1.mp4','./videos/5/19/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','720223676',1424706281,'recording'),
	(29,'rec2.mp4','video/mp4','./videos/6/20/tmp_rec/rec2.mp4','./videos/6/20/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','2497847688',1424731199,'recording'),
	(30,'rec1.mp4','video/mp4','./videos/6/20/tmp_rec/rec1.mp4','./videos/6/20/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','1499179079',1424730369,'recording'),
	(31,'rec1.mp4','video/mp4','./videos/9/22/tmp_rec/rec1.mp4','./videos/9/22/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','1768059509',1424894371,'recording'),
	(32,'rec1.mp4','video/mp4','./videos/10/23/tmp_rec/rec1.mp4','./videos/10/23/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','24065648',1425031400,'recording'),
	(33,'rec2.mp4','video/mp4','./videos/9/24/tmp_rec/rec2.mp4','./videos/9/24/tmp_rec/rec2.mp4','rec2.mp4','rec2.mp4','rec2.mp4','mp4','657438602',1425497919,'recording'),
	(34,'rec1.mp4','video/mp4','./videos/6/26/tmp_rec/rec1.mp4','./videos/6/26/tmp_rec/rec1.mp4','rec1.mp4','rec1.mp4','rec1.mp4','mp4','2610283',1428570284,'recording'),
	(35,'trim.C5B630C1-6C58-4BD4-96F6-8B22D6A27993_.MOV','video/quicktime','/var/www/html/videos/5/27/','/var/www/html/videos/5/27/trim.C5B630C1-6C58-4BD4-96F6-8B22D6A27993_.MOV','trim.C5B630C1-6C58-4BD4-96F6-8B22D6A27993_','trim.C5B630C1-6C58-4BD4-96F6-8B22D6A27993_.MOV','trim.C5B630C1-6C58-4BD4-96F6-8B22D6A27993_.MOV','.MOV','15468.21',1428674561,'upload'),
	(36,'trim.ADFF1CA7-C20E-4A03-87B9-156F039C5046_.MOV','video/quicktime','/var/www/html/videos/5/28/','/var/www/html/videos/5/28/trim.ADFF1CA7-C20E-4A03-87B9-156F039C5046_.MOV','trim.ADFF1CA7-C20E-4A03-87B9-156F039C5046_','trim.ADFF1CA7-C20E-4A03-87B9-156F039C5046_.MOV','trim.ADFF1CA7-C20E-4A03-87B9-156F039C5046_.MOV','.MOV','3807.64',1428675022,'upload'),
	(37,'trim.026A02D6-73AF-46B1-8396-E9801AE5D74E_.MOV','video/quicktime','/var/www/html/videos/5/29/','/var/www/html/videos/5/29/trim.026A02D6-73AF-46B1-8396-E9801AE5D74E_.MOV','trim.026A02D6-73AF-46B1-8396-E9801AE5D74E_','trim.026A02D6-73AF-46B1-8396-E9801AE5D74E_.MOV','trim.026A02D6-73AF-46B1-8396-E9801AE5D74E_.MOV','.MOV','13758.42',1428680288,'upload'),
	(38,'testvansysteem.avi','video/x-msvideo','/var/www/html/videos/5/30/','/var/www/html/videos/5/30/testvansysteem.avi','testvansysteem','testvansysteem.avi','testvansysteem.avi','.avi','18793.23',1428680485,'upload'),
	(39,'trim.3886D40F-EFEF-4950-AAA4-5460C39B6F39_.MOV','video/quicktime','/var/www/html/videos/5/31/','/var/www/html/videos/5/31/trim.3886D40F-EFEF-4950-AAA4-5460C39B6F39_.MOV','trim.3886D40F-EFEF-4950-AAA4-5460C39B6F39_','trim.3886D40F-EFEF-4950-AAA4-5460C39B6F39_.MOV','trim.3886D40F-EFEF-4950-AAA4-5460C39B6F39_.MOV','.MOV','13758.42',1428682019,'upload'),
	(40,'trim.A974152E-F8EB-456E-86E2-31CEF948D8B0_.MOV','video/quicktime','/var/www/html/videos/5/32/','/var/www/html/videos/5/32/trim.A974152E-F8EB-456E-86E2-31CEF948D8B0_.MOV','trim.A974152E-F8EB-456E-86E2-31CEF948D8B0_','trim.A974152E-F8EB-456E-86E2-31CEF948D8B0_.MOV','trim.A974152E-F8EB-456E-86E2-31CEF948D8B0_.MOV','.MOV','3431.67',1428739355,'upload'),
	(41,'trim.64DAF7E1-603B-45F1-A5C9-355951CFBDE2_2_.MOV','video/quicktime','/var/www/html/videos/11/34/','/var/www/html/videos/11/34/trim.64DAF7E1-603B-45F1-A5C9-355951CFBDE2_2_.MOV','trim.64DAF7E1-603B-45F1-A5C9-355951CFBDE2_2_','trim.64DAF7E1-603B-45F1-A5C9-355951CFBDE2_2_.MOV','trim.64DAF7E1-603B-45F1-A5C9-355951CFBDE2 2_.MOV','.MOV','10102.56',1428747401,'upload');

/*!40000 ALTER TABLE `clips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `creator_to_event` WRITE;
/*!40000 ALTER TABLE `creator_to_event` DISABLE KEYS */;

INSERT INTO `creator_to_event` (`creator_id`, `event_id`)
VALUES
	(71,4),
	(4,5),
	(4,6),
	(72,8),
	(72,9),
	(4,11);

/*!40000 ALTER TABLE `creator_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `event_to_organisation` WRITE;
/*!40000 ALTER TABLE `event_to_organisation` DISABLE KEYS */;

INSERT INTO `event_to_organisation` (`event_id`, `organisation_id`)
VALUES
	(4,2),
	(5,2),
	(6,2),
	(8,2),
	(9,2),
	(11,2);

/*!40000 ALTER TABLE `event_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`event_id`, `event_name`, `event_description`, `date_created`)
VALUES
	(4,'Test 1','Eerst Test Systeem','1424445487'),
	(5,'Test 2','Tweede deel van de test','1424453349'),
	(6,'maandag training','demo','1424724698'),
	(8,'RTC verspringtraining','Verbeteren van de verspringtechniek van de RTC atleten','1424785316'),
	(9,'Verspringtraining PH','De verspringtraining die gegeven wordt aan de PH atleten','1424889176'),
	(11,'Discus werpen (test)','Testen van verschillende camera\'s','1428747349');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_id` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_file_to_moment` WRITE;
/*!40000 ALTER TABLE `moment_file_to_moment` DISABLE KEYS */;

INSERT INTO `moment_file_to_moment` (`moment_file_id`, `moment_id`, `cam`, `cam_id`)
VALUES
	(44,22,1,42),
	(43,22,2,43),
	(42,21,1,42),
	(41,21,2,43),
	(40,20,1,42),
	(39,20,2,43),
	(38,19,1,42),
	(37,19,2,43),
	(36,18,1,42),
	(35,18,2,43),
	(34,17,1,42),
	(33,17,2,43),
	(32,16,1,42),
	(31,16,2,43),
	(30,15,1,42),
	(29,15,2,43),
	(28,14,1,42),
	(27,14,2,43),
	(26,13,1,42),
	(25,13,2,43),
	(45,23,2,43),
	(46,23,1,42),
	(47,24,2,43),
	(48,24,1,42),
	(49,25,2,43),
	(50,25,1,42),
	(51,26,2,43),
	(52,26,1,42),
	(53,27,2,43),
	(54,27,1,42),
	(55,28,2,43),
	(56,28,1,42),
	(57,29,2,43),
	(58,29,1,42),
	(59,30,2,43),
	(60,30,1,42),
	(61,31,2,43),
	(62,31,1,42),
	(63,32,2,43),
	(64,32,1,42),
	(65,33,2,43),
	(66,33,1,42),
	(67,34,2,43),
	(68,34,1,42),
	(69,35,2,43),
	(70,35,1,42),
	(71,36,2,43),
	(72,36,1,42),
	(73,37,2,43),
	(74,37,1,42),
	(75,38,2,43),
	(76,38,1,42),
	(77,39,2,43),
	(78,39,1,42),
	(79,40,2,43),
	(80,40,1,42),
	(81,41,2,43),
	(82,41,1,42),
	(83,42,2,43),
	(84,42,1,42),
	(85,43,2,43),
	(86,43,1,42),
	(87,44,0,0),
	(88,45,0,0),
	(89,46,0,0),
	(90,47,0,0),
	(91,48,0,0),
	(92,49,0,0),
	(93,50,0,0),
	(94,51,0,0),
	(95,52,0,0),
	(96,53,0,0),
	(97,54,0,0),
	(98,55,0,0),
	(99,56,0,0),
	(100,57,0,0),
	(101,58,2,43),
	(102,58,1,42),
	(103,59,2,43),
	(104,59,1,42),
	(105,60,2,43),
	(106,60,1,42),
	(107,61,1,42),
	(108,61,2,43),
	(109,62,2,43),
	(110,62,1,42),
	(111,63,2,43),
	(112,63,1,42),
	(113,64,1,42),
	(114,65,1,42),
	(115,66,1,42),
	(116,67,1,42),
	(117,68,1,42),
	(118,69,1,42),
	(119,70,1,42),
	(120,71,1,42),
	(121,72,1,42),
	(122,73,1,42),
	(123,74,1,42),
	(124,75,1,42),
	(125,76,1,42),
	(126,77,1,42),
	(127,78,1,42),
	(128,79,1,42),
	(129,80,1,42),
	(130,81,1,42),
	(131,82,1,42),
	(132,83,1,42),
	(133,84,1,42),
	(134,85,1,42),
	(135,86,1,42),
	(136,87,1,42),
	(137,88,1,42),
	(138,89,1,42),
	(139,90,0,0),
	(140,91,2,43),
	(141,92,2,43),
	(142,93,2,43),
	(143,94,2,43),
	(144,95,2,43),
	(145,96,2,43),
	(146,97,2,43),
	(147,98,2,43),
	(149,100,2,43),
	(150,101,2,43),
	(151,102,0,0);

/*!40000 ALTER TABLE `moment_file_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_id` int(11) NOT NULL,
  PRIMARY KEY (`moment_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_files` WRITE;
/*!40000 ALTER TABLE `moment_files` DISABLE KEYS */;

INSERT INTO `moment_files` (`moment_file_id`, `filename`, `path`, `cam`, `cam_id`)
VALUES
	(40,'40.mp4','videos/4/7/20',1,42),
	(39,'39.mp4','videos/4/7/20',2,43),
	(38,'38.mp4','videos/4/7/19',1,42),
	(37,'37.mp4','videos/4/7/19',2,43),
	(36,'36.mp4','videos/4/7/18',1,42),
	(35,'35.mp4','videos/4/7/18',2,43),
	(34,'34.mp4','videos/4/7/17',1,42),
	(33,'33.mp4','videos/4/7/17',2,43),
	(32,'32.mp4','videos/4/7/16',1,42),
	(31,'31.mp4','videos/4/7/16',2,43),
	(30,'30.mp4','videos/4/7/15',1,42),
	(29,'29.mp4','videos/4/7/15',2,43),
	(28,'28.mp4','videos/4/7/14',1,42),
	(27,'27.mp4','videos/4/7/14',2,43),
	(26,'26.mp4','videos/4/6/13',1,42),
	(25,'25.mp4','videos/4/6/13',2,43),
	(41,'41.mp4','videos/4/7/21',2,43),
	(42,'42.mp4','videos/4/7/21',1,42),
	(43,'43.mp4','videos/4/7/22',2,43),
	(44,'44.mp4','videos/4/7/22',1,42),
	(45,'45.mp4','videos/4/7/23',2,43),
	(46,'46.mp4','videos/4/7/23',1,42),
	(47,'47.mp4','videos/4/7/24',2,43),
	(48,'48.mp4','videos/4/7/24',1,42),
	(49,'49.mp4','videos/4/7/25',2,43),
	(50,'50.mp4','videos/4/7/25',1,42),
	(51,'51.mp4','videos/4/7/26',2,43),
	(52,'52.mp4','videos/4/7/26',1,42),
	(53,'53.mp4','videos/4/7/27',2,43),
	(54,'54.mp4','videos/4/7/27',1,42),
	(55,'55.mp4','videos/4/7/28',2,43),
	(56,'56.mp4','videos/4/7/28',1,42),
	(57,'57.mp4','videos/4/7/29',2,43),
	(58,'58.mp4','videos/4/7/29',1,42),
	(59,'59.mp4','videos/4/7/30',2,43),
	(60,'60.mp4','videos/4/7/30',1,42),
	(61,'61.mp4','videos/4/7/31',2,43),
	(62,'62.mp4','videos/4/7/31',1,42),
	(63,'63.mp4','videos/4/7/32',2,43),
	(64,'64.mp4','videos/4/7/32',1,42),
	(65,'65.mp4','videos/4/7/33',2,43),
	(66,'66.mp4','videos/4/7/33',1,42),
	(67,'67.mp4','videos/4/8/34',2,43),
	(68,'68.mp4','videos/4/8/34',1,42),
	(69,'69.mp4','videos/4/8/35',2,43),
	(70,'70.mp4','videos/4/8/35',1,42),
	(71,'71.mp4','videos/4/8/36',2,43),
	(72,'72.mp4','videos/4/8/36',1,42),
	(73,'73.mp4','videos/4/8/37',2,43),
	(74,'74.mp4','videos/4/8/37',1,42),
	(75,'75.mp4','videos/4/8/38',2,43),
	(76,'76.mp4','videos/4/8/38',1,42),
	(77,'77.mp4','videos/4/8/39',2,43),
	(78,'78.mp4','videos/4/8/39',1,42),
	(79,'79.mp4','videos/5/9/40',2,43),
	(80,'80.mp4','videos/5/9/40',1,42),
	(81,'81.mp4','videos/5/9/41',2,43),
	(82,'82.mp4','videos/5/9/41',1,42),
	(83,'83.mp4','videos/5/13/42',2,43),
	(84,'84.mp4','videos/5/13/42',1,42),
	(85,'85.mp4','videos/5/13/43',2,43),
	(86,'86.mp4','videos/5/13/43',1,42),
	(87,'87.mp4','videos/5/12/44',0,0),
	(88,'88.mp4','videos/5/14/45',0,0),
	(89,'89.mp4','videos/5/14/46',0,0),
	(90,'90.mp4','videos/5/14/47',0,0),
	(91,'91.mp4','videos/5/14/48',0,0),
	(92,'92.mp4','videos/5/14/49',0,0),
	(93,'93.mp4','videos/5/14/50',0,0),
	(94,'94.mp4','videos/5/16/51',0,0),
	(95,'95.mp4','videos/5/16/52',0,0),
	(96,'96.mp4','videos/5/16/53',0,0),
	(97,'97.mp4','videos/5/16/54',0,0),
	(98,'98.mp4','videos/5/16/55',0,0),
	(99,'99.mp4','videos/5/16/56',0,0),
	(100,'100.mp4','videos/5/16/57',0,0),
	(101,'101.mp4','videos/5/19/58',2,43),
	(102,'102.mp4','videos/5/19/58',1,42),
	(103,'103.mp4','videos/5/19/59',2,43),
	(104,'104.mp4','videos/5/19/59',1,42),
	(105,'105.mp4','videos/5/19/60',2,43),
	(106,'106.mp4','videos/5/19/60',1,42),
	(107,'107.mp4','videos/5/9/61',1,42),
	(108,'108.mp4','videos/5/9/61',2,43),
	(109,'109.mp4','videos/6/20/62',2,43),
	(110,'110.mp4','videos/6/20/62',1,42),
	(111,'111.mp4','videos/6/20/63',2,43),
	(112,'112.mp4','videos/6/20/63',1,42),
	(113,'113.mp4','videos/9/22/64',1,42),
	(114,'114.mp4','videos/9/22/65',1,42),
	(115,'115.mp4','videos/9/22/66',1,42),
	(116,'116.mp4','videos/9/22/67',1,42),
	(117,'117.mp4','videos/9/22/68',1,42),
	(118,'118.mp4','videos/9/22/69',1,42),
	(119,'119.mp4','videos/9/22/70',1,42),
	(120,'120.mp4','videos/9/22/71',1,42),
	(121,'121.mp4','videos/9/22/72',1,42),
	(122,'122.mp4','videos/9/22/73',1,42),
	(123,'123.mp4','videos/9/22/74',1,42),
	(124,'124.mp4','videos/9/22/75',1,42),
	(125,'125.mp4','videos/9/22/76',1,42),
	(126,'126.mp4','videos/9/22/77',1,42),
	(127,'127.mp4','videos/9/22/78',1,42),
	(128,'128.mp4','videos/9/22/79',1,42),
	(129,'129.mp4','videos/9/22/80',1,42),
	(130,'130.mp4','videos/9/22/81',1,42),
	(131,'131.mp4','videos/9/22/82',1,42),
	(132,'132.mp4','videos/9/22/83',1,42),
	(133,'133.mp4','videos/9/22/84',1,42),
	(134,'134.mp4','videos/9/22/85',1,42),
	(135,'135.mp4','videos/9/22/86',1,42),
	(136,'136.mp4','videos/9/22/87',1,42),
	(137,'137.mp4','videos/9/22/88',1,42),
	(138,'138.mp4','videos/9/22/89',1,42),
	(139,'139.mp4','videos/5/14/90',0,0),
	(140,'140.mp4','videos/9/24/91',2,43),
	(141,'141.mp4','videos/9/24/92',2,43),
	(142,'142.mp4','videos/9/24/93',2,43),
	(143,'143.mp4','videos/9/24/94',2,43),
	(144,'144.mp4','videos/9/24/95',2,43),
	(145,'145.mp4','videos/9/24/96',2,43),
	(146,'146.mp4','videos/9/24/97',2,43),
	(147,'147.mp4','videos/9/24/98',2,43),
	(149,'149.mp4','videos/9/24/100',2,43),
	(150,'150.mp4','videos/9/24/101',2,43),
	(151,'151.mp4','videos/11/34/102',0,0);

/*!40000 ALTER TABLE `moment_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_part` WRITE;
/*!40000 ALTER TABLE `moment_to_part` DISABLE KEYS */;

INSERT INTO `moment_to_part` (`moment_id`, `part_id`)
VALUES
	(22,7),
	(21,7),
	(20,7),
	(19,7),
	(18,7),
	(17,7),
	(16,7),
	(15,7),
	(14,7),
	(13,6),
	(23,7),
	(24,7),
	(25,7),
	(26,7),
	(27,7),
	(28,7),
	(29,7),
	(30,7),
	(31,7),
	(32,7),
	(33,7),
	(34,8),
	(35,8),
	(36,8),
	(37,8),
	(38,8),
	(39,8),
	(40,9),
	(41,9),
	(42,13),
	(43,13),
	(44,12),
	(45,14),
	(46,14),
	(47,14),
	(48,14),
	(49,14),
	(50,14),
	(51,16),
	(52,16),
	(53,16),
	(54,16),
	(55,16),
	(56,16),
	(57,16),
	(58,19),
	(59,19),
	(60,19),
	(61,9),
	(62,20),
	(63,20),
	(64,22),
	(65,22),
	(66,22),
	(67,22),
	(68,22),
	(69,22),
	(70,22),
	(71,22),
	(72,22),
	(73,22),
	(74,22),
	(75,22),
	(76,22),
	(77,22),
	(78,22),
	(79,22),
	(80,22),
	(81,22),
	(82,22),
	(83,22),
	(84,22),
	(85,22),
	(86,22),
	(87,22),
	(88,22),
	(89,22),
	(90,14),
	(91,24),
	(92,24),
	(93,24),
	(94,24),
	(95,24),
	(96,24),
	(97,24),
	(98,24),
	(100,24),
	(101,24),
	(102,34);

/*!40000 ALTER TABLE `moment_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;

INSERT INTO `moments` (`moment_id`, `date_created`, `time_in_clip`, `lead`, `lapse`, `comment`)
VALUES
	(18,1424448302,414,5,2,''),
	(17,1424448295,407,5,2,''),
	(16,1424448283,395,5,2,''),
	(15,1424448046,158,2,5,''),
	(14,1424447974,86,5,2,''),
	(13,1424445548,36,5,1,''),
	(19,1424448314,426,5,2,''),
	(20,1424448325,437,5,2,''),
	(21,1424448333,445,2,5,''),
	(22,1424448370,482,5,2,''),
	(23,1424448394,506,5,2,''),
	(24,1424448410,522,2,5,''),
	(25,1424448696,808,2,5,''),
	(26,1424448712,824,5,2,''),
	(27,1424448730,842,2,5,''),
	(28,1424448757,869,2,5,''),
	(29,1424448808,920,2,5,''),
	(30,1424448823,925,2,5,''),
	(31,1424449641,1740,5,1,''),
	(32,1424449679,1787,5,1,''),
	(33,1424449711,1822,5,1,''),
	(34,1424449802,30,5,1,''),
	(35,1424449851,79,5,1,''),
	(36,1424449889,117,5,1,''),
	(37,1424450005,233,5,1,''),
	(38,1424450309,539,5,2,''),
	(39,1424450735,963,5,2,''),
	(40,1424453384,22,5,2,''),
	(41,1424453389,27,5,2,''),
	(42,1424538693,189,2,5,''),
	(43,1424538743,239,5,2,''),
	(44,1424539811,3,5,2,''),
	(45,1424539942,288,2,5,''),
	(46,1424539978,321,2,2,''),
	(47,1424540039,334,2,2,''),
	(48,1424540155,608,2,5,''),
	(49,1424540210,623,2,2,''),
	(50,1424540227,640,2,2,''),
	(51,1424699354,373,2,2,''),
	(52,1424699370,389,2,2,''),
	(53,1424699395,382,2,2,''),
	(54,1424699429,416,2,2,''),
	(55,1424699444,431,2,2,''),
	(56,1424699452,439,2,2,''),
	(57,1424704437,9,2,2,''),
	(58,1424704510,29,2,5,''),
	(59,1424704554,73,2,2,''),
	(60,1424704573,92,2,5,''),
	(61,1424724449,35,2,2,''),
	(62,1424725020,51,2,2,''),
	(63,1424725694,725,5,2,''),
	(64,1424890549,1351,1,8,''),
	(65,1424890625,1427,1,8,''),
	(66,1424890681,1483,1,8,''),
	(67,1424890910,1712,1,8,''),
	(68,1424890981,1783,1,8,''),
	(69,1424891012,1814,1,8,''),
	(70,1424891044,1846,1,8,''),
	(71,1424891130,1932,1,8,''),
	(72,1424891177,1979,1,8,''),
	(73,1424891209,2011,1,8,''),
	(74,1424891236,2038,1,8,''),
	(75,1424891432,2234,1,8,''),
	(76,1424891508,2310,1,8,''),
	(77,1424891561,2363,1,8,''),
	(78,1424891723,2525,1,8,''),
	(79,1424891753,2555,1,8,''),
	(80,1424891947,2749,6,3,''),
	(81,1424892107,2909,6,3,''),
	(82,1424892290,3092,1,8,''),
	(83,1424892390,3192,6,3,''),
	(84,1424892483,3285,1,8,''),
	(85,1424892544,3346,6,3,''),
	(86,1424892588,3390,1,8,''),
	(87,1424892621,3423,1,8,''),
	(88,1424892675,3477,1,8,''),
	(89,1424892824,3626,1,8,''),
	(90,1424928007,752,2,2,''),
	(91,1425496334,54,1,8,''),
	(92,1425496355,75,1,8,''),
	(93,1425496379,99,1,8,''),
	(94,1425496519,239,1,8,''),
	(95,1425496640,360,1,8,''),
	(96,1425496688,408,1,8,''),
	(97,1425496711,431,1,8,''),
	(98,1425497124,844,6,3,''),
	(100,1425497196,916,6,3,''),
	(101,1425497561,1281,6,3,''),
	(102,1428747433,0,2,10,'');

/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Test organisatie'),
	(2,'PH Vught');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `part_to_event` WRITE;
/*!40000 ALTER TABLE `part_to_event` DISABLE KEYS */;

INSERT INTO `part_to_event` (`part_id`, `event_id`)
VALUES
	(7,4),
	(9,5),
	(8,4),
	(19,5),
	(20,6),
	(12,5),
	(13,5),
	(14,5),
	(16,5),
	(21,8),
	(22,9),
	(24,9),
	(25,4),
	(27,5),
	(28,5),
	(29,5),
	(30,5),
	(31,5),
	(32,5),
	(34,11),
	(35,11);

/*!40000 ALTER TABLE `part_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`part_id`, `part_name`, `date_created`)
VALUES
	(7,'Training Deel 1','1424447861'),
	(9,'Eerste Test ','1424453351'),
	(8,'Training Deel 2','1424449757'),
	(19,'no part name','1424704457'),
	(12,'Opname Deel 1','1424465683'),
	(13,'Opname Deel 2 oud','1424466330'),
	(14,'Opnames Deel 3','1424538848'),
	(20,'no part name','1424724702'),
	(16,'Deel 4','1424698460'),
	(21,'no part name','1424785319'),
	(22,'no part name','1424889181'),
	(24,'no part name','1425496257'),
	(25,'no part name','1428567583'),
	(27,'no part name','1428674539'),
	(28,'no part name','1428674998'),
	(29,'no part name','1428680266'),
	(30,'no part name','1428680450'),
	(31,'no part name','1428681773'),
	(32,'no part name','1428711158'),
	(34,'no part name','1428747351'),
	(35,'no part name','1429889639');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `player_to_moment` WRITE;
/*!40000 ALTER TABLE `player_to_moment` DISABLE KEYS */;

INSERT INTO `player_to_moment` (`id`, `player_id`, `moment_id`, `user_id`)
VALUES
	(66,4,51,4),
	(67,4,56,4),
	(68,4,61,4),
	(69,71,63,4);

/*!40000 ALTER TABLE `player_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `player_to_team` WRITE;
/*!40000 ALTER TABLE `player_to_team` DISABLE KEYS */;

INSERT INTO `player_to_team` (`player_id`, `team_id`)
VALUES
	(84,21),
	(73,21),
	(75,21),
	(74,21),
	(83,21),
	(71,20);

/*!40000 ALTER TABLE `player_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cam_number` int(11) NOT NULL,
  `cam_id` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table shares
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shares`;

CREATE TABLE `shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `share_token` varchar(254) NOT NULL DEFAULT '',
  `visits` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `type` varchar(254) NOT NULL DEFAULT 'moment',
  `id` int(11) NOT NULL,
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_token` (`share_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shares` WRITE;
/*!40000 ALTER TABLE `shares` DISABLE KEYS */;

INSERT INTO `shares` (`share_id`, `share_token`, `visits`, `creator_id`, `type`, `id`)
VALUES
	(2,'ELkUhJBBeq',0,4,'moment',39),
	(3,'yqvJpvlMRN',0,4,'moment',36),
	(4,'ghAkGcmz5g',0,4,'moment',63);

/*!40000 ALTER TABLE `shares` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  `system_from_email` varchar(128) NOT NULL,
  `support_email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`, `system_from_email`, `support_email`)
VALUES
	(1,'Fieldback PH Vught','Fieldback PH Vught','/usr/local/bin/ffmpeg','openRTSP -I 192.168.2.2',60,'s.fransen@spinnov.com','info@fieldback.net','support@fieldback.net');

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_moment` WRITE;
/*!40000 ALTER TABLE `tag_to_moment` DISABLE KEYS */;

INSERT INTO `tag_to_moment` (`tag_id`, `moment_id`, `user_id`, `tag_name`)
VALUES
	(46,13,71,'Krachtige Afzet'),
	(44,14,4,'Afzet'),
	(46,15,4,'Krachtige Afzet'),
	(44,16,4,'Afzet'),
	(42,19,4,'Sprong'),
	(46,23,4,'Krachtige Afzet'),
	(46,31,71,'Krachtige Afzet'),
	(43,32,71,'Goed'),
	(43,37,71,'Goed'),
	(43,36,71,'Goed'),
	(45,35,71,'Hoogte'),
	(46,38,4,'Krachtige Afzet'),
	(42,39,4,'Sprong'),
	(42,41,4,'Sprong'),
	(46,42,4,'Krachtige Afzet'),
	(44,44,4,'Afzet'),
	(44,45,4,'Afzet'),
	(44,46,4,'Afzet'),
	(44,47,4,'Afzet'),
	(42,48,4,'Sprong'),
	(44,49,4,'Afzet'),
	(44,50,4,'Afzet'),
	(42,51,4,'Sprong'),
	(44,52,4,'Afzet'),
	(46,53,4,'Krachtige Afzet'),
	(42,54,4,'Sprong'),
	(44,55,4,'Afzet'),
	(42,21,4,'Sprong'),
	(42,20,4,'Sprong'),
	(44,58,4,'Afzet'),
	(44,61,4,'Afzet'),
	(45,62,4,'Hoogte'),
	(44,63,4,'Afzet'),
	(44,64,72,'Afzet'),
	(44,70,72,'Afzet'),
	(47,77,72,'Verbetering'),
	(44,78,72,'Afzet'),
	(47,84,72,'Verbetering'),
	(43,86,72,'Goed'),
	(47,88,72,'Verbetering'),
	(44,102,4,'Afzet');

/*!40000 ALTER TABLE `tag_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_organisation` WRITE;
/*!40000 ALTER TABLE `tag_to_organisation` DISABLE KEYS */;

INSERT INTO `tag_to_organisation` (`tag_id`, `organisation_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(3,2),
	(4,2),
	(5,2),
	(6,2),
	(7,2),
	(9,2),
	(11,2),
	(12,2),
	(13,2),
	(14,2),
	(17,2),
	(18,2),
	(19,2),
	(20,2),
	(21,2),
	(22,2),
	(23,2),
	(24,2),
	(25,2),
	(26,2),
	(27,2),
	(28,2),
	(29,2),
	(30,2),
	(31,2),
	(32,1),
	(37,2),
	(38,2),
	(39,2),
	(42,2),
	(43,2),
	(44,2),
	(45,2),
	(46,2),
	(47,2),
	(48,2);

/*!40000 ALTER TABLE `tag_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `tag_to_tagfield` (`tag_id`, `tagfield_id`, `left`, `top`, `label`, `original_tag_id`)
VALUES
	(0,53,13,19,'Afzet',44),
	(0,53,63,29,'Krachtige Afzet',46),
	(0,53,20,42,'Sprong',42),
	(0,52,5,20,'Hoogte',45),
	(0,52,67,13,'Goed',43),
	(0,52,64,23,'Krachtige Afzet',46),
	(0,52,5,33,'Sprong',42),
	(0,51,64,53,'Afzet',44),
	(0,51,59,30,'Hoogte',45),
	(0,51,28,51,'Krachtige Afzet',46),
	(0,51,26,39,'Goed',43),
	(0,51,28,27,'Sprong',42),
	(0,52,7,11,'Afzet',44),
	(0,53,41,37,'Hoogte',45),
	(0,54,50,46,'Verbetering',47),
	(0,54,13,9,'Hoogte',45),
	(0,54,36,13,'Goed',43),
	(0,54,16,26,'Afzet',44);

/*!40000 ALTER TABLE `tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tagfields` WRITE;
/*!40000 ALTER TABLE `tagfields` DISABLE KEYS */;

INSERT INTO `tagfields` (`tagfield_id`, `tagfield_name`, `show_players`, `date_saved`)
VALUES
	(53,'Simple Tagfield',1,1424724630),
	(52,'Simple Tagfield',1,1424445540),
	(51,'Simple Tagfield',1,1421857864),
	(54,'Verspringen',1,1424852548);

/*!40000 ALTER TABLE `tagfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`tag_id`, `tag_name`)
VALUES
	(48,'Sierlijke vlucht'),
	(47,'Verbetering'),
	(46,'Krachtige Afzet'),
	(45,'Hoogte'),
	(44,'Afzet'),
	(43,'Goed'),
	(42,'Sprong'),
	(41,'Foute Sprong'),
	(40,'Goede Sprong');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_event` WRITE;
/*!40000 ALTER TABLE `team_to_event` DISABLE KEYS */;

INSERT INTO `team_to_event` (`team_id`, `event_id`)
VALUES
	(20,4),
	(21,5),
	(20,6),
	(21,8),
	(21,9),
	(20,11);

/*!40000 ALTER TABLE `team_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(21,2),
	(20,2);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(21,'Atleten Groep 1'),
	(20,'Test Groep 1');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `track_login` WRITE;
/*!40000 ALTER TABLE `track_login` DISABLE KEYS */;

INSERT INTO `track_login` (`id`, `user_id`, `username`, `email`, `active`, `ip_address`)
VALUES
	(1,1,'admin','tim@lessormore.nl',1,'0'),
	(497,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(3,1,'admin','tim@lessormore.nl',1,'0'),
	(362,1,'admin','tim@lessormore.nl',1,'0'),
	(6,1,'admin','tim@lessormore.nl',1,'0'),
	(7,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(8,1,'admin','tim@lessormore.nl',1,'0'),
	(9,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(206,1,'admin','tim@lessormore.nl',1,'0'),
	(11,1,'admin','tim@lessormore.nl',1,'0'),
	(207,1,'admin','tim@lessormore.nl',1,'0'),
	(13,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(14,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(15,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(16,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(17,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(18,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(19,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(20,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(21,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(496,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(23,24,'irisberkvens@gmail.com','irisberkvens@gmail.com',1,'77.164.5.37'),
	(24,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(25,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(26,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(27,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(28,25,'jesse.cortooms@gmail.com','jesse.cortooms@gmail.com',1,'77.164.5.37'),
	(29,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(30,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(31,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(32,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(33,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(360,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(35,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(36,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(37,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(38,24,'irisberkvens@gmail.com','irisberkvens@gmail.com',1,'77.164.5.37'),
	(39,24,'irisberkvens@gmail.com','irisberkvens@gmail.com',1,'77.164.5.37'),
	(40,24,'irisberkvens@gmail.com','irisberkvens@gmail.com',1,'77.164.5.37'),
	(41,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(42,24,'irisberkvens@gmail.com','irisberkvens@gmail.com',1,'77.164.5.37'),
	(43,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(44,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(359,1,'admin','tim@lessormore.nl',1,'0'),
	(46,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(47,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(48,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(49,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(50,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(51,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(53,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(495,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(56,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(494,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(358,1,'admin','tim@lessormore.nl',1,'0'),
	(357,1,'admin','tim@lessormore.nl',1,'0'),
	(60,1,'admin','tim@lessormore.nl',1,'0'),
	(61,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(62,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(63,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(64,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(65,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(66,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(67,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(356,1,'admin','tim@lessormore.nl',1,'0'),
	(69,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(70,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(71,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(72,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(73,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(75,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(76,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(77,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(78,1,'admin','tim@lessormore.nl',1,'0'),
	(79,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(80,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(81,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(82,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(83,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(84,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(85,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(493,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(87,1,'admin','tim@lessormore.nl',1,'0'),
	(88,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(89,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(90,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(91,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(92,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(93,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(94,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(95,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(353,1,'admin','tim@lessormore.nl',1,'0'),
	(97,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(492,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(99,1,'admin','tim@lessormore.nl',1,'0'),
	(100,1,'admin','tim@lessormore.nl',1,'0'),
	(352,1,'admin','tim@lessormore.nl',1,'0'),
	(491,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(103,1,'admin','tim@lessormore.nl',1,'0'),
	(105,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(106,1,'admin','tim@lessormore.nl',1,'0'),
	(351,1,'admin','tim@lessormore.nl',1,'0'),
	(350,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(109,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(110,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(111,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(112,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(113,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(114,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(115,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(116,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(117,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(118,1,'admin','tim@lessormore.nl',1,'0'),
	(119,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(120,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(121,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(122,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(123,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(124,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(490,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(489,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(127,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(128,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(129,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(130,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(131,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(488,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(133,1,'admin','tim@lessormore.nl',1,'0'),
	(134,1,'admin','tim@lessormore.nl',1,'0'),
	(349,1,'admin','tim@lessormore.nl',1,'0'),
	(136,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(195,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(139,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(140,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(141,1,'admin','tim@lessormore.nl',1,'0'),
	(487,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(143,1,'admin','tim@lessormore.nl',1,'0'),
	(144,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(145,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(146,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(147,1,'admin','tim@lessormore.nl',1,'0'),
	(148,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(149,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(150,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(151,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(152,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(153,1,'admin','tim@lessormore.nl',1,'0'),
	(154,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(155,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(156,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(158,1,'admin','tim@lessormore.nl',1,'0'),
	(486,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(161,1,'admin','tim@lessormore.nl',1,'0'),
	(162,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(163,1,'admin','tim@lessormore.nl',1,'0'),
	(348,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(165,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(166,1,'admin','tim@lessormore.nl',1,'0'),
	(167,1,'admin','tim@lessormore.nl',1,'0'),
	(169,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(170,1,'admin','tim@lessormore.nl',1,'0'),
	(171,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(172,1,'admin','tim@lessormore.nl',1,'0'),
	(173,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(174,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(175,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(176,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(177,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(178,1,'admin','tim@lessormore.nl',1,'0'),
	(180,1,'admin','tim@lessormore.nl',1,'0'),
	(181,1,'admin','tim@lessormore.nl',1,'0'),
	(485,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(183,1,'admin','tim@lessormore.nl',1,'0'),
	(484,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(483,1,'admin','tim@lessormore.nl',1,'0'),
	(187,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(188,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(347,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(190,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(192,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(193,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(194,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(196,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(197,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(498,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(199,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(200,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(201,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(202,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(203,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(204,1,'admin','tim@lessormore.nl',1,'0'),
	(213,44,'tim@timscholten.nl','tim@timscholten.nl',1,'88.159.82.158'),
	(209,1,'admin','tim@lessormore.nl',1,'0'),
	(211,1,'admin','tim@lessormore.nl',1,'0'),
	(212,1,'admin','tim@lessormore.nl',1,'0'),
	(214,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(482,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(217,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(346,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(219,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(221,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(222,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(481,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(230,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(480,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(231,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(479,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(478,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(233,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(234,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(235,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(236,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(237,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(240,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(241,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(242,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(477,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(476,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(475,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(474,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(473,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(472,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(250,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(471,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(252,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(470,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(254,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(469,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(256,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(257,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(258,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(259,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(260,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(261,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(262,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(263,59,'van.verre@hetnet.nl','van.verre@hetnet.nl',1,'77.164.5.37'),
	(264,59,'van.verre@hetnet.nl','van.verre@hetnet.nl',1,'77.164.5.37'),
	(468,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(266,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(267,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(467,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(269,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(270,1,'admin','tim@lessormore.nl',1,'0'),
	(466,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(465,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(464,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(274,1,'admin','tim@lessormore.nl',1,'0'),
	(275,54,'jeanetteverberne@upcmail.nl','jeanetteverberne@upcmail.nl',1,'77.164.5.37'),
	(276,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(277,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(278,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(279,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(280,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(281,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(282,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(283,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(284,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(285,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(286,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(287,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(288,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(289,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(290,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(291,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(292,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(293,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(294,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(295,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(296,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(297,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(298,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(299,61,'j.wismans@hotmail.com','j.wismans@hotmail.com',1,'77.164.5.37'),
	(463,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(301,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(302,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(304,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(305,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(306,49,'info@gerrykantersmakelaardij.nl','info@gerrykantersmakelaardij.nl',1,'77.164.5.37'),
	(307,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(308,17,'rikpijnen@hotmail.com','rikpijnen@hotmail.com',1,'0'),
	(309,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(310,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(311,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(312,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(313,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(314,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(315,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(316,58,'rinnievanpelt@hotmail.com','rinnievanpelt@hotmail.com',1,'77.164.5.37'),
	(317,58,'rinnievanpelt@hotmail.com','rinnievanpelt@hotmail.com',1,'77.164.5.37'),
	(318,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(319,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(320,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(321,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(322,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(323,63,'sdejong1988@live.nl','sdejong1988@live.nl',1,'77.164.5.37'),
	(324,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(325,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(500,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(499,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(328,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(329,1,'admin','tim@lessormore.nl',1,'0'),
	(330,65,'bascoenen@icloud.com','bascoenen@icloud.com',1,'77.164.5.37'),
	(331,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(332,55,'jeannettehartog@hotmail.com','jeannettehartog@hotmail.com',1,'77.164.5.37'),
	(333,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(344,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(335,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(336,64,'mijobo@dse.nl','mijobo@dse.nl',1,'62.131.194.10'),
	(462,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(461,1,'admin','tim@lessormore.nl',1,'0'),
	(339,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(460,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(341,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(342,1,'admin','tim@lessormore.nl',1,'0'),
	(459,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(364,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(365,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(366,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(367,1,'admin','tim@lessormore.nl',1,'0'),
	(368,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(369,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(370,12,'j.habets10@chello.nl','j.habets10@chello.nl',1,'0'),
	(371,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(372,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(373,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(458,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(375,1,'admin','tim@lessormore.nl',1,'0'),
	(376,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(377,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(378,1,'admin','tim@lessormore.nl',1,'0'),
	(379,1,'admin','tim@lessormore.nl',1,'0'),
	(380,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(457,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(456,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(383,1,'admin','tim@lessormore.nl',1,'0'),
	(384,1,'admin','tim@lessormore.nl',1,'0'),
	(385,1,'admin','tim@lessormore.nl',1,'0'),
	(386,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(387,1,'admin','tim@lessormore.nl',1,'0'),
	(388,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(389,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(391,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(392,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(455,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(394,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(395,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(396,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(397,68,'rogr@threeships.nl','rogr@threeships.nl',1,'77.164.5.37'),
	(454,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(399,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(400,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(401,8,'geert.berkvens@gmail.com','geert.berkvens@gmail.com',1,'0'),
	(402,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(403,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(404,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(405,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(406,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(407,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(408,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(409,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(411,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(412,1,'admin','tim@lessormore.nl',1,'0'),
	(453,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(414,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(415,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(416,28,'mitchellcronin16@gmail.com','mitchellcronin16@gmail.com',1,'77.164.5.37'),
	(417,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(418,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(420,67,'edwinmeijne@gmail.com','edwinmeijne@gmail.com',1,'88.159.82.158'),
	(421,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(422,52,'frank.van.cleef@upcmail.nl','frank.van.cleef@upcmail.nl',1,'77.164.5.37'),
	(452,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(424,2,'info@fdejong.com','info@fdejong.com',1,'0'),
	(425,1,'admin','tim@lessormore.nl',1,'0'),
	(451,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(450,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(449,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(448,70,'sjeffransen+phvught@gmail.com','sjeffransen+phvught@gmail.com',1,'192.168.2.16'),
	(447,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(501,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(502,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(503,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(504,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(505,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(506,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(507,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(508,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(509,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(510,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(511,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(512,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(513,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(514,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(515,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(516,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(517,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(518,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(519,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(520,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(521,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(522,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(523,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(524,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(525,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(526,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(527,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(528,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(529,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(530,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(531,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(532,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(533,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(534,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(535,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(536,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(537,74,'valentinekortbeek@me.com','valentinekortbeek@me.com',1,'88.159.158.85'),
	(538,73,'nikki.jacobs@live.nl','nikki.jacobs@live.nl',1,'88.159.158.85'),
	(539,75,'maureenpelgrim@hotmail.com','maureenpelgrim@hotmail.com',1,'88.159.158.85'),
	(540,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(541,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(542,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(543,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(544,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(545,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(546,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(547,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(548,82,'t.kortbeek@me.com','t.kortbeek@me.com',1,'88.159.158.85'),
	(549,77,'rik@berkelbike.com','rik@berkelbike.com',1,'88.159.158.85'),
	(550,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(551,81,'bart.van.leerdam67@kpnmail.nl','bart.van.leerdam67@kpnmail.nl',1,'88.159.158.85'),
	(552,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(553,82,'t.kortbeek@me.com','t.kortbeek@me.com',1,'88.159.158.85'),
	(554,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(555,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(556,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(557,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(558,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(559,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(560,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(561,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(562,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(563,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(564,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(565,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(566,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(567,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(568,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(569,72,'robbertjanjansen@home.nl','robbertjanjansen@home.nl',1,'88.159.158.85'),
	(570,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(571,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(572,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(573,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(574,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(575,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(576,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(577,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(578,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(579,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(580,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(581,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(582,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(583,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(584,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(585,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(586,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(587,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(588,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(589,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(590,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(591,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(592,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(593,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(594,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(595,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(596,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16'),
	(597,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(598,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(599,4,'sjef@smartgoals.nl','sjef@smartgoals.nl',1,'0'),
	(600,71,'fbphvught@gmail.com','fbphvught@gmail.com',1,'192.168.2.16');

/*!40000 ALTER TABLE `track_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lead` int(11) DEFAULT NULL,
  `lapse` int(11) DEFAULT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `trigger_tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `trigger_tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `trigger_tag_to_tagfield` (`trigger_tag_id`, `tagfield_id`, `name`, `lead`, `lapse`)
VALUES
	(0,52,'Na sprong',5,1),
	(0,53,'kort',2,2),
	(0,53,'na',2,5),
	(0,53,'voor',5,2),
	(0,53,'test',2,10),
	(0,54,'Start aanloop',1,8),
	(0,54,'Afzet',6,3);

/*!40000 ALTER TABLE `trigger_tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(1,'Tim','Scholten','tim@lessormore.nl'),
	(4,'Sjef','Fransen','sjef@spinnov.nl'),
	(67,'Edwin','Meijne','edwinmeijne@gmail.com'),
	(75,'Maureen','Pelgrim','maureenpelgrim@hotmail.com'),
	(44,'Timmm','asdkjhashjkd','tim@timscholten.nl'),
	(81,'Bart','van Leerdam','bart.van.leerdam67@kpnmail.nl'),
	(80,'Frans','Kortbeek','f.kortbeek@mac.com'),
	(79,'Sam','Ballak','s.b.ballak@hotmail.com'),
	(82,'Thomas','Kortbeek','t.kortbeek@me.com'),
	(77,'Rik','Berlkemans','rik@berkelbike.com'),
	(84,'Lianne','van Krieken','Lianne_vk@live.nl'),
	(73,'Nikki','Jacobs','nikki.jacobs@live.nl'),
	(74,'Valentine','Kortbeek','valentinekortbeek@me.com'),
	(72,'Robbert Jan','Jansen','robbertjanjansen@home.nl'),
	(71,'ph','vught','fbphvught@gmail.com'),
	(70,'PH','Sjef Fransen','sjeffransen+phvught@gmail.com'),
	(83,'Olav','Donker','hans.donker@onsmail.nl');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_clip` WRITE;
/*!40000 ALTER TABLE `user_to_clip` DISABLE KEYS */;

INSERT INTO `user_to_clip` (`user_id`, `clip_id`)
VALUES
	(71,9),
	(71,10),
	(4,11),
	(4,12),
	(71,13),
	(71,14),
	(4,15),
	(4,16),
	(4,17),
	(4,18),
	(4,19),
	(4,20),
	(4,21),
	(4,22),
	(4,23),
	(4,24),
	(4,25),
	(4,26),
	(4,27),
	(4,28),
	(4,29),
	(4,30),
	(72,31),
	(81,32),
	(72,33),
	(4,34),
	(4,35),
	(4,36),
	(4,37),
	(4,38),
	(4,39),
	(4,40),
	(4,41);

/*!40000 ALTER TABLE `user_to_clip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1),
	(1,2),
	(4,1),
	(4,3),
	(4,2),
	(44,1),
	(81,2),
	(80,2),
	(79,2),
	(78,2),
	(77,2),
	(76,2),
	(75,2),
	(74,2),
	(73,2),
	(72,2),
	(67,1),
	(71,2),
	(67,2),
	(70,2),
	(82,2),
	(83,2),
	(84,2);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_tagfield` WRITE;
/*!40000 ALTER TABLE `user_to_tagfield` DISABLE KEYS */;

INSERT INTO `user_to_tagfield` (`user_id`, `tagfield_id`)
VALUES
	(4,53),
	(72,54),
	(71,52);

/*!40000 ALTER TABLE `user_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(1,20),
	(82,21),
	(4,21),
	(77,21),
	(80,20),
	(70,20),
	(79,20),
	(71,20),
	(72,21),
	(4,20),
	(81,20);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(1,1),
	(4,1),
	(67,1),
	(75,3),
	(44,3),
	(81,3),
	(80,3),
	(79,3),
	(82,3),
	(74,3),
	(77,3),
	(84,3),
	(73,3),
	(72,3),
	(71,3),
	(70,3),
	(83,3);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(1,1,0),
	(4,4,0),
	(67,67,0),
	(75,75,0),
	(44,44,0),
	(81,81,0),
	(80,80,0),
	(79,79,0),
	(82,82,0),
	(74,74,0),
	(77,77,0),
	(84,84,0),
	(73,73,0),
	(72,72,0),
	(71,71,0),
	(70,70,0),
	(83,83,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(11) DEFAULT '1',
  `api_key` varchar(64) DEFAULT NULL,
  `selected_organisation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`, `selected_organisation`)
VALUES
	(1,X'30','admin','$2y$08$6J7l5H..nAcG7/w2UCVz8.xYHCY3OnvDKGpELhucW1ReJIkpKMU9K','','tim@lessormore.nl','','',0,'y12c2iqaYdLL7Ym8pCcOeO',0,1423060807,1,'','',0,NULL,2),
	(4,X'30','sjef@smartgoals.nl','$2y$08$RhcQxvjRs8AlPDimRkp8ge98f3Intl/7avLZvXai9ZTPIgmt.eHhm','','sjef@smartgoals.nl','','',0,'pEzxBwwIXkGKb.v1njmICu',0,1431446578,1,'','',0,NULL,2),
	(72,X'38382E3135392E3135382E3835','robbertjanjansen@home.nl','$2y$08$5THTzdN4FoMdmJv7SZZRn.d7eOIqMGHKhW3QIA9ahd1h.z3X.pdwm','','robbertjanjansen@home.nl','','',0,'J4CEPp4E7nZQIDp.5VSUYu',1424360986,1426426925,1,'','',0,NULL,2),
	(67,X'38382E3135392E38322E313538','edwinmeijne@gmail.com','$2y$08$MiF74Uwxpp/R7JJJaXbedOBHUfkGB54KJ9EM6y12EiKfHXiTQ/IKG','','edwinmeijne@gmail.com','','',0,'',1416925367,1422963934,1,'','',0,NULL,2),
	(44,X'38382E3135392E38322E313538','tim@timscholten.nl','$2y$08$6QjHKvnSmR/WLWoFgEIUGewRau1Shz4tqGfCJ2UZMnwwV9GtTy6Tm','','tim@timscholten.nl','','',0,'',1415293327,1415293358,1,'','',0,NULL,NULL),
	(81,X'38382E3135392E3135382E3835','bart.van.leerdam67@kpnmail.nl','$2y$08$jn58UuD5KvATglnFNYftRuKehcyTsFy4coNgo42Hfx0mqOaivD9pS','','bart.van.leerdam67@kpnmail.nl','','',0,'',1424361187,1425030765,1,'','',0,NULL,2),
	(80,X'38382E3135392E3135382E3835','f.kortbeek@mac.com','$2y$08$7hDwOnzdUCe2/u6u3ujakODmcRE082z61nhNbCTjkd2zC6QcXnLxC','','f.kortbeek@mac.com','331ee660a50948a0c2d5c8b884d2d27f54977091','',0,'',1424361161,1424361161,0,'','',1,NULL,NULL),
	(79,X'38382E3135392E3135382E3835','s.b.ballak@hotmail.com','$2y$08$LgcaaMfDp5BieRgNL1dTqOL/6Vvmnp.YM6UgtAWjqh9ZYDeQyphy2','','s.b.ballak@hotmail.com','29dd0d22771a87c0577cbdf6fe6e4a877b712c89','',0,'',1424361136,1424361136,0,'','',1,NULL,NULL),
	(82,X'38382E3135392E3135382E3835','t.kortbeek@me.com','$2y$08$GfmyoZTIQydu1Zy1v88y0OwcwkFCWIzTFGdjzQ6kiOUICTpE1NukW','','t.kortbeek@me.com','','',0,'',1424944554,1425149108,1,'','',0,NULL,2),
	(77,X'38382E3135392E3135382E3835','rik@berkelbike.com','$2y$08$w9SjOUPNBsGII11RJSJx4eHwFFLmylVCPTMzHOMvehRfqyYLQHgi2','','rik@berkelbike.com','','',0,'',1424361092,1424982910,1,'','',0,NULL,2),
	(84,X'39322E3131302E33332E313334','Lianne_vk@live.nl','$2y$08$ITiRv5FYThQlZlGnsYZpZ.ykisgXkz3UNtsGGweQjfeCYvkydwK9W','','Lianne_vk@live.nl','0346676c91f351589ca90cabd95ee19e2a5be283','',0,'',1425580694,1425580694,0,'','',1,NULL,NULL),
	(75,X'38382E3135392E3135382E3835','maureenpelgrim@hotmail.com','$2y$08$HBF6ny10QyRVW0BEhJqISuyNqmmPBiwxlFoKjqVvDuM3Xygxevlpa','','maureenpelgrim@hotmail.com','','',0,'',1424361057,1424786452,1,'','',0,NULL,2),
	(74,X'38382E3135392E3135382E3835','valentinekortbeek@me.com','$2y$08$vhrfYXa4YoxgAPyFfmNgbOeDYQz5nsJktVv8TlOFaoNJ.jFT78KT6','','valentinekortbeek@me.com','','',0,'',1424361035,1424782358,1,'','',0,NULL,2),
	(73,X'38382E3135392E3135382E3835','nikki.jacobs@live.nl','$2y$08$OOG1tFBqYWd1iKULkICkcuzuR.Q4HLYcwPIKO0UBxkPr1WkrhL.fO','','nikki.jacobs@live.nl','','',0,'',1424361007,1424785327,1,'','',0,NULL,2),
	(71,X'3139322E3136382E322E3136','fbphvught@gmail.com','$2y$08$e6nJx8qoW2o5.9/Gx8RbJuuWxl5pFnXhdt2YLJS0E1WZ6lyaNqBJi','','fbphvught@gmail.com','','',0,'WUnLc.KYp/HZCn4UqCiNZ.',1421918819,1432546854,1,'','',0,NULL,2),
	(70,X'3139322E3136382E322E3136','sjeffransen+phvught@gmail.com','$2y$08$4807Cy9fn2wDMEEsKGdjOeRtLQJzhZkqqfhlGHU0jhSWVUPIihdJi','','sjeffransen+phvught@gmail.com','','',0,'',1421918417,1421918444,1,'','',0,NULL,2),
	(83,X'39322E3131302E33332E313334','hans.donker@onsmail.nl','$2y$08$4KXF2r4MyrUEODyTU2FVFOSlkb99qJhhlsxG51rL5Cacil77/2tf2','','hans.donker@onsmail.nl','86049168736dd552daaaa825910efdcab69e7be6','',0,'',1425580641,1425580641,0,'','',1,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(3,4,1),
	(29,44,2),
	(66,81,2),
	(65,80,2),
	(64,79,2),
	(63,78,2),
	(62,77,2),
	(61,76,2),
	(60,75,2),
	(59,74,2),
	(58,73,2),
	(57,72,2),
	(52,67,2),
	(56,71,2),
	(55,70,2),
	(67,82,2),
	(68,83,2),
	(69,84,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
