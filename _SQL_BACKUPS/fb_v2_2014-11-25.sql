# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: fb_v2
# Generation Time: 2014-11-25 14:29:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  PRIMARY KEY (`camera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`)
VALUES
	(39,'Pelco 1 Noord','rtsp://192.168.0.146/stream1',1,1,'25','1',1),
	(40,'Pelco 2 Zuid','rtsp://192.168.0.146/stream2',2,1,'25','1',1),
	(41,'Axis cam','rtsp://192.168.0.142/axis-media/media.amp',3,0,'25','1.2',0);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clip_to_part` WRITE;
/*!40000 ALTER TABLE `clip_to_part` DISABLE KEYS */;

INSERT INTO `clip_to_part` (`clip_id`, `part_id`, `cam`)
VALUES
	(251,205,1),
	(252,205,2),
	(253,206,2),
	(254,206,1),
	(255,207,2),
	(256,207,1),
	(257,208,2),
	(258,208,1),
	(259,209,2),
	(260,209,1),
	(261,210,2),
	(262,210,1),
	(263,211,3),
	(264,212,3),
	(265,213,3),
	(266,214,3),
	(267,215,2),
	(268,215,1);

/*!40000 ALTER TABLE `clip_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `clips` WRITE;
/*!40000 ALTER TABLE `clips` DISABLE KEYS */;

INSERT INTO `clips` (`clip_id`, `file_name`, `file_type`, `file_path`, `full_path`, `raw_name`, `orig_name`, `client_name`, `file_ext`, `file_size`, `date_created`, `kind`)
VALUES
	(251,'rec1-video-H264-1','video/mp4','./videos/154/205/tmp_rec/rec1-video-H264-1','./videos/154/205/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','96432817',1414486857,'recording'),
	(252,'rec2-video-H264-1','video/mp4','./videos/154/205/tmp_rec/rec2-video-H264-1','./videos/154/205/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','72439850',1414486858,'recording'),
	(253,'rec2-video-H264-1','video/mp4','./videos/155/206/tmp_rec/rec2-video-H264-1','./videos/155/206/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','57633673',1414487513,'recording'),
	(254,'rec1-video-H264-1','video/mp4','./videos/155/206/tmp_rec/rec1-video-H264-1','./videos/155/206/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','75565675',1414487514,'recording'),
	(255,'rec2-video-H264-1','video/mp4','./videos/155/207/tmp_rec/rec2-video-H264-1','./videos/155/207/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','888330928',1415969460,'recording'),
	(256,'rec1-video-H264-1','video/mp4','./videos/155/207/tmp_rec/rec1-video-H264-1','./videos/155/207/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','1184044683',1415969461,'recording'),
	(257,'rec2-video-H264-1','video/mp4','./videos/155/208/tmp_rec/rec2-video-H264-1','./videos/155/208/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','192993813',1415972214,'recording'),
	(258,'rec1-video-H264-1','video/mp4','./videos/155/208/tmp_rec/rec1-video-H264-1','./videos/155/208/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','259758520',1415972215,'recording'),
	(259,'rec2-video-H264-1','video/mp4','./videos/155/209/tmp_rec/rec2-video-H264-1','./videos/155/209/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','822413701',1415976745,'recording'),
	(260,'rec1-video-H264-1','video/mp4','./videos/155/209/tmp_rec/rec1-video-H264-1','./videos/155/209/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','1104957531',1415976746,'recording'),
	(261,'rec2-video-H264-1','video/mp4','./videos/155/210/tmp_rec/rec2-video-H264-1','./videos/155/210/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','142790195',1415980520,'recording'),
	(262,'rec1-video-H264-1','video/mp4','./videos/155/210/tmp_rec/rec1-video-H264-1','./videos/155/210/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','190166111',1415980520,'recording'),
	(263,'rec3-video-H264-1','video/mp4','./videos/155/211/tmp_rec/rec3-video-H264-1','./videos/155/211/tmp_rec/rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','mp4','1048068993',1415983345,'recording'),
	(264,'rec3-video-H264-1','video/mp4','./videos/155/212/tmp_rec/rec3-video-H264-1','./videos/155/212/tmp_rec/rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','mp4','42989748',1415983524,'recording'),
	(265,'rec3-video-H264-1','video/mp4','./videos/155/213/tmp_rec/rec3-video-H264-1','./videos/155/213/tmp_rec/rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','mp4','142644660',1415983971,'recording'),
	(266,'rec3-video-H264-1','video/mp4','./videos/155/214/tmp_rec/rec3-video-H264-1','./videos/155/214/tmp_rec/rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','rec3-video-H264-1','mp4','542681143',1415985022,'recording'),
	(267,'rec2-video-H264-1','video/mp4','./videos/154/215/tmp_rec/rec2-video-H264-1','./videos/154/215/tmp_rec/rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','rec2-video-H264-1','mp4','83708014',1416236924,'recording'),
	(268,'rec1-video-H264-1','video/mp4','./videos/154/215/tmp_rec/rec1-video-H264-1','./videos/154/215/tmp_rec/rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','rec1-video-H264-1','mp4','111756813',1416236924,'recording');

/*!40000 ALTER TABLE `clips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `creator_to_event` WRITE;
/*!40000 ALTER TABLE `creator_to_event` DISABLE KEYS */;

INSERT INTO `creator_to_event` (`creator_id`, `event_id`)
VALUES
	(1,153),
	(1,154),
	(1,155);

/*!40000 ALTER TABLE `creator_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `event_to_organisation` WRITE;
/*!40000 ALTER TABLE `event_to_organisation` DISABLE KEYS */;

INSERT INTO `event_to_organisation` (`event_id`, `organisation_id`)
VALUES
	(153,1),
	(154,1),
	(155,1);

/*!40000 ALTER TABLE `event_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`event_id`, `event_name`, `event_description`, `date_created`)
VALUES
	(153,'Camera testing','Dit event is om te testen of de cameras het doen op kantoor','1414486248'),
	(154,'Camera testing','Dit event is om te testen of de cameras het doen op kantoor','1414486273'),
	(155,'Taggen op iPad','zoals hierboven beschreven','1414487215');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`version`)
VALUES
	(2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_file_to_moment` WRITE;
/*!40000 ALTER TABLE `moment_file_to_moment` DISABLE KEYS */;

INSERT INTO `moment_file_to_moment` (`moment_file_id`, `moment_id`, `cam`)
VALUES
	(1251,659,1),
	(1250,659,2),
	(1249,658,1),
	(1248,658,2),
	(1247,657,1),
	(1246,657,2),
	(1245,656,1),
	(1243,655,3),
	(1242,654,3),
	(1241,653,3),
	(1240,652,3),
	(1239,651,3),
	(1238,650,3),
	(1237,649,3),
	(1236,648,3),
	(1235,647,3),
	(1234,646,3),
	(1233,645,3),
	(1232,644,3),
	(1231,643,3),
	(1230,642,3),
	(1244,656,2),
	(1228,640,3),
	(1227,639,3),
	(1226,638,3),
	(1225,637,3),
	(1224,636,3),
	(1223,635,3),
	(1222,630,1),
	(1221,630,2),
	(1220,629,1),
	(1219,629,2),
	(1218,628,1),
	(1217,628,2),
	(1216,627,1),
	(1215,627,2),
	(1214,626,1),
	(1213,626,2),
	(1212,625,1),
	(1211,625,2),
	(1210,624,1),
	(1209,624,2),
	(1208,623,1),
	(1207,623,2),
	(1206,622,1),
	(1205,622,2),
	(1204,621,2),
	(1203,621,1),
	(1202,620,2),
	(1201,620,1),
	(1114,574,1),
	(1115,574,2),
	(1116,575,1),
	(1117,575,2),
	(1118,576,1),
	(1119,576,2),
	(1120,577,1),
	(1121,577,2),
	(1122,578,1),
	(1123,578,2),
	(1124,579,1),
	(1125,579,2),
	(1126,580,1),
	(1127,580,2),
	(1128,581,1),
	(1129,581,2),
	(1130,582,1),
	(1131,582,2),
	(1132,583,1),
	(1133,583,2),
	(1134,584,1),
	(1135,584,2),
	(1136,585,1),
	(1137,585,2),
	(1138,586,1),
	(1139,586,2),
	(1140,587,1),
	(1141,587,2),
	(1142,588,1),
	(1143,588,2),
	(1144,589,1),
	(1145,589,2),
	(1146,590,1),
	(1147,590,2),
	(1148,591,1),
	(1149,591,2),
	(1150,592,1),
	(1151,592,2),
	(1152,593,1),
	(1153,593,2),
	(1154,594,1),
	(1155,594,2),
	(1156,595,1),
	(1157,595,2),
	(1158,596,1),
	(1159,596,2),
	(1160,597,1),
	(1161,597,2),
	(1162,598,1),
	(1163,598,2),
	(1164,599,1),
	(1165,599,2),
	(1166,600,0),
	(1167,601,1),
	(1168,601,2),
	(1169,602,1),
	(1170,602,2),
	(1171,603,0),
	(1172,604,0),
	(1173,605,0),
	(1174,606,0),
	(1175,607,1),
	(1176,607,2),
	(1177,608,1),
	(1178,608,2),
	(1179,609,1),
	(1180,609,2),
	(1181,610,1),
	(1182,610,2),
	(1183,611,1),
	(1184,611,2),
	(1185,612,1),
	(1186,612,2),
	(1187,613,1),
	(1188,613,2),
	(1189,614,1),
	(1190,614,2),
	(1191,615,1),
	(1192,615,2),
	(1193,616,1),
	(1194,616,2),
	(1195,617,1),
	(1196,617,2),
	(1200,619,2),
	(1199,619,1);

/*!40000 ALTER TABLE `moment_file_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  PRIMARY KEY (`moment_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_files` WRITE;
/*!40000 ALTER TABLE `moment_files` DISABLE KEYS */;

INSERT INTO `moment_files` (`moment_file_id`, `filename`, `path`, `cam`)
VALUES
	(1251,'1251.mp4','videos/154/215/659',1),
	(1250,'1250.mp4','videos/154/215/659',2),
	(1249,'1249.mp4','videos/154/215/658',1),
	(1248,'1248.mp4','videos/154/215/658',2),
	(1247,'1247.mp4','videos/154/215/657',1),
	(1246,'1246.mp4','videos/154/215/657',2),
	(1245,'1245.mp4','videos/154/215/656',1),
	(1243,'1243.mp4','videos/155/214/655',3),
	(1242,'1242.mp4','videos/155/214/654',3),
	(1241,'1241.mp4','videos/155/214/653',3),
	(1240,'1240.mp4','videos/155/214/652',3),
	(1239,'1239.mp4','videos/155/214/651',3),
	(1238,'1238.mp4','videos/155/214/650',3),
	(1237,'1237.mp4','videos/155/214/649',3),
	(1236,'1236.mp4','videos/155/214/648',3),
	(1235,'1235.mp4','videos/155/214/647',3),
	(1234,'1234.mp4','videos/155/214/646',3),
	(1233,'1233.mp4','videos/155/214/645',3),
	(1232,'1232.mp4','videos/155/214/644',3),
	(1231,'1231.mp4','videos/155/214/643',3),
	(1230,'1230.mp4','videos/155/214/642',3),
	(1244,'1244.mp4','videos/154/215/656',2),
	(1228,'1228.mp4','videos/155/213/640',3),
	(1227,'1227.mp4','videos/155/213/639',3),
	(1226,'1226.mp4','videos/155/212/638',3),
	(1225,'1225.mp4','videos/155/212/637',3),
	(1224,'1224.mp4','videos/155/211/636',3),
	(1223,'1223.mp4','videos/155/211/635',3),
	(1222,'1222.mp4','videos/155/206/630',1),
	(1221,'1221.mp4','videos/155/206/630',2),
	(1220,'1220.mp4','videos/155/206/629',1),
	(1219,'1219.mp4','videos/155/206/629',2),
	(1218,'1218.mp4','videos/155/206/628',1),
	(1217,'1217.mp4','videos/155/206/628',2),
	(1216,'1216.mp4','videos/155/206/627',1),
	(1215,'1215.mp4','videos/155/206/627',2),
	(1214,'1214.mp4','videos/155/206/626',1),
	(1213,'1213.mp4','videos/155/206/626',2),
	(1212,'1212.mp4','videos/155/206/625',1),
	(1211,'1211.mp4','videos/155/206/625',2),
	(1210,'1210.mp4','videos/155/206/624',1),
	(1209,'1209.mp4','videos/155/206/624',2),
	(1208,'1208.mp4','videos/155/206/623',1),
	(1207,'1207.mp4','videos/155/206/623',2),
	(1206,'1206.mp4','videos/155/206/622',1),
	(1205,'1205.mp4','videos/155/206/622',2),
	(1204,'1204.mp4','videos/154/205/621',2),
	(1203,'1203.mp4','videos/154/205/621',1),
	(1202,'1202.mp4','videos/154/205/620',2),
	(1201,'1201.mp4','videos/154/205/620',1),
	(1114,'1114.mp4','videos/145/193/574',1),
	(1115,'1115.mp4','videos/145/193/574',2),
	(1116,'1116.mp4','videos/146/194/575',1),
	(1117,'1117.mp4','videos/146/194/575',2),
	(1118,'1118.mp4','videos/145/193/576',1),
	(1119,'1119.mp4','videos/145/193/576',2),
	(1120,'1120.mp4','videos/145/193/577',1),
	(1121,'1121.mp4','videos/145/193/577',2),
	(1122,'1122.mp4','videos/145/193/578',1),
	(1123,'1123.mp4','videos/145/193/578',2),
	(1124,'1124.mp4','videos/145/193/579',1),
	(1125,'1125.mp4','videos/145/193/579',2),
	(1126,'1126.mp4','videos/145/193/580',1),
	(1127,'1127.mp4','videos/145/193/580',2),
	(1128,'1128.mp4','videos/145/193/581',1),
	(1129,'1129.mp4','videos/145/193/581',2),
	(1130,'1130.mp4','videos/145/193/582',1),
	(1131,'1131.mp4','videos/145/193/582',2),
	(1132,'1132.mp4','videos/145/193/583',1),
	(1133,'1133.mp4','videos/145/193/583',2),
	(1134,'1134.mp4','videos/145/193/584',1),
	(1135,'1135.mp4','videos/145/193/584',2),
	(1136,'1136.mp4','videos/145/193/585',1),
	(1137,'1137.mp4','videos/145/193/585',2),
	(1138,'1138.mp4','videos/145/193/586',1),
	(1139,'1139.mp4','videos/145/193/586',2),
	(1140,'1140.mp4','videos/145/193/587',1),
	(1141,'1141.mp4','videos/145/193/587',2),
	(1142,'1142.mp4','videos/145/193/588',1),
	(1143,'1143.mp4','videos/145/193/588',2),
	(1144,'1144.mp4','videos/145/193/589',1),
	(1145,'1145.mp4','videos/145/193/589',2),
	(1146,'1146.mp4','videos/145/193/590',1),
	(1147,'1147.mp4','videos/145/193/590',2),
	(1148,'1148.mp4','videos/145/193/591',1),
	(1149,'1149.mp4','videos/145/193/591',2),
	(1150,'1150.mp4','videos/145/193/592',1),
	(1151,'1151.mp4','videos/145/193/592',2),
	(1152,'1152.mp4','videos/145/193/593',1),
	(1153,'1153.mp4','videos/145/193/593',2),
	(1154,'1154.mp4','videos/145/193/594',1),
	(1155,'1155.mp4','videos/145/193/594',2),
	(1156,'1156.mp4','videos/145/193/595',1),
	(1157,'1157.mp4','videos/145/193/595',2),
	(1158,'1158.mp4','videos/137/182/596',1),
	(1159,'1159.mp4','videos/137/182/596',2),
	(1160,'1160.mp4','videos/147/195/597',1),
	(1161,'1161.mp4','videos/147/195/597',2),
	(1162,'1162.mp4','videos/147/195/598',1),
	(1163,'1163.mp4','videos/147/195/598',2),
	(1164,'1164.mp4','videos/147/195/599',1),
	(1165,'1165.mp4','videos/147/195/599',2),
	(1166,'1166.mp4','videos/104/174/600',0),
	(1167,'1167.mp4','videos/148/197/601',1),
	(1168,'1168.mp4','videos/148/197/601',2),
	(1169,'1169.mp4','videos/148/197/602',1),
	(1170,'1170.mp4','videos/148/197/602',2),
	(1171,'1171.mp4','videos/104/130/603',0),
	(1172,'1172.mp4','videos/104/188/604',0),
	(1173,'1173.mp4','videos/104/188/605',0),
	(1174,'1174.mp4','videos/104/198/606',0),
	(1175,'1175.mp4','videos/145/193/607',1),
	(1176,'1176.mp4','videos/145/193/607',2),
	(1177,'1177.mp4','videos/145/193/608',1),
	(1178,'1178.mp4','videos/145/193/608',2),
	(1179,'1179.mp4','videos/145/193/609',1),
	(1180,'1180.mp4','videos/145/193/609',2),
	(1181,'1181.mp4','videos/145/193/610',1),
	(1182,'1182.mp4','videos/145/193/610',2),
	(1183,'1183.mp4','videos/149/199/611',1),
	(1184,'1184.mp4','videos/149/199/611',2),
	(1185,'1185.mp4','videos/149/199/612',1),
	(1186,'1186.mp4','videos/149/199/612',2),
	(1187,'1187.mp4','videos/137/182/613',1),
	(1188,'1188.mp4','videos/137/182/613',2),
	(1189,'1189.mp4','videos/137/182/614',1),
	(1190,'1190.mp4','videos/137/182/614',2),
	(1191,'1191.mp4','videos/137/182/615',1),
	(1192,'1192.mp4','videos/137/182/615',2),
	(1193,'1193.mp4','videos/104/200/616',1),
	(1194,'1194.mp4','videos/104/200/616',2),
	(1195,'1195.mp4','videos/141/187/617',1),
	(1196,'1196.mp4','videos/141/187/617',2),
	(1200,'1200.mp4','videos/151/203/619',2),
	(1199,'1199.mp4','videos/151/203/619',1);

/*!40000 ALTER TABLE `moment_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moment_to_part` WRITE;
/*!40000 ALTER TABLE `moment_to_part` DISABLE KEYS */;

INSERT INTO `moment_to_part` (`moment_id`, `part_id`)
VALUES
	(620,205),
	(621,205),
	(622,206),
	(623,206),
	(624,206),
	(625,206),
	(626,206),
	(627,206),
	(628,206),
	(629,206),
	(630,206),
	(631,211),
	(632,211),
	(633,211),
	(634,211),
	(635,211),
	(636,211),
	(637,212),
	(638,212),
	(639,213),
	(640,213),
	(656,215),
	(642,214),
	(643,214),
	(644,214),
	(645,214),
	(646,214),
	(647,214),
	(648,214),
	(649,214),
	(650,214),
	(651,214),
	(652,214),
	(653,214),
	(654,214),
	(655,214),
	(657,215),
	(658,215),
	(659,215);

/*!40000 ALTER TABLE `moment_to_part` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;

INSERT INTO `moments` (`moment_id`, `date_created`, `time_in_clip`, `lead`, `lapse`, `comment`)
VALUES
	(620,1414486690,186,3,3,''),
	(621,1414486770,266,6,6,''),
	(622,1414487247,13,3,3,'mooi momentje'),
	(623,1414487487,253,6,6,''),
	(624,1414488041,124,3,3,''),
	(625,1414488136,232,3,3,''),
	(626,1414488875,2,3,3,''),
	(627,1414489051,126,6,6,''),
	(628,1414490801,231,6,6,''),
	(629,1414491142,175,3,3,''),
	(630,1414498077,258,3,3,''),
	(631,1415982710,1228,10,0,''),
	(632,1415982732,1250,10,0,''),
	(633,1415982762,1280,10,0,''),
	(634,1415982848,1366,10,0,''),
	(635,1415983131,1649,10,0,''),
	(636,1415983171,1689,10,0,''),
	(637,1415983458,17,10,0,''),
	(638,1415983485,44,10,0,''),
	(639,1415983711,10,10,0,''),
	(640,1415983729,28,10,0,''),
	(656,1416236540,38,10,0,''),
	(642,1415984074,82,10,0,''),
	(643,1415984106,114,10,0,''),
	(644,1415984136,144,10,0,''),
	(645,1415984182,200,10,0,''),
	(646,1415984350,358,10,0,''),
	(647,1415984441,449,10,0,''),
	(648,1415984684,692,10,0,''),
	(649,1415984698,706,10,0,''),
	(650,1415984705,713,10,0,''),
	(651,1415984767,775,10,0,''),
	(652,1415984864,872,10,0,''),
	(653,1415984869,877,10,0,''),
	(654,1415984957,965,10,0,''),
	(655,1415984966,974,10,0,''),
	(657,1416236551,49,10,0,''),
	(658,1416236609,107,10,0,''),
	(659,1416236898,396,10,0,'');

/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Local testing');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `part_to_event` WRITE;
/*!40000 ALTER TABLE `part_to_event` DISABLE KEYS */;

INSERT INTO `part_to_event` (`part_id`, `event_id`)
VALUES
	(205,154),
	(206,155),
	(215,154);

/*!40000 ALTER TABLE `part_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `parts` WRITE;
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;

INSERT INTO `parts` (`part_id`, `part_name`, `date_created`)
VALUES
	(205,'no part name','1414486303'),
	(206,'no name','1414487218'),
	(215,'no part name','1416236463');

/*!40000 ALTER TABLE `parts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `player_to_moment` WRITE;
/*!40000 ALTER TABLE `player_to_moment` DISABLE KEYS */;

INSERT INTO `player_to_moment` (`id`, `player_id`, `moment_id`, `user_id`)
VALUES
	(43,41,626,1),
	(42,41,627,1);

/*!40000 ALTER TABLE `player_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `player_to_team` WRITE;
/*!40000 ALTER TABLE `player_to_team` DISABLE KEYS */;

INSERT INTO `player_to_team` (`player_id`, `team_id`)
VALUES
	(41,20),
	(1,21),
	(45,21),
	(41,21);

/*!40000 ALTER TABLE `player_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cam_number` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`)
VALUES
	(1,'LOCAL','LOCAL version','/usr/local/bin/ffmpeg','/usr/local/bin/openRTSP',120,'tim@lessormore.nl');

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_moment` WRITE;
/*!40000 ALTER TABLE `tag_to_moment` DISABLE KEYS */;

INSERT INTO `tag_to_moment` (`tag_id`, `moment_id`, `user_id`, `tag_name`)
VALUES
	(40,620,1,'Midden'),
	(43,620,1,'Links'),
	(45,620,1,'Verdediging'),
	(42,621,1,'Rechts'),
	(41,621,1,'Voor'),
	(39,621,1,'Achter'),
	(40,622,1,'Midden'),
	(39,622,1,'Achter'),
	(42,623,1,'Rechts'),
	(48,623,1,'Goed moment'),
	(46,623,1,'Aanval'),
	(40,624,1,'Midden'),
	(43,624,1,'Links'),
	(41,624,1,'Voor'),
	(40,625,1,'Midden'),
	(41,625,1,'Voor'),
	(42,625,1,'Rechts'),
	(43,625,1,'Links'),
	(39,625,1,'Achter'),
	(42,626,1,'Rechts'),
	(40,626,1,'Midden'),
	(41,626,1,'Voor'),
	(41,627,1,'Voor'),
	(42,627,1,'Rechts'),
	(47,627,1,'Slecht moment'),
	(47,630,1,'Slecht moment'),
	(42,630,1,'Rechts');

/*!40000 ALTER TABLE `tag_to_moment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_organisation` WRITE;
/*!40000 ALTER TABLE `tag_to_organisation` DISABLE KEYS */;

INSERT INTO `tag_to_organisation` (`tag_id`, `organisation_id`)
VALUES
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1);

/*!40000 ALTER TABLE `tag_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `tag_to_tagfield` (`tag_id`, `tagfield_id`, `left`, `top`, `label`, `original_tag_id`)
VALUES
	(0,47,46,80,'Achter',39),
	(0,47,45,42,'Midden',40),
	(0,47,45,11,'Voor',41),
	(0,47,66,41,'Rechts',42),
	(0,47,26,41,'Links',43),
	(0,47,7,9,'Balbezit',44),
	(0,47,7,61,'Verdediging',45),
	(0,47,7,69,'Aanval',46),
	(0,47,80,63,'Slecht moment',47),
	(0,47,81,70,'Goed moment',48);

/*!40000 ALTER TABLE `tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tagfields` WRITE;
/*!40000 ALTER TABLE `tagfields` DISABLE KEYS */;

INSERT INTO `tagfields` (`tagfield_id`, `tagfield_name`, `show_players`, `date_saved`)
VALUES
	(47,'Hockey tagfield',1,1414486667);

/*!40000 ALTER TABLE `tagfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`tag_id`, `tag_name`)
VALUES
	(48,'Goed moment'),
	(47,'Slecht moment'),
	(46,'Aanval'),
	(45,'Verdediging'),
	(44,'Balbezit'),
	(43,'Links'),
	(42,'Rechts'),
	(41,'Voor'),
	(40,'Midden'),
	(39,'Achter');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_event` WRITE;
/*!40000 ALTER TABLE `team_to_event` DISABLE KEYS */;

INSERT INTO `team_to_event` (`team_id`, `event_id`)
VALUES
	(20,155),
	(20,154),
	(20,153);

/*!40000 ALTER TABLE `team_to_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(21,1),
	(20,1);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(21,'Test team 2'),
	(20,'Test team 1');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `track_login` WRITE;
/*!40000 ALTER TABLE `track_login` DISABLE KEYS */;

INSERT INTO `track_login` (`id`, `user_id`, `username`, `email`, `active`, `ip_address`)
VALUES
	(1,1,'admin','tim@lessormore.nl',1,'0'),
	(3,1,'admin','tim@lessormore.nl',1,'0'),
	(6,1,'admin','tim@lessormore.nl',1,'0'),
	(8,1,'admin','tim@lessormore.nl',1,'0'),
	(11,1,'admin','tim@lessormore.nl',1,'0'),
	(60,1,'admin','tim@lessormore.nl',1,'0'),
	(78,1,'admin','tim@lessormore.nl',1,'0'),
	(87,1,'admin','tim@lessormore.nl',1,'0'),
	(99,1,'admin','tim@lessormore.nl',1,'0'),
	(100,1,'admin','tim@lessormore.nl',1,'0'),
	(103,1,'admin','tim@lessormore.nl',1,'0'),
	(106,1,'admin','tim@lessormore.nl',1,'0'),
	(118,1,'admin','tim@lessormore.nl',1,'0'),
	(133,1,'admin','tim@lessormore.nl',1,'0'),
	(134,1,'admin','tim@lessormore.nl',1,'0'),
	(141,1,'admin','tim@lessormore.nl',1,'0'),
	(143,1,'admin','tim@lessormore.nl',1,'0'),
	(147,1,'admin','tim@lessormore.nl',1,'0'),
	(191,1,'admin','tim@lessormore.nl',1,'0'),
	(190,1,'admin','tim@lessormore.nl',1,'0'),
	(189,1,'admin','tim@lessormore.nl',1,'0'),
	(153,1,'admin','tim@lessormore.nl',1,'0'),
	(192,1,'admin','tim@lessormore.nl',1,'0'),
	(186,1,'admin','tim@lessormore.nl',1,'0'),
	(188,45,'tim@timscholten.nl','tim@timscholten.nl',1,'::1'),
	(158,1,'admin','tim@lessormore.nl',1,'0'),
	(161,1,'admin','tim@lessormore.nl',1,'0'),
	(163,1,'admin','tim@lessormore.nl',1,'0'),
	(187,45,'tim@timscholten.nl','tim@timscholten.nl',1,'::1'),
	(166,1,'admin','tim@lessormore.nl',1,'0'),
	(167,1,'admin','tim@lessormore.nl',1,'0'),
	(170,1,'admin','tim@lessormore.nl',1,'0'),
	(184,1,'admin','tim@lessormore.nl',1,'0'),
	(172,1,'admin','tim@lessormore.nl',1,'0'),
	(181,1,'admin','tim@lessormore.nl',1,'0'),
	(182,1,'admin','tim@lessormore.nl',1,'0'),
	(183,1,'admin','tim@lessormore.nl',1,'0'),
	(180,1,'admin','tim@lessormore.nl',1,'0'),
	(178,1,'admin','tim@lessormore.nl',1,'0'),
	(179,1,'admin','tim@lessormore.nl',1,'0');

/*!40000 ALTER TABLE `track_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lead` int(11) DEFAULT NULL,
  `lapse` int(11) DEFAULT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `trigger_tag_to_tagfield` WRITE;
/*!40000 ALTER TABLE `trigger_tag_to_tagfield` DISABLE KEYS */;

INSERT INTO `trigger_tag_to_tagfield` (`trigger_tag_id`, `tagfield_id`, `name`, `lead`, `lapse`)
VALUES
	(0,47,'Lang',6,6),
	(0,47,'Kort',3,3);

/*!40000 ALTER TABLE `trigger_tag_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(1,'Tim','Scholten','tim@lessormore.nl'),
	(41,'Sjef','Fransen','sjeffransen@gmail.com'),
	(45,'timmmawdkjhasdjgh','jksdfhusdjkhf','tim@timscholten.nl');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_clip` WRITE;
/*!40000 ALTER TABLE `user_to_clip` DISABLE KEYS */;

INSERT INTO `user_to_clip` (`user_id`, `clip_id`)
VALUES
	(1,251),
	(1,252),
	(1,253),
	(1,254),
	(1,255),
	(1,256),
	(1,257),
	(1,258),
	(1,259),
	(1,260),
	(1,261),
	(1,262),
	(1,263),
	(1,264),
	(1,265),
	(1,266),
	(1,267),
	(1,268);

/*!40000 ALTER TABLE `user_to_clip` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1),
	(39,1),
	(45,1),
	(44,1),
	(43,1),
	(42,1),
	(41,1),
	(40,1);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_tagfield` WRITE;
/*!40000 ALTER TABLE `user_to_tagfield` DISABLE KEYS */;

INSERT INTO `user_to_tagfield` (`user_id`, `tagfield_id`)
VALUES
	(1,47);

/*!40000 ALTER TABLE `user_to_tagfield` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(1,1),
	(1,20);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(1,1),
	(41,3),
	(45,3);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(1,1,0),
	(41,41,0),
	(45,45,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(1) NOT NULL DEFAULT '1',
  `api_key` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`)
VALUES
	(1,X'30','admin','$2y$08$2Sl8T9X12YCqyZNbUHlpY.Gr6RsrUh1e6SLDR8z.t9iKn3gZkxxce','','tim@lessormore.nl','','',0,'',0,1416919449,1,'','',0,'007'),
	(41,X'3139322E3136382E302E313133','sjeffransen@gmail.com','$2y$08$JONM22pjgDQ5jquBtV8IZeE1np8JPM3w5ibQf4LhW6RGR5IdhWac2','','sjeffransen@gmail.com','a46f9139dc2daecb984acc3481397c5e96bd3351','',0,'',1414490776,1414490776,1,'','',0,''),
	(45,X'3A3A31','tim@timscholten.nl','$2y$08$Rltu3Hy0pJrs9X8y8NcQMeT7O2Dvde/oeb97q7q7UFIBBTTWot1wC','','tim@timscholten.nl','','',0,'',1415271795,1415273113,1,'','',1,'');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(30,45,2),
	(29,44,2),
	(28,43,2),
	(27,42,2),
	(26,41,2),
	(25,40,2),
	(24,39,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
