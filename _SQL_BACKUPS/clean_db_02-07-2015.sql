# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.43-0ubuntu0.12.04.1)
# Database: scotchbox
# Generation Time: 2015-07-02 12:32:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cameras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cameras`;

CREATE TABLE `cameras` (
  `camera_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `stream` varchar(256) NOT NULL,
  `stream_live` varchar(256) NOT NULL DEFAULT '',
  `stream_live_preview` varchar(256) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `copy` int(11) NOT NULL,
  `live_enabled` int(1) NOT NULL DEFAULT '0',
  `is_instacam` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`camera_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;

INSERT INTO `cameras` (`camera_id`, `description`, `stream`, `stream_live`, `stream_live_preview`, `number`, `active`, `cam_fps`, `cam_constante`, `copy`, `live_enabled`, `is_instacam`)
VALUES
	(1,'Camera','rtsp://192.168.0.1/axis-media/media.amp','http://192.168.0.1/mjpeg.php','http://192.168.0.1/mjpeg.php?preview=true',1,1,'30','cam_1',1,0,0);

/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clip_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clip_to_part`;

CREATE TABLE `clip_to_part` (
  `clip_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table clips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clips`;

CREATE TABLE `clips` (
  `clip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `file_type` varchar(128) NOT NULL,
  `file_path` varchar(128) NOT NULL,
  `full_path` varchar(128) NOT NULL,
  `raw_name` varchar(128) NOT NULL,
  `orig_name` varchar(128) NOT NULL,
  `client_name` varchar(128) NOT NULL,
  `file_ext` varchar(128) NOT NULL,
  `file_size` varchar(128) NOT NULL,
  `date_created` int(64) NOT NULL,
  `kind` varchar(64) NOT NULL,
  PRIMARY KEY (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table creator_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creator_to_event`;

CREATE TABLE `creator_to_event` (
  `creator_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `creator_id` (`creator_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table event_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_to_organisation`;

CREATE TABLE `event_to_organisation` (
  `event_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(64) NOT NULL,
  `event_description` varchar(128) NOT NULL,
  `date_created` varchar(128) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table instacordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `instacordings`;

CREATE TABLE `instacordings` (
  `instacording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(200) NOT NULL DEFAULT '0',
  `camera_identifier` varchar(30) NOT NULL DEFAULT '0' COMMENT 'matches the cam_constante from cameras table',
  `event_id` int(11) unsigned NOT NULL DEFAULT '0',
  `part_id` int(11) unsigned NOT NULL DEFAULT '0',
  `cam` int(11) NOT NULL DEFAULT '0',
  `cam_description` varchar(50) DEFAULT NULL,
  `session_start` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instacording_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` mediumint(9) NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_file_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_file_to_moment`;

CREATE TABLE `moment_file_to_moment` (
  `moment_file_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `cam` int(11) NOT NULL,
  KEY `moment_file_id` (`moment_file_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_files`;

CREATE TABLE `moment_files` (
  `moment_file_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(32) NOT NULL,
  `path` varchar(64) NOT NULL,
  `cam` int(11) NOT NULL,
  `cam_description` varchar(256) NOT NULL DEFAULT '',
  `started_timestamp` bigint(20) DEFAULT NULL,
  `is_uploaded` tinyint(1) NOT NULL DEFAULT '1',
  `fps` int(11) DEFAULT NULL,
  PRIMARY KEY (`moment_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_to_part
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_part`;

CREATE TABLE `moment_to_part` (
  `moment_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  KEY `moment_id` (`moment_id`),
  KEY `part_id` (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moment_to_playlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moment_to_playlist`;

CREATE TABLE `moment_to_playlist` (
  `playlist_id` int(11) unsigned NOT NULL,
  `moment_id` int(11) unsigned NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `playlist_id` (`playlist_id`,`moment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table moments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moments`;

CREATE TABLE `moments` (
  `moment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` int(32) NOT NULL,
  `time_in_clip` int(11) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table opponents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opponents`;

CREATE TABLE `opponents` (
  `opponent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `official_name` varchar(64) NOT NULL,
  `name` int(64) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`opponent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table organisations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organisations`;

CREATE TABLE `organisations` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(64) NOT NULL,
  PRIMARY KEY (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;

INSERT INTO `organisations` (`organisation_id`, `organisation_name`)
VALUES
	(1,'Local testing');

/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table part_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `part_to_event`;

CREATE TABLE `part_to_event` (
  `part_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `part_id` (`part_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parts`;

CREATE TABLE `parts` (
  `part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `part_name` varchar(64) NOT NULL,
  `date_created` varchar(64) NOT NULL,
  `type` varchar(11) NOT NULL DEFAULT 'streaming',
  PRIMARY KEY (`part_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table player_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_moment`;

CREATE TABLE `player_to_moment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table player_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_to_team`;

CREATE TABLE `player_to_team` (
  `player_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table playlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlists`;

CREATE TABLE `playlists` (
  `playlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table recordings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recordings`;

CREATE TABLE `recordings` (
  `recording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cam_number` int(11) NOT NULL,
  `cam_fps` varchar(11) NOT NULL,
  `cam_constante` varchar(11) NOT NULL,
  `cam_description` varchar(256) DEFAULT '',
  `stream` varchar(56) NOT NULL,
  `copy` int(11) NOT NULL,
  `time_to_record` int(32) NOT NULL,
  `start_time` int(32) NOT NULL,
  `process_id` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table shares
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shares`;

CREATE TABLE `shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `share_token` varchar(254) NOT NULL DEFAULT '',
  `visits` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `type` varchar(254) NOT NULL DEFAULT 'moment',
  `id` int(11) NOT NULL,
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_token` (`share_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_info`;

CREATE TABLE `system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_name` varchar(64) NOT NULL,
  `system_information` longtext NOT NULL,
  `ffmpeg` varchar(128) NOT NULL,
  `openrtsp` varchar(128) NOT NULL,
  `max_rec_minutes` int(11) NOT NULL,
  `admin_email` varchar(64) NOT NULL,
  `is_online` int(1) NOT NULL DEFAULT '0',
  `system_from_email` varchar(128) NOT NULL DEFAULT 'fieldback@fieldback.net',
  `support_email` varchar(128) DEFAULT 'support@fieldback.net',
  `downloads` int(1) NOT NULL DEFAULT '0',
  `max_instacord_seconds` int(11) NOT NULL DEFAULT '10',
  `insta_url` varchar(500) DEFAULT NULL,
  `shares` tinyint(1) NOT NULL DEFAULT '0',
  `insta_key` varchar(200) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `system_info` WRITE;
/*!40000 ALTER TABLE `system_info` DISABLE KEYS */;

INSERT INTO `system_info` (`id`, `system_name`, `system_information`, `ffmpeg`, `openrtsp`, `max_rec_minutes`, `admin_email`, `is_online`, `system_from_email`, `support_email`, `downloads`, `max_instacord_seconds`, `insta_url`, `shares`, `insta_key`)
VALUES
	(1,'CLEAN','CLEAN FRESH','/usr/bin/ffmpeg','/usr/bin/openRTSP',120,'tim@lessormore.nl',0,'fieldback@fieldback.net','support@fieldback.net',0,30,'http://this_ip/insta/moment_file',1,'RANDOMKEY');

/*!40000 ALTER TABLE `system_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag_to_moment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_moment`;

CREATE TABLE `tag_to_moment` (
  `tag_id` int(11) NOT NULL,
  `moment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `moment_id` (`moment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_organisation`;

CREATE TABLE `tag_to_organisation` (
  `tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag_to_tagfield`;

CREATE TABLE `tag_to_tagfield` (
  `tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `original_tag_id` int(11) NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfield_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfield_to_organisation`;

CREATE TABLE `tagfield_to_organisation` (
  `tagfield_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `tagfield_id` (`tagfield_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagfields`;

CREATE TABLE `tagfields` (
  `tagfield_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tagfield_name` varchar(32) NOT NULL,
  `show_players` tinyint(1) NOT NULL,
  `date_saved` int(11) NOT NULL,
  PRIMARY KEY (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_to_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_event`;

CREATE TABLE `team_to_event` (
  `team_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_organisation`;

CREATE TABLE `team_to_organisation` (
  `team_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `team_to_organisation` WRITE;
/*!40000 ALTER TABLE `team_to_organisation` DISABLE KEYS */;

INSERT INTO `team_to_organisation` (`team_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `team_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_to_team_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_to_team_type`;

CREATE TABLE `team_to_team_type` (
  `team_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL,
  KEY `team_id` (`team_id`),
  KEY `team_type_id` (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table team_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_types`;

CREATE TABLE `team_types` (
  `team_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_type` varchar(64) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `team_name` varchar(64) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`team_id`, `team_name`)
VALUES
	(1,'Test Team');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table track_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_login`;

CREATE TABLE `track_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `active` int(2) NOT NULL,
  `ip_address` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table trigger_tag_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_organisation`;

CREATE TABLE `trigger_tag_to_organisation` (
  `trigger_tag_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tag_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tag_to_tagfield`;

CREATE TABLE `trigger_tag_to_tagfield` (
  `trigger_tag_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `lead` int(11) DEFAULT NULL,
  `lapse` int(11) DEFAULT NULL,
  KEY `trigger_tag_id` (`trigger_tag_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table trigger_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trigger_tags`;

CREATE TABLE `trigger_tags` (
  `trigger_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trigger_tag_name` varchar(32) NOT NULL,
  `lead` int(11) NOT NULL,
  `lapse` int(11) NOT NULL,
  PRIMARY KEY (`trigger_tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_levels`;

CREATE TABLE `user_levels` (
  `user_level_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(64) NOT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_levels` WRITE;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;

INSERT INTO `user_levels` (`user_level_id`, `level`)
VALUES
	(1,'admin'),
	(2,'org_admin'),
	(3,'user'),
	(4,'guest');

/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `user_profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`user_profile_id`, `first_name`, `last_name`, `email`)
VALUES
	(1,'Admin','Admin','admin@fieldback.net');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_clip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_clip`;

CREATE TABLE `user_to_clip` (
  `user_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `clip_id` (`clip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_to_organisation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_organisation`;

CREATE TABLE `user_to_organisation` (
  `user_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `organisation_id` (`organisation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_organisation` WRITE;
/*!40000 ALTER TABLE `user_to_organisation` DISABLE KEYS */;

INSERT INTO `user_to_organisation` (`user_id`, `organisation_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_organisation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_tagfield
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_tagfield`;

CREATE TABLE `user_to_tagfield` (
  `user_id` int(11) NOT NULL,
  `tagfield_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `tagfield_id` (`tagfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user_to_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_team`;

CREATE TABLE `user_to_team` (
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_team` WRITE;
/*!40000 ALTER TABLE `user_to_team` DISABLE KEYS */;

INSERT INTO `user_to_team` (`user_id`, `team_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_team` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_level`;

CREATE TABLE `user_to_user_level` (
  `user_id` int(11) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_level_id` (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_level` WRITE;
/*!40000 ALTER TABLE `user_to_user_level` DISABLE KEYS */;

INSERT INTO `user_to_user_level` (`user_id`, `user_level_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `user_to_user_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_to_user_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_to_user_profile`;

CREATE TABLE `user_to_user_profile` (
  `user_id` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `user_to_user_profile` WRITE;
/*!40000 ALTER TABLE `user_to_user_profile` DISABLE KEYS */;

INSERT INTO `user_to_user_profile` (`user_id`, `user_profile_id`, `deleted`)
VALUES
	(1,1,0);

/*!40000 ALTER TABLE `user_to_user_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) NOT NULL,
  `forgotten_password_time` int(11) NOT NULL,
  `remember_code` varchar(40) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `temporary_password` int(1) NOT NULL DEFAULT '1',
  `api_key` varchar(64) DEFAULT NULL,
  `selected_organisation` int(11) DEFAULT NULL,
  `selected_tagfield` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `company`, `phone`, `temporary_password`, `api_key`, `selected_organisation`, `selected_tagfield`)
VALUES
	(1,X'30','admin','$2y$08$Ur3gT56i0f3v658/IDZePeAgehbErEIB32gVcHrTowkBLLBcvIu3.','','admin@fieldback.net','','',0,'0qKv5FJevkmxrDTVynki8O',0,1435673563,1,'','',0,'007',1,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
