########################         Welcome to the DB alteration log:
#Please keep this file up to date each time you update the database structure
#You can even use this as a collector to keep track of changes before each commit and push to git (handy if changing multiple tables)
#
#
#      USE # FOR COMMENTS SO THAT YOU CAN SIMPLY COPY PASTE THE SQL INTO YOUR SQL PROGRAM !!!!!!
#
#         DATE FORMAT:   YYYY - MM - DD - NAME
#         DESCRIPTION OF UPDATE
#




#2015-02-06: Edwin
#Added cam_id at various places in database to be able to retrieve cam info

ALTER TABLE `recordings` ADD `cam_id` INT(11) NOT NULL AFTER `cam_number`;
ALTER TABLE `moment_files` ADD `cam_id` INT(11)  NOT NULL  AFTER `cam`;
ALTER TABLE `moment_file_to_moment` ADD `cam_id` INT(11)  NOT NULL  AFTER `cam`;
ALTER TABLE `clip_to_part` ADD `cam_id` INT(11)  NOT NULL  AFTER `cam`;


#2015-02-09 Edwin
# Added 'shares' table to save and collect shares

CREATE TABLE `shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `share_token` varchar(254) NOT NULL DEFAULT '',
  `visits` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `type` varchar(254) NOT NULL DEFAULT 'moment',
  `id` int(11) NOT NULL,
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `share_token` (`share_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#2015-02-25 Edwin
#// Added live_stream and live_stream_preview fields in the cameras table -> used for saving the live stream address

ALTER TABLE `cameras` ADD `stream_live` VARCHAR(256)  CHARACTER SET utf8  NOT NULL  DEFAULT ''  AFTER `stream`;
ALTER TABLE `cameras` ADD `stream_live_preview` VARCHAR(256)  CHARACTER SET utf8  NOT NULL  DEFAULT ''  AFTER `stream_live`;
ALTER TABLE `cameras` ADD `live_enabled` INT(1)  NOT NULL  DEFAULT '0'  AFTER `stream_live_preview`;

#// This piece of system info makes sure that you can place the whole system offline or online
ALTER TABLE `system_info` ADD `is_online` INT(1)  NOT NULL  DEFAULT '1'  AFTER `admin_email`;

#2015-02-25 Edwin + Tim
#// Na overleg toch besloten cam_description op te slaan ipv cam_id -> indien database geupdate op 06-02 dan deze queries uitvoeren

ALTER TABLE `moment_file_to_moment` DROP `cam_id`;
ALTER TABLE `clip_to_part` DROP `cam_id`;
ALTER TABLE `recordings` DROP `cam_id`;
ALTER TABLE `moment_files` DROP `cam_id`;
#// En dan wel:
ALTER TABLE `recordings` ADD `cam_description` VARCHAR(256) NULL  DEFAULT ''  AFTER `cam_constante`;
ALTER TABLE `moment_files` ADD `cam_description` VARCHAR(256) NULL  DEFAULT ''  AFTER `cam`;


# 2015-03-05 Tim
ALTER TABLE `system_info` ADD `system_from_email` VARCHAR(128)  NOT NULL  DEFAULT 'fieldback@fieldback.net'  AFTER `is_online`;
ALTER TABLE `system_info` ADD `support_email` VARCHAR(128)  NULL  DEFAULT 'support@fieldback.net'  AFTER `system_from_email`;


# 2015-04-14 Edwin
# BUGFIX: cam_description can now be NULL when inserted from a recording

ALTER TABLE `moment_files` CHANGE `cam_description` `cam_description` VARCHAR(256)  CHARACTER SET utf8  NOT NULL  DEFAULT '';

# 2015-04-14 Tim
# Added download on/off functionality
ALTER TABLE `system_info` ADD `downloads` INT(1) NOT NULL  DEFAULT '0' ;

# 2015-05-05 Tim
# Added selected tagfield to the user
ALTER TABLE `users` ADD `selected_tagfield` INT(11) NULL;

# 2014-06-04 Edwin
# Added started_timestamp for the insta-cam function synchronisation
# Added file_uploaded tinyInt boolean that is toggled once a file was uploaded. default to 1
ALTER TABLE `moment_files` ADD `started_timestamp` INT  NULL  DEFAULT NULL  AFTER `cam_description`;
ALTER TABLE `moment_files` ADD `is_uploaded` TINYINT(1)  NOT NULL  DEFAULT '1'  AFTER `started_timestamp`;
ALTER TABLE `moment_files` ADD `fps` INT  NULL  DEFAULT NULL  AFTER `is_uploaded`;

# 2015-05-12 Edwin
# Shares switch in system settings
ALTER TABLE `system_info` ADD `shares` TINYINT(1)  NOT NULL  DEFAULT '0';

# 2015-06-03 Edwin
# Added playlist functionality

CREATE TABLE `moment_to_playlist` (
  `playlist_id` int(11) unsigned NOT NULL,
  `moment_id` int(11) unsigned NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `playlist_id` (`playlist_id`,`moment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `playlists` (
  `playlist_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# 2015-07-02 Edwin
# Bugfix: if recording failed and it tries to insert a failed recording, you get an error that it cannot be NULL. We want NULL to be allowed on many places
ALTER TABLE `clips` CHANGE `file_name` `file_name` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `file_type` `file_type` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `file_path` `file_path` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `full_path` `full_path` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `raw_name` `raw_name` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `orig_name` `orig_name` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `client_name` `client_name` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `file_ext` `file_ext` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `file_size` `file_size` VARCHAR(128)  CHARACTER SET utf8  NULL  DEFAULT '';
ALTER TABLE `clips` CHANGE `date_created` `date_created` INT(64)  NULL;
ALTER TABLE `clips` CHANGE `kind` `kind` VARCHAR(64)  CHARACTER SET utf8  NULL  DEFAULT '';

# BUGFIXES - Forgotten SQL updates

CREATE TABLE `instacordings` (
  `instacording_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(200) NOT NULL DEFAULT '0',
  `camera_identifier` varchar(30) NOT NULL DEFAULT '0' COMMENT 'matches the cam_constante from cameras table',
  `event_id` int(11) unsigned NOT NULL DEFAULT '0',
  `part_id` int(11) unsigned NOT NULL DEFAULT '0',
  `cam` int(11) NOT NULL DEFAULT '0',
  `cam_description` varchar(50) DEFAULT NULL,
  `session_start` int(32) NOT NULL,
  `rec_owner` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instacording_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

ALTER TABLE `parts` ADD `type` varchar(11) NOT NULL DEFAULT 'streaming' AFTER `date_created`;

ALTER TABLE `cameras` ADD `live_enabled` int(1) NOT NULL DEFAULT '0';
ALTER TABLE `cameras` ADD `is_instacam` int(1) NOT NULL DEFAULT '0';

ALTER TABLE `system_info` ADD `max_instacord_seconds` int(11) NOT NULL DEFAULT '10';
ALTER TABLE `system_info` ADD `insta_url` varchar(500) DEFAULT NULL;
ALTER TABLE `system_info` ADD `insta_key` varchar(200) DEFAULT '';

# Edwin 2015-08-21 Delay after releasing the capture button too early -> useful for tagging without a trainer or other players around
ALTER TABLE `system_info` ADD `instacording_timer_lapse` INT(11)  NOT NULL  DEFAULT '2500'  AFTER `insta_url`;

# Branch feature / Sync -> master sync url
# set the sync_auth_key to an unique key, and edit the config.php file
# authorisation key to be set on master and slave system
ALTER TABLE `system_info` ADD `sync_auth_key` VARCHAR(200)  NULL  DEFAULT NULL  AFTER `insta_url`;

# 2015-08-26 Edwin
# Lots of adding of PRIMARY keys to database tables so that the Sync script works
ALTER TABLE `tag_to_tagfield` CHANGE `tag_id` `tag_id` INT(11)  NOT NULL  AUTO_INCREMENT;

ALTER TABLE `player_to_team` ADD PRIMARY KEY (`player_id`, `team_id`);
ALTER TABLE `tag_to_organisation` ADD PRIMARY KEY (`tag_id`, `organisation_id`);
ALTER TABLE `tag_to_tagfield` ADD PRIMARY KEY (`tag_id`, `tagfield_id`);
ALTER TABLE `tagfield_to_organisation` ADD PRIMARY KEY (`tagfield_id`, `organisation_id`);
ALTER TABLE `team_to_organisation` ADD PRIMARY KEY (`team_id`, `organisation_id`);
ALTER TABLE `team_to_team_type` ADD PRIMARY KEY (`team_id`, `team_type_id`);
ALTER TABLE `trigger_tag_to_organisation` ADD PRIMARY KEY (`trigger_tag_id`, `organisation_id`);
ALTER TABLE `trigger_tag_to_tagfield` ADD PRIMARY KEY (`tagfield_id`, `name`);
ALTER TABLE `user_to_organisation` ADD PRIMARY KEY (`user_id`, `organisation_id`);
ALTER TABLE `user_to_tagfield` ADD PRIMARY KEY (`user_id`, `tagfield_id`);
ALTER TABLE `user_to_team` ADD PRIMARY KEY (`user_id`, `team_id`);
ALTER TABLE `user_to_user_level` ADD PRIMARY KEY (`user_id`, `user_level_id`);
ALTER TABLE `user_to_user_profile` ADD PRIMARY KEY (`user_profile_id`, `user_id`);

# 2015-08-26 Edwin
# Last sync date in the system_info table to keep track of the last sync
ALTER TABLE `system_info` ADD `last_sync` INT(20)  NOT NULL  DEFAULT '0'  AFTER `instacording_timer_lapse`;

# 2015-08-31 Edwin
# Sync exports and Sync imports tables used in export/import scripts
CREATE TABLE `sync_exports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `export_timestamp` int(32) NOT NULL,
  `event_id` int(11) NOT NULL,
  `original_file_name` varchar(30) NOT NULL DEFAULT '',
  `unique_file_name` varchar(100) NOT NULL DEFAULT '',
  `upload_to` varchar(200) NOT NULL,
  `uploaded` tinyint(1) NOT NULL DEFAULT '0',
  `upload_timestamp` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

CREATE TABLE `sync_imports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `import_timestamp` int(32) NOT NULL,
  `unique_file_name` varchar(100) NOT NULL DEFAULT '',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

# 2015-09-03 Edwin
# Bugs solved when inserting NULL values in db
ALTER TABLE `events` CHANGE `event_description` `event_description` VARCHAR(128) NULL DEFAULT '';
