<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	
$lang['term_team'] = "Group";
$lang['term_team_l'] = "group";
$lang['term_teams'] = "Groups";
$lang['term_teams_l'] = "groups";

$lang['term_player'] = "Player";
$lang['term_player_l'] = "player";
$lang['term_players'] = "Players";
$lang['term_players_l'] = "player";



// EOF