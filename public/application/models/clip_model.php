<?php

class Clip_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'clips';
    }
    
    function add_new_clip($file_data, $user_id, $part_id, $cam = 0)
    {
    	$this->db->insert('clips', $file_data);
    	$clip_id = $this->db->insert_id();
    	$data = array(
    		'clip_id' 	=> $clip_id,
    		'part_id'	=> $part_id,
            'cam'       => $cam
    	);
    	$this->db->insert('clip_to_part', $data);
    	$data = array(
    		'user_id' 	=> $user_id,
    		'clip_id'	=> $clip_id
    	);
    	$this->db->insert('user_to_clip', $data);
    	return $clip_id;
    }
    function get_clips_by_part_id($part_id)
    {	
    	$this->db->select('*');
    	$this->db->from('clip_to_part');
    	$this->db->where('part_id', $part_id);
    	$this->db->join('clips', 'clips.clip_id = clip_to_part.clip_id');
    	$clips = $this->db->get()->result_array();
    	return $clips;
    }

    function get_clip_by_part_id_and_cam($part_id, $cam)
    {
        $this->db->where('part_id', $part_id);
        $this->db->where('cam', $cam);
        $clip = $this->db->get('clip_to_part')->row_array();
        return $clip;
    }

    function get_all_clips_for_user($user_id = null){
        if($user_id !== null){
            $this->db->select('*');
            $this->db->from('user_to_clip');
            $this->db->where('user_to_clip.user_id', $user_id);
            $this->db->join('clips', 'clips.clip_id = user_to_clip.clip_id');
            $clips = $this->db->get()->result_array();
            return $clips;
        }
        return false;
    }

    function delete_user_from_clips($user_id = null){
        if($user_id !== null){
            $clips = self::get_all_clips_for_user($user_id);
            foreach ($clips as $clip) {
                self::remove_user_from_clip($user_id, $clip['clip_id']);
            }
            return;
        }
        return false;
    }

    function remove_user_from_clip($user_id = null, $clip_id = null){
        if($user_id !== null && $clip_id !== null){
            $this->db->where('user_id', $user_id);
            $this->db->delete('user_to_clip');
            return;
        }
        return false;
    }
}

// EOF