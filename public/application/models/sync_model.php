<?php

class Sync_model extends MY_Model {
	
	function __construct() {
	    parent::__construct();
	}

    /**
     * Check if there is an entry in the database with this filename
     * which was not received yet
     * @param $unique_file_id
     * @return bool
     */
    public function get_import_timestamp($unique_file_id) {
        $import_timestamp = $this->db->select("import_timestamp")
            ->from("sync_imports")
            ->where("unique_file_name", $unique_file_id)
            ->where("received", "0")
            ->get()->result_array();
        if(!empty($import_timestamp)) {
            // If the entry was in the database, also switch the flag to received
            self::set_to_received($unique_file_id);
            return $import_timestamp[0]['import_timestamp'];
        } else {
            return 0;
        }
    }

    /**
     * Check if the batch with the import_timestamp has pending uploads that need to be received
     * @param $import_timestamp
     * @return bool
     */
    public function check_if_last($import_timestamp) {
        $imports = $this->db->select("*")
            ->from("sync_imports")
            ->where("import_timestamp", $import_timestamp)
            ->where("received", "0")
            ->get()->result_array();
        // If there is no result, it should be the last
        return empty($imports);
    }

    /**
     * Add a batch of uploads to the cue which is used by the daemon to upload
     * @param $data
     * @return object
     */
    public function add_to_upload_que($data) {
        return $this->db->insert_batch('sync_exports', $data);
    }

    /**
     * Add a batch of imports to the cue which is flagged 'received' over time
     * @param $data
     * @return object
     */
    public function add_to_import_cue($data) {
        return $this->db->insert_batch('sync_imports', $data);
    }

    /**
     * Set the received flag in the database to 1 for the unique_file_name
     * @param $unique_file_id
     */
    public function set_to_received($unique_file_id) {
        $this->db->where("unique_file_name", $unique_file_id)
            ->update("sync_imports", ["received" => 1]);
    }

    public function set_to_uploaded($unique_file_id) {
        $this->db->where("unique_file_name", $unique_file_id)
            ->update("sync_exports", [
                "uploaded"          => 1,
                "upload_timestamp"  => time()
            ]);
    }

    /**
     * Retrieve an array with the 'export' status, a list of exports and info about being uploaded or not
     * @return mixed
     */
    public function get_export_status($incomplete, $event_id) {
        $this->db->select("sync_exports.*, events.*")->from("sync_exports")->where("events.event_id", $event_id);
        if($incomplete) {
            $this->db->where("uploaded", 0);
        } else {
            $this->db->where("uploaded", 1);
        }
        $exports = $this->db->order_by("
                sync_exports.export_timestamp,
                sync_exports.event_id,
                sync_exports.uploaded,
                sync_exports.upload_timestamp
            ")
            ->join("events", "events.event_id = sync_exports.event_id", "INNER")
            ->get()->result_array();
        return $exports;
    }

    public function get_import_status() {
        $imports = $this->db->select("sync_imports.*")
            ->from("sync_imports")
            ->order_by("
                sync_imports.import_timestamp,
                sync_imports.received
            ")
            ->get()->result_array();
        return $imports;
    }

    public function get_exports() {
        $this->db->select("sync_exports.*")->from("sync_exports")->where("uploaded", 0);
        $exports = $this->db->order_by("
                sync_exports.export_timestamp,
                sync_exports.event_id,
                sync_exports.uploaded,
                sync_exports.upload_timestamp
            ")
            ->get()->result_array();
        return $exports;
    }

}