<?php

class Instacording_Model extends MY_Model {
	
	function __construct() {
	    parent::__construct();
	}

	public function get_part_ids_of_active_instacordings()
	{
		// Search for only part_id's as then we get all recordings for a single part
		$instacordings = $this->db->distinct()->select('part_id')->from('instacordings')->get()->result_array();
		return $instacordings;
	}

    public function get_active_instacordings_for_part_id($part_id)
    {
        $active_instacordings = $this->db
            ->select('*')
            ->from('instacordings')
            ->where('part_id', $part_id)
            ->get()
            ->result_array();
        return $active_instacordings;
    }

    /**
     * Get all instacordings and metadata used on the active recordings page
     *
     * @return mixed
     */
    public function get_instacordings()
    {
        $instacordings = $this->db
            ->select('*')
            ->from('instacordings')
            ->get()
            ->result_array();
        $event_ids = array();
        foreach ($instacordings as $key => &$instacording) {
            //load models
            $this->load->model('event_model');
            $this->load->model('part_model');
            // get the event info
            $event_info = $this->event_model->get_event_info_by_event_id($instacording['event_id']);

            // if multiple cams are recording multiple recordings are shown for one event
            // delete that from the array so only one instance will exist for the event
            if (in_array($event_info['event_id'], $event_ids)) {
                unset($instacordings[$key]);
            } else {
                $event_ids[] = $event_info['event_id'];
                $instacording['event_info'] = $event_info;
                $instacording['rec_owner'] = $this->user_model->get_user_by_id($instacording['rec_owner']);
                $instacording['part_info'] = $this->part_model->get_part_info_by_part_id($instacording['part_id']);
            }
        }
        return $instacordings;
    }
	
	public function create_instacording($session_id, $url, $camera_identifier, $event_id, $part_id, $cam, $cam_description, $owner_id)
	{
		$data = [
			'session_id'			=> $session_id,
			'url' 					=> $url,
			'camera_identifier' 	=> $camera_identifier,
			'event_id'				=> $event_id,
			'part_id'				=> $part_id,
            'cam'                   => $cam,
            'cam_description'       => $cam_description,
            'session_start'         => now(),
            'rec_owner'             => $owner_id
		];
		$this->db->insert('instacordings', $data);
		// Return the insert_id
		return $this->db->insert_id();
	}
	
	public function instacording_to_clip($instacording_id, $part_id, $rec_owner, $cam, $date_created)
	{
		$data = array(
			'file_name'		=>	'',
			'file_type'		=>	'',
			'file_path'		=>	'',
			'full_path'		=>	'',
			'raw_name'		=>	'',
			'orig_name'		=>	'',
			'client_name'	=>	'',
			'file_ext'		=>	'',
			'file_size'		=>	'',
			'date_created'	=>	$date_created,
			'kind'			=>	'instacording',
		);
		$this->db->insert('clips', $data);
		$clip_id = $this->db->insert_id();
		$data = array(
			'clip_id' 	=> $clip_id,
			'part_id'	=> $part_id,
			'cam'		=> $cam
		);
		$this->db->insert('clip_to_part', $data);
		$data = array(
			'user_id' 	=> $rec_owner,
			'clip_id'	=> $clip_id
		);
		$this->db->insert('user_to_clip', $data);
		$this->db->where('instacording_id', $instacording_id);
		$this->db->delete('instacordings');
        // Return clip_id for thumbnail
        return $clip_id;
	}
	
	
	
}