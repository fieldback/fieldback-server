<?php

class Share_model extends MY_Model {

    function __construct() {
        parent::__construct();
        
    }

	function create_share($type = 'moment', $id = 0, $creator_id) {
		// check if $id is numeric and not 0 - otherwise there would be nothing to share
		if(is_numeric($id) && $id != 0) {	
			// ! Check for valid types (moments, etc)
			if($type == "moment" || $type == "event") {
				// Load the token helper
				$this->load->helper('token');
				$share_token = random_text('alnum', 10); // alpha numerical token with length of 10 should give plenty of unique id's
				// Fill the data array
				$data = array(
					"share_token" => $share_token,
					"creator_id" => $creator_id, // Creator id comes from iauth, and should always be because the user should be logged in with this function
					"type" => $type,
					"id" => $id
				);
				$this->db->insert('shares', $data);
				// return the token, the insert_id is not important in this case
				return $share_token;
			} else {
				die("Non valid type!");
			}
		}
	}

	function get_share_by_token($share_token = null) {
		// Check if not null
		if($share_token !== null) {
			// Select from db and return a single row (there can only be 1)
			$this->db->select('*');
			$this->db->from('shares');
			$this->db->where('shares.share_token', $share_token);
			$share = $this->db->get()->row_array();
			return $share;
		} else {
			// Return an empty array - no share was found (check for empty at the controller)
			return array();
		}
	}
	
	/*
		This function gets all type = moment entries with the correct id (moment_id) and creator_id
		This is used to see if this user already shared this moment and clicks again on the link
		*/
	function get_moment_share_by_id_and_user($moment_id = 0, $user_id = 0) {
		// Test if numeric and not 0
		if((is_numeric($moment_id) && $moment_id != 0) && (is_numeric($user_id) && $user_id != 0)) {
			// Perform a search in the database
			$this->db->select('*');
			$this->db->from('shares');
			$this->db->where('shares.id', $moment_id);
			$this->db->where('shares.creator_id', $user_id);
			$this->db->where('shares.type', 'moment'); // IMPORTANT - this function searches only for moments
			$share = $this->db->get()->row_array();
			return $share;
		} else {
			die("no valid moment_id or user_id!");
		}
	}

}

// EOF