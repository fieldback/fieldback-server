<?php

class Tagfield_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'tagfields';
    }

    public function check_tag_by($tag_id, $tag_name) {
        $this->db->select('*');
        // Select on id and name
        $this->db->where('tag_id', $tag_id);
        $this->db->where('tag_name', $tag_name);
        $this->db->from('tags');
        $tag = $this->db->get()->result_array();
        // Return if empty true/false
        return !empty($tag);
    }

    /**
     * GETS
     */

    function get_tagfields_for_organisation($organisation_id = null)
    {
        if($organisation_id !== null){
            $this->db->select('*');
            $this->db->from('tagfield_to_organisation');
            $this->db->join('tagfields', 'tagfields.tagfield_id = tagfield_to_organisation.tagfield_id');
            $this->db->where('organisation_id', $organisation_id);
            $tagfields = $this->db->get()->result_array();
            foreach ($tagfields as &$tagfield) {
                $tagfield['trigger_tags'] = self::get_trigger_tags_for_tagfield($tagfield['tagfield_id']);
                $tagfield['tags'] = self::get_tags_for_tagfield($tagfield['tagfield_id']);
            }    
            return $tagfields;
        }
        return false;
    }
    function get_tags_for_organisation($organisation_id = null)
    {
        if($organisation_id !== null){
            $tags = $this->db->select('*')
            ->from('tag_to_organisation')
            ->join('tags', 'tags.tag_id = tag_to_organisation.tag_id')
            ->where('organisation_id', $organisation_id)
            ->order_by('tags.tag_name')
            ->get()->result_array();
            return $tags;
        }
        return false;
    }
    function get_tagfields_for_user($user_id = null)
    {
        if($user_id !== null){
        	$this->db->select('*');
        	$this->db->from('user_to_tagfield');
        	$this->db->join('tagfields', 'tagfields.tagfield_id = user_to_tagfield.tagfield_id');
        	$this->db->where('user_id', $user_id);
        	$tagfields = $this->db->get()->result_array();
        	foreach ($tagfields as &$tagfield) {
        		$tagfield['trigger_tags'] = self::get_trigger_tags_for_tagfield($tagfield['tagfield_id']);
        		$tagfield['tags'] = self::get_tags_for_tagfield($tagfield['tagfield_id']);
        	}
        	return $tagfields;
        }
        return false;
    }
   	function get_trigger_tags_for_tagfield($tagfield_id = null)
    {
        if($tagfield_id !== null){
       		$this->db->select('*');
       		$this->db->from('trigger_tag_to_tagfield');
       		$this->db->where('tagfield_id', $tagfield_id);
       		$trigger_tags = $this->db->get()->result_array();
       		return $trigger_tags;
        }
        return false;
   	}    
    function get_tagfield_info($tagfield_id = null)
    {
        if($tagfield_id !== null){
            $this->db->where('tagfield_id', $tagfield_id);
            $tagfield_info = $this->db->get('tagfields')->row_array();
            return $tagfield_info;
        }
        return false;
    }
    function get_tags_for_tagfield($tagfield_id = null)
    {
        if($tagfield_id !== null){
        	$this->db->select('tag_to_tagfield.*');
        	$this->db->from('tag_to_tagfield');
        	$this->db->where('tagfield_id', $tagfield_id);
        	$tags = $this->db->get()->result_array();
        	return $tags;
        }
        return false;
    }

    function get_selected_tagfield_for_user($user_id = null)
    {
        if($user_id !== null){
            $this->db->select('users.selected_tagfield');
            $this->db->from('users');
            $this->db->where('id', $user_id);
            $tagfield_id = $this->db->get()->row_array();
            return $tagfield_id;
        }
        return false;

    }

    function get_moments_for_tag($tag_id = null)
    {
        if($tag_id !== null){
            $this->db->select('*');
            $this->db->from('tag_to_moment');
            $this->db->where('tag_id', $tag_id);
            $this->db->join('moments', 'moments.moment_id = tag_to_moment.moment_id');
            $moments = $this->db->get()->result_array();
            return $moments;
        }
        return false;
    }

    /**
     * ADDS
     */
    
    function create_tagfield($user_id = null, $tagfield_name = null, $now = null)
    {
        if($user_id !== null){
            if($tagfield_name === null){
                $tagfield_name = 'Unnamed tagfield';
            }
            $data = array(
                'tagfield_name' => $tagfield_name,
                'show_players' => 0,
                'date_saved' => $now
            );
            $this->db->insert('tagfields', $data);
            $tagfield_id = $this->db->insert_id();

            $data = array(
                'user_id' => $user_id,
                'tagfield_id' => $tagfield_id
            );
            $this->db->insert('user_to_tagfield', $data);
            return $tagfield_id;
        }
        return false;
    }
    
    function add_tags_to_tagfield($tagfield_id = null, $tags = null)
    {
        if($tagfield_id !== null && $tags !== null){
            $this->db->where('tagfield_id', $tagfield_id);
            $this->db->delete('tag_to_tagfield');

            foreach ($tags as $tag) {
                $tag['tagfield_id'] = $tagfield_id;
                $this->db->insert('tag_to_tagfield', $tag);
            }
            return;
        }
        return false;
    }

    function add_trigger_tags_to_tagfield($tagfield_id = null, $trigger_tags = null)
    {
        if($tagfield_id !== null && $trigger_tags !== null) {
            if (!empty($trigger_tags)) {
                $this->db->where('tagfield_id', $tagfield_id);
                $this->db->delete('trigger_tag_to_tagfield');

                foreach ($trigger_tags as $trigger_tag) {
                    $trigger_tag['tagfield_id'] = $tagfield_id;
                    $this->db->insert('trigger_tag_to_tagfield', $trigger_tag);
                }
            }
            return;
        }
        return false;
    }

    function add_tag_to_organisation($organisation_id = null, $tag_info = null)
    {
        if($organisation_id !== null && $tag_info !== null){

            // insert to tags table
            $this->db->insert('tags', $tag_info);
            $tag_id = $this->db->insert_id();

            // add to organisation
            $data = array(
                'organisation_id' => $organisation_id,
                'tag_id' => $tag_id
            );
            $this->db->insert('tag_to_organisation', $data);
            return;
        }
        return false;
    }
    
    /**
     * UPDATES
     */
    
    function update_tagfield_info($tagfield_id = null, $data = null)
    {
        if($tagfield_id !== null && $data !== null){
            $this->db->where('tagfield_id', $tagfield_id);
            $this->db->update('tagfields', $data);
            return;
        }
        return false;
    }

    /**
     * Set the selected tagfield for a user
     * @param int $tagfield_id The ID of the tagfield
     * @param int $user_id     The ID of the user
     */
    function update_selected_tagfield_for_user($tagfield_id = null, $user_id = null)
    {
        if($tagfield_id !== null && $user_id !== null) {
            $this->db->where('id', $user_id);
            $this->db->update('users', array('selected_tagfield' => $tagfield_id));
            return true;
        }
        return false; 
    }

    /**
     * DELETES
     */
    
    function delete_tagfield($tagfield_id = null)
    {
        if($tagfield_id !== null){
            $this->db->where('tagfield_id', $tagfield_id);
            $this->db->delete('tagfields');
            
            $this->db->where('tagfield_id', $tagfield_id);
            $this->db->delete('tag_to_tagfield');
            return;
        }
        return false;
    }

    function delete_tag_from_organisation($organisation_id = null, $tag_id = null)
    {
        if($tag_id !== null){
            $this->db->where('tag_id', $tag_id);
            $this->db->where('organisation_id', $organisation_id);
            $this->db->delete('tag_to_organisation');
            return;
        }
        return false;
    }

    function delete_user_from_tagfields($user_id = null, $organisation_id = null){
        if($user_id !== null && $organisation_id !== null){
            $tagfields = self::get_tagfields_for_user($user_id);
            foreach ($tagfields as $tagfield) {
                self::delete_user_from_tagfield($user_id, $tagfield['tagfield_id']);
            }
            return;
        }
        return false;
    }

    function delete_user_from_tagfield($user_id = null, $tagfield_id = null){
        if($user_id !== null && $tagfield_id !== null){
            $this->db->where('user_id', $user_id);
            $this->db->where('tagfield_id', $tagfield_id);
            $this->db->delete('user_to_tagfield');
            return;
        }
        return false;
    }
}

//EOF