<?php

class Part_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'parts';
    }
    function add_part_to_event($data, $event_id)
    {
    	$this->db->insert('parts', $data);
    	$part_id = $this->db->insert_id();
    	$data = array(
    		'part_id' => $part_id,
    		'event_id' => $event_id
    	);
    	$this->db->insert('part_to_event', $data);
    	return $part_id;
    }
    function get_parts_for_event_by_event_id($event_id)
    {
    	$this->db->select('parts.*');
    	$this->db->from('part_to_event');
    	$this->db->where('part_to_event.event_id', $event_id);
    	$this->db->join('parts', 'parts.part_id = part_to_event.part_id');
        $this->db->order_by('date_created', 'DESC');
    	$parts = $this->db->get()->result_array();
    	$i = 0;
    	foreach($parts as $part){
    		$this->db->select('clips.*, clip_to_part.cam');
    		$this->db->from('clip_to_part');
    		$this->db->where('part_id', $part['part_id']);
    		$this->db->join('clips', 'clips.clip_id = clip_to_part.clip_id');
    		$clips = $this->db->get()->result_array();
    		$parts[$i]['clips'] = $clips;
    		$this->db->where('part_id', $part['part_id']);
    		$recordings = $this->db->get('recordings')->result_array();
    		$parts[$i]['recordings'] = $recordings;
            // VERY DIRTY TO PUT THIS HERE; BUT WE DO IT ANYWAY - used to check if there are recordings or instacordings
            $this->db->where('part_id', $part['part_id']);
            $instacordings = $this->db->get('instacordings')->result_array();
            $parts[$i]['instacordings'] = $instacordings;
    		$i++;
    	}
    	return $parts;
    }
    function get_part_info_by_part_id($part_id)
    {
    	$this->db->where('part_id', $part_id);
    	$part_info = $this->db->get('parts')->row_array();
    	return $part_info;
   	}

    /*
     * Check if the part is an instaPart, is used to redirect in 'clips@live_tagging
     */
    function check_if_instapart($part_id) {
        $this->db->select('type');
        $this->db->from('parts');
        $this->db->where('part_id', $part_id);
        $part = $this->db->get()->row_array();
        return ($part['type'] == "instant");
    }

    /*
     * Set part type to instant, used in the instacam library
     */
    function set_part_type_to_instant($part_id)
    {
        $this->db->where('part_id', $part_id);
        $this->db->set('type', "instant");
        $this->db->update('parts');
    }

   	function update_part_information($data, $part_id)
   	{
   		$this->db->where('part_id', $part_id);
   		$this->db->update('parts', $data);
   		return;
   	} 
    function delete_part_by_part_id($part_id)
    {
    	$this->db->where('part_id', $part_id);
    	$this->db->delete('parts');
    	
    	$this->db->where('part_id', $part_id);
    	$this->db->delete('recordings');
    	
    	$this->db->where('part_id', $part_id);
    	$this->db->delete('part_to_event');
    	return;
    } 
}

// EOF