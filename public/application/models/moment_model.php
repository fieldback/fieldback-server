<?php

class Moment_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'moments';
    }

    /**
     * GETS.
     */
    public function get_moment_info($moment_id = null)
    {
        if ($moment_id !== null) {
            $this->db->select('*');
            $this->db->from('moments');
            $this->db->join('moment_to_part', 'moment_to_part.moment_id = moments.moment_id');
            $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id');
            $this->db->join('parts', 'parts.part_id = part_to_event.part_id');
            $this->db->join('events', 'events.event_id = part_to_event.event_id');
            $this->db->join('team_to_event', 'team_to_event.event_id = events.event_id');
            $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
            $this->db->where('moments.moment_id', $moment_id);
            $moment_info = $this->db->get()->row_array();
            if (!empty($moment_info)) {
                $this->load->model('recording_model');
                $recordings = $this->recording_model->get_recordings_by_event_id_and_part_id($moment_info['event_id'], $moment_info['part_id']);
                if (!empty($recordings)) {
                    $moment_info['recording'] = '1';
                } else {
                    $moment_info['recording'] = '0';
                }
                $moment_info['tags'] = $this->moment_model->get_tags_by_moment_id($moment_id);
                $moment_info['players'] = $this->moment_model->get_players_by_moment_id($moment_id);
                $this->load->model('playlist_model');
                $moment_info['playlists'] = $this->playlist_model->get_all_playlists_for_user_flag_moment($this->data['user']->id, $moment_id);

                return $moment_info;
            }

            return false;
        }

        return false;
    }

    /*
        Get moment info like the event name and part name and moment_create date
        This is used for the 'download' function
        */
    public function get_moment_meta_info($moment_id = null)
    {
        if ($moment_id !== null) {
            $this->db->select('
            	parts.part_name,
            	events.event_name,
	        	moments.date_created
            ');
            $this->db->from('moments');
            $this->db->where('moments.moment_id', $moment_id);
            $this->db->join('moment_to_part', 'moment_to_part.moment_id = moments.moment_id', 'INNER');
            $this->db->join('parts', 'parts.part_id = moment_to_part.part_id', 'INNER');
            $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id', 'INNER');
            $this->db->join('events', 'events.event_id = part_to_event.event_id', 'INNER');
            $moment_info = $this->db->get()->row_array();

            return $moment_info;
        }

        return false;
    }

    /*
        Get part info like the event name and part name, and part_create date
        This is used for the 'download' function
        */
    public function get_part_meta_info($part_id = null)
    {
        if ($part_id !== null) {
            $this->db->select('
            	parts.*,
            	events.event_name
            ');
            $this->db->from('parts');
            $this->db->where('parts.part_id', $part_id);
            $this->db->join('part_to_event', 'part_to_event.part_id = parts.part_id', 'INNER');
            $this->db->join('events', 'events.event_id = part_to_event.event_id', 'INNER');
            $moment_info = $this->db->get()->row_array();

            return $moment_info;
        }

        return false;
    }

    public function get_all_moments_for_user_and_organisation($user_id = null, $organisation_id = null)
    {
        if ($user_id !== null && $organisation_id !== null) {
            $this->db->select('moments.*, events.event_name, events.event_id, teams.team_name');
            $this->db->from('moments');
            $this->db->order_by('moments.date_created', 'DESC');
            $this->db->join('moment_to_part', 'moment_to_part.moment_id = moments.moment_id');
            $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id');
            $this->db->join('events', 'events.event_id = part_to_event.event_id');
            $this->db->join('event_to_organisation', 'event_to_organisation.event_id = part_to_event.event_id');
            $this->db->join('team_to_event', 'team_to_event.event_id = event_to_organisation.event_id');
            $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
            //$this->db->join('user_to_team', 'user_to_team.team_id = team_to_event.team_id');
            //$this->db->where('user_to_team.user_id', $user_id);
            $this->db->where('organisation_id', $organisation_id);
            $moments = $this->db->get()->result_array();
            foreach ($moments as &$moment) {
                $moment['moment_files'] = self::get_moment_files_by_moment_id($moment['moment_id']);
                $moment['tags'] = self::get_tags_by_moment_id($moment['moment_id']);
                $moment['players'] = self::get_players_by_moment_id($moment['moment_id']);
            }

            return $moments;
        }
    }
    public function get_latest_moments_by_user_id($user_id, $organisation_id)
    {
        $this->db->select('moments.*, events.event_name, events.event_id, teams.team_name');
        $this->db->from('moments');
        $this->db->limit(5);
        $this->db->order_by('moments.date_created', 'DESC');
        $this->db->join('moment_to_part', 'moment_to_part.moment_id = moments.moment_id');
        $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id');
        $this->db->join('events', 'events.event_id = part_to_event.event_id');
        $this->db->join('event_to_organisation', 'event_to_organisation.event_id = part_to_event.event_id');
        $this->db->join('team_to_event', 'team_to_event.event_id = event_to_organisation.event_id');
        $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
        $this->db->join('user_to_team', 'user_to_team.team_id = team_to_event.team_id');
        $this->db->where('user_to_team.user_id', $user_id);
        $this->db->where('organisation_id', $organisation_id);
        $moments = $this->db->get()->result_array();
        foreach ($moments as &$moment) {
            $moment['moment_files'] = self::get_moment_files_by_moment_id($moment['moment_id']);
            $moment['tags'] = self::get_tags_by_moment_id($moment['moment_id']);
            $moment['players'] = self::get_players_by_moment_id($moment['moment_id']);
            $moment = self::get_thumbnail_for_moment($moment);
        }

        return $moments;
    }
    public function get_tags_by_moment_id($moment_id)
    {
        $this->db->select('*');
        $this->db->from('tag_to_moment');
        $this->db->where('moment_id', $moment_id);
        $tags = $this->db->get()->result_array();

        return $tags;
    }
    public function get_players_by_moment_id($moment_id)
    {
        $this->db->select('users.username, player_to_moment.*, user_profiles.*'); // IMPORTANT: NEVER DO A users.* and return the value -> gives away passwords!
        $this->db->from('player_to_moment');

        $this->db->join('users', 'users.id = player_to_moment.player_id');
        $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id');
        $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');

        $this->db->where('moment_id', $moment_id);
        $players = $this->db->get()->result_array();

        return $players;
    }

    public function get_moment_files_by_moment_id($moment_id)
    {
        $this->db->select('*');
        $this->db->from('moment_file_to_moment');
        $this->db->where('moment_file_to_moment.moment_id', $moment_id);
        $this->db->join('moment_files', 'moment_files.moment_file_id = moment_file_to_moment.moment_file_id');
        $this->db->join('moment_to_part', 'moment_to_part.moment_id = moment_file_to_moment.moment_id');
        $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id');
        $moment_files = $this->db->get()->result_array();

        return $moment_files;
    }

    public function get_moment_file_info_by_moment_file_id($moment_file_id = '')
    {
        // Get the moment file location and camera description -> used to describe the moment_clip
        // Also get some basic moment information -> the file name of a downloaded clip needs to hold this
        // Due to the database structure we need a lot of joins....
        $this->db->select('
	    	moment_files.*,
	    	events.event_name,
	    	events.event_id,
	    	parts.part_name,
	    	parts.part_id,
	    	moments.date_created AS moment_timestamp,
	    	moments.moment_id
	    ');
        $this->db->from('moment_files');
        $this->db->where('moment_files.moment_file_id', $moment_file_id);
        $this->db->join('moment_file_to_moment', 'moment_file_to_moment.moment_file_id = moment_files.moment_file_id', 'INNER');
        $this->db->join('moments', 'moments.moment_id = moment_file_to_moment.moment_id', 'INNER');
        $this->db->join('moment_to_part', 'moment_to_part.moment_id = moment_file_to_moment.moment_id', 'INNER');
        $this->db->join('parts', 'parts.part_id = moment_to_part.part_id', 'INNER');
        $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id', 'INNER');
        $this->db->join('events', 'events.event_id = part_to_event.event_id', 'INNER');
        $moment_file_info = $this->db->get()->row_array();
        //dump($this->db->last_query());
        return $moment_file_info;
    }

    public function get_moment_by_id($moment_id)
    {
        $this->db->select('*');
        $this->db->from('moments');
        $this->db->where('moment_id', $moment_id);
        $moment = $this->db->get()->row_array();
        if (!empty($moment)) {
            $moment['moment_files'] = self::get_all_moment_files_for_moment($moment['moment_id']);
            $moment['tags'] = self::get_tags_by_moment_id($moment['moment_id']);
            $moment['players'] = self::get_players_by_moment_id($moment['moment_id']);

            return $moment;
        } else {
            return false;
        }
    }

    public function get_all_moments_for_part($part_id, $order_flip = false)
    {
        $this->db->select('*');
        $this->db->from('moment_to_part');
        $this->db->where('part_id', $part_id);
        $this->db->join('moments', 'moments.moment_id = moment_to_part.moment_id');
        if ($order_flip) { // Sometimes we want to flip the order
            $this->db->order_by('moments.time_in_clip', 'DESC');
        } else {
            $this->db->order_by('moments.time_in_clip', 'ASC');
        }
        $moments = $this->db->get()->result_array();
        $i = 0;
        foreach ($moments as &$moment) {
            $moment['moment_files'] = self::get_all_moment_files_for_moment($moment['moment_id']);
            $moment['number_of_moment_files'] = count($moment['moment_files']);
            $moment['tags'] = self::get_tags_by_moment_id($moment['moment_id']);
            $moment['players'] = self::get_players_by_moment_id($moment['moment_id']);
        }

        return $moments;
    }
    public function get_all_moment_files_for_moment($moment_id)
    {
        $this->db->select('moment_files.*, moment_file_to_moment.cam');
        $this->db->from('moment_files');
        $this->db->where('moment_file_to_moment.moment_id', $moment_id);
        $this->db->join('moment_file_to_moment', 'moment_file_to_moment.moment_file_id = moment_files.moment_file_id', 'LEFT');
        $moment_files = $this->db->get()->result_array();

        return $moment_files;
    }

    public function get_tag_by_tag_id($tag_id)
    {
        $this->db->where('tag_id', $tag_id);
        $tag = $this->db->get('tags')->row_array();

        return $tag;
    }

    public function get_selected_tags_for_moment($moment_id = null)
    {
        if ($moment_id !== null) {
            $this->db->select('tag_to_moment.tag_id');
            $this->db->from('tag_to_moment');
            $this->db->where('moment_id', $moment_id);
            $selected_tags = $this->db->get()->result_array();

            return $selected_tags;
        }

        return false;
    }
    public function get_selected_players_for_moment($moment_id = null)
    {
        if ($moment_id !== null) {
            $this->db->select('player_to_moment.player_id');
            $this->db->from('player_to_moment');
            $this->db->where('moment_id', $moment_id);
            $selected_tags = $this->db->get()->result_array();

            return $selected_tags;
        }

        return false;
    }

    protected function get_thumbnail_for_moment($moment)
    {
        if (!empty($moment['moment_files'])) {
            if (file_exists('./'.$moment['moment_files'][0]['path'].'/'.$moment['moment_files'][0]['moment_file_id'].'.jpg')) {
                $moment['thumbnail'] = site_url().$moment['moment_files'][0]['path'].'/'.$moment['moment_files'][0]['moment_file_id'].'.jpg';
            } else {
                $moment['thumbnail'] = base_url('/images/360x240.gif');
            }
        } else {
            $moment['thumbnail'] = base_url('/images/360x240.gif');
        }

        return $moment;
    }

    /**
     * Search query for searching the moments.
     *
     * @param array $options array of options:
     *                       'event_id'
     *                       'part_id'
     *                       'team_id'
     *                       'playlist_id'
     *                       'date_from'
     *                       'date_to'
     *                       'tags'
     *
     * @return [type] [description]
     */
    public function search_moments($options = array())
    {
        $this->db->distinct();
        //get all the moments with the information to filter on
        $this->db->select('moments.*, events.event_name, events.event_id, teams.team_name, parts.part_name');
        $this->db->from('moments');
        // Number of INNER joins -> the joins should be present in the database or return empty when data is corrupt -> otherwise you get a lot of errors in the pages
        $this->db->join('moment_to_part', 'moment_to_part.moment_id = moments.moment_id', 'inner');
        $this->db->join('part_to_event', 'part_to_event.part_id = moment_to_part.part_id', 'inner');
        $this->db->join('parts', 'parts.part_id = part_to_event.part_id', 'inner');
        $this->db->join('events', 'events.event_id = part_to_event.event_id', 'inner');
        $this->db->join('event_to_organisation', 'event_to_organisation.event_id = part_to_event.event_id', 'inner');
        $this->db->join('team_to_event', 'team_to_event.event_id = event_to_organisation.event_id', 'inner');
        $this->db->join('teams', 'teams.team_id = team_to_event.team_id', 'inner');
        // Joins with left join - users can be a player of team or a user of team (use where_or statement)
        // Also, playlists and tags are not a necessity
        $this->db->join('player_to_team', 'player_to_team.team_id = teams.team_id', 'LEFT');
        $this->db->join('user_to_team', 'user_to_team.team_id = teams.team_id', 'LEFT');
        $this->db->join('tag_to_moment', 'tag_to_moment.moment_id = moments.moment_id', 'LEFT');
        $this->db->join('player_to_moment', 'player_to_moment.moment_id = moments.moment_id', 'LEFT');
        $this->db->join('moment_to_playlist', 'moment_to_playlist.moment_id = moments.moment_id', 'LEFT');
        $this->db->where('organisation_id', $this->session->userdata('organisation_id'));

        // Make sure the user is player or user of the moments that belong to the team of the event (see only your own events/moments)
        $this->db->where('(
        	`player_to_team`.`player_id` = '.$this->data['user']->user_id.' OR
			`user_to_team`.`user_id` = '.$this->data['user']->user_id.'
		)');

        //set the events filter if selected
        if (!empty($options['events'])) {
            $this->db->where_in('events.event_id', $options['events']);
        }
        //set the teams filter if selected
        if (!empty($options['teams'])) {
            $this->db->where_in('teams.team_id', $options['teams']);
        }
        //set the playlist filter if selected
        if (!empty($options['playlists'])) {
            $this->db->where_in('moment_to_playlist.playlist_id', $options['playlists']);
            // If a single playlist id is given, also order by this playlist order
            if (count($options['playlists']) == 1) {
                $this->db->order_by('moment_to_playlist.order');
            }
        }
        // Set the tags filter if selected
        if (!empty($options['tags'])) {
            $this->db->where_in('tag_to_moment.tag_id', $options['tags']);
        }
        // Set the player tag filter if selected
        if (!empty($options['players'])) {
            $this->db->where_in('player_to_moment.player_id', $options['players']);
        }

        //set the part filter if selected
        if (!empty($options['part_ids'])) {
            $this->db->where_in('part_to_event.part_id', $options['part_ids']);
        }

        //set the from filter if selected
        if (array_key_exists('date_from', $options) && $options['date_from'] !== false) {
            $this->db->where('moments.date_created >=', $options['date_from']);
        }
        //set the to filter if selected
        if (array_key_exists('date_to', $options) && $options['date_to'] !== false) {
            $this->db->where('moments.date_created <=', $options['date_to']);
        }

        $this->db->order_by('moments.date_created', 'DESC');

        //get all the moments!
        $moments = $this->db->get()->result_array(); //end of query build
        // DEBUG: comment when going live
        // echo $this->db->last_query();

        // Load the playlist model as we use this to get playlist info
        $this->load->model('playlist_model');

        //add information to moments to filter on
        foreach ($moments as $key => &$moment) {
            $moment['moment_files'] = self::get_moment_files_by_moment_id($moment['moment_id']);
            $moment['number_of_moment_files'] = count($moment['moment_files']);
            $moment['tags'] = self::get_tags_by_moment_id($moment['moment_id']);
            // $moment['tag_ids'] = array_column($moment['tags'], 'tag_id');
            // $moment['player_ids'] = array_column($moment['players'], 'id');
            $moment['players'] = self::get_players_by_moment_id($moment['moment_id']);
            $moment['playlists'] = $this->playlist_model->get_all_playlists_for_user_and_moment($this->data['user']->user_id, $moment['moment_id']);
        }

        /*
        //check if filter tags in moment
        foreach($options['tags'] as $filter_tag) {
            //decide whether the moment can stay or not
            // TODO: why do we get all moments and delete results after that.... This produces overhead!
            // Use a where_in query instead!
            $moments = array_filter($moments, function($moment) use ($filter_tag){
                return in_array($filter_tag, $moment['tag_ids'], true);
            });
        }

        //check if filter players in moment
        foreach($options['players'] as $filter_player) {
            //decide whether the moment can stay or not
            // TODO: why do we get all moments and delete results after that.... This produces overhead!
            // Use a where_in query instead!
            $moments = array_filter($moments, function($moment) use ($filter_player){
                return in_array($filter_player, $moment['player_ids'], true);
            });
        }
        */

        return $moments;
    }

    /**
     * CREATES / ADDS.
     */
    public function create_moment($data, $part_id)
    {
        $this->db->insert('moments', $data);
        $moment_id = $this->db->insert_id();

        $data = array(
            'moment_id' => $moment_id,
            'part_id' => $part_id,
        );
        $this->db->insert('moment_to_part', $data);

        return $moment_id;
    }
    public function add_moment_file($data, $moment_id)
    {
        $this->db->insert('moment_files', $data);
        $moment_file_id = $this->db->insert_id();

        $data = array(
            'moment_id' => $moment_id,
            'moment_file_id' => $moment_file_id,
            'cam' => $data['cam'],
        );
        $this->db->insert('moment_file_to_moment', $data);

        $this->db->where('moment_file_id', $moment_file_id);
        $data = array(
            'filename' => "$moment_file_id.mp4",
        );
        $this->db->update('moment_files', $data);

        return $moment_file_id;
    }
    public function add_tag_to_moment($data)
    {
        $this->db->insert('tag_to_moment', $data);

        return;
    }

    public function add_player_to_moment($data)
    {
        $this->db->insert('player_to_moment', $data);

        return;
    }

    /**
     * UPDATES.
     */
    public function set_uploaded_info_of($moment_file_id, $fps, $timestamp)
    {
        $this->db->where('moment_file_id', $moment_file_id)
            ->update('moment_files', [
                'fps' => $fps,
                'started_timestamp' => $timestamp,
                'is_uploaded' => 1,
            ]);
    }

    public function update_moment_by_moment_id($moment_id, $data)
    {
        if ($moment_id !== null && $data !== null) {
            $this->db->where('moment_id', $moment_id);
            $this->db->update('moments', $data);

            return;
        }

        return false;
    }

    /**
     * DELETES.
     */
    public function delete_moment($moment_id = null)
    {
        if ($moment_id !== null) {
            //get moment_files and delete them
            $moment_files = self::get_moment_files_by_moment_id($moment_id);
            foreach ($moment_files as $moment_file) {
                self::delete_moment_file_by_moment_file_id($moment_file['moment_file_id']);
            }

            //delete tags related to the moment
            $tags = self::get_tags_by_moment_id($moment_id);
            foreach ($tags as $tag) {
                $this->db->where('moment_id', $moment_id);
                $this->db->delete('tag_to_moment');
            }

            //delete the moment_file and moment connection
            $this->db->where('moment_id', $moment_id);
            $this->db->delete('moment_file_to_moment');

            //delete the moment to part connection
            $this->db->where('moment_id', $moment_id);
            $this->db->delete('moment_to_part');

            // delete the moment to playlist connection
            $this->db->where('moment_id', $moment_id);
            $this->db->delete('moment_to_playlist');

            //delete the moment from the database
            $this->db->where('moment_id', $moment_id);
            $this->db->delete('moments');

            return;
        }

        return false;
    }

    public function delete_moment_file_by_moment_file_id($moment_file_id = null)
    {
        if ($moment_file_id !== null) {
            $this->db->where('moment_file_id', $moment_file_id);
            $this->db->delete('moment_files');

            return;
        }

        return false;
    }

    public function delete_tag_from_moment($moment_id = null, $tag_id = null)
    {
        if ($moment_id !== null && $tag_id !== null) {
            $this->db->where('moment_id', $moment_id);
            $this->db->where('tag_id', $tag_id);
            $this->db->delete('tag_to_moment');

            return;
        }

        return false;
    }

    public function delete_user_from_moments($user_id = null, $organisation_id = null)
    {
        if ($user_id !== null && $organisation_id !== null) {
            $moments = self::get_all_moments_for_user_and_organisation($user_id, $organisation_id);
            foreach ($moments as $moment) {
                self::delete_player_from_moment($moment['moment_id'], $user_id);
            }

            return;
        }

        return false;
    }

    public function delete_player_from_moment($moment_id = null, $player_id = null)
    {
        if ($moment_id !== null && $player_id !== null) {
            $this->db->where('moment_id', $moment_id);
            $this->db->where('player_id', $player_id);
            $this->db->delete('player_to_moment');

            return;
        }

        return false;
    }

    public function delete_tags_from_moment($moment_id = null)
    {
        if ($moment_id !== null) {
            $this->db->where('moment_id', $moment_id);
            $this->db->delete('tag_to_moment');

            return;
        }

        return false;
    }
}

// EOF

