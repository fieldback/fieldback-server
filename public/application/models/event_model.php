<?php

class Event_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'system_info';
    }

    function get_all_events_for_player($user_id = null , $organisation_id = null, $limit = null)
    {
        if($user_id !== null && $organisation_id !== null){
            $this->db->select('events.*, teams.*');
            
            $this->db->from('events');
            
            $this->db->join('event_to_organisation', 'event_to_organisation.event_id = events.event_id');
            $this->db->join('team_to_event', 'team_to_event.event_id = events.event_id');
            $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
            $this->db->join('player_to_team', 'player_to_team.team_id = teams.team_id');

            $this->db->order_by('events.date_created', 'DESC');

            $this->db->where('player_id', $user_id);
            $this->db->where('organisation_id', $organisation_id);

            if($limit !== null){
                $this->db->limit($limit);
            }

            $events = $this->db->get()->result_array();

            foreach($events as &$event){
                $event['role'] = 'player';
                $event['creator'] = self::get_creator_for_event($event['event_id']);
            }

            return $events;
        }
        return false;
    }

    function get_all_events_for_owner($user_id = null, $organisation_id = null, $limit = null)
    {
        if($user_id !== null && $organisation_id !== null){
            $this->db->select('events.*, teams.*');

            $this->db->from('events');

            $this->db->join('event_to_organisation', 'event_to_organisation.event_id = events.event_id');
            $this->db->join('team_to_event', 'team_to_event.event_id = events.event_id');
            $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
            $this->db->join('user_to_team', 'user_to_team.team_id = teams.team_id');

            $this->db->order_by('events.date_created', 'DESC');

            $this->db->where('user_to_team.user_id', $user_id);
            $this->db->where('organisation_id', $organisation_id);

            if($limit !== null){
                $this->db->limit($limit);
            }

            $events = $this->db->get()->result_array();

            foreach($events as &$event){
                $event['role'] = 'owner';
                $event['creator'] = self::get_creator_for_event($event['event_id']);
            }
            return $events;
        }
        return false;
    }

    function get_latest_events_by_user_id($user_id, $organisation_id)
    {
        if($user_id !== null || $organisation_id !== null){
        	$this->db->select('events.*, creator_to_event.creator_id, teams.team_name, teams.team_id');
        	$this->db->where('organisation_id', $organisation_id);
            $this->db->where('user_to_team.user_id', $user_id);
            $this->db->or_where('player_to_team.player_id', $user_id);
        	$this->db->from('creator_to_event');
        	$this->db->join('events', 'events.event_id = creator_to_event.event_id');
            $this->db->join('event_to_organisation', 'event_to_organisation.event_id = events.event_id');
        	$this->db->join('team_to_event', 'team_to_event.event_id = events.event_id');
            $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
            $this->db->join('player_to_team', 'player_to_team.team_id = teams.team_id');
            $this->db->join('user_to_team', 'user_to_team.team_id = teams.team_id');
            $this->db->order_by('date_created', 'DESC');
        	$this->db->limit(5);
        	$latest_events = $this->db->get()->result_array();
        	return $latest_events;
        }
        return false;
   	}

   	function get_all_events_by_user_id($user_id, $organisation_id)
   	{
   		$this->db->select('events.*, creator_to_event.creator_id');
   		$this->db->where('creator_id', $user_id);
        $this->db->where('organisation_id', $organisation_id);
   		$this->db->from('creator_to_event');
   		$this->db->join('events', 'events.event_id = creator_to_event.event_id');
        $this->db->join('event_to_organisation', 'event_to_organisation.event_id = events.event_id');
   		$this->db->order_by('date_created', 'DESC');
   		$events = $this->db->get()->result_array();
        foreach($events as &$event){
            $event['creator'] = $this->user_model->get_user_by_id($event['creator_id']);
        }
   		return $events;
   	}

    function get_all_events_by_organisation($organisation_id)
    {
        $this->db->select('events.*, teams.*');
        $this->db->from('event_to_organisation');
        $this->db->where('event_to_organisation.organisation_id', $organisation_id);
        $this->db->join('events', 'events.event_id = event_to_organisation.event_id');
        $this->db->join('creator_to_event', 'creator_to_event.event_id = events.event_id');
        $this->db->join('team_to_event', 'team_to_event.event_id = events.event_id');
        $this->db->join('teams', 'teams.team_id = team_to_event.team_id');

        $this->db->order_by('events.date_created', 'DESC');
        $events = $this->db->get()->result_array();
        foreach ($events as &$event) {
            $event['creator'] = self::get_creator_for_event($event['event_id']);
        }
        return $events;
    }

    function get_event_info_by_event_id($event_id)
    {
        $this->db->select('events.*, organisations.organisation_name, organisations.organisation_id, users.username, users.id AS user_id');
        $this->db->where('events.event_id', $event_id);
        $this->db->from('events');
        $this->db->join('creator_to_event', 'creator_to_event.event_id = events.event_id');
        $this->db->join('users', 'users.id = creator_to_event.creator_id');
        $this->db->join('event_to_organisation', 'event_to_organisation.event_id = events.event_id');
        $this->db->join('organisations', 'organisations.organisation_id = event_to_organisation.organisation_id');
        $event_info = $this->db->get()->row_array();
        // Test to see if empty -> only then fill in the rest
        if(!empty($event_info)) {
	        $event_info['team'] = self::get_team_for_event($event_id);
	        if(!empty($event_info['team']['team_id'])){
	            $event_info['players'] = $this->user_model->get_players_for_team($event_info['team']['team_id'], true);
	        }
			$event_info['creator'] = self::get_creator_for_event($event_id);
        }
        // Return, even if empty
        return $event_info;
    }
    function get_team_for_event($event_id){
        $this->db->select('*');
        $this->db->where('event_id', $event_id);
        $this->db->from('team_to_event');
        $this->db->join('teams', 'teams.team_id = team_to_event.team_id');
        $team = $this->db->get()->row_array();
        return $team;
    }

    function get_creator_for_event($event_id)
    {
        $this->db->select('user_profiles.*');
        $this->db->from('creator_to_event');
        $this->db->join('users', 'users.id = creator_to_event.creator_id');
        $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id');
        $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');
        $this->db->where('event_id', $event_id);
        $owner = $this->db->get()->row_array();
        return $owner;

    }
    function add_event($now)
    {
    	$data = array(
    		'event_name' => "set event name",
    		'event_description' => "set description",
    		'date_created' => $now
    	);
    	$this->db->insert('events', $data);
    	$event_id = $this->db->insert_id();
    	$data = array(
    		'creator_id' => $this->data['user']->id,
    		'event_id'	=> $event_id
    	);
    	$this->db->insert('creator_to_event', $data);
    	$data = array(
    		'event_id' => $event_id,
    		'organisation_id' => $this->session->userdata('organisation_id')
    	);
    	$this->db->insert('event_to_organisation', $data);
    	return $event_id;
    }

    /**
     * 'Import' an event - for the import/sync option
     * @param $event_name
     * @param $event_description
     * @param $date_created
     * @param $creator_id
     * @param $team_id
     * @param $organisation_id
     */
    function import_new_event($event_name, $event_description, $date_created, $creator_id, $team_id, $organisation_id) {
        // Add event
        $data = array(
            'event_name' => $event_name,
            'event_description' => $event_description,
            'date_created' => $date_created
        );
        $this->db->insert('events', $data);
        $new_event_id = $this->db->insert_id();

        // add creator to event
        $data = array(
            'creator_id' => $creator_id,
            'event_id' => $new_event_id
        );
        $this->db->insert('creator_to_event', $data);

        //add event to organisation
        $data = array(
            'event_id' => $new_event_id,
            'organisation_id' => $organisation_id
        );
        $this->db->insert('event_to_organisation', $data);

        // add team to event
        self::add_team_to_event($new_event_id, $team_id);

        // return new event id
        return $new_event_id;
    }

    function create_event($event_info = null, $team_id = null, $event_owner = null)
    {
        if($event_info !== null && $team_id !== null)
        {   
            // add the event to the database
            $data = array(
                'event_name' => $event_info['event_name'],
                'event_description' => $event_info['event_description'],
                'date_created' => $event_info['date_created']
            );
            $this->db->insert('events', $data);
            $event_id = $this->db->insert_id();

            // check if the event_owner is set...
            if($event_owner === null){
                $event_owner = $this->data['user']->id;
            }

            // add creator to event
            $data = array(
                'creator_id' => $event_owner,
                'event_id' => $event_id
            );
            $this->db->insert('creator_to_event', $data);

            //add event to organisation
            $data = array(
                'event_id' => $event_id,
                'organisation_id' => $this->data['user']->selected_organisation['organisation_id']
            );
            $this->db->insert('event_to_organisation', $data);

            // add team to event
            self::add_team_to_event($event_id, $team_id);

            return $event_id;
        }
        return false;
    }

    function add_team_to_event($event_id, $team_id){
        $data = array(
            'team_id' => $team_id,
            'event_id' => $event_id
        );
        $this->db->insert('team_to_event', $data);
        return;
    }  
    function update_team_to_event($event_id, $team_id){
        $data = array(
            'team_id' => $team_id
        );
        $this->db->where('event_id', $event_id);
        $this->db->update('team_to_event', $data);
        return;
    }
    
    
    function delete_event_by_event_id($event_id = null)
    {
        if($event_id !== null){

            //delete the creator to event
            $this->db->where('event_id', $event_id);
            $this->db->delete('creator_to_event');

            //delete the team to event
            $this->db->where('event_id', $event_id);
            $this->db->delete('team_to_event');

            //delete event to organisation
            $this->db->where('event_id', $event_id);
            $this->db->delete('event_to_organisation');

            //get the parts for the event
            $this->db->where('event_id', $event_id);
            $parts = $this->db->get('part_to_event')->result_array();

            foreach ($parts as $part) {
                
                //get the moments for the part
                $this->db->where('part_id', $part['part_id']);
                $moments = $this->db->get('moment_to_part')->result_array();

				// Delete all moment_to_part entries with this part_id
				$this->db->where('part_id', $part['part_id']);
				$this->db->delete('moment_to_part');

                //delete the moments
                foreach($moments as $moment){
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('moments');
                    
                    // delete the tag_to_moment
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('tag_to_moment');
                    
                    // delete the player_to_moment
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('player_to_moment');
                    
                    // delete the tag_to_moments
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('tag_to_moment');
                    
                    // Get the moment_files that belong to this moment
                    $this->db->where('moment_id', $moment['moment_id']);
                    $moment_files = $this->db->get('moment_file_to_moment')->result_array();
                    
                    // Delete all moment_file_to_moments with this moment_id
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('moment_file_to_moment');

                    // Loop through the moment_file_to_moment array and delete all moment files
                    foreach($moment_files as $moment_file) {
	                    // Delete from moment_file
		                $this->db->where('moment_file_id', $moment_file['moment_file_id']);
		                $this->db->delete('moment_files');
                    }
                    
                    // delete the moment_to_part (which was used to get into this loop)
                    $this->db->where('moment_id', $moment['moment_id']);
                    $this->db->delete('moment_to_part');

                }
                
                // Get all clips for this part
                $this->db->where('part_id', $part['part_id']);
                $clips = $this->db->get('clip_to_part')->result_array();
                
                // Delete all clip_to_part with this part_id
                $this->db->where('part_id', $part['part_id']);
                $this->db->delete('clip_to_part');
                
                foreach($clips as $clip) {
	                // Delete all user_to_clip with this clip_id
	                $this->db->where('clip_id', $clip['clip_id']);
	                $this->db->delete('user_to_clip');

	                // Delete all clips with this clip_id
	                $this->db->where('clip_id', $clip['clip_id']);
	                $this->db->delete('clips');
                }

                //delete the parts
                $this->db->where('part_id', $part['part_id']);
                $this->db->delete('parts');
            }

            // delete the parts to event connection
            $this->db->where('event_id', $event_id);
            $this->db->delete('part_to_event');

            //delete the event
        	$this->db->where('event_id', $event_id);
        	$this->db->delete('events');
        	return;
        }
        return false;
    }   

    function update_event_information($data, $event_id)
    {
    	$this->db->where('event_id', $event_id);
    	$this->db->update('events', $data);
    	return;
    }

    /**
     * CHECKS
     */
    
    function check_if_event_has_team($event_id){
        $this->db->where('event_id', $event_id);
        $team = $this->db->get('team_to_event')->row_array();
        if(empty($team)){
            return false;
        }else{
            return true;
        }
    }

    function check_if_owner_of_event($user_id, $event_id){
        $this->db->select('*');
        $this->db->from('creator_to_event');
        $this->db->where('creator_id', $user_id);
        $this->db->where('event_id', $event_id);
        $output = $this->db->get()->row_array();
        return $output;
    }

    function check_if_owner_of_events($user_id){
        $this->db->select('*');
        $this->db->from('creator_to_event');
        $this->db->where('creator_id', $user_id);
        $output = $this->db->get()->row_array();
        return $output;
    }
}

// EOF