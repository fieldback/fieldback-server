<?php

class Playlist_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'playlists';
    }

    function is_moment_in_playlist($moment_id, $playlist_id)
    {
        $this->db->select('moment_to_playlist.playlist_id')->from('moment_to_playlist')
            ->where('moment_to_playlist.moment_id', $moment_id)
            ->where('moment_to_playlist.playlist_id', $playlist_id);
        return ($this->db->get()->num_rows() > 0) ? true : false;
    }

    /*
     * Get all playlists for a user -> useful on many pages and received through ajax
     *
     * @param int $user_id
     * return array         playlists
     */
    function get_all_playlists_for_user($user_id)
    {
        $this->db->select('playlists.*, COUNT(moment_to_playlist.playlist_id) as number_of_moments')->from('playlists')
            ->join('moment_to_playlist', 'moment_to_playlist.playlist_id = playlists.playlist_id', 'LEFT') // LEFT join to include playlists without moments
            ->where('playlists.owner_id', $user_id)
            ->group_by('playlists.playlist_id');
        $playlists = $this->db->get()->result_array();
        return $playlists;
    }

    function get_playlist_by_id($playlist_id)
    {
        $this->db->select('playlists.*')->from('playlists')
            ->where('playlists.playlist_id', $playlist_id);
        $playlist = $this->db->get()->row_array();
        return $playlist;
    }

    /*
     * Get all playlists for a moment of a specific user -> received through ajax as this is easier
     *
     * @param int $user_id
     * @param int $moment_id
     * return array             playlists
     */
    function get_all_playlists_for_user_and_moment($user_id, $moment_id)
    {
        $this->db->select('playlists.*')->from('playlists')
            ->join('moment_to_playlist', 'moment_to_playlist.playlist_id = playlists.playlist_id', 'INNER') // INNER -> the playlist needs to have a moment otherwise it wouldn't be in it
            ->where('moment_to_playlist.moment_id', $moment_id)
            ->where('playlists.owner_id', $user_id);
        $playlists = $this->db->get()->result_array();
        return $playlists;
    }

    /*
     * Get all playlists of a the currently logged in user and flag all playlists that have that moment_id (the has_moment entry)
     *
     * @param $user_id      int
     * @param $moment_id    int
     *
     * return array                 playlists
     */
    function get_all_playlists_for_user_flag_moment($user_id, $moment_id)
    {
        $this->db->select('playlists.*')->from('playlists')
        ->where('playlists.owner_id', $user_id);
        $playlists = $this->db->get()->result_array();
        foreach($playlists as &$playlist) {
            $playlist['has_moment'] = self::is_moment_in_playlist($moment_id, $playlist['playlist_id']);
        }

        return $playlists;
    }

    /**
     * Get the 'user_id' of the playlist owner -> used for authorisation
     *
     * @param int $playlist_id
     * @return bool|int         user_id
     */
    function get_playlist_owner($playlist_id)
    {
        $this->db->select('owner_id')
            ->from('playlists')
            ->where('playlist_id', $playlist_id);
        $playlist = $this->db->get()->row_array();
        if(!empty($playlist)) return $playlist['owner_id'];
        return false;
    }

    /**
     * Create a new playlist
     *
     * @param $title    String  Title of new playlist
     * @param $user_id  int     User ID of owner
     * @return int|bool         Inserted ID or false
     */
    function create_playlist($title, $user_id)
    {
        $data = array(
            'owner_id'  => $user_id,
            'title'     => $title
        );
        if($this->db->insert('playlists', $data)) {
            return [
                'playlist_id'       => $this->db->insert_id(),
                'title'             => $title,
                'number_of_moments' => 0
            ];
        } else {
            return false;
        }
    }

    function set_title_of_playlist($title, $playlist_id)
    {
        $this->db->update('playlists', ['title' => $title], ['playlist_id' => $playlist_id]);
        return $this->db->affected_rows();
    }

    function switch_order($moment_cur, $moment_new, $playlist_id)
    {
        // Get order numbers for the moments
        $cur_order = $this->db->select("moment_to_playlist.order")
            ->from("moment_to_playlist")
            ->where("moment_id", $moment_cur)
            ->where("playlist_id", $playlist_id)
            ->get()
            ->row_array();
        $new_order = $this->db->select("moment_to_playlist.order")
            ->from("moment_to_playlist")
            ->where("moment_id", $moment_new)
            ->where("playlist_id", $playlist_id)
            ->get()
            ->row_array();

        if(!empty($cur_order) && !empty($new_order)) {
            // Update order numbers with new values
            $this->db
                ->where("moment_id", $moment_cur)
                ->where("playlist_id", $playlist_id)
                ->update("moment_to_playlist", ["order" => $new_order['order']]);
            $this->db
                ->where("moment_id", $moment_new)
                ->where("playlist_id", $playlist_id)
                ->update("moment_to_playlist", ["order" => $cur_order['order']]);
            return true;
        }

        set_status_header(400);
        return "Cur order or Prev order are/were empty...";
    }

    /**
     * Remove a playlist and everything from the pivot table
     *
     * @param int|bool $playlist_id
     * @return bool|string              Error code or true on success
     */
    function remove_playlist($playlist_id = false)
    {
        if($playlist_id != false)
        {
            $where = array(
                'playlist_id'   => $playlist_id
            );
            $this->db->delete('playlists', $where, 1); // LIMIT 1! IMPORTANT!
            if($this->db->affected_rows() == 1)
            {
                $this->db->delete('moment_to_playlist', $where); // Delete the pivot entries as well
                return true;
            } else {
                return "Error: the playlist did not exist so it's not deleted!";
            }
        }
        return "Error: no moment_id and/or playlist_id set!";
    }

    /**
     * Adds a moment to a playlist
     *
     * @param null $moment_id       The moment ID
     * @param null $playlist_id     The playlist ID
     * @return object|string        True on success, error on false
     */
    function add_moment_to_playlist($moment_id = false, $playlist_id = false)
    {
        if($moment_id != false && $playlist_id != false)
        {
            // Get the highest order from db
            $new_order = $this->db->select("MAX(`order`) AS max_order")
                ->from("moment_to_playlist")
                ->where("playlist_id", $playlist_id)
                ->get()
                ->row_array();

            if($new_order['max_order'] == null) $new_order['max_order'] = 0;

            $data = array(
                'moment_id'     => $moment_id,
                'playlist_id'   => $playlist_id,
                'order'         => $new_order['max_order'] + 1
            );
            return $this->db->insert('moment_to_playlist', $data);
        }
        return "Error: no moment_id and/or playlist_id set!";
    }

    /**
     * Remove a moment from a playlist
     *
     * @param null $moment_id   The moment ID
     * @param null $playlist_id The playlist ID
     *
     * @return bool             True / False
     */
    function remove_moment_from_playlist($moment_id = false, $playlist_id = false)
    {
        if($moment_id != false && $playlist_id != false)
        {
            $where = array(
                'moment_id'     => $moment_id,
                'playlist_id'   => $playlist_id
            );
            $this->db->delete('moment_to_playlist', $where, 1); // LIMIT 1! IMPORTANT!
            return ($this->db->affected_rows() == 1);
        }
        return "Error: no moment_id and/or playlist_id set!";
    }

    function remove_all_playlists_from_moment($moment_id = false)
    {
        if($moment_id != false)
        {
            $where = array(
                'moment_id' => $moment_id
            );
            $this->db->delete('moment_to_playlist', $where);
            return true;
        }
        return "Error: no moment_id given!";
    }
}

// EOF