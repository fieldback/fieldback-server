<?php

class System_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'system_info';
    }

    function get_system_info(){
    	$system_info = $this->db->get('system_info')->row_array();
    	return $system_info;
    }

    function get_all_roles(){
    	$roles = $this->db->get('user_levels')->result_array();
    	return $roles;
    }

    function update_system_info($data = null){
    	if($data !== null){
    		$this->db->update('system_info', $data);
    		return;
    	}
    	return false;
    }

    function update_last_sync() {
        $this->db->update('system_info', ['last_sync' => time()]);
        return true;
    }



}

// EOF