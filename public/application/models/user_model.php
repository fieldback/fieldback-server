<?php

/**
 *      @author     Tim Scholten <tim@lessormore.nl>
 *      @copyright  2013 Less or more web solutions
 *
 *      @version    dev
 *
 *      PARTS
 *
 *      1:  GETS
 *      2:  ADDS
 *      3:  UPDATE
 *      4:  DELETE
 *      5:  CHECKS
 */
class User_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }
    /**
     * GETS.
     */
    public function get_user_by_id($id = null)
    {
        if ($id !== null) {
            return self::get_user(array('users.id' => $id));
        }

        return false;
    }

    public function get_user_by_login($login = null)
    {
        if ($login !== null) {
            return self::get_user(array('username' => $login));
        }

        return false;
    }

    public function get_user($where = array())
    {
        $select = 'user_profiles.*, user_levels.*';
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from('users');
        $this->db->select('users.id, users.username, users.password, user_levels.level');
        // USE INNER JOINS -> IF THE TABLE IS CORRUPT THE USER SHOULD NOT BE FOUND TO PREVENT ERRORS
        $this->db->join('user_to_user_level', 'user_to_user_level.user_id = users.id', 'inner');
        $this->db->join('user_levels', 'user_levels.user_level_id = user_to_user_level.user_level_id', 'inner');
        $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id', 'inner');
        $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id', 'inner');
        $user = $this->db->get()->row_array();
        //$user['organisations'] = self::get_organisations_for_user($user['id']);

        // Nasty piece of code: but if something is wrong with the data the user gets to see all kinds of errors
        // Instead, if the user data is corrupt, redirect to home and show a more user friendly error

        if (!empty($user)) {
            return $user;
        } else {
            flash_error('user_data_corrupt');
            redirect('home');
        }
    }

    /**
     * Check if an organisation exists by name and ID and return true/false.
     *
     * @param $organisation_id
     * @param $organisation_name
     *
     * @return bool
     */
    public function check_organisation_by($organisation_id, $organisation_name)
    {
        $this->db->select('*');
        // Select on id and name
        $this->db->where('organisation_id', $organisation_id);
        $this->db->where('organisation_name', $organisation_name);
        $this->db->from('organisations');
        $organisation = $this->db->get()->result_array();
        // Return if empty true/false
        return !empty($organisation);
    }

    /**
     * Find a user by a number of fields, and return true if this user REALLY exists.
     *
     * @param $user_id
     * @param $username
     * @param $first_name
     * @param $last_name
     * @param $email
     *
     * @return bool
     */
    public function check_user_by($user_id, $username, $first_name, $last_name, $email)
    {
        $this->db->select('users.id');
        // Only find a result when ALL fields are present in the database AND if the JOINS are correct (inner joins)
        $this->db->where('users.id', $user_id);
        $this->db->where('users.username', $username);
        $this->db->where('user_profiles.first_name', $first_name);
        $this->db->where('user_profiles.last_name', $last_name);
        $this->db->where('user_profiles.email', $email);
        $this->db->from('users');
        // USE INNER JOINS -> IF THE TABLE IS CORRUPT THE USER SHOULD NOT BE FOUND TO PREVENT ERRORS
        $this->db->join('user_to_user_level', 'user_to_user_level.user_id = users.id', 'inner');
        $this->db->join('user_levels', 'user_levels.user_level_id = user_to_user_level.user_level_id', 'inner');
        $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id', 'inner');
        $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id', 'inner');
        $user = $this->db->get()->row_array();
        // Return true or false on finding this user
        return !empty($user);
    }

    public function get_user_by_full_name($organisation_id = null, $user_name = null)
    {
        if ($organisation_id !== null && $user_name !== null) {
            $users = self::get_users_for_organisation($organisation_id, null, true);
            foreach ($users as $user) {
                $check = $user['first_name'].' '.$user['last_name'];
                if ($check == $user_name) {
                    return $user;
                }
            }
        }

        return false;
    }

    /**
     * Get the organisations for a user.
     *
     * @param int $user_id User ID
     *
     * @return array Organisations
     */
    public function get_organisations_for_user($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->select('*');
            $this->db->where('user_id', $user_id);
            $this->db->from('user_to_organisation');
            $this->db->join('organisations', 'organisations.organisation_id = user_to_organisation.organisation_id');
            $organisations = $this->db->get()->result_array();

            return $organisations;
        }
    }
    public function get_organisation_by_organisation_id($organisation_id = null)
    {
        if ($organisation_id !== null) {
            $this->db->where('organisation_id', $organisation_id);
            $organisation = $this->db->get('organisations')->row_array();

            return $organisation;
        }

        return false;
    }

    /**
     * Get all the users for one organisation
     * Possibility to add a query to search on first and last name.
     *
     * @param int    $organisation_id  The ID of the organisation
     * @param string $query            A string containing the search query
     * @param bool   $include_inactive include the inactive users or not
     *
     * @return array All the users that are in the organisation
     */
    public function get_users_for_organisation($organisation_id = null, $query = null, $include_inactive = false)
    {
        if ($organisation_id !== null) {
            $this->db->select('users.id, users.active, user_profiles.*');
            $this->db->from('user_to_organisation');
            // if include_inactive = true, include the inactive users of the organisation
            if ($include_inactive === false) {
                $this->db->where('users.active', 1);
            }
            $this->db->where('organisation_id', $organisation_id);
            // check if a search is included
            if ($query !== null) {
                // search on the query in the first AND last name
                $this->db->like('first_name', $query);
                $this->db->or_like('last_name', $query);
            }
            $this->db->join('users', 'users.id = user_to_organisation.user_id');
            $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id');
            $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');
            $this->db->order_by('user_profiles.last_name');
            $players = $this->db->get()->result_array();

            return $players;
        }

        return false;
    }

    /**
     * Get all the players for a team.
     *
     * @param int  $team_id          Team ID
     * @param bool $include_inactive Include the inactive users or not
     *
     * @return array The players
     */
    public function get_players_for_team($team_id, $include_inactive = false)
    {
        if ($team_id !== null) {
            $select = 'user_profiles.*, player_to_team.player_id, users.id';
            $this->db->select($select);
            $this->db->from('player_to_team');
            $this->db->where('team_id', $team_id);

            // include the inactive if true
            if ($include_inactive === false) {
                $this->db->where('users.active', 1);
            }

            $this->db->join('users', 'users.id = player_to_team.player_id');
            $this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = users.id');
            $this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');
            $this->db->order_by('user_profiles.first_name');
            $players = $this->db->get()->result_array();

            return $players;
        }

        return false;
    }

    public function get_teams_for_player($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->select('*');
            $this->db->from('player_to_team');
            $this->db->where('player_id', $user_id);
            $this->db->join('teams', 'teams.team_id = player_to_team.team_id');
            $teams = $this->db->get()->result_array();

            return $teams;
        }

        return false;
    }

    public function get_teams_for_owner($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->select('*');
            $this->db->from('user_to_team');
            $this->db->where('user_id', $user_id);
            $this->db->join('teams', 'teams.team_id = user_to_team.team_id');
            $teams = $this->db->get()->result_array();

            return $teams;
        }

        return false;
    }

    public function get_selected_organisation_for_user($user_id = null)
    {
        // set user id to logged in user if user_id is not given
       if ($user_id === null) {
           $user_id = $this->ion_auth->user()->row()->id;
       }

        $this->db->where('id', $user_id);
        $user = $this->db->get('users')->row_array();

        return $user['selected_organisation'];
    }
    /**
     * ADDS.
     */
    public function add_user($user_profile = null, $role = null, $organisation_id = null, $user = null, $team = null)
    {
        if ($user_profile !== null || $role !== null || $organisation_id !== null || $user !== null) {
            $username = $user['username'];
            $password = $user['password'];
            $email = $user['username'];
            $additional_data = array();

            //register user using ion_auth
            $user_id = $this->ion_auth->register($username, $password, $email, $additional_data);
            //insert new user to users table
            //$this->db->insert('users', $user);
            //$user_id = $this->db->insert_id();

            //insert new user to user profile table
            $this->db->insert('user_profiles', $user_profile);
            $user_profile_id = $this->db->insert_id();

            //insert into pairing tables
            //user_to_user_profile
            $data = array(
                'user_id' => $user_id,
                'user_profile_id' => $user_profile_id,
            );
            $this->db->insert('user_to_user_profile', $data);

            //user_to_organisation
            $data = array(
                'user_id' => $user_id,
                'organisation_id' => $organisation_id,
            );
            $this->db->insert('user_to_organisation', $data);

            //user_to_user_level
            $data = array(
                'user_id' => $user_id,
                'user_level_id' => $role,
            );
            $this->db->insert('user_to_user_level', $data);

            //add user as player to selected team
            if ($team !== null && is_array($team)) {
                $team_id = $team['team_id'];
                if ($team_id !== '0') {
                    if (isset($team['player'])) {
                        $this->team_model->add_player_to_team($user_id, $team_id);
                    }
                    if (isset($team['owner'])) {
                        $this->team_model->add_owner_to_team($user_id, $team_id);
                    }
                }
            }

            return;
        }

        return false;
    }

    public function follow_login($user = null)
    {
        if ($user !== null) {
            $data = array(
                'user_id' => $user->id,
                'username' => $user->username,
                'email' => $user->email,
                'active' => $user->active,
                'ip_address' => $user->ip_address,
            );
            $this->db->insert('track_login', $data);

            return;
        }

        return false;
    }

    /**
     * UPDATE.
     */
    public function update_user_password_status($user_id = null)
    {
        if ($user_id !== null) {
            $data = array(
                'temporary_password' => '0',
            );

            $this->db->where('id', $user_id);
            $this->db->update('users', $data);

            return true;
        }

        return false;
    }

    public function set_organisation_for_user($organisation_id = null, $user_id = null)
    {
        if ($organisation_id !== null) {

            // set user id to logged in user if user_id is not given
            if ($user_id === null) {
                $user_id = $this->ion_auth->user()->row()->id;
            }

            // select and update the users selected organisation
            $this->db->where('id', $user_id);
            $data = array(
                'selected_organisation' => $organisation_id,
            );
            $this->db->update('users', $data);
        }
    }

    /**
     * DELETE.
     */
    public function hard_delete_user($user_id = null, $organisation_id = null)
    {
        $this->load->model('moment_model');
        $this->load->model('tagfield_model');
        $this->load->model('clip_model');
        $this->load->model('team_model');
        $this->load->model('tagfield_model');

        if ($user_id !== null && $organisation_id !== null) {
            if (is_admin()) {
                //delete user from moments
                $this->moment_model->delete_user_from_moments($user_id, $organisation_id);

                //delete user from teams
                $this->team_model->delete_user_from_teams($user_id, $organisation_id);

                //delete user from tracking
                $this->db->where('user_id', $user_id);
                $this->db->delete('track_login');

                //delete user from clip
                $this->clip_model->delete_user_from_clips($user_id);

                //delete user from organisation
                self::delete_user_from_organisations($user_id);

                //delete user from tagfields
                $this->tagfield_model->delete_user_from_tagfields($user_id, $organisation_id);

                //delete user as user
                $user = self::get_user_by_id($user_id);

                $user_profile_id = $user['user_profile_id'];

                $this->db->where('user_profile_id', $user_profile_id);
                $this->db->delete('user_profiles');

                $this->db->where('user_profile_id', $user_profile_id);
                $this->db->delete('user_to_user_profile');

                $this->db->where('user_id', $user_id);
                $this->db->delete('user_to_user_level');

                $this->db->where('id', $user_id);
                $this->db->delete('users');

                return;
            }
        }

        return false;
    }

    public function delete_user_from_organisations($user_id = null)
    {
        if ($user_id !== null) {
            if (is_admin()) {
                $organisations = self::get_organisations_for_user($user_id);

                return;
            }
        }

        return false;
    }

    public function delete_user_from_organisation($user_id = null, $organisation_id = null)
    {
        if ($user_id !== null && $organisation_id !== null) {
            $this->db->where('user_id', $user_id);
            $this->db->where('organisation_id', $organisation_id);
            $this->db->delete('user_to_organisation');

            return;
        }

        return false;
    }

    /**
     * CHECKS.
     *
     * @return bool
     */
    public function is_admin($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->select('*');
            $this->db->from('user_to_user_level');
            $this->db->where('user_id', $user_id);
            $this->db->where('user_level_id', 1);
            $result = $this->db->get()->result_array();
            if (!empty($result)) {
                return true;
            }
        }

        return false;
    }

    public function is_org_admin($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->select('*');
            $this->db->from('user_to_user_level');
            $this->db->where('user_id', $user_id);
            $this->db->where('user_level_id', 2);
            $result = $this->db->get()->result_array();
            if (!empty($result)) {
                return true;
            }
        }

        return false;
    }

    public function check_if_user_exists($user_name, $organisation_id)
    {
        $users = self::get_users_for_organisation($organisation_id);
        $check = array();
        foreach ($users as $user) {
            $check[] = $user['first_name'].' '.$user['last_name'];
        }
        $result = in_array($user_name, $check);

        return $result;
    }

    public function check_temporary_password($user_id = null)
    {
        if ($user_id !== null) {
            $this->db->where('id', $user_id);
            $this->db->where('temporary_password', '1');
            $user = $this->db->get('users')->row_array();
            if (empty($user)) {
                return true;
            }

            return false;
        }
    }
}

// EOF

