<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends MY_Model {

	function __construct()
	{
		parent::__construct();
	}

    /**
     * Check if a team exists by id AND name and return true/false
     * @param $team_id
     * @param $team_name
     * @return bool
     */
    function check_team_by($team_id, $team_name) {
        $this->db->select('*');
        $this->db->from('teams');
        $this->db->where('team_id', $team_id);
        $this->db->where('team_name', $team_name);
        $team = $this->db->get()->row_array();
        return !empty($team);
    }

	function get_all_teams_for_organisation($organisation_id = null)
	{
		$this->db->select('teams.*');
		$this->db->from('team_to_organisation');
		$this->db->where('team_to_organisation.organisation_id', $organisation_id);
		$this->db->join('teams', 'teams.team_id = team_to_organisation.team_id');
		$teams = $this->db->get()->result_array();
		return $teams;
	}

	function get_all_teams_for_user_and_organisation($user_id = null, $organisation_id = null)
	{
		$this->db->select('teams.*');
		$this->db->from('user_to_team');
		$this->db->where('user_to_team.user_id', $user_id);
		$this->db->where('team_to_organisation.organisation_id', $organisation_id);
		$this->db->join('teams', 'teams.team_id = user_to_team.team_id');
		$this->db->join('team_to_organisation', 'team_to_organisation.team_id = teams.team_id');
		$teams = $this->db->get()->result_array();
		return $teams;
	}
	function get_team_info_by_team_id($team_id = null)
	{
		if($team_id !== null){
			$this->db->select('*');
			$this->db->from('teams');
			$this->db->where('team_id', $team_id);
			$team_info = $this->db->get()->row_array();
			return $team_info;
		}
		return false;
	}
	function get_all_players_for_team($team_id = null){
		
		if($team_id !== null){
			$this->db->select('*');
			$this->db->from('player_to_team');
			$this->db->where('team_id', $team_id);
			$this->db->where('deleted', 0);
			$this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = player_to_team.player_id');
			$this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');
			$this->db->order_by('user_profiles.first_name', 'ASC');
			$players = $this->db->get()->result_array();
			return $players;
		}
		return false;
	}
	function get_all_players_for_organisation($organisation_id = null)
	{
		if($organisation_id !== null)
		{
			$users = $this->user_model->get_users_for_organisation($organisation_id, null, true);
			
			$players = array();
			foreach($users as $user){
				if($this->team_model->check_if_user_is_player($user['id'])){
					$players[] = $user;
				}
			}
			return $players;
		}
	}

	function get_all_owners_for_team($team_id = null){
		if($team_id !== null){
			$this->db->select('*');
			$this->db->from('user_to_team');
			$this->db->where('team_id', $team_id);
			$this->db->where('deleted', 0);
			$this->db->join('user_to_user_profile', 'user_to_user_profile.user_id = user_to_team.user_id');
			$this->db->join('user_profiles', 'user_profiles.user_profile_id = user_to_user_profile.user_profile_id');
			$this->db->order_by('user_profiles.first_name', 'ASC');
			$owners = $this->db->get()->result_array();
			return $owners;
		}
		return false;
	}

	function add_team_to_organisation($team_info = null, $organisation_id = null)
	{
		if($team_info !== null && $organisation_id !== null)
		{
			$this->db->insert('teams', $team_info);
			$team_id = $this->db->insert_id();

			$data = array(
				'team_id' => $team_id,
				'organisation_id' => $organisation_id
			);
			$this->db->insert('team_to_organisation', $data);
			return $team_id;
		}
		return false;
	}

	function add_player_to_team($user_id = null, $team_id = null)
	{
		if($user_id !== null && $team_id !== null)
		{
			$data = array(
				'player_id' => $user_id,
				'team_id' => $team_id
			);
			$this->db->insert('player_to_team', $data);
			return;
		}
		return false;
	}

	function delete_user_from_teams($user_id) {
		$teams = $this->user_model->get_teams_for_player($user_id);
		foreach($teams as $team){
			self::remove_player_from_team($user_id, $team['team_id']);
		}

		$teams = $this->user_model->get_teams_for_owner($user_id);
		foreach ($teams as $team) {
			self::remove_owner_from_team($user_id, $team['team_id']);
		}
	}

	function remove_player_from_team($player_id = null, $team_id = null)
	{
		if($player_id !== null && $team_id !== null)
		{
			$this->db->where('player_id', $player_id);
			$this->db->where('team_id', $team_id);
			$this->db->delete('player_to_team');
			return;
		}
		return false;
	}

	function add_owner_to_team($user_id, $team_id)
	{
		if($user_id !== null && $team_id !== null)
		{
			$data = array(
				'user_id' => $user_id,
				'team_id' => $team_id
			);
			$this->db->insert('user_to_team', $data);
			return;
		}
		return false;
	}

	function remove_owner_from_team($user_id = null, $team_id = null)
	{
		if($user_id !== null && $team_id !== null)
		{
			$this->db->where('user_id', $user_id);
			$this->db->where('team_id', $team_id);
			$this->db->delete('user_to_team');
			return;
		}
		return false;
	}

	function check_if_player_is_in_team($user_id = null, $team_id = null)
	{
		$this->db->where('player_id', $user_id);
		$this->db->where('team_id', $team_id);
		$result = $this->db->get('player_to_team')->row_array();
		if(empty($result)){
			$in_team_check = false;
		}else{
			$in_team_check = true;
		}
		return $in_team_check;
	}

	function check_if_user_is_player($user_id = null)
	{
		if($user_id !== null)
		{
			$this->db->where('player_id', $user_id);
			$result = $this->db->get('player_to_team')->row_array();
			if(empty($result)){
				$has_team_check = false;
			}else{
				$has_team_check = true;
			}
			return $has_team_check;
		}
		return false;
	}

	function check_if_owner_of_team($user_id = null, $team_id = null)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('team_id', $team_id);
		$result = $this->db->get('user_to_team')->row_array();
		if(empty($result)){
			$in_team_check = false;
		}else{
			$in_team_check = true;
		}
		return $in_team_check;
	}
	

}

/* End of file team_model.php */
/* Location: ./application/models/team_model.php */?>