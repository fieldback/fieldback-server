<?php

class Sensor_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'sensors';
    }

    /**
     * Insert active sensor in database
     * @return
     */
    public function insertIntoActiveList()
    {
        //get the ip address of the sensor
        $sensor_ip = $_SERVER['REMOTE_ADDR'];

        // check if sensor already exists
        $this->db->where('ip', $sensor_ip);
        $this->db->from('sensors');
        $count = $this->db->count_all_results();

        if($count == 0){  //check if not already is registerd

            //not yet in db
            $data = array(
                'ip' => $sensor_ip
            );

            $this->db->insert('sensors', $data);

        } else {
            //already in db
            $this->db->set('status', '1', FALSE);
            $this->db->where('ip', $sensor_ip);
            $this->db->update('sensors');
        }
        return;
    }

    public function get_sensor_by_ip($sensor_ip = null) {
        if($sensor_ip !== null) {
            $this->db->where('ip', $sensor_ip);
            $sensor_info = $this->db->get('sensors')->row_array();
            return $sensor_info;
        }else{
            return false;
        }

    }

    public function createMomentInDatabase()
    {
        echo 'not working yet';

    }

    public function get_sensors()
    {

        $sensors = $this->db->get('sensors')->result_array();

        $sensorsToDelete = array(); //array with ids to disable later

        foreach($sensors as $sensor)
        {
            $status = $this->pingAddress($sensor['ip']);
            if($status == 0)
            {
                //sensor is no longer active, therefore set to array to set disabled
                array_push($sensorsToDelete, $sensor['id']);
            }
        }


        if(count($sensorsToDelete) > 0)
        {
            //$query = $this->db->set('sensors', 'status = 0', "id IN (".implode(",", $sensorsToDelete).")");

            $this->db->set('status', '0', FALSE);
            $this->db->where_in('id', $sensorsToDelete);
            $this->db->update('sensors');

        }

        $updatedSensors = $this->db->get('sensors')->result_array();

        foreach($updatedSensors as $index => $value)
        {
            if($value['status'] == 1)
            {
                $updatedSensors[$index]['status'] = 'Available';
            }
            elseif($value['status'] == 2)
            {
                $updatedSensors[$index]['status'] = 'In use';
            }
            else
            {
                $updatedSensors[$index]['status'] = 'Offline';
            }

        }

        return  $updatedSensors;

    }

    public function pingAddress($ip) {
        exec("ping -c 1 -t 1 ".$ip, $outcome, $status);
        //dump($outcome);
        if(!empty($outcome[4]))
        {
            $explode = explode(',', $outcome[4]);
            if(substr(trim($explode[0]), 0, 1) == substr(trim($explode[1]), 0, 1))
            {
                //echo('no loss to ' . $ip . '<br />');
                //no pakkage has been lost while pinging, so senser is still active
                $status = 1;
            }
            else {
                $status = 0;
            }

            return $status;
        }
        else
        {
            return 0;
        }
    }

    public function set_sensor_to_event($sensor_id, $event_id, $part_id, $lead, $lapse) {
        $data = array(
            'event_id' => $event_id,
            'part_id' => $part_id,
            'lead' => $lead,
            'lapse' => $lapse
        );
        $this->db->where('id', $sensor_id);
        $this->db->update('sensors', $data);
        return;
    }

}
