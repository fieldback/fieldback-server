<?php

class Camera_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'cameras';
    }
    
    function get_all_cameras(){
    	$cameras = $this->db->get('cameras')->result_array();
    	return $cameras;
    }

    	/*
	    *	Returns all active camera's that are not 'instacam's - hack to keep the database changes as minimal as possible
	    */
	function get_all_active_cameras()
	{
		$this->db->where('active', '1');
		$this->db->where('is_instacam', '0');
		$cameras = $this->db->get('cameras')->result_array();
		return $cameras;
	}
	
		/*
		*	Returns all active insta_camera's by checking for the flags active and is_instacam
		*/
	function get_all_insta_cameras()
	{
		$this->db->where('active', '1');
		$this->db->where('is_instacam', '1');
		$cameras = $this->db->get('cameras')->result_array();
		return $cameras;		
	}
	
	/*
		Returns a list of camera's that are active AND can be viewed live
		*/
	function get_all_live_cameras() {
		$this->db->where('active', '1');
		//$this->db->where('is_instacam', '0');
		$this->db->where('live_enabled', '1');
		$cameras = $this->db->get('cameras')->result_array();
		return $cameras;
	}

	function get_stream_by_cam_id($cam_id)
	{
		$this->db->where('camera_id', $cam_id);
		$camera = $this->db->get('cameras')->row_array();
		return $camera;
	}
	
	function update_cameras($cameras = null){
    	if($cameras !== null){
	    	foreach($cameras as $camera){ 
		    	// Check if the key camera_id exists -> update
		    	if(!isset($camera['camera_id'])) {
			    	// Insert new camera as this comes from the new_camera field
					$this->db->insert('cameras', $camera);
		    	} else {
			    	// Simply update the camera -> WHERE camera_id = $camera['camera_id']
			    	$this->db->update('cameras', $camera, array('camera_id' => $camera['camera_id']));
		    	}
	    	}
	    }
    }
    function delete_camera($camera_id = null){
    	if($camera_id !== null){
    		$this->db->where('camera_id', $camera_id);
    		$this->db->delete('cameras');
    		return;
    	}
    	return false;
    }

}

// EOF