<?php

class Recording_Model extends MY_Model {
	
	function __construct() {
	    parent::__construct();
	    $this->table = 'recordings';
	}
	function check_if_recording()
	{
		$recordings = $this->db->get('recordings')->result_array();
		if(empty($recordings)){
			return false;
		}else{
			return true;
		}
	}
	function insert_new_recording($data = null)
	{
		if($data !== null)
		{
			$this->db->insert('recordings', $data);
			$recording_id = $this->db->insert_id();
			return $recording_id;
		}
		return;
	}
	function get_recordings()
	{
		$recordings = $this->db->get('recordings')->result_array();
		$event_ids = array();
		foreach ($recordings as $key => &$recording) {
			//load models
			$this->load->model('event_model');
			$this->load->model('part_model');

			// get the event info
			$event_info = $this->event_model->get_event_info_by_event_id($recording['event_id']);
		
			// if multiple cams are recording multiple recordings are shown for one event
			// delete that from the array so only one instance will exist for the event
			if(in_array($event_info['event_id'], $event_ids)){

				unset($recordings[$key]);

			}else{

				$event_ids[] = $event_info['event_id'];

				$recording['event_info'] = $event_info;
				$recording['rec_owner'] = $this->user_model->get_user_by_id($recording['rec_owner']);
				$recording['part_info'] = $this->part_model->get_part_info_by_part_id($recording['part_id']);
			}
		}
		return $recordings;
	}

	function get_overtime_recordings()
	{
		$recordings = self::get_recordings();
		$overtime_recordings = array();

		$i = 0;
		foreach ($recordings as $recording) {
			if(now() > ($recording['time_to_record'] + $recording['start_time'])){
				$overtime_recordings[$i] = $recording;
			}
			$i++; 
		}
		return $overtime_recordings;
	}

	function get_recordings_by_event_id_and_part_id($event_id, $part_id)
	{
		$this->db->where('event_id', $event_id);
		$this->db->where('part_id', $part_id);
		$recordings = $this->db->get('recordings')->result_array();
		return $recordings;
	}
	function get_recording_by_event_id($event_id)
	{
		$this->db->where('event_id', $event_id); 
		$recordings = $this->db->get('recordings')->result_array();
		return $recordings;
	}
	
	function get_recording_info_by_recording_id($recording_id)
	{
		$this->db->where('recording_id', $recording_id);
		$recording_info = $this->db->get('recordings')->row_array();
		return $recording_info;
	}
	function get_recording_by_cam_and_part_id($cam, $part_id)
	{
		$this->db->where('cam_number', $cam);
		$this->db->where('part_id', $part_id);
		$recording_info = $this->db->get('recordings')->row_array();
		return $recording_info;
	}
	function recording_to_clip($recording_id, $event_id, $part_id, $name, $server_path, $size, $date_created, $rec_owner, $cam, $cam_description)
	{
		$data = array(
			'file_name'		=>	$name,
			'file_type'		=>	'video/mp4',
			'file_path'		=>	$server_path,
			'full_path'		=>	$server_path,
			'raw_name'		=>	$name,
			'orig_name'		=>	$name,
			'client_name'	=>	$name,
			'file_ext'		=>	'mp4',
			'file_size'		=>	$size,
			'date_created'	=>	$date_created,
			'kind'			=>	'recording',
		);
		$this->db->insert('clips', $data);
		$clip_id = $this->db->insert_id();
		$data = array(
			'clip_id' 	=> $clip_id,
			'part_id'	=> $part_id,
			'cam'		=> $cam
		);
		$this->db->insert('clip_to_part', $data);
		$data = array(
			'user_id' 	=> $rec_owner,
			'clip_id'	=> $clip_id
		);
		$this->db->insert('user_to_clip', $data);
		$this->db->where('recording_id', $recording_id);
		$this->db->delete('recordings');
		return $clip_id;
		
	}
}