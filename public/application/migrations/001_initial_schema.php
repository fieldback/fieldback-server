<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

	public function up()
	{
		// add cameras
		$this->dbforge->add_field(array(
			'camera_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'description' => array(
				'type' => 'VARCHAR',
				'constraint' => '128'
			),
			'stream' => array(
				'type' => 'VARCHAR',
				'constraint' => '256',
			),
			'number' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'active' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'cam_fps' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
			'cam_constante' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
			'copy' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('camera_id', TRUE);
		$this->dbforge->create_table('cameras');


		// add clip_to_part
		$this->dbforge->add_field(array(
			'clip_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'part_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'cam' => array(
				'type' => 'INT',
				'constraint' => 11,
			)
		));
		$this->dbforge->add_key('clip_id');
		$this->dbforge->create_table('clip_to_part');


		// add clips
		$this->dbforge->add_field(array(
			'clip_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'file_type' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'file_path' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'full_path' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'raw_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),	
			'orig_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'client_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'file_ext' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),	
			'file_size' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'date_created' => array(
				'type' => 'INT',
				'constraint' => 64
			),
			'kind' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			)
		));
		$this->dbforge->add_key('clip_id', TRUE);
		$this->dbforge->create_table('clips');

		//add creator_to_event
		$this->dbforge->add_field(array(
			'creator_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('creator_id');
		$this->dbforge->add_key('event_id');
		$this->dbforge->create_table('creator_to_event');

		//add event_to_organisation
		$this->dbforge->add_field(array(
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('event_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('event_to_organisation');

		//add events
		$this->dbforge->add_field(array(
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'event_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'event_description' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'date_created' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
		));
		$this->dbforge->add_key('event_id', TRUE);
		$this->dbforge->create_table('events');

		//add moment_file_to_moment
		$this->dbforge->add_field(array(
			'moment_file_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'moment_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'cam' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('moment_file_id');
		$this->dbforge->add_key('moment_id');
		$this->dbforge->create_table('moment_file_to_moment');

		//add moment_files
		$this->dbforge->add_field(array(
			'moment_file_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'filename' => array(
				'type' => 'VARCHAR',
				'constraint' => 32
			),
			'path' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'cam' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('moment_file_id', TRUE);
		$this->dbforge->create_table('moment_files');

		//add moment_to_part
		$this->dbforge->add_field(array(
			'moment_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'part_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('moment_id');
		$this->dbforge->add_key('part_id');
		$this->dbforge->create_table('moment_to_part');

		//add moments
		$this->dbforge->add_field(array(
			'moment_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'date_created' => array(
				'type' => 'INT',
				'constraint' => 32
			),
			'time_in_clip' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'lead' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'lapse' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'comment' => array(
				'type' => 'LONGTEXT'
			)
		));
		$this->dbforge->add_key('moment_id', TRUE);
		$this->dbforge->create_table('moments');

		//opponents
		$this->dbforge->add_field(array(
			'opponent_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'official_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'name' => array(
				'type' => 'INT',
				'constraint' => 64
			),
			'creation_date' => array(
				'type' => 'DATE'
			),
		));
		$this->dbforge->add_key('opponent_id', TRUE);
		$this->dbforge->create_table('opponents');

		// add organisations
		$this->dbforge->add_field(array(
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'organisation_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			)
		));
		$this->dbforge->add_key('organisation_id', TRUE);
		$this->dbforge->create_table('organisations');

		//add part_to_event
		$this->dbforge->add_field(array(
			'part_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('part_id');
		$this->dbforge->add_key('event_id');
		$this->dbforge->create_table('part_to_event');

		//add parts
		$this->dbforge->add_field(array(
			'part_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'part_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'date_created' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			)
		));
		$this->dbforge->add_key('part_id', TRUE);
		$this->dbforge->create_table('parts');

		//add player_to_team
		$this->dbforge->add_field(array(
			'player_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('player_id');
		$this->dbforge->add_key('team_id');
		$this->dbforge->create_table('player_to_team');

		//add recordings
		$this->dbforge->add_field(array(
			'recording_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'cam_number' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'cam_fps' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
			'cam_constante' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
			'stream' => array(
				'type' => 'VARCHAR',
				'constraint' => 56
			),
			'copy' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'time_to_record' => array(
				'type' => 'INT',
				'constraint' => 32
			),
			'start_time' => array(
				'type' => 'INT',
				'constraint' => 32
			),
			'process_id' => array(
				'type' => 'INT',
				'constraint' => 32
			),
			'rec_owner' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'part_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('recording_id', TRUE);
		$this->dbforge->create_table('recordings');

		//add system_info
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'system_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'system_information' => array(
				'type' => 'LONGTEXT'
			),
			'ffmpeg' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'openrtsp' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'max_rec_minutes' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('system_info');

		//add tag_to_moment
		$this->dbforge->add_field(array(
			'tag_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'moment_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'tag_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			)
		));
		$this->dbforge->add_key('tag_id');
		$this->dbforge->add_key('moment_id');
		$this->dbforge->create_table('tag_to_moment');

		//add tag_to_organisation
		$this->dbforge->add_field(array(
			'tag_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('tag_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('tag_to_organisation');

		//add tag_to_tagfield
		$this->dbforge->add_field(array(
			'tag_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'tagfield_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'left' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'top' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'label' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'original_tag_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('tag_id', TRUE);
		$this->dbforge->create_table('tag_to_tagfield');

		//add tagfield_to_organisation
		$this->dbforge->add_field(array(
			'tagfield_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('tagfield_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('tagfield_to_organisation');

		//add tagfields
		$this->dbforge->add_field(array(
			'tagfield_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'tagfield_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 32,
			),
			'show_players' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
			),
			'date_saved' => array(
				'type' => 'INT',
				'constraint' => 11,
			)
		));
		$this->dbforge->add_key('tagfield_id', TRUE);
		$this->dbforge->create_table('tagfields');

		//add tags
		$this->dbforge->add_field(array(
			'tag_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'tag_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 32,
			)
		));
		$this->dbforge->add_key('tag_id', TRUE);
		$this->dbforge->create_table('tags');

		//add team_to_event
		$this->dbforge->add_field(array(
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('team_id');
		$this->dbforge->add_key('event_id');
		$this->dbforge->create_table('team_to_event');

		//add team_to_organisation
		$this->dbforge->add_field(array(
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('team_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('team_to_organisation');

		//add team_to_team_type
		$this->dbforge->add_field(array(
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'team_type_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('team_id');
		$this->dbforge->add_key('team_type_id');
		$this->dbforge->create_table('team_to_team_type');

		//add team_types
		$this->dbforge->add_field(array(
			'team_type_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'team_type' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
			)
		));
		$this->dbforge->add_key('team_type_id', TRUE);
		$this->dbforge->create_table('team_types');

		//add teams
		$this->dbforge->add_field(array(
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'team_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
			)
		));
		$this->dbforge->add_key('team_id', TRUE);
		$this->dbforge->create_table('teams');

		//add trigger_tag_to_organisation
		$this->dbforge->add_field(array(
			'trigger_tag_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('trigger_tag_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('trigger_tag_to_organisation');

		//add trigger_tag_to_tagfield
		$this->dbforge->add_field(array(
			'trigger_tag_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'tagfield_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
				'null' => TRUE
			),
			'lead' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			),
			'lapse' =>  array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			)
		));
		$this->dbforge->add_key('trigger_tag_id');
		$this->dbforge->add_key('tagfield_id');
		$this->dbforge->create_table('trigger_tag_to_tagfield');

		//add trigger_tags
		$this->dbforge->add_field(array(
			'trigger_tag_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'trigger_tag_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 32,
			),
			'lead' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'lapse' => array(
				'type' => 'INT',
				'constraint' => 11,
			)
		));
		$this->dbforge->add_key('trigger_tag_id', TRUE);
		$this->dbforge->create_table('trigger_tags');

		//add user_levels
		$this->dbforge->add_field(array(
			'user_level_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'level' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
			)
		));
		$this->dbforge->add_key('user_level_id', TRUE);
		$this->dbforge->create_table('user_levels');

		//add user_profiles
		$this->dbforge->add_field(array(
			'user_profile_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'first_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
			),
			'last_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64,
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
			)
		));
		$this->dbforge->add_key('user_profile_id', TRUE);
		$this->dbforge->create_table('user_profiles');

		//add user_to_clip
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'clip_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('clip_id');
		$this->dbforge->create_table('user_to_clip');

		//add user_to_organisation
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'organisation_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('organisation_id');
		$this->dbforge->create_table('user_to_organisation');

		//add user_to_tagfield
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'tagfield_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('tagfield_id');
		$this->dbforge->create_table('user_to_tagfield');

		//add user_to_team
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'team_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('team_id');
		$this->dbforge->create_table('user_to_team');

		//add user_to_user_level
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_level_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('user_level_id');
		$this->dbforge->create_table('user_to_user_level');
		
		//add user_to_user_profile
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_profile_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('user_id');
		$this->dbforge->add_key('user_profile_id');
		$this->dbforge->create_table('user_to_user_profile');

		//add users
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			)
		));
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->create_table('users');
	}

	public function down()
	{
		$this->dbforge->drop_table('cameras');
		$this->dbforge->drop_table('clip_to_part');
		$this->dbforge->drop_table('clips');
		$this->dbforge->drop_table('creator_to_event');
	}

}

//end of file 001_initial_schema.php