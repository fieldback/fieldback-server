<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('flash_error'))
{
    /**
     * All the error codes together
     * @param  string $error_code
     * @return flashdata
     */
    function flash_error($error_code = null)
    {   
        $CI =& get_instance();
        switch ($error_code) {

            //login 
            case 'account_fail':
                $es = "Your account is already activated. Try to login.";
                break;
            case 'change_password':
                $es = "You have to change your password, because you are using the temporary password send in the activation mail.";
                break;
            case 'user_data_corrupt':
            	$es = "There was something wrong with the user data, if this error keeps occurring please contact the administrator.";
            	break;

            //roles
            case 'not_logged_in':
                $es = "You have to be logged in";
                break;
                
            case 'not_admin':
                $es = "You have to be admin to perform this action";
                break;

            case 'no_permission':
                $es = "You don't have the permission to perform this action";
                break;
            
            //events
            case 'event_not_deleted':
                $es = "The event has not been deleted, no event selected";
                break;
            
            case 'event_has_recordings':
            	$es = "The event has active recordings, stop recording before deleting the event.";
            	break;
            
            //tagfields
            case 'select_tagfield':
                $es = "You have to select a tagfield";
                break;

            // When a tag is deleted when there are still moments connected (should never come up as this is also checked on the page itself
            case 'tag_not_without_moments':
            	$es = "This tag still has moments connected to it, first delete them!";
				break;
            
            // When an event is visited but the event does not exist anymore -> should never happen in normal cases as the link that took a user here should not exist on the page in the first place
            case 'event_does_not_exist':
            	$es = "This event does not exist (anymore). Check the link you clicked that took you here.";
            	break;
            
            // When the live page is visited while there were no live cameras -> should never happen in normal cases as the link is removed if no live cameras
            case 'no_live_cameras':
         		$es = "There are currently no live camera's to view.";   
    	        break;
    	        
    	    // When the camera live preview page (or any camera related page) can not find the camera that should be there (should not happen)
    	    case 'camera_does_not_exist':
    	    	$es = "This camera does not exist. Please check the link you clicked that took you here.";
			    break;

            // Recordings - INSTACAM
            case 'instacording_not_active':
                $es = "The recording session is already terminated. It is not possible to capture moments anymore.";
                break;

            //default
            default:
                $es = "Something went wrong";
                break;
        }
        $CI->session->set_flashdata('error', $es);
    }
}

if ( ! function_exists('flash_success'))
{
    /**
     * All the succes codes together
     * @param  string $success_code
     * @return flashdata
     */
    function flash_success($success_code)
    {
        $CI =& get_instance();
        switch ($success_code) {
            //login 
            case 'password_success':
                $ss = "Your password is changed successfully";
                break;
            case 'account_success':
                $ss = "Your account has been activated successfully, you can now login with your temporary password.";
                break;

            //event
            case 'event_added':
                $ss = "The event was added successfully";
                break;
            
            //part
            case 'part_added':
                $ss = "The part was added successfully";
                break;
            case 'part_deleted':
                $ss = "The part and its clips are deleted";
                break;
            
            //event
            case 'event_deleted':
                $ss = "The event, its parts and its clips are deleted";
                break;
            
            //users
            case 'user_added':
                $ss = "The user has been successfully added";
                break;
            
            //tagfields
            case 'tagfield_added':
                $ss = "The tagfield is added successfully";
                break;
            case 'tagfield_saved':
                $ss = "The tagfield is successfully saved";
                break;
            case 'tagfield_deleted':
                $ss = "The tagfield has been deleted";
                break;

            //tags
            case 'tag_deleted':
                $ss = "The tag has been deleted";
                break;
                
            //settings
            case 'system_settings_updated':
                $ss = "The system settings have been updated";
                break;
            
            //default
            default:
                $ss = "Your action went great!";
                break;
        }
        $CI->session->set_flashdata('success', $ss);
    }
}
