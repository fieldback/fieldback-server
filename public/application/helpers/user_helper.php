<?php
 if (!defined('BASEPATH')) {
     exit('No direct script access allowed');
 }

if (!function_exists('is_admin')) {
    function is_admin()
    {
        $CI = &get_instance();
        $CI->load->model('user_model');
        $user = $CI->ion_auth->user()->row();
        if (!empty($user)) {
            return $CI->user_model->is_admin($user->id);
        }

        return false;
    }
}

if (!function_exists('is_org_admin')) {
    function is_org_admin()
    {
        $CI = &get_instance();
        $CI->load->model('user_model');
        $user = $CI->ion_auth->user()->row();
        if (!empty($user)) {
            return $CI->user_model->is_org_admin($user->id);
        }

        return false;
    }
}

if (!function_exists('is_system_admin')) {
    function is_system_admin()
    {
        $CI = &get_instance();

        return $CI->ion_auth->is_admin();
    }
}

if (!function_exists('logged_in')) {
    function logged_in()
    {
        $CI = &get_instance();

        return $CI->ion_auth->logged_in();
    }
}

if (!function_exists('temporary_password')) {
    function temporary_password()
    {
        $CI = &get_instance();
        $user_id = $CI->ion_auth->user()->row()->id;

        return $CI->user_model->check_temporary_password($user_id);
    }
}

if (!function_exists('has_events')) {
    function has_events()
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $CI->load->model('event_model');
            $check = $CI->event_model->check_if_owner_of_events($user_id);
            if (empty($check)) {
                return false;
            } else {
                return true;
            }
        }
    }
}

if (!function_exists('has_event')) {
    function has_event($event_id)
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $CI->load->model('event_model');
            $check = $CI->event_model->check_if_owner_of_event($user_id, $event_id);
            if (empty($check)) {
                return false;
            } else {
                return true;
            }
        }
    }
}

if (!function_exists('has_teams')) {
    function has_teams()
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $organisation_id = $CI->session->userdata('organisation_id');
            $CI->load->model('team_model');
            $check = $CI->team_model->get_all_teams_for_user_and_organisation($user_id, $organisation_id);
            if (empty($check)) {
                return false;
            } else {
                return true;
            }
        }
    }
}

if (!function_exists('get_teams')) {
    function get_teams()
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $organisation_id = $CI->session->userdata('organisation_id');
            $CI->load->model('team_model');
            $teams = $CI->team_model->get_all_teams_for_user_and_organisation($user_id, $organisation_id);

            return json_encode($teams);
        }
    }
}

if (!function_exists('has_team')) {
    function has_team($team_id)
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $CI->load->model('team_model');
            $check = $CI->team_model->check_if_owner_of_team($user_id, $team_id);

            return $check;
        }

        return false;
    }
}

if (!function_exists('is_player')) {
    function is_player()
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $CI->load->model('team_model');
            $check = $CI->team_model->check_if_user_is_player($user_id);

            return $check;
        }

        return false;
    }
}

if (!function_exists('is_player_of_team')) {
    function is_player_of_team($team_id)
    {
        $CI = &get_instance();
        if ($CI->ion_auth->logged_in()) {
            $user_id = $CI->ion_auth->user()->row()->id;
            $CI->load->model('team_model');
            $check = $CI->team_model->check_if_player_is_in_team($user_id, $team_id);

            return $check;
        }

        return false;
    }
}

if (!function_exists('recording_in_progress')) {
    function recording_in_progress()
    {
        $CI = &get_instance();
        $CI->load->model('recording_model');
        $check = $CI->recording_model->check_if_recording();
        $CI->load->library('instacam');
        $check_insta = $CI->instacam->check_active_instacordings();

        return $check || $check_insta;
    }
}
