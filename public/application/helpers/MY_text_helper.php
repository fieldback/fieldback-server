<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function unix_to_time($unix = null)
{
    if($unix !== null){
        $time = date('d-m-Y H:i');
        return $time;
    }else{
        return 'undefined';
    }
}

function seconds_to_time($seconds) {
    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds - ($hours * 3600)) / 60);
    $seconds = floor($seconds % 60);
    if($hours < 10){
        $hours = '0' . $hours;
    }
    if($minutes < 10){
        $minutes = '0' . $minutes;
    }
    if($seconds < 10){
        $seconds = '0' . $seconds;
    } 
    return $hours . ':' . $minutes . ':' . $seconds;
}

function milliseconds_to_time($milliseconds) {
    $seconds = floor($milliseconds / 1000);
    $timepart = seconds_to_time($seconds);
    $milliseconds = substr($milliseconds, -0, 3);
    if($milliseconds < 100) {
        $milliseconds = '0' . $milliseconds;
    }
    if($milliseconds < 10) {
        $milliseconds = '0' . $milliseconds;
    }
    return $timepart . "." . $milliseconds;
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}