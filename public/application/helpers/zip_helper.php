<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	This function creates a zip file from a $files array and writes it to destination
	The files array consists of arrays with: address (original addres) and name (new name)
	*/
function prepare_zip($files, $destination = '') {
	//cycle through each file and check if it exists
	foreach($files as $file) {
		if(!file_exists($file['address'])) {
			// If a file does not exist, return false
			return false;
		}
	}
	//create the archive - all files exist and should not give problems
	$zip = new ZipArchive();
	$result = $zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE); // Overwrite or create a new one
	// Add each file
	foreach($files as $file) {
		$zip->addFile($file['address'], $file['name']);
	}
	//close the zip
	$zip->close();
	//check to make sure the file exists and return the result
	return file_exists($destination);
}
// EOF