<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dump'))
{
	function dump($data, $die = false)
	{
		echo "<pre>";
		if(is_array($data))
		{
			print_r($data);
		}
		else
		{
			var_dump($data);
		}
		echo "</pre>";
		($die)? die:'';
	}
}
?>