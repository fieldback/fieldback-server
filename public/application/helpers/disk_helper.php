<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_folder_size'))
{
	/**
	 * Get the size of a specific folder
	 * @param  relative_path $folder The relative path to the folder you want to get the size of
	 * @return size in MB         
	 */
	function get_folder_size($folder)
	{
	    $path = $folder;
	    return filesize_r($path);
	}

	function filesize_r($path){
	      if(!file_exists($path)) return 0;
	      if(is_file($path)) return filesize($path);
	      $ret = 0;
	      foreach(glob($path."/*") as $fn)
	        $ret += filesize_r($fn);
	      return $ret;
	    }
}

if ( ! function_exists('byteFormat'))
{
	function byteFormat($bytes, $unit = "", $decimals = 2) {
		$units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4, 
				'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);

		$value = 0;
		if ($bytes > 0) {
			// Generate automatic prefix by bytes 
			// If wrong prefix given
			if (!array_key_exists($unit, $units)) {
				$pow = floor(log($bytes)/log(1024));
				$unit = array_search($pow, $units);
			}

			// Calculate byte value by prefix
			$value = ($bytes/pow(1024,floor($units[$unit])));
		}

		// If decimals is not numeric or decimals is less than 0 
		// then set default value
		if (!is_numeric($decimals) || $decimals < 0) {
			$decimals = 2;
		}

		// Format output
		return sprintf('%.' . $decimals . 'f '.$unit, $value);
	}
}

if ( ! function_exists('rrmdir'))
{
	function rrmdir($dir) { 
	   if (is_dir($dir)) { 
	     $objects = scandir($dir); 
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
	       } 
	     } 
	     reset($objects); 
	     rmdir($dir); 
	   }
	   return; 
	 }
}
?>