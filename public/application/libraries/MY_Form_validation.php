<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    public function __construct()
    {
        parent::__construct();
    }
    
    function matches($str, $field)
    {
        
        // Modification of matches array
        $pattern = '/^([a-z_]+)\[(.*)\]$/';
        preg_match($pattern, $field, $matches);

        if (count($matches) > 2) {
            
            
            if ( ! isset($_POST[$matches[1]][$matches[2]]))
            {
                return FALSE;
            }
            
            $field = $_POST[$matches[1]][$matches[2]];

            return ($str !== $field) ? FALSE : TRUE;
            
        }

        
        if ( ! isset($_POST[$field]))
        {
            return FALSE;
        }

        $field = $_POST[$field];

        return ($str !== $field) ? FALSE : TRUE;
    }    
    
}

?>