<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Flash {

    /**
     * Show a success message
     * @param string $data
     * @param boolean $leave 
     */
    public function success($data, $leave = TRUE) {
        $this->set($data, 'success', $leave);
    }

    /**
     * Show a error message
     * @param string $data
     * @param boolean $leave 
     */
    public function error($data, $leave = TRUE) {
        $this->set($data, 'error', $leave);
    }

    /**
     * Show a notify message
     * @param string $data
     * @param boolean $leave 
     */
    public function notify($data, $leave = TRUE) {
        $this->set($data, 'notify', $leave);
    }

    /**
     * Get the flash messages
     * @return array 
     */
    public function get() {
        $CI = & get_instance();
        $flashs = $CI->session->userdata('flashs');
        if ($flashs != FALSE) {
            $CI->session->unset_userdata('flashs');
            return $flashs;
        }
        return FALSE;
    }
    
    /**
     * Set the flash message
     * @param string $data
     * @param string $type
     * @param boolean $leave 
     */
    private function set($data, $type = 'notify', $leave = TRUE) {
        $CI = & get_instance();
        $flashs = $CI->session->userdata('flashs');
        if (!$flashs) {
            $flashs = array();
        }

        if (!array_key_exists($type, $flashs) || !is_array($flashs[$type])) {
            $flashs[$type] = array('leave' => TRUE);
        }

        if (!is_array($data)) {
            $data = array($data);
        }

        if ($flashs[$type]['leave'] == TRUE) {
            $flashs[$type]['leave'] = $leave;
        }

        foreach ($data as $key => $flash) {
            if (in_array($flash, $flashs[$type], TRUE)) {
                unset($data[$key]);
            }
        }

        $flashs[$type] = array_merge($flashs[$type], $data);

        $CI->session->set_userdata('flashs', $flashs);
    }

}