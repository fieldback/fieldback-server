<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class is the main controller for instant camera recording logic, on push record
 */

Class Recording {
    private $CI;

    public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->model('system_model');
        $this->CI->load->model('recording_model');
        $this->CI->load->model('camera_model');

        // get_file_info helper
        $this->CI->load->helper('file');

        $this->system_info = $this->CI->system_model->get_system_info();
    }

    public function stop_recording($event_id, $part_id)
    {
        $ffmpeg = $this->system_info['ffmpeg'];

        $recordings = $this->CI->recording_model->get_recordings_by_event_id_and_part_id($event_id, $part_id);
        foreach ($recordings as $recording)
        {
            $recording_id = $recording['recording_id'];
            // WHY IS THE RECORDING MODEL QUERIED TWICE? $recording[] has all the data you need!?
            $rec = $this->CI->recording_model->get_recording_info_by_recording_id($recording_id);
            $cam 			= $rec['cam_number'];
            $cam_description= $rec['cam_description'];
            $cam_fps		= $rec['cam_fps'];
            $cam_constante	= $rec['cam_constante'];
            $copy 			= $rec['copy'];
            $rec_owner 		= $rec['rec_owner'];
            $file_info 		= get_file_info("./videos/$event_id/$part_id/tmp_rec/rec$cam.mp4");
            $size 			= $file_info['size'];
            $date_created 	= $file_info['date'];
            $server_path 	= $file_info['server_path'];
            $name 			= $file_info['name'];
            $clip_id 		= $this->CI->recording_model->recording_to_clip($recording_id, $event_id, $part_id, $name, $server_path, $size, $date_created, $rec_owner, $cam, $cam_description);
            $file_path 		= "videos/$event_id/$part_id";
            $file 			= "$file_path/tmp_rec/rec$cam.mp4";
            $keyframes 		= $cam_constante*2;

            //kill openrtsp
            if(isset($recording['process_id'])){
                $process_id = $recording['process_id'];
                $kill_openrtsp = "kill -HUP $process_id";
                shell_exec($kill_openrtsp);
            }

            //CREATE FILE
            if($copy == '1'){
                $stop_rec = "$ffmpeg -i $file -vcodec copy $file_path/$clip_id.mp4 </dev/null >/dev/null 2> $file_path/progress.txt &";
            }else{
                $stop_rec = "$ffmpeg -r $cam_fps -i $file  -vcodec libx264 -pix_fmt yuv420p -g $keyframes $file_path/$clip_id.mp4 </dev/null >/dev/null 2> $file_path/progress.txt &";
            }
            shell_exec($stop_rec);

            $screenshot = "$ffmpeg -i $file -ss 00:00:00.300 -y -f image2 -vcodec mjpeg -vframes 1 -s 360x240 -aspect 16:10 $file_path/$clip_id.jpg </dev/null >/dev/null &";
            shell_exec($screenshot);
        }
        return;
    }
}