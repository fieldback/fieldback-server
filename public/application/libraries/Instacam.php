<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
  * This class is the main controller for instant camera recording logic, on push record
  */

Class Instacam {

	private $CI;

	public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->model('system_model');
        $this->CI->load->model('instacording_model');
        $this->system_info = $this->CI->system_model->get_system_info();
        
        $this->CI->load->model('camera_model');
    }

		/**
		*	This function starts a short session with all available camera's and returns url's for created thumbnails
		*	These are placed in the tmp folder at /event_id/part_id, where it will also be deleted by the clear_tmp function (on_leave_page with javascript) - might not always be called but who cares
		*/
	public function get_thumbnails_for_active_cameras($event_id, $part_id)
	{
		// Create an empty array to return if no camera's active
		$thumbnails = array();
		// Get all instacams
		$insta_cameras = $this->CI->camera_model->get_all_insta_cameras();
		// Loop through the camera's to create a thumbnail for each of them
		foreach($insta_cameras as $insta_camera)
		{
			// The 'target' folder for the thumbnail to be placed in
			$target_path = "videos/" . $event_id . "/" . $part_id . "/tmp/" . $insta_camera['camera_id'] . ".jpg";
			// Create a thumbnail for this camera and put it at the target path
			if(self::_create_thumbnail($insta_camera['stream'], $insta_camera['cam_constante'], $target_path)) {
				// Push the thumbnail with the camera id into the thumbnails array
				$thumbnails[] = [
					'camera_id' 	=> $insta_camera['camera_id'],
					'description'	=> $insta_camera['description'],
					'thumbnail'		=> $target_path,
                    'online'        => true
				];
			} else {
                // Push the thumbnail with the camera id into the thumbnails array
                $thumbnails[] = [
                    'camera_id' 	=> $insta_camera['camera_id'],
                    'description'	=> $insta_camera['description'],
                    'thumbnail'		=> "images/icons/offline.png",
                    'online'        => false
                ];
            }
			
		}
		// Return the array
		return $thumbnails;
	}

    public function get_instacordings()
    {
        $instacordings = $this->CI->instacording_model->get_instacordings();
        return $instacordings;
    }

    /**
     * Get active instacordings for this part_id; useful to do something with these instacordings
     * @param int $part_id
     *
     * @return array with instacordings
     */
    public function get_active_instacordings_for_part_id($part_id)
    {
        $instacordings = $this->CI->instacording_model->get_active_instacordings_for_part_id($part_id);
        return $instacordings;
    }

	public function start_session($camera_ids, $event_id, $part_id)
	{
        $cam_counter = 0;
		//start session for all cameras so you can tag 
		foreach ($camera_ids as $camera_id) {
            $cam_counter++;
			// Get the rest of the info for each camera
			$camera = $this->CI->camera_model->get_stream_by_cam_id($camera_id);
			// (pass the url and camera identifier for each cam)
            $session_id = self::_start_session($camera['stream'], $camera['cam_constante']);
            // Create a thumbnail and place it at the tmp/rec folder until we finish the session
            $target_path = "videos/" . $event_id . "/" . $part_id . "/thumb_" . $cam_counter . ".jpg";
            if(self::_create_thumbnail($camera['stream'], $camera['cam_constante'], $target_path, $session_id)) {
                $this->CI->instacording_model->create_instacording($session_id, $camera['stream'], $camera['cam_constante'], $event_id, $part_id, $cam_counter, $camera['description'], $this->CI->data['user']->id);
            }
		}
        // Set the part type to instant
        $this->CI->load->model('part_model');
        $this->CI->part_model->set_part_type_to_instant($part_id);
		return true; // success - always...
	}

	public function stop_session_for($event_id, $part_id)
	{
		$instacordings = $this->CI->instacording_model->get_active_instacordings_for_part_id($part_id);
		foreach($instacordings as $instacording)
		{
			$clip_id = $this->CI->instacording_model->instacording_to_clip($instacording['instacording_id'], $instacording['part_id'], $instacording['rec_owner'], $instacording['cam'], $instacording['session_start']);
            // rename the thumbnail we created when starting the recording to the clip_id we received now
            $file_dir = "videos/" . $event_id . "/" . $part_id . "/";
            rename($file_dir . "thumb_" . $instacording['cam'] . ".jpg", $file_dir . $clip_id . ".jpg");
		}
		return true; // success
	}

    /**
     * The core responsibility of this function is to initiate a capture with a standard duration of x seconds
     * It creates a moment directory, and goes through the active instacamera's and activates the capturing
     *
     * @param     $event_id
     * @param     $part_id
     * @param int $duration
     *
     * @return bool success
     */
    public function start_capture($event_id, $part_id, $duration = -1)
    {
        // Get all active instacordings
        $instacordings = self::get_active_instacordings_for_part_id($part_id);
        if(!empty($instacordings)) {
            // Calculate the time_in_clip -> this is the difference between NOW and when the recording started
            $started_at = $instacordings[0]['session_start'];
            $now = now();
            $time_in_clip = $now - $started_at;

            // Fill the data array for a new moment
            $data = [
                'date_created' => $now,
                'time_in_clip' => $time_in_clip,
                'lead'         => 0, // There is no lead..
                'lapse'        => $duration, // This is the 'duration'
                'comment'      => ''
            ];
            // Create a new moment in the database
            $this->CI->load->model("moment_model");
            $moment_id = $this->CI->moment_model->create_moment($data, $part_id);
            // Create the capture_dir variable
            $capture_dir = "videos/" . $event_id . "/" . $part_id . "/";
            // Create a moment directory
            if (!is_dir($capture_dir . $moment_id)) {
                $old_umask = umask(0);
                mkdir($capture_dir . $moment_id, 0777);
                umask($old_umask);
            }

            // Create an empty array to hold capture_session_ids
            $capture_session_ids = array();

            // loop through the instacordings
            foreach ($instacordings as $instacording) {
                // Create a moment_file for this camera/moment
                $data = [
                    'path'		        => $capture_dir . $moment_id,
                    'cam'		        => $instacording['cam'],
                    'cam_description'	=> $instacording['cam_description'],
                    'is_uploaded'       => 0
                ];
                $moment_file_id = $this->CI->moment_model->add_moment_file($data, $moment_id);
                // start capturing this camera
                $capture_dir_moment = $capture_dir . $moment_id . "/";
                $capture_session_id = self::_start_capture($instacording['url'], $instacording['camera_identifier'], $instacording['session_id'], $capture_dir_moment, $moment_file_id, $duration);
                if($capture_session_id == false) { return false; }
                $capture_session_ids[] = $capture_session_id;
            }
            // Format a capture_data array for the controller to return
            $capture_data = [
                'capture_session_ids' => $capture_session_ids,
                'moment_id'           => $moment_id
            ];
            return $capture_data;
        }
        return false;
    }

    /**
     * This function loops through all the capture session ids it receives and delegated to the private methods _stop_capture
     * to stop the capturing of these capture sessions. It returns true on success, but the captures always stop after a defined time anyway
     *
     * @param $capture_session_ids
     *
     * @return bool
     */
    public function stop_capture($capture_session_ids)
    {
        $success[] = true;
        // We receive a list of capture_session_ids, which are unique and generated by the external software to keep track of recordings.
        // If it does not exist anymore or the recording is already over, it should not matter. But, if the capture is still running, terminate it
        foreach($capture_session_ids as $capture_session_id)
        {
            $success[] = self::_stop_capture($capture_session_id);
        }
        // check if all elements of success are true by testing each element
        foreach($success as $test) {
            if($test == false) return false;
        }
        return true;
    }
	
	/*
		*	To properly decouple the instacording_model from the rest of the app, this library is the only place where you should use this model. 
		*	When needed, the functions are simply passed through without much processing, keeping it all simpler to maintain
		*/

	/*
		*	Get a list of part_ids that have instacordings active, used for linking from active cordings 
		*/
	public function get_part_ids_of_active_instacordings()
	{
		return $this->CI->instacording_model->get_part_ids_of_active_instacordings();
	}
	
	/*
		*	Simply count the part_ids of active recordings to see if there is a recording
		*/
	public function check_active_instacordings()
	{
        $part_ids_of_active_recordings = $this->CI->instacording_model->get_part_ids_of_active_instacordings();
		return (!empty($part_ids_of_active_recordings));
	}


	////////////// ---------- PRIVATE METHODS OF LIBRARY -------------- //////////////////
	
		/*
		*	Create a thumbnail for a camera at $url and put it in $target_dir. Success = true, failure to connect or capture = false
		*/
	private function _create_thumbnail($url, $camera_identifier, $target_path, $session_id = 0)
	{
		// Check if active session is given or else create a new session
		if($session_id == 0)
		{
			// Start a session
			$session_id = self::_start_session($url, $camera_identifier);
			// Return true or false -> capture an image -> success?
			$success = self::_capture_image($url, $camera_identifier, $target_path);
			// Close the session
			self::_stop_session($url, $camera_identifier, $session_id);
		} else {
			// Return true or false -> capture an image -> success?
			$success = self::_capture_image($url, $camera_identifier, $target_path);
		}
		// Return true or false on succesfull creation of thumbnail
		return $success;
	}

	///////////// ----------- RPC section of library -> here we connect to the external camera controller ////////////////
    /*
     * Each camera has an $camera_identifier set in the database (through the standard camera settings) and an URL:
     * The camera_identifier is the 'camera_identifier' and the url is 'stream' from db
     * For example:
     * url:                 http://10.0.0.110
     * camera_identifier:   cam_1
     *
     * This can be done for IDS camera's as well with their Unique device ID's:
     * url:                 http://10.0.0.111
     * camera_identifier:   UadCjdALKdfwDw
     */

	/*
		* Start a session with a camera and return the session_id to be able to stop the session as well
		*/
	private function _start_session($url, $camera_identifier)
	{
        // Send a start session request to the camera
        $data = [
            'action'            => 'start_session',
            'camera_identifier' => $camera_identifier,
            'insta_key'             =>  $this->CI->data['system_info']['insta_key']
        ];
        $session_id = self::_send_POST($url, $data);

		// Return the session id or identifier to be able to stop the session with this camera (is saved in the instacordings database as well)
		return $session_id;
	}

	/*
		* Stop a session with a camera based on session_id and url of camera
		*/
	private function _stop_session($url, $camera_identifier, $session_id)
	{
        // Send a start session request to the camera
        $data = [
            'action'            => 'stop_session',
            'camera_identifier' => $camera_identifier,
            'session_id'        => $session_id,
            'insta_key'             =>  $this->CI->data['system_info']['insta_key']
        ];
        $stopped = self::_send_POST($url, $data);

		return $stopped;
	}

	/*
		*	Capture an image of an active camera and place it at the 'target_path' and return true on success
		*/
	private function _capture_image($url, $camera_identifier, $target_path)
	{
        $post_data = [
            'action'                =>  'get_thumbnail',
            'camera_identifier'     =>  $camera_identifier,
            'insta_key'             =>  $this->CI->data['system_info']['insta_key']
        ];
		// Open a capture which returns an image, and save this image in the tmp folder
        $success = self::_get_img_POST($url, $post_data, $target_path);
		return $success;
	}
	
	/*
		*	Capture a stream of images for $duration seconds and return success or failure
		*/
	private function _start_capture($url, $camera_identifier, $session_id, $target_dir, $moment_file_id, $duration = -1)
	{
        // Check if duration is set or is higher than the system wide max
        if($duration == -1 || $duration > $this->CI->data['system_info']['max_instacord_seconds']) $duration = $this->CI->data['system_info']['max_instacord_seconds'];

        // Send the record request to the camera, and gat a process_id as response
        $data = [
            'action'            => 'start_capture',
            'camera_identifier' => $camera_identifier,
            'session_id'        => $session_id,
            'duration'          => $duration,
            'unique_identifier' => base64_encode($moment_file_id),
            'insta_key'         => $this->CI->data['system_info']['insta_key']
        ];
        $response = self::_send_POST($url, $data);

        if($response != false) {
            return base64_encode(json_encode([$url, $camera_identifier, $session_id, $response]));
        } else {
            return false;
        }
	}
	
	/*
		*	Stop the stream capture if not already stopped by timeout
		*/
	private function _stop_capture($capture_session_id)
	{
        // Decode the data from the base64 string $capture_session_id
        $data_array = json_decode(base64_decode($capture_session_id));
        $url                = $data_array[0];
        $camera_identifier  = $data_array[1];
        $session_id         = $data_array[2];
        $process_id         = $data_array[3];

        $data = [
            'action'            => 'stop_capture',
            'camera_identifier' => $camera_identifier,
            'session_id'        => $session_id,
            'process_id'        => $process_id,
            'insta_key'         =>  $this->CI->data['system_info']['insta_key']
        ];

        $response = self::_send_POST($url, $data);

        return $response;

	}

    private function _send_POST($url, $post_data)
    {
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($post_data),
            CURLOPT_HTTP_VERSION => 1.0,
            CURLOPT_RETURNTRANSFER => 1, // return the result to pass to the page
            CURLOPT_TIMEOUT => 10
        );
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        $result = curl_exec($curl);
        // Get the result code
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($code == 200) {
            return $result;
        }

        return false;
    }

    private function _get_img_POST($url, $post_data, $target_path)
    {
        $fp = fopen($target_path, 'w');
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($post_data),
            CURLOPT_HTTP_VERSION => 1.0,
            CURLOPT_RETURNTRANSFER => 1, // return the result to pass to the page
            CURLOPT_BINARYTRANSFER => 1,
            CURLOPT_FILE => $fp,
            CURLOPT_TIMEOUT => 10
        );
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        curl_exec($curl);
        // Get the result code
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        fclose($fp);

        if($code == 200) {
            return true;
        }

        return false;
    }

}

// EOF