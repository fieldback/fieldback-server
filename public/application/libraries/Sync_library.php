<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Class Sync
 *
 * All authorisation and HTTP things are done in the controller, the library connects with the models and does the rest
 */

Class Sync_Library {

	private $CI;

	public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->model('system_model');
        $this->CI->load->model('sync_model');
        $this->CI->load->model('event_model');

        $this->system_info = $this->CI->system_model->get_system_info();
    }

    /********************************************************************/
    /*                    Event import checking                         */
    /********************************************************************/


    /**
     * @param $json_file
     * Check the JSON file and return an array of results
     * fill the 'imports' table with an unique Import ID and a list of files that need to be received
     * @return array
     */
    public function check_json($json_file) {
        // Create an empty files array for later
        $files = [];
        // Create an empty response array
        $response = [];
        // Load the models we need for checking
        $this->CI->load->model('user_model');
        $this->CI->load->model('team_model');
        $this->CI->load->model('tagfield_model');
        // Check for each element if they already exist on this system
        $response['creator'] = $this->CI->user_model->check_user_by(
            $json_file['creator']['user_id'],
            $json_file['creator']['username'],
            $json_file['creator']['first_name'],
            $json_file['creator']['last_name'],
            $json_file['creator']['email']
        );
        $response['team'] = $this->CI->team_model->check_team_by(
            $json_file['team']['team_id'],
            $json_file['team']['team_name']
        );
        $response['organisation'] = $this->CI->user_model->check_organisation_by(
            $json_file['organisation']['organisation_id'],
            $json_file['organisation']['organisation_name']
        );
        // Prepare empty 'tags' and 'playertags' responses
        $response['tags'] = [];
        $response['player_tags'] = [];
        // Loop through all parts
        foreach($json_file['parts'] as $part) {
            // Loop through all clips and add them to the files array
            if(array_key_exists('clips', $part)) {
                foreach ($part['clips'] as $clip) {
                    // Add to files array
                    $files[] = $clip['video_file'] . $clip['video_extension'];
                    $files[] = $clip['thumb_file'] . $clip['thumb_extension'];
                }
            }
            // Loop through all moments to get the files and the tags and player tags
            if(array_key_exists('moments', $part)) {
                foreach($part['moments'] as $moment) {
                    // Loop through the moment_files to add them to files array
                    foreach ($moment['moment_files'] as $moment_file) {
                        // Add to files array
                        $files[] = $moment_file['video_file'] . $moment_file['video_extension'];
                        $files[] = $moment_file['thumb_file'] . $moment_file['thumb_extension'];
                    }
                    // Loop through all tags
                    if(array_key_exists('tags', $moment)) {
                        foreach ($moment['tags'] as $tag) {
                            $response['tags'][] = $this->CI->tagfield_model->check_tag_by(
                                $tag['original_tag_id'],
                                $tag['tag_name']
                            );
                        }
                    }
                    // Loop through all playertag
                    if(array_key_exists('player_tags', $moment)) {
                        foreach ($moment['player_tags'] as $player_tag) {
                            $response['player_tags'][] = $this->CI->user_model->check_user_by(
                                $player_tag['original_player_tag_id'],
                                $player_tag['player_username'],
                                $player_tag['player_first_name'],
                                $player_tag['player_last_name'],
                                $player_tag['player_email']
                            );
                        }
                    }
                }
            }
        }
        // Add the files array to the response array
        $response['files'] = $files;
        // Return the response array which consists of trues and falses for each element that was present or not in this installation
        return $response;
    }

    /**
     * Add a list of files to the import cue, which is used to check against received files.
     * Returns the import timestamp that is created as an unique identifier for this import/export session
     * @param $files
     * @return int
     */
    public function add_files_to_import_cue($files) {
        // Set an unique import_id to group imports
        $import_timestamp = time();
        // Loop through the files and create a batched array
        $data = [];
        foreach($files as $file) {
            $data[] = [
                'import_timestamp'  =>  $import_timestamp,
                'unique_file_name'  =>  $file
            ];
        }
        $result = $this->CI->sync_model->add_to_import_cue($data);
        // Return the import timestamp as an unique identifier
        return $import_timestamp;
    }

    /**
     * Simply receive a file, check if it exists in the 'imports' table and return it's import_timestamp
     * @param $unique_file_id
     * @return int
     */
    public function get_import_timestamp($unique_file_id) {
        // Check if in $moment
        $import_timestamp = $this->CI->sync_model->get_import_timestamp($unique_file_id);
        // If file was oke return true
        if(!empty($import_timestamp)) {
            return $import_timestamp;
        } else {
            return 0;
        }
    }

    /**
     * Check if there are more uploads that still need to be received -> if so, return false. If last; true
     * @param $import_timestamp
     * @return mixed
     */
    public function check_if_last($import_timestamp) {
        // a bit dangerous, but check for imports with this timestamp and status of 0 with uploaded
        return $this->CI->sync_model->check_if_last($import_timestamp);
    }

    /********************************************************************/
    /*                          Event import                            */
    /********************************************************************/

    /**
     * Import an event and match all tags and other meta data the event has with this system.
     * Move the files from the uploads directory to a newly created event directory
     * Check if the entities exist, if not -> create new ones and use that id for insertion into the db
     * @param $json_file
     */
    public function import_event($json_file) {
        // For debugging:
        // var_dump($json_file);
        // Empty array of messages about the import
        $debug = [];
        $status = [];

        // Load the models we need for checking and importing
        $this->CI->load->model('user_model');
        $this->CI->load->model('event_model');
        $this->CI->load->model('moment_model');
        $this->CI->load->model('part_model');
        $this->CI->load->model('moment_model');
        $this->CI->load->model('clip_model');
        $this->CI->load->model('team_model');
        $this->CI->load->model('tagfield_model');

        // Check if the user is logged in; we can use it's id and organisation for creator if not matched in this system
        $logged_in = $this->CI->ion_auth->logged_in();
        if($logged_in) {
            $current_user = $this->CI->ion_auth->user()->row();
        }

        // Fill the array for new event, starting with simple JSON data:
        $event_name             = $json_file['event_name'];
        $event_description      = $json_file['event_description'];
        $date_created           = $json_file['date_created'];

        // Check if creator exists; otherwise use admin or current logged in user
        $creator_exists = $this->CI->user_model->check_user_by(
            $json_file['creator']['user_id'],
            $json_file['creator']['username'],
            $json_file['creator']['first_name'],
            $json_file['creator']['last_name'],
            $json_file['creator']['email']
        );
        if($creator_exists) {
            $creator_id = $json_file['creator']['user_id'];
            $debug[] = "Event creator set to: ".$json_file['creator']['username'];
        } else {
            if($logged_in) {
                $debug[] = "Event creator did not exist yet, setting it to current user.";
                $creator_id = $current_user->id;
            } else {
                $debug[] = "Event creator did not exist yet, setting it to the standard user.";
                $creator_id = 2; // Standard user for imports should always be user_id = 2
            }
        }

        // Check if organisation exists; otherwise use default organisation or the current user organisation
        $organisation_exists = $this->CI->user_model->check_organisation_by(
            $json_file['organisation']['organisation_id'],
            $json_file['organisation']['organisation_name']
        );
        if($organisation_exists) {
            $organisation_id = $json_file['organisation']['organisation_id'];
            $debug[] = "Organisation set to: ".$json_file['organisation']['organisation_name'];
        } else {
            // Get the organisation for the creator -> this can be the current user or the admin depending on login state
            $organisation_id = $this->CI->user_model->get_selected_organisation_for_user($creator_id);
            $debug[] = "Organisation of exported event did not exist; changed it to the current organisation.";
        }

        // Check if team exists; otherwise create a new team
        $team_exists = $this->CI->team_model->check_team_by(
            $json_file['team']['team_id'],
            $json_file['team']['team_name']
        );
        if($team_exists) {
            $team_id = $json_file['team']['team_id'];
            $debug[] = "Team set to: ".$json_file['team']['team_name'];
        } else {
            // For now, use a 'standard team' that we always add imported events to
            $team_id = 1;
            $debug[] = "Team did not exist on this server, set to the standard team.";
            // Create a new team using the team_model, which returns a team_id for the newly created team
            // $team_id = $this->CI->team_model->add_team_to_organisation(["team_name" => $json_file['team']['team_name']], $organisation_id);
        }

        /* Not used anymore -> uncomment if tags need to be imported when out of sync or manual imported
        // Loop through the JSON and find all tags and player tags. We need to add them only once to the database if they are not present in this system
        // initiate empty tag arrays
        $tag_array = [];
        $player_tag_array = [];
        // Loop through all moments
        foreach($json_file['parts'] as $part) {
            foreach($part['moments'] as $moment) {
                // Loop through all tags
                foreach($moment['tags'] as $tag) {
                    // If this tag_name does not exist in the tag_array, add it
                    if(array_key_exists($tag['tag_name'] ,$tag_array)) {
                        $tag_array[$tag['tag_name']] = [
                            "original_tag_id"   =>  $tag['original_tag_id'],
                            "tag_name"          =>  $tag['tag_name']
                        ];
                    } else {
                        // Do nothing; this tag is used double and we need it only once
                    }
                }
                // Loop through all player_tags
                foreach($moment['player_tags'] as $player_tag) {
                    // If this player_tag_username does not exist in the player_tag_array, add it
                    if(array_key_exists($player_tag['player_username'] ,$player_tag_array)) {
                        $player_tag_array[$player_tag['player_username']] = [
                            "player_tag_id"             =>  $player_tag['original_player_tag_id'],
                            "player_username"           =>  $player_tag['player_username'],
                            "player_first_name"         =>  $player_tag['player_first_name'],
                            "player_last_name"          =>  $player_tag['player_last_name'],
                            "player_email"              =>  $player_tag['player_email']
                        ];
                    } else {
                        // Do nothing; this tag is used double and we need it only once
                    }
                }
            }
        }
        // Loop through the newly created arrays and check
        foreach($tag_array as $key => $tag) {
            // Check if the tag exists
            $tag_exists = $this->CI->tagfield_model->check_tag_by(
                $tag['original_tag_id'],
                $tag['tag_name']
            );
            if($tag_exists) {

            }
        }

        $player_tag_exists = $this->CI->user_model->check_user_by(
            $player_tag['original_player_tag_id'],
            $player_tag['player_username'],
            $player_tag['player_first_name'],
            $player_tag['player_last_name'],
            $player_tag['player_email']
        );
        */


        // Create a new event in the database
        $new_event_id = $this->CI->event_model->import_new_event($event_name, $event_description, $date_created, $creator_id, $team_id, $organisation_id);
        $debug[] = "New event created with name: ".$event_name;

        // Create a folder for this event
        $event_dir = './videos/' . $new_event_id;
        if(!is_dir($event_dir)){
            $old_umask = umask(0);
            if (!mkdir($event_dir, 0777)) {
                set_status_header(500);
                die('Could not create event directory. Please contact an administrator.');
            }
            umask($old_umask);
        }

        // Loop through all parts
        foreach($json_file['parts'] as $part) {
            // Create a new part in the database
            $new_part = [
                "part_name"     =>  $part['part_title'],
                "date_created"  =>  $part['date_created'],
                "type"          =>  $part['type']
            ];
            $new_part_id = $this->CI->part_model->add_part_to_event($new_part, $new_event_id);
            $debug[] = "New part created with name: ".$part['part_title'];
            // Create a folder for this part
            $part_dir = $event_dir."/".$new_part_id;
            if(!is_dir($part_dir)){
                $old_umask = umask(0);
                if (!mkdir($part_dir, 0777)) {
                    set_status_header(500);
                    die('Could not create part directory. Please contact an administrator.');
                }
                umask($old_umask);
            }

            // Loop through all clips of this part IF they are present (only for streaming parts)
            if(array_key_exists('clips', $part)) {
                foreach($part['clips'] as $clip) {
                    // Create a new clip in database
                    $new_clip = [
                        "date_created"  =>  $clip['date_created'],
                        "kind"          =>  $clip['kind']
                    ];
                    $new_clip_id = $this->CI->clip_model->add_new_clip(
                        $new_clip,
                        $creator_id, // Use the creator_id we deducted earlier
                        $new_part_id,
                        $clip['cam_nr'] // Cam nr is saved in clip_to_part table, the add_new_clip inserts it there
                    );
                    if($clip['video_file'] != "" && $clip['video_extension'] != "" && $clip['thumb_file'] != "" && $clip['thumb_extension'] != "") {
                        // Move the clip files to their new location, as the event/part folder is now created and their clip_id is known (video file and thumbnail file)
                        if(!rename(
                            "./videos/uploads/".$clip['video_file'].$clip['video_extension'], // Old location (uploads folder)
                            $part_dir."/".$new_clip_id.$clip['video_extension'] // New location, in part_dir named after the clip_id with original extension
                        )) {
                            die("A video file could not be moved. Please contact an administrator.");
                        }
                        if(!rename(
                            "./videos/uploads/".$clip['thumb_file'].$clip['thumb_extension'], // Old location (uploads folder)
                            $part_dir."/".$new_clip_id.$clip['thumb_extension'] // New location, in part_dir named after the clip_id with original extension
                        )) {
                            die("A video thumbnail could not be moved. Please contact an administrator.");
                        }
                        $debug[] = "Clip from cam ".$clip['cam_nr']." added to part.";
                    } else {
                        set_status_header(500);
                        die("The video_file information is missing. Please contact an administrator.");
                    }
                }
            }

            // Loop through all moments
            if(array_key_exists('moments', $part)) {
                foreach($part['moments'] as $moment) {
                    // Add this moment to the database
                    $new_moment = [
                        "date_created"      =>  $moment['date_created'],
                        "time_in_clip"      =>  $moment['time_in_clip'],
                        "lead"              =>  $moment['lead'],
                        "lapse"             =>  $moment['lapse'],
                        "comment"           =>  $moment['comment']
                    ];
                    $new_moment_id = $this->CI->moment_model->create_moment($new_moment, $new_part_id);
                    $debug[] = "New moment with time_in_clip '".$moment['time_in_clip']."'' created.";
                    // Create a new folder for this moment
                    $moment_dir = $part_dir."/".$new_moment_id;
                    if(!is_dir($moment_dir)){
                        $old_umask = umask(0);
                        if (!mkdir($moment_dir, 0777)) {
                            set_status_header(500);
                            die('Could not create moment directory. Please contact an administrator.');
                        }
                        umask($old_umask);
                    }

                    // Loop through the moment_files for this moment to add to database and move files
                    foreach($moment['moment_files'] as $moment_file) {
                        // Check values for not-null
                        if(!array_key_exists('cam_nr', $moment_file)) { $moment_file['cam_nr'] = 0; }
                        if(!array_key_exists('cam_description', $moment_file)) { $moment_file['cam_description'] = ""; }
                        if(!array_key_exists('started_timestamp', $moment_file)) { $moment_file['started_timestamp'] = 0; }
                        if(!array_key_exists('fps', $moment_file)) { $moment_file['fps'] = 0; }
                        $new_moment_file = [
                            "path"              =>  $moment_dir,
                            "cam"               =>  $moment_file['cam_nr'],
                            "cam_description"   =>  $moment_file['cam_description'],
                            "started_timestamp" =>  $moment_file['started_timestamp'],
                            "fps"               =>  $moment_file['fps']
                        ];
                        $new_moment_file_id = $this->CI->moment_model->add_moment_file($new_moment_file, $new_moment_id);
                        if($moment_file['video_file'] != "" && $moment_file['video_extension'] != "" && $moment_file['thumb_file'] != "" && $moment_file['thumb_extension'] != "") {
                            // Move the moment_files to their new location, as we now know where to put them
                            if(!rename(
                                "./videos/uploads/".$moment_file['video_file'].$moment_file['video_extension'], // Old location (uploads folder)
                                $moment_dir."/".$new_moment_file_id.$moment_file['video_extension'] // New location, in part_dir named after the clip_id with original extension
                            )) {
                                die("A moment video file could not be moved. Please contact an administrator.");
                            }
                            if(!rename(
                                "./videos/uploads/".$moment_file['thumb_file'].$moment_file['thumb_extension'], // Old location (uploads folder)
                                $moment_dir."/".$new_moment_file_id.$moment_file['thumb_extension'] // New location, in part_dir named after the clip_id with original extension
                            )) {
                                die("A moment video thumbnail could not be moved. Please contact an administrator.");
                            }
                            $debug[] = "Moment file from cam ".$moment_file['cam_nr']." added to moment.";
                        } else {
                            set_status_header(500);
                            die("The video_file information is missing. Please contact an administrator.");
                        }
                    }

                    // Loop through all tags and see if they exist on this system
                    if(array_key_exists('tags', $moment)) {
                        foreach($moment['tags'] as $tag) {
                            // Check if tag exists on this system
                            $tag_exists = $this->CI->tagfield_model->check_tag_by(
                                $tag['original_tag_id'],
                                $tag['tag_name']
                            );
                            if($tag_exists) {
                                $tag_to_moment = [
                                    "tag_id"        =>  $tag['original_tag_id'],
                                    "moment_id"     =>  $new_moment_id,
                                    "user_id"       =>  $creator_id, // We use the standard creator as the owner of this tag
                                    "tag_name"      =>  $tag['tag_name']
                                ];
                                $this->CI->moment_model->add_tag_to_moment($tag_to_moment);
                                $debug[] = "Tag '".$tag['tag_name']."' existed and was added to moment: ".$new_moment_id;
                            } else {
                                // Tag does not exist: we delete is with a status message
                                // In future, this could change -> see commented part above
                                $debug[] = "Tag '".$tag['tag_name']."' did not exist on this system and was removed from moment: ".$new_moment_id;
                            }
                        }
                    }

                    // Loop through all player tags and see if they exist
                    if(array_key_exists('player_tags', $moment)) {
                        foreach($moment['player_tags'] as $player_tag) {
                            $player_tag_exists = $this->CI->user_model->check_user_by(
                                $player_tag['original_player_tag_id'],
                                $player_tag['player_username'],
                                $player_tag['player_first_name'],
                                $player_tag['player_last_name'],
                                $player_tag['player_email']
                            );
                            if($player_tag_exists) {
                                $player_to_moment = [
                                    "player_id"     =>  $player_tag['original_player_tag_id'],
                                    "moment_id"     =>  $new_moment_id,
                                    "user_id"       =>  $creator_id // We use the standard creator as the owner of this tag
                                ];
                                $this->CI->moment_model->add_player_to_moment($player_to_moment);
                                $debug[] = "Player tag '".$player_tag['player_first_name']." ".$player_tag['player_last_name']."' existed and added to moment: ".$new_moment_id;
                            } else {
                                // Player does not exist: we delete it with a status message
                                // In future, this could change -> see commented part above
                                $debug[] = "Player tag '".$player_tag['player_first_name']." ".$player_tag['player_last_name']."' did not exist on this system and was removed from moment: ".$new_moment_id;
                            }
                        }
                    }
                }
            }
        }
        $status[] = "Event import complete.";
        // Return the new event_id and the status message
        return [
            "new_event_id"  => $new_event_id,
            "debug"         => $debug,
            "status"        => $status
        ];
    }

    /********************************************************************/
    /*                          Event export                            */
    /********************************************************************/

    /**
     * Add an array of files from the 'export_event' to the upload cue, with the event_id and target_url (the import script at the other side)
     * @param $event_id int         event id is used for display purposes and to see if an event was already uploaded
     * @param $target_url string    url of the master import script
     * @param $files                array with files -> 'address', 'name'
     */
    public function add_to_upload_cue($event_id, $target_url, $files) {
        // Set an unique export id to group exports
        $export_timestamp = time();
        // Loop through to the $files and build an array for the model to process in a batch
        $data = [];
        foreach($files as $file) {
            $data[] = [
                'event_id'              =>  $event_id,
                'export_timestamp'      =>  $export_timestamp,
                'original_file_name'    =>  $file['address'],
                'unique_file_name'      =>  $file['name'],
                'upload_to'             =>  $target_url
            ];
        }
        $result = $this->CI->sync_model->add_to_upload_que($data);
        return $result;
    }

    public function set_to_uploaded($unique_file_name) {
        return $this->CI->sync_model->set_to_uploaded($unique_file_name);
    }

    /**
     * Export an event by putting all data in an array, with the 'event' as JSON
     * and the original files linked so that we can make a .zip or send them to a server
     * @return mixed
     */
    public function export_event($event_id) {
        // Create an empty placeholder for 'original files'
        $original_files = [];

        // Errors array -> if files did not exist put this in the error log
        $errors = [];

        // Some basic event info
        $event_info = $this->CI->event_model->get_event_info_by_event_id($event_id);

        // Fill the 'event' array with values -> this format should always stay the same
        $event['event_name']            = $event_info['event_name'];
        $event['event_description']    = $event_info['event_description'];
        $event['date_created'] = $event_info['date_created'];
        $event['creator'] = [
            "user_id"           => $event_info['user_id'],
            "username"          => $event_info['username'],
            "email"             => $event_info['creator']['email'],
            "first_name"        => $event_info['creator']['first_name'],
            "last_name"         => $event_info['creator']['last_name']
        ];
        $event['team'] = [
            "team_id"           => $event_info['team']['team_id'],
            "team_name"         => $event_info['team']['team_name']
        ];
        $event['organisation'] = [
            "organisation_id"   => $event_info['organisation_id'],
            "organisation_name" => $event_info['organisation_name']
        ];
        $event['parts'] = [];

        // Load part and moment models as we need them later
        $this->CI->load->model('part_model');
        $this->CI->load->model('moment_model');
        // Get all parts of this event and fill the parts array
        $parts = $this->CI->part_model->get_parts_for_event_by_event_id($event_id);

        // Loop through the part while filling the JSON output array
        foreach($parts as $part) {
            $p['part_title']            = $part['part_name'];
            $p['date_created']          = $part['date_created'];
            $p['type']                  = $part['type'];

            $p['clips']                 = [];
            // Fill the clips array with all clips of this part
            foreach($part['clips'] as $clip) {
                // The extensions are fixed
                $video_extension = ".mp4";
                $thumb_extension = ".jpg";
                // Build the current location of the files
                $file_location_base = "./videos/".$event_id."/".$part['part_id']."/".$clip['clip_id'];
                // Check if the file exists; otherwise we don't want to include it in the JSON
                if(file_exists($file_location_base.$video_extension) && file_exists($file_location_base.$thumb_extension)) {
                    // Each clip has a thumbnail and a video file -> create new, unique names for these with base64
                    $unique_video_name = urlencode(base64_encode(time().$file_location_base.$video_extension));
                    $unique_thumb_name = urlencode(base64_encode(time().$file_location_base.$thumb_extension));
                    // Fill the 'clips' array with stuff
                    $p['clips'][] = [
                        "video_file"        => $unique_video_name,
                        "video_extension"  => $video_extension,
                        "thumb_file"        => $unique_thumb_name,
                        "thumb_extension"   => $thumb_extension,
                        "cam_nr"            => $clip['cam'], // cam_nr from clip_to_part table
                        "date_created"      => $clip['date_created'],
                        "kind"              => $clip['kind']
                    ];
                    // Add the oringinal file to the original files array (video file and thumbnail file
                    $original_files[] = [
                        'address'           => $file_location_base.$video_extension,
                        'name'              => $unique_video_name.$video_extension
                    ];
                    $original_files[] = [
                        'address'           => $file_location_base.$thumb_extension,
                        'name'              => $unique_thumb_name.$thumb_extension
                    ];
                } else {
                    $errors[] = [
                        'type'              => "clip",
                        'file_id'           => $clip["clip_id"],
                        'msg'               => "A clip file was missing!"
                    ];
                }
            }

            $p['moments'] = [];
            // Get all moments for this part
            $moments = $this->CI->moment_model->get_all_moments_for_part($part['part_id']);

            // Fill the moments array with all moments of this part
            foreach($moments as $moment) {
                $m['date_created']      = $moment['date_created'];
                $m['time_in_clip']      = $moment['time_in_clip'];
                $m['lead']              = $moment['lead'];
                $m['lapse']             = $moment['lapse'];
                $m['comment']           = $moment['comment'];

                $m['moment_files']      = [];
                // Fill the moment_files array with all moment_clips of this moment
                foreach($moment['moment_files'] as $moment_file) {
                    // The extensions are fixed
                    $video_extension = ".mp4";
                    $thumb_extension = ".jpg";
                    // Build the current location of the files
                    $file_location_base = "./videos/".$event_id."/".$part['part_id']."/".$moment['moment_id']."/".$moment_file['moment_file_id'];
                    // Check if the file exists; otherwise we don't want to include it in the JSON
                    if(file_exists($file_location_base.$video_extension) && file_exists($file_location_base.$thumb_extension)) {
                        // Each clip has a thumbnail and a video file -> create new, unique names for these with base64
                        $unique_video_name = urlencode(base64_encode(time().$file_location_base.$video_extension));
                        $unique_thumb_name = urlencode(base64_encode(time().$file_location_base.$thumb_extension));
                        // Fill the moment_files array with the moment_file
                        $m['moment_files'][] = [
                            "video_file"        => $unique_video_name,
                            "video_extension"   => $video_extension,
                            "thumb_file"        => $unique_thumb_name,
                            "thumb_extension"   => $thumb_extension,
                            "cam_nr"            => $moment_file["cam"],
                            "cam_description"   => $moment_file["cam_description"],
                            "started_timestamp" => $moment_file["started_timestamp"],
                            "fps"               => $moment_file["fps"],
                        ];
                        // Add the original file to the original files array (video file and thumbnail file)
                        $original_files[] = [
                            'address'   => $file_location_base . $video_extension,
                            'name'      => $unique_video_name . $video_extension
                        ];
                        $original_files[] = [
                            'address'   => $file_location_base . $thumb_extension,
                            'name'      => $unique_thumb_name . $thumb_extension
                        ];
                    } else {
                        $errors[] = [
                            'type'              => "moment_clip",
                            'file_id'           => $moment_file['moment_file_id'],
                            'msg'               => "A moment file was missing!"
                        ];
                    }
                }

                $m['tags'] = [];
                // Fill the tags array with tags for this moment
                if(array_key_exists('tags', $moment)) {
                    foreach($moment['tags'] as $tag) {
                        $m['tags'][] = [
                            "original_tag_id"   => $tag["tag_id"],
                            "tag_name"          => $tag["tag_name"]
                        ];
                    }
                    $m['player_tags'] = [];
                }
                if(array_key_exists('players', $moment)) {
                    // Fill the player_tags array with player_tags for this moment
                    foreach($moment['players'] as $player_tag) {
                        $m['player_tags'][] = [
                            "original_player_tag_id"    => $player_tag["player_id"],
                            "player_username"           => $player_tag["username"],
                            "player_first_name"         => $player_tag["first_name"],
                            "player_last_name"          => $player_tag["last_name"],
                            "player_email"              => $player_tag["email"]
                        ];
                    }
                }
                // Add the moment to the moments array of this part, but only if there is at least 1 moment_file
                if(!empty($m['moment_files'])) {
                    $p['moments'][] = $m;
                }
            }
            // Add the part to the parts array in the event
            $event['parts'][] = $p;
        }

        // Add the original files to the export
        $export['original_files'] = $original_files;

        // Add the event to the export
        $export['event'] = $event;

        $export['error'] = $errors;

        return $export;
    }

    /********************************************************************/
    /*                        Settings sync                             */
    /********************************************************************/


    /**
     * As a slave server, use the external db_sync script from the command line to sync database tables between two Fieldback installs
     * Return a response array that can be used by the receiving scripts to determine status
     * @return array|mixed
     */
    public function sync_settings() {

        // Get settings from config and database class
        $this_db_hostname = $this->CI->db->hostname;
        $this_db_username = $this->CI->db->username;
        $this_db_password = $this->CI->db->password;
        $this_db_database = $this->CI->db->database;
        $master_db_hostname = $this->CI->config->item('master_db_hostname');
        $master_db_username = $this->CI->config->item('master_db_username');
        $master_db_password = $this->CI->config->item('master_db_password');
        $master_db_database = $this->CI->config->item('master_db_database');

        // Tables to sync:
        $sync_tables = [
            "groups",
            "opponents",
            "organisations",
            "player_to_team",
            "tag_to_organisation",
            "tag_to_tagfield",
            "tagfield_to_organisation",
            "tagfields",
            "tags",
            "team_to_organisation",
            "team_to_team_type",
            "team_types",
            "teams",
            "trigger_tag_to_organisation",
            "trigger_tag_to_tagfield",
            "trigger_tags",
            "user_levels",
            "user_profiles",
            "user_to_organisation",
            "user_to_tagfield",
            "user_to_team",
            "user_to_user_level",
            "user_to_user_profile",
            "users",
            "users_groups"
        ];

        // it should respond within a few seconds or the sync will fail anyway due to a bad connection (wifi?)
        $waitTimeoutInSeconds = 10;

        //initialize curl
        $curlInit = curl_init($master_db_hostname);
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT, $waitTimeoutInSeconds);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
        //get answer
        $response = curl_exec($curlInit);
        curl_close($curlInit);
        if ($response) {
            // Use command line tool db-sync.phar that is in this directory (libraries)
            exec(__DIR__."/db-sync.phar "
                .$master_db_username.":".$master_db_password."@".$master_db_hostname.":".$master_db_database." "
                .$this_db_username.":".$this_db_password."@".$this_db_hostname.":".$this_db_database." "
                ."--tables='".implode(",", $sync_tables)."' "
                ."-n " // No interaction -> this doesnt work from PHP
                ."--execute" // Uncomment if testing
            , $output);

            // Todo: go through output string and look for errors like -> unknown database, sql errors, etc. For now, just assume it went well.
            /* something like this:
            $response = [
                'error' => true,
                'msg'   => "Error while syncing!",
                'debug' => $output
            ];
             */

            // We assume/hope it went well, update the last_sync flag in the system_info table
            $this->CI->system_model->update_last_sync();
            // Everything went well, respond with an array with info
            $response = [
                'error' => false,
                'msg'   => "Synchronized!", // Todo: replace with language string for success
                'debug' => $output
            ];
        } else {
            $response = [
                'error' => true,
                'msg'   => 'The system is offline.', // Todo: replace with language string for error
                'debug' => ''
            ];
        }
        return $response;
    }

}
// EOF