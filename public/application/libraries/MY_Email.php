<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email
{  	
  	protected $CI;

	public function __construct($config = array() )
	{
		parent::__construct();
        $this->CI =& get_instance();
	}

	public function send()
	{
		// Get the mandrill Library!
		$this->CI->load->config('mandrill');

		$this->CI->load->library('mandrill');

		$mandrill_ready = NULL;

		/* Who is the sender? */
		$sender = $this->CI->config->item('mandrill_default_sender_email');
		if(!empty($this->_headers['From'])){
			$sender = $this->clean_email($this->_headers['From']);
		}
		

		/* Transform recipients to array for Mandrill */
		$recipients = array();
		if(!is_array($this->_recipients))
		{
			//turn string to array
			$this->_recipients = explode(", ", $this->_recipients);
		}
		foreach($this->_recipients as $recipient){
			//add array for each recipient and add email
			$recipients[] = array('email' => $recipient);
		}

		/* Transform CC recipients to array for Mandrill */
		if(array_key_exists('Cc', $this->_headers))
		{
			$cc = $this->_headers['Cc'];
			if(!is_array($cc))
			{
				// turn string to array
				$cc = explode(",", $cc);
			}
			foreach($cc as $c){
				// add array for each cc recipient and add email
				$c = trim($c);
				$recipients[] = array('email' => $c, 'type' => 'cc');
			}
		}

		/* Transform BCC recipients to array for Mandrill */	
		if(array_key_exists('Bcc', $this->_headers))
		{
			$bcc = $this->_headers['Bcc'];
			if(!is_array($bcc))
			{
				// turn string to array
				$bcc = explode(",", $bcc);
			}
			foreach($bcc as $bc){
				// add array for each cc recipient and add email
				$bc = trim($bc);
				$recipients[] = array('email' => $bc, 'type' => 'bcc');
			}
		}

		// is mandrill ready?
		try {

		    $this->CI->mandrill->init( $this->CI->config->item('mandrill_api_key') );
		    $mandrill_ready = TRUE;

		} catch(Mandrill_Exception $e) {

		    $mandrill_ready = FALSE;

		}

		// yes it is! 
		// Now send that e-mail!
		if( $mandrill_ready ) {

		    // Send us some email!
		    // set the template
		    $template_name = $this->CI->config->item('mandrill_template');

		    // tell us the content of the template
		    // name = mc:edit="name", content = the html to put in that place. 
		    $template_content = array(
		    		array(
				    	'name' => "content", 
				    	'content' => $this->_body,
				    ),
				    array(
				    	'name' => 'logo',
				    	'content' => '<img alt="logo" style="max-width: 100%" src="http://s3-media3.ak.yelpcdn.com/bphoto/wu-UW7LNOoyNvzUzD_JF-A/ls.jpg" />'
				    ),
				    array(
				    	'name' => 'image_header',
				    	'content' => '<img alt="header" style="width: 100%;" src="https://scontent-a-ams.xx.fbcdn.net/hphotos-prn2/t1.0-9/1507047_617619054951861_862387956_n.jpg" />'
				    )
		    );
		    //set the email information
		    $email = array(
		    	"html" => $this->_body,	    	   
		        'text' => $this->_body,
		        'subject' => $this->_headers['Subject'],
		        'from_email' => $this->CI->config->item('mandrill_default_sender_email'),
		        'from_name' => $this->CI->config->item('mandrill_default_sender_name'),
		        'to' => $recipients, //Check documentation for more details on this one
		    );

		    //do the sending please! 
		    $result = $this->CI->mandrill->messages_send_template($template_name, $template_content, $email);

		   	foreach($result as $res)
		   	{
		   		//is it send?! 
		   		if($res['status'] !== 'sent')
		   		{
		   			// let's hope this doesn't happen...
		   			return FALSE;
		   		}
		   	}

		   	// yes!
		   	return TRUE;
		}
	}
}

/* End of file MY_email.php */
/* Location: ./application/libraries/MY_email.php */
