<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Ffmpeg {

    public function init()
    {
        $CI = & get_instance();
        $CI->load->model('system_model');
        $system_info = $CI->system_model->get_system_info();
        $this->ffmpeg = $system_info['ffmpeg'];
    }

    /**
     * Cut moment with ffmpeg
     * @param  array $data 
     * 
     *         input            file to cut from
     *         vcodec           0 = libx264 , 1 = copy 
     *         seektime_cam
     *         vframes
     *         output_path
     *         output_name
     *   
     * @return [type]       [description]
     */ 
    public function cut_moment($data)
    {
        $this->init();
        
        //variables
        $input = $data['input'];
        if(isset($data['vcodec'])){
            switch ($data['vcodec']) {
                case '0':
                    $vcodec = 'libx264';
                    break;
                case '1':
                    $vcodec = 'copy';
                    break;
                default:
                    $vcodec = 'libx264';
                    break;
            }
        }
        $seektime_cam = $data['seektime_cam'];
        $duration = $data['duration'];
        $output_path = $data['output_path'];
        if(substr($output_path, -1) !== '/'){
            $output_path = $output_path . '/';
        }
        $output_name = $data['output_name'];
        $output_file = $output_path . $output_name . '.mp4';
        $output_log = $output_path . $output_name . '.txt';

        $exec = $this->ffmpeg . " -ss $seektime_cam -i $input -analyzeduration 2147483647 -strict -2 -vcodec $vcodec -t $duration $output_file </dev/null >/dev/null 2> $output_log";
        shell_exec($exec);

        if($data['thumb'] == 'true'){
            $data = array(
                'input' => $output_file,
                'output' => $output_path . $output_name . '.jpg',
                'output_log' => $output_path . $output_name . '-image.txt'
            );
            $this->thumbnail($data);
        }
        return;
    }

    public function thumbnail($data)
    {
        $input = $data['input'];
        $output = $data['output'];
        $output_log = $data['output_log'];

        $exec = $this->ffmpeg . " -i $input -analyzeduration 2147483647 -y -f image2 -vcodec mjpeg -s 360x240 -vframes 1 $output </dev/null >/dev/null 2> $output_log &";
        shell_exec($exec);
        return;
    }

}