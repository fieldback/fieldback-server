<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Export
 * The export class can send an event to the master, of receive a list of download links containing event files
 */
class Export extends MY_Controller {

    public function __construct(){
        parent::__construct();

		// Load system info
		$this->load->model('system_model');
		$this->data['system_info'] = $this->system_model->get_system_info();

		// Load sync library
		$this->load->library('sync_library');

        if(!$this->ion_auth->logged_in()){
            flash_error('not_logged_in');
            redirect('login');
        }

    }


	/**
	 * @param $event_id
	 * Creates a .json export of events that are flagged for syncing and sends this to the sync_master_url
	 */
	public function send_event_to_master($event_id) {
        // Export the event with the sync library and receive a list of files and the event JSON
        $export = $this->sync_library->export_event($event_id);
        // Define the target URL for this export -> this would be the place to change if you want to change this into multiple remotes
        $target_url = $this->config->item('master_import_url');
        // send the JSON to the server to announce incoming files
        $curl_options = array(
            CURLOPT_URL => $target_url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query([
                "sync_auth_key"    => $this->data['system_info']['sync_auth_key'], // Send the auth key for authentication
                "event_json"       => $export['event'] // Send the JSON so that the remote knows which files to expect
            ]),
            CURLOPT_HTTP_VERSION => 1.0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 10
        );
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        $result = curl_exec($curl);
        // Get the result code
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($code == 200) {
            // The page: 'api/import' echoes a json array with true/false values and the files list.
            // This is used to display if the export was accepted by the receiving system
            // A possibility would be to offer the user a choice to not upload an event if the systems were not in sync
            echo $result;
            // Something like this:
            // if($result['creator'] == false) { -> don't export and don't add to cue }

            // Add all the files to the 'sync_export' database table that is used to cue the uploads and monitor their status
            $cue_added = $this->sync_library->add_to_upload_cue($event_id, $target_url, $export['original_files']);
            // This should always work, so no further output needed
        } else if($code == 0) {
            // System offline
            set_status_header(503); // Offline code
            die("System offline.");
        } else {
            // other error -> display error
            set_status_header(500);
            die($result);
        }
	}


    /**
     * This function returns a JSON list with the upload status of events and their uploads (files).
     * Used on the /events/export/# page to indicate upload progress
     */
    public function get_upload_status($event_id) {
        $status['uploads_incomplete'] = $this->sync_model->get_export_status(true, $event_id);
        $status['uploads_complete'] = $this->sync_model->get_export_status(false, $event_id);
        echo json_encode($status);
    }

    /**
     * Get a array with download links to download -> an event might be too big to export at once
     * zip the event in a few .zip files depending on the size of the videos with the .json of the event and all the videos
     * the urls are base64 encoded to the location of the .zip in the download directory -> saves us from making another database table
     * @param $event_id
     */
    public function get_download_links($event_id) {
        $export = $this->sync_library->export_event($event_id);
        if($export != false) {
            // Create a directory for this download in the /videos/download/ directory for this event
            $exportdir = './videos/download/export_event_' . $event_id;
            if(!is_dir($exportdir)){
                $old_umask = umask(0);
                if (!mkdir($exportdir, 0777)) {
                    die('Could not create event export directory. Please contact an administrator.');
                }
                umask($old_umask);
            }
            // Create a for loop for multiple files if needed, for now just a single .zip for a single event
            $file_name = "fb_event_videos_".$event_id.".fb"; // Todo: better name for exported event
            $destination = $exportdir."/".$file_name;

            // Create the .JSON file in the same directory
            $json_file_location = $exportdir.'/fb_event_'.$event_id.'.json';
            $fp = fopen($json_file_location, 'w');
            fwrite($fp, json_encode($export['event']));
            fclose($fp);

            // Include the newly written .json into the .zip we are going to create
            /*
            $export['original_files'][] = [
                'address'   => $json_file_location,
                'name'      => "event.json"
            ];
            */
            // The .json should also be downloaded
            $download_links[] = [
                "url"   => site_url("export/download/".urlencode(base64_encode($json_file_location))),
                "name"  => "fb_event.json"
            ];

            if(!empty($export['original_files'])) {
                // Initiate CI zip library
                $this->load->helper('zip');
                // Create the zip with the original files and their new names
                if(prepare_zip($export['original_files'], $destination)) {
                    $download_links[] = [
                        "url"   =>  site_url("export/download/".urlencode(base64_encode($destination))),
                        "name"  =>  "fb_event_videos.fb"
                    ];
                } else {
                    // error happens when the zip fails -> mostly because a a file does not exist
                    set_status_header(400);
                    die("The download links could not be created, please contact an administrator.");
                }
            }
            // Output the links (even when there are no files)
            $this->output->set_content_type('application/json')
                ->set_output(json_encode($download_links));
        } else {
            set_status_header(400);
            die("The export failed. Please contact an administrator.");
        }
    }


    public function download($file_id) {
        $destination = base64_decode(urldecode($file_id));
        // Check for safety if this file is within the download directory
        if(dirname(dirname($destination)) == './videos/download') {
            // Check some errors before outputting the file
            if(headers_sent()) {
                die("HTTP header already sent");
            } else {
                if (!is_file($destination)) {
                    header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
                    die('File not found. Please contact an administrator.');
                } else if (!is_readable($destination)) {
                    header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
                    die('File not readable. Please contact an administrator.');
                } else {
                    // SEND THE FILE AS A FORCED DOWNLOAD
                    header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
                    header("Content-Type: application/zip");
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-Length: ".filesize($destination));
                    header("Content-Disposition: attachment; filename=\"".basename($destination)."\"");
                    readfile($destination);
                    exit;
                }
            }
        } else {
            set_status_header(400);
            die("You are not allowed to download this file.");
        }
    }
	
}


// EOF