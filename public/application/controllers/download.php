<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends MY_Controller {

	function __construct(){
		parent::__construct();
		
		// Check if logged in
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		
		// Load models
		$this->load->model('moment_model');
	}
	
	/**
		Download a single video clip defined by moment_clip_id (a single moment can have multiple moment_clips (one for each camera)
		site_name/download/moment_clip/id
		*/
	public function moment_clip($moment_file_id = '') {
		$moment_file_info = $this->moment_model->get_moment_file_info_by_moment_file_id($moment_file_id);
		if(!empty($moment_file_info)) {
			// Create the file location form path and filename info
			$file_location = $moment_file_info['path'] . "/" . $moment_file_info['filename'];
			// Create new title for human readable downloadable content -> watch out for strange characters - not all browsers support these (keep it simple)
			$file_title = date("Ymd H-i", $moment_file_info['moment_timestamp']) . "-" . $moment_file_info['event_name'] . "-" . $moment_file_info['part_name'] . "-" . $moment_file_info['cam'] ."_". $moment_file_info['cam_description'] . ".mp4";
			// Check some errors before outputting the file
			if (headers_sent()) {
			    die("HTTP header already sent");
			} else {
			    if (!is_file($file_location)) {
			        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			        echo 'File not found';
			    } else if (!is_readable($file_location)) {
			        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
			        echo 'File not readable';
			    } else {
			        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
			        header("Content-Type: application/zip");
			        header("Content-Transfer-Encoding: Binary");
			        header("Content-Length: ".filesize($file_location));
			        header("Content-Disposition: attachment; filename=\"".$file_title."\"");
			        readfile($file_location);
			        exit;
			    }
			}
		} else {
			// The moment_file_id is empty: something is missing (event is gone, camera is deleted, etc... should not happen!)
			die("moment_file_id does not exist! Contact the system administrator!");
		}
	}
	
	/**
		Download all moment_clips of a moment in a .zip file
		site_name/download/moment/id
		*/
	public function moment($moment_id = '') {
		// Initiate CI zip library
		$this->load->helper('zip');
		// Get all moment_clips for this moment
		$moment_files = $this->moment_model->get_moment_files_by_moment_id($moment_id);
		// see if there was a result:
		if(!empty($moment_files)) {
			// Create a files array for the zip_helper function
			$files = array();
			// Go through all moments and collect the moment_file info
			foreach($moment_files as $moment_file) {
				// Get additional info about the file
				$moment_file_info = $this->moment_model->get_moment_file_info_by_moment_file_id($moment_file['moment_file_id']);
				// Create the address of the file
				$file_address = $moment_file_info['path'] . "/" . $moment_file_info['filename'];
				// Make a more human readable name
				$file_name = date("Ymd H-i", $moment_file_info['moment_timestamp']) . "-" . $moment_file_info['event_name'] . "-" . $moment_file_info['part_name'] . "-nr " . $moment_id . "-" . $moment_file_info['cam'] ."_". $moment_file_info['cam_description'] . ".mp4";
				// Add to the $files array
				array_push($files, array(
					'address' => $file_address,
					'name' => $file_name
				));
			}
			// Get info for naming the .zip
			$moment_info = $this->moment_model->get_moment_meta_info($moment_id);
			// Create a filename for the zip from the event and part name
			$file_name = date("Ymd H-i", $moment_info['date_created']) . "-" . $moment_info['event_name'] . "-" . $moment_file_info['part_name'] ."-nr " . $moment_id . ".zip";
			// Where to post this zip?
			$destination = "videos/download/" . $file_name;
			$zip_created = prepare_zip($files, $destination);
			if($zip_created) {
				// Check some errors before outputting the file
				if (headers_sent()) {
				    die("HTTP header already sent");
				} else {
				    if (!is_file($destination)) {
				        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
				        echo 'File not found';
				    } else if (!is_readable($destination)) {
				        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
				        echo 'File not readable';
				    } else {
				        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
				        header("Content-Type: application/zip");
				        header("Content-Transfer-Encoding: Binary");
				        header("Content-Length: ".filesize($destination));
				        header("Content-Disposition: attachment; filename=\"".$file_name."\"");
				        readfile($destination);
				        exit;
				    }
				}
			    
			} else {
				// Zip was not created.. hmm
				die("Creating the zip did not work. Please contact the system administrator.");
			}
		} else {
			// Moment files not found
			die("This moment does not have any moment files. Please contact the system administrator.");
		}
	}
	
	/*
		Download all moments of a part -> biggest export
		Goes through all moments of a part, and creates a big zip file with all moment_files with proper names
		*/
	public function moments_of_part($part_id = '') {
		// Initiate CI zip library
		$this->load->helper('zip');
		// Get all moments for this part
		$moments = $this->moment_model->get_all_moments_for_part($part_id);
		// Check if there was a result
		if(!empty($moments)) {
			// dump($moments, true);
			
			// Create a files array for the zip_helper function
			$files = array();
			// Loop through all moments
			foreach($moments as $moment) {
				// Loop through all moment_files for this moment (the get_all_moments_for_part returns this as well)
				foreach($moment['moment_files'] as $moment_file) {
					// Get additional info about the file
					$moment_file_info = $this->moment_model->get_moment_file_info_by_moment_file_id($moment_file['moment_file_id']);
					// Create the address of the file
					$file_address = $moment_file_info['path'] . "/" . $moment_file_info['filename'];
					// Make a more human readable name
					$file_name = date("Ymd H-i", $moment_file_info['moment_timestamp']) . "-" . $moment_file_info['event_name'] . "-" . $moment_file_info['part_name'] . "-nr " . $moment['moment_id'] . "-" . $moment_file_info['cam'] ."_". $moment_file_info['cam_description'] . ".mp4";
					// Add to the $files array
					array_push($files, array(
						'address' => $file_address,
						'name' => $file_name
					));
				}
			}
			// Get the part meta info to name the .zip file
			$part_info = $this->moment_model->get_part_meta_info($part_id);
			// Check if correct
			if(!empty($part_info)) {
				// Create a filename for the zip from the event and part name
				$file_name = date("Ymd H-i", $part_info['date_created']) . "-" . $part_info['event_name'] . "-" . $part_info['part_name'] .".zip";
				// Where to post this zip?
				$destination = "videos/download/" . $file_name;
				$zip_created = prepare_zip($files, $destination);
				if($zip_created) {
					// Check some errors before outputting the file
					if (headers_sent()) {
					    die("HTTP header already sent");
					} else {
					    if (!is_file($destination)) {
					        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
					        echo 'File not found';
					    } else if (!is_readable($destination)) {
					        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
					        echo 'File not readable';
					    } else {
					        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
					        header("Content-Type: application/zip");
					        header("Content-Transfer-Encoding: Binary");
					        header("Content-Length: ".filesize($destination));
					        header("Content-Disposition: attachment; filename=\"".$file_name."\"");
					        readfile($destination);
					        exit;
					    }
					}
				    
				} else {
					// Zip was not created.. hmm
					die("Creating the zip did not work. Please contact the system administrator.");
				}
			}
		} else {
			// The part has no moments -> clicking the download link should not be possibles
			die("This part has no moments to download.");
		}

	}
	
}


// EOF