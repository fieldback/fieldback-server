<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_part extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('part_model');
		$this->load->model('system_model');
		$this->data['system_info'] = $this->system_model->get_system_info();
		$this->load->helper('date');

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}
	public function index()
	{
		redirect('events');
	}
	public function edit_part_info()
	{
		$part_id = $this->input->post('part_id');
		$part_info_id = $this->input->post('part_info_id');
		$part_info_text = $this->input->post('part_info_text');
		if($part_info_id == "part_name"){
			$data = "<div class='input-group'><input id='part_name' class='form-control edit-part-field' type='text' data-value='" . $part_info_text . "' value='" . $part_info_text . "' /><span class='input-group-btn'><input class='edit_submit btn btn-primary' id='part_name_submit' type='submit' onclick='updatePartInfo(&apos;part_name&apos;," . $part_id . ")' value='OK' /><input class='btn btn-default' id='cancel_update_part' type='submit' onclick='cancelUpdatePart(" . $part_id . ")' value='X' /></span></div>";
			echo $data;
		}
	}
	public function update_part_info()
	{
		$part_id = $this->input->post('part_id');
		$part_info_id = $this->input->post('part_info_id');
		$part_info_new_text = $this->input->post('part_info_new_text');
		if($part_info_new_text == ''){
			$data = "error1";
			echo json_encode($data);
		}else{
			$data = array(
				$part_info_id => $part_info_new_text
			);
			$this->part_model->update_part_information($data, $part_id);
			$part_info = $this->part_model->get_part_info_by_part_id($part_id);
			echo json_encode($part_info);
		}
	}
	
}
