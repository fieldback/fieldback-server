<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('event_model');
		$this->load->model('system_model');
		$this->load->model('part_model');
		$this->load->model('team_model');

		$this->data['system_info'] = $this->system_model->get_system_info();

		$this->load->helper('date');
		$this->load->helper('disk');
		
		if(!logged_in()){
			redirect('login');
		}

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
		$this->load->library('form_validation');
	}
	/*VIEWS*/
	public function index()
	{
		$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Events',
					'link' => 'events',
					'state' => 'active'
				)
			);

		//get all the events by the organisation and for that user
		$user = $this->ion_auth->user()->row();
		if(is_player()){
			$this->data['events_player'] = $this->event_model->get_all_events_for_player($user->id, $this->session->userdata('organisation_id'));
		}
		if(has_teams()){
			$this->data['events_owner'] = $this->event_model->get_all_events_for_owner($user->id, $this->session->userdata('organisation_id'));
		}
		if(is_admin()){
			$this->data['events_admin'] = $this->event_model->get_all_events_by_organisation($this->session->userdata('organisation_id'));
		}

		//load form to create new event
		if(is_admin()){
			$data['teams'] = $this->team_model->get_all_teams_for_organisation($this->data['user']->selected_organisation['organisation_id']);
		}else{
			$data['teams'] = $this->team_model->get_all_teams_for_user_and_organisation($this->data['user']->id, $this->data['user']->selected_organisation['organisation_id']);
		}	
		$this->data['create_event_form'] = $this->load->view('events/create_event_form', $data);

		$this->template->load('templates/template_view', 'events/events_view', $this->data);
		return;
	}
	public function event($event_id)
	{
		$this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
		// Test to see if this event exists by checking the results from the first query (returns empty when no event)
		if(!empty($this->data['event_info'])) {
			$this->data['parts'] = $this->part_model->get_parts_for_event_by_event_id($event_id);
			$this->data['team'] = $this->event_model->get_team_for_event($event_id);
			$this->data['user_teams'] = $this->team_model->get_all_teams_for_user_and_organisation($this->data['user']->id, $this->session->userdata('organisation_id'));
			if(empty($this->data['team'])){
				$this->data['team']['team_id'] = 'undefined';
			}
			if(is_admin() || has_team($this->data['team']['team_id']) || is_player_of_team($this->data['team']['team_id'])){
				$this->data['breadcrumbs'] = array(
					'home' => array(
						'name' => 'Home',
						'link' => 'home',
						'state' => 'none'
					),
					'events' => array(
						'name' => 'Events',
						'link' => 'events',
						'state' => 'none'
					),
					'event' => array(
						'name' => $this->data['event_info']['event_name'],
						'link' => 'events/event/' . $event_id,
						'state' => 'active'
					)
				);
	
				$this->template->load('templates/template_view', 'events/event_view', $this->data);
				return;
			} else {
				// the user has no permission to see this event and needs to be redirected home
				flash_error('no_permission');
				redirect('home');
			}
		} else {
			// The event does not exist
			flash_error('event_does_not_exist');
			redirect('home');			
		}
	}
	
	public function events_list()
	{
		$this->data['head']['javascripts'][] = 'vendor/canvasjs.min.js';

		if(is_admin()){

            $this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Events',
					'link' => 'events',
					'state' => 'none'
				),
				'event_list' => array(
					'name' => 'Events list',
					'link' => 'events/events_list',
					'state' => 'active'
				)
			);		

			$user_id = $this->data['user']->id;
			
			$this->data['space']['total'] = disk_total_space('/');
			$this->data['space']['free'] = disk_free_space('/');

			if(is_admin()){
				$this->data['events'] = $this->event_model->get_all_events_by_organisation($this->session->userdata('organisation_id'));
			}else{
				$this->data['events'] = $this->event_model->get_all_events_by_user_id($user_id, $this->session->userdata('organisation_id'));
			}
			
			//add directory sizes 
			foreach ($this->data['events'] as &$event) {
				$event['folder_size'] = get_folder_size('./videos/' . $event['event_id']);
			}

			$this->template->load('templates/template_view', 'events/events_list', $this->data);
		}else{
			flash_error('no_permission');
			redirect('home');
		}
	}

	/**
	 * Create event
	 * This creates a new event with all information
	 * @return  event view
	 */
	public function create_event()
	{
		// load parts controller in this controller
		$this->load->library('../controllers/parts');

		$this->form_validation->set_rules('event[event_name]', 'Event name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('event[event_description]', 'Event description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('team_id', 'Team', 'required');
		
		$this->form_validation->set_error_delimiters('<div class="bg-warning">', '</div>');

		if($this->form_validation->run()) {

			//input
			$event_info = $this->input->post('event');
			$event_info['date_created'] = now();
			$team_id = $this->input->post('team_id');

			// who is the event owner? this is the event owner!
			$event_owner = null; // default value
			if(is_admin()){
				$event_owner = $this->input->post('event_owner');
			}

			// create the event and get the id
			$event_id = $this->event_model->create_event($event_info, $team_id, $event_owner);

			// make dir for the event
			if(!is_dir("./videos/$event_id")){
				$old_umask = umask(0);
				mkdir("./videos/$event_id", 0777);
				umask($old_umask);
			}

			// create first part for event
			$this->parts->add_part_to_event($event_id);
			
			// redirect to the event
			flash_success('event_added');
			redirect('events/event' . '/' . $event_id);

		}else{
			self::index();
		}
		
	}
	
	public function delete_event($event_id = null) {
		$this->load->helper('file');
		if($event_id !== null)
		{
			// See if the user has the rights to delete the event (is also checked in javascript - but not safe enough)
			if(is_admin() || has_event($event_id)) {
				// *bugfix: Check if the event is still recording, if yes give a flash_error
				$this->load->model('recording_model');
				$recording = $this->recording_model->get_recording_by_event_id($event_id);
				// Check if empty -> does it exist?
				if(empty($recording)) {
					//delete the event
					$this->event_model->delete_event_by_event_id($event_id);
					if(is_dir('./videos/' . $event_id))
					{
						//delete the files in the event folder and the folder itself
						delete_files('./videos/' . $event_id);
						exec("rm -r ./videos/$event_id");
					}
					flash_success('event_deleted');
					// redirect to home, because coaches and non admins are not allowed to see the events_list
					redirect('home');
				} else {
					flash_error('event_has_recordings');
					redirect('home');
				}
				
			}
		}
		flash_error('event_not_deleted');
		redirect('home');
	}

	// AJAX REQUESTS
	
	/**
	 * Get all the owners for a team. Returns the owners in array
	 * @param  int $team_id The ID of the team
	 * @return array          the owners
	 */
	public function get_all_owners_for_team() {

		$team_id = $this->input->post('team_id');

		$owners = $this->team_model->get_all_owners_for_team($team_id);

		echo json_encode($owners);
		return;

	}

    /**
     * Show the 'export' page where users can download an event or send an event to a remote server
     * Most of the stuff is done through ajax so this just shows the template file
     * @param $event_id
     */
    public function export($event_id) {
        // Get event info
        $this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
        $this->data['event_id'] = $event_id;
        $this->data['master_server_active'] = $this->config->item('master_server_active');
        $this->data['server_name'] = $this->config->item('master_db_name');
        if(!empty($this->data['event_info'])) {
            $this->data['breadcrumbs'] = array(
                'home' => array(
                    'name' => 'Home',
                    'link' => 'home',
                    'state' => 'none'
                ),
                'events' => array(
                    'name' => 'Events',
                    'link' => 'events',
                    'state' => 'none'
                ),
                'event' => array(
                    'name' => $this->data['event_info']['event_name'],
                    'link' => 'events/event/' . $event_id,
                    'state' => 'none'
                ),
                'export' => array(
                    'name' => 'Export event',
                    'link' => 'events/event/export/'.$event_id,
                    'state' => 'active'
                )
            );
        }

        $this->template->load('templates/template_view', 'events/export_view', $this->data);
    }

    public function import() {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none'
            ),
            'events' => array(
                'name' => 'Events',
                'link' => 'events',
                'state' => 'none'
            ),
            'import' => array(
                'name' => 'Import event',
                'link' => 'events/import/',
                'state' => 'active'
            )
        );

        $this->template->load('templates/template_view', 'events/import_view', $this->data);
    }
}
