<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Moments extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('system_model');
		$this->load->model('part_model');
		$this->load->model('event_model');
		$this->load->model('clip_model');
		$this->load->model('camera_model');
		$this->load->model('moment_model');
		$this->load->model('recording_model');
		$this->load->model('tagfield_model');
		$this->load->model('team_model');
        $this->load->model('playlist_model');

		$this->load->helper('date');
		$this->load->helper('file');

		$this->load->library('ffmpeg');

        $ffmpeg     = $this->data['system_info']['ffmpeg'];
        $openrtsp   = $this->data['system_info']['openrtsp'];
	}
	public function index()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		if(is_player() || has_teams() || is_admin()){

			$this->data['head']['javascripts'][] = 'vendor/bootstrap-select.min.js';
			$this->data['head']['stylesheets'][] = 'vendor/bootstrap-select.min.css';
			$this->data['head']['javascripts'][] = 'vendor/fuelux.min.js';
			$this->data['head']['stylesheets'][] = 'vendor/fuelux.min.css';
			$this->data['head']['javascripts'][] = 'vendor/bootstrap-datepicker.js';
			$this->data['head']['stylesheets'][] = 'vendor/bootstrap-datepicker.css';


			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'moments' => array(
					'name' => 'Moments',
					'link' => 'moments',
					'state' => 'active'
				)
			);
            $this->data['head']['javascripts'][] = 'video.controls.js';

			$user = $this->ion_auth->user()->row();
			$this->data['moments'] = $this->moment_model->get_all_moments_for_user_and_organisation($user->id, $this->session->userdata('organisation_id'));
			if(is_admin()){
				$this->data['events'] = $this->event_model->get_all_events_by_organisation($this->session->userdata('organisation_id'));
			}else{
				$this->data['events'] = $this->event_model->get_all_events_by_user_id($user->id, $this->session->userdata('organisation_id'));

			}
			$this->data['players'] = $this->team_model->get_all_players_for_organisation($this->session->userdata('organisation_id'));
			$this->data['teams'] = $this->team_model->get_all_teams_for_user_and_organisation($user->id, $this->session->userdata('organisation_id'));
			$this->data['tags'] = $this->tagfield_model->get_tags_for_organisation($this->session->userdata('organisation_id'));
            $this->data['playlists'] = $this->playlist_model->get_all_playlists_for_user($this->data['user']->id);
			$this->template->load('templates/template_view', 'moments/moments_view', $this->data);
		}else{
			flash_error('no_permission');
			redirect('home');
		}
	}

	public function search()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}

		$options['events'] = $this->input->post('events');
		$options['teams'] = $this->input->post('teams');
        $options['playlist_ids'] = $this->input->post('playlist_ids');
		$options['date_from'] = strtotime($this->input->post('date_from'));
		$options['date_to'] = strtotime($this->input->post('date_to'));
		$options['tags'] = $this->input->post('selected_tags');
		$options['players'] = $this->input->post('selected_players');
        $options['playlists'] = $this->input->post('selected_playlists');
        $options['part_ids'] = $this->input->post('part_ids');

        if(empty($options['events'])){
			$options['events'] = array();
		}
		if(empty($options['teams'])){
			$options['teams'] = array();
		}
        if(empty($options['playlist_ids'])){
            $options['playlist_ids'] = array();
        }
        if(empty($options['tags'])){
            $options['tags'] = array();
        }
        if(empty($options['players'])){
            $options['players'] = array();
        }
        if(empty($options['playlists'])){
            $options['playlists'] = array();
        }
        if(empty($options['part_ids'])){
            $options['part_ids'] = array();
        }
        $this->data['moments'] = $this->moment_model->search_moments($options);

		// $result = $this->load->view('moments/search_results', $this->data);

		echo json_encode($this->data['moments']);
	}
	/* LIVE TAGGING */
	public function create_moment($data = null)
	{
		$i = 1;
		if($this->input->post()){
			/* variables */
			$part_id 	= $this->input->post('part_id');
			$event_id 	= $this->input->post('event_id');
			$lead 		= $this->input->post('lead');
			$lapse 		= $this->input->post('lapse');

		}else{
			$part_id = $data['part_id'];
			$event_id = $data['event_id'];
			$lead = $data['lead'];
			$lapse = $data['lapse'];
		}

		$now = now();


		/* check if recording */
		$recordings = $this->recording_model->get_recordings_by_event_id_and_part_id($event_id, $part_id);
		if(!empty($recordings)){
			foreach ($recordings as $recording)
			{
				if($i == 1){
					$start_time = $recording['start_time'];
					$seektime 	= $now - $start_time;
					$data = array(
						'date_created' 	=> $now,
						'time_in_clip' 	=> $seektime,
						'lead'			=> $lead,
						'lapse'			=> $lapse
					);
					$moment_id = $this->moment_model->create_moment($data, $part_id);
				}
				$i++;
			}
		}else{
			$seektime = $this->input->post('seektime');
			$seektime = round($seektime);
			$data = array(
				'date_created' => $now,
				'time_in_clip' => $seektime,
				'lead' => $lead,
				'lapse' => $lapse
			);
			$moment_id = $this->moment_model->create_moment($data, $part_id);
		}
		if($this->input->post()){
			echo json_encode($moment_id);
		}

		return $moment_id;

	}


    /**
     * Start capturing a moment for instant camera's
     *
     * @return json reply
     */
    public function insta_create($data = false)
    {
        $ajax = false;
		// check if post or get
		if($data === false) {
            $ajax = true;
	        // Get the post parameter $part_id, it's all we need if tagging from a page
	        $data['part_id'] = $this->input->post('part_id');
	        $data['event_id'] = $this->input->post('event_id');
		}
        // Load the library and start capture -> this returns the capture session id's we * could * use to stop the capture again
        $this->load->library("instacam");
        $capture_data = $this->instacam->start_capture($data['event_id'], $data['part_id']);
        if($capture_data != false) {
            // Get the HTML representation of this newly created moment -> a bit dirty as we just created it and we are querying the server again...
            // Get a single moment by ID and put this into the moments array so that the view makes the pretty HTML for us
            $data['moments'][] = $this->moment_model->get_moment_by_id($capture_data['moment_id']);
            $moment_html = $this->load->view('moments/tagged_insta_moments_view', $data, true);
            // Create a json response with the HTML and the session ids
            $json_response = [
                'capture_session_ids' => $capture_data['capture_session_ids'],
                'moment_html'         => $moment_html
            ];
			if($ajax) {
            	echo json_encode($json_response);
			}else{
				return $json_response;
			}
        } else {
			if($ajax) {
				echo json_encode($capture_data);
			}else{
				return false;
			}
        }
    }

    /**
     * Stop the insta recording by its session id
     */
    public function insta_stop($capture_session_ids = false)
    {
        $ajax = false;
		// check if post or get
		if($capture_session_ids === false) {
            $ajax = true;
        	$capture_session_ids = $this->input->post('capture_session_ids');
		}
        $this->load->library("instacam");
        // Return the result which can be true or false
        if($ajax) {
			echo json_encode($this->instacam->stop_capture($capture_session_ids));
		}else{
			$this->instacam->stop_capture($capture_session_ids);
			return;
		}
    }

	public function delete_moment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$moment_id = $this->input->post('moment_id');

		$moment_info = $this->moment_model->get_moment_info($moment_id);

		if(has_team($moment_info['team_id']) || is_admin()){
			$dir = 'videos/' . $moment_info['event_id'] . '/' . $moment_info['part_id'] . '/' . $moment_info['moment_id'];
			$this->load->helper('disk');
			rrmdir($dir);
			$this->moment_model->delete_moment($moment_id);
			echo json_encode('yes');
			return;
		}
		return false;
	}

	public function render_moments($data = null)
	{
		if($this->input->post()){
			$lead 		= $this->input->post('lead');
			$lapse 		= $this->input->post('lapse');
			$event_id 	= $this->input->post('event_id');
			$part_id	= $this->input->post('part_id');
			$moment_id 	= $this->input->post('moment_id');
		}else{
			if($data !== null){
				$lead = $data['lead'];
				$lapse = $data['lapse'];
				$event_id = $data['event_id'];
				$part_id = $data['part_id'];
				$moment_id = $data['moment_id'];
			}else{
				return false;
			}
		}
		$now 		= now() - $lapse;
		$file_path 		= "videos/$event_id/$part_id";
		$moment_path	= "$file_path/$moment_id";
        $recordings     = $this->recording_model->get_recordings_by_event_id_and_part_id($event_id, $part_id);
        $old_umask = umask(0);
        mkdir($moment_path, 0777);
        umask($old_umask);

		// if it is a recording...
        if(!empty($recordings)){
			foreach ($recordings as $recording)
			{
				$cam 			= $recording['cam_number'];
				$cam_description= $recording['cam_description'];
				$input			= "$file_path/tmp_rec/rec$cam.mp4";
				$start_time 	= $recording['start_time'];
				$seektime 		= $now - $start_time - $lead;
				$duration		= $lead + $lapse;
				$ffmpeg 		= $this->data['system_info']['ffmpeg'];
				$time			= $now - $start_time;
				$data = array(
					'path'		=> $moment_path,
					'cam'		=> $cam,
					'cam_description'	=> $cam_description
				);
				/*Add moment file*/
				$moment_file_id = $this->moment_model->add_moment_file($data, $moment_id);

				//if the cam is good for vcodec copy, copy directly.
				$output			= "$moment_path/$moment_file_id.mp4";
				$command 		= "$ffmpeg -ss $seektime -i $input -analyzeduration 2147483647 -c copy  -strict -2 -t $duration $output </dev/null >/dev/null 2> $moment_path/$moment_file_id-h264.txt";
				shell_exec($command);
				/*Generate thumbnail*/
				$image			= "$moment_path/$moment_file_id.jpg";
				$command		= "$ffmpeg -i $output -analyzeduration 2147483647 -y -f image2 -vcodec mjpeg -s 360x240 -vframes 1 $image 2> $moment_path/$moment_file_id-image.txt &";
				shell_exec($command);
			}
		// if it is an uploaded or finished clip
		}else{
			$copy = '1';
			$cam_constante = '1';
			$cam_fps = '30';

			$clips = $this->clip_model->get_clips_by_part_id($part_id);

			foreach ($clips as $clip) {
				$input = $file_path . '/' . $clip['clip_id'] . '.mp4';
				$seektime_cam = $this->input->post('seektime') - $lead;
				$duration = $lead + $lapse;
				$vframes = ($duration*$cam_fps);
				$ffmpeg = $this->data['system_info']['ffmpeg'];

				if(isset($clip['cam_description'])){
					$cam_description = $clip['cam_description'];
				}else{
					$cam_description = '';
				}

				/* Add moment file */
				$data = array(
					'path' => $moment_path,
					'cam' => $clip['cam'],
					'cam_description'	=> $cam_description
				);
				$moment_file_id = $this->moment_model->add_moment_file($data, $moment_id);

				$output			= "$moment_path/$moment_file_id.mp4";
				$command 		= "$ffmpeg -i $input -analyzeduration 2147483647 -vcodec copy -ss $seektime_cam -strict -2 -vframes $vframes $output </dev/null >/dev/null 2> $moment_path/$moment_file_id-h264.txt";
				shell_exec($command);
				/*Generate thumbnail*/
				$image			= "$moment_path/$moment_file_id.jpg";
				$command		= "$ffmpeg -i $output -analyzeduration 2147483647 -y -f image2 -vcodec mjpeg -s 360x240 -vframes 1 $image </dev/null >/dev/null 2> $moment_path/$moment_file_id-image.txt &";
				shell_exec($command);
			}
		}
		echo json_encode('yes');
		return;
	}
	public function add_tag_to_moment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$moment_id 	= $this->input->post('moment_id');
		$label 		= $this->input->post('label');
		$kind 		= $this->input->post('kind');

		if($moment_id !== false) {
			if($kind === 'tag'){
				//do if tag
				$tag_id 	= $this->input->post('tag_id');
				$data = array(
					'moment_id' => $moment_id,
					'tag_id' 	=> $tag_id,
					'user_id'	=> $this->data['user']->id,
					'tag_name'	=> $label
				);
				$this->moment_model->add_tag_to_moment($data);
				echo json_encode('tag added');
			}

			if($kind === 'player'){
				//do if player
				$player_id 	= $this->input->post('tag_id');
				$data = array(
					'moment_id' => $moment_id,
					'player_id' => $player_id,
					'user_id' => $this->data['user']->id,
				);
				$this->moment_model->add_player_to_moment($data);
				echo json_encode('player added');
			}
			return;
		}
		return false;
	}
	public function delete_tag_from_moment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
        $moment_id  = $this->input->post('moment_id');
		$kind = $this->input->post('kind');

		if($kind === 'tag'){
			$tag_id 	= $this->input->post('tag_id');
			$this->moment_model->delete_tag_from_moment($moment_id, $tag_id);
		}

		if($kind === 'player'){
			$player_id 	= $this->input->post('tag_id');
			$this->moment_model->delete_player_from_moment($moment_id, $player_id);
		}
		echo json_encode('yes');
		return;
	}
	public function get_moments_for_live_tagging()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$event_id = $this->input->post('event_id');
		$part_id = $this->input->post('part_id');
		$moments = $this->moment_model->get_all_moments_for_part($part_id);

		$this->data['event_id'] = $event_id;
		$this->data['part_id'] = $part_id;
		$this->data['moments'] = $moments;

		$view = $this->load->view('moments/tagged_moments_view', $this->data);
		echo $view;
		return;
	}

	/*
		*	This function returns moments in a different view for insta-camera's, there is no stream running so moments can just be a list
		*/
	public function get_moments_for_insta_tagging()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$event_id = $this->input->post('event_id');
		$part_id = $this->input->post('part_id');
		$moments = $this->moment_model->get_all_moments_for_part($part_id, true);

        // Built the JSON array
		$response['event_id'] = $event_id;
		$response['part_id'] = $part_id;
		$response['moments'] = $moments;

		echo json_encode($response);
	}


	public function show_moment_files()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		//variables from input post
		$moment_id = $this->input->post('moment_id');
		$moment_info = $this->moment_model->get_moment_info($moment_id);

        // Check if moment_info is not empty/null
        if(!empty($moment_info)) {
            $moment_files = $this->moment_model->get_moment_files_by_moment_id($moment_id);

            //get all tags for this moment
            // $tags = $this->moment_model->get_tags_by_moment_id($moment_id);
            // the $tags variable is already in the get_moment_info function and is not even used....

            //construct data for view
            $data = array(
                'files' => $moment_files,
                'moment_info' => $moment_info,
                'system_info' => $this->data['system_info']
            );

            //load view
            $page = $this->load->view('moments/edit_moment_view', $data);
            echo $page;
            return true;
        }
        echo "error: no moment found";
		return false;
	}

    /**
     * JSON version of show_moment_files -> used for the main jsRender template
	 * only variable needed is a POST moment_id
     */
    public function get_moment_json() {
        // Check if still logged in
        if(!$this->ion_auth->logged_in()){
            set_status_header(403);
            exit("Not logged in anymore!");
        }
        //variables from input post
        $moment_id = $this->input->post('moment_id');
        $moment_info = $this->moment_model->get_moment_info($moment_id);

        // Check if moment_info is not empty/null
        if(!empty($moment_info)) {
            $moment_files = $this->moment_model->get_moment_files_by_moment_id($moment_id);
            //construct data for view
            $moment_info["moment_files"] = $moment_files;
            $moment_info["number_of_moment_files"] = count($moment_files);

            set_status_header(200);
            echo json_encode($moment_info);
            return;
        }
        set_status_header(404);
        echo json_encode(["error" => "No moments found!"]);
        return;
    }

	public function update_moment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		//variables
		$lead 			= $this->input->post('lead');
		$lapse 			= $this->input->post('lapse');
		$time_in_clip 	= $this->input->post('time_in_clip');
		$event_id 		= $this->input->post('event_id');
		$part_id 		= $this->input->post('part_id');
		$moment_id 		= $this->input->post('moment_id');
		$recording 		= $this->input->post('recording');

		$file_path 		= "videos/$event_id/$part_id";

		//update information
		$data = array(
			'lead' 			=> $lead,
			'lapse' 		=> $lapse,
			'time_in_clip' 	=> $time_in_clip
		);
		//do update in db
		$this->moment_model->update_moment_by_moment_id($moment_id, $data);

		//remove existing files
		delete_files($file_path . '/' . $moment_id);

		$output = array();
		//check if recording or not

		$moment_files 	= $this->moment_model->get_moment_files_by_moment_id($moment_id);
		foreach ($moment_files as $moment_file) {
			//do ffmpeg magic
			$cam  			= $moment_file['cam'];
			$copy 			= '1';
			$seektime 		= $time_in_clip - $lead;
			$cam_constante 	= '1';
			$seektime_cam 	= $seektime*$cam_constante;
			$duration		= $lead + $lapse;
			$cam_fps		= '30';
			$vframes		= ($duration*$cam_fps)/$cam_constante;
			$output_path  	= $file_path . '/' . $moment_id;
			$output_name	= $moment_file['moment_file_id'];

			if($recording === '0'){
				$clip 			= $this->clip_model->get_clip_by_part_id_and_cam($part_id, $cam);
				$input			= $file_path . '/' . $clip['clip_id'] . '.mp4';
			}
			if($recording === '1'){

				$recording_info = $this->recording_model->get_recording_by_cam_and_part_id($cam, $part_id);
				$input 			= $file_path . '/tmp_rec/rec' . $cam . '.mp4';
				$copy 			= $recording_info['copy'];
				$cam_constante 	= $recording_info['cam_constante'];
				$cam_fps 		= $recording_info['cam_fps'];
				$seektime 		= $time_in_clip - $lead;
				$seektime_cam 	= $seektime*$cam_constante;
				$vframes		= ($duration*$cam_fps)/$cam_constante;
			}

			$data = array(
				'input' 		=> $input,
				'vcodec' 		=> $copy,
				'seektime_cam' 	=> $seektime_cam,
				'duration' 		=> $duration,
				'output_path' 	=> $output_path,
				'output_name' 	=> $output_name,
				'thumb'			=> 'true'
			);
			$this->ffmpeg->cut_moment($data);

			//create output
			$output[$moment_file['moment_file_id']] = array(
				'moment_file_id' => $moment_file['moment_file_id'],
				'image' => base_url() . $file_path . '/' . $moment_id . '/' . $moment_file['moment_file_id'] . '.jpg',
				'file' => base_url() . $file_path . '/' . $moment_id . '/' . $moment_file['moment_file_id'] . '.mp4',
				'vid_width' => 100/count($moment_files)
			);
		}

		echo json_encode($output);
		return;
	}

	function submit_comment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$moment_id = $this->input->post('moment_id');
		$comment = $this->input->post('comment');
		$data = array(
			'comment' => $comment
		);
		$this->moment_model->update_moment_by_moment_id($moment_id, $data);
		$moment_info = $this->moment_model->get_moment_info($moment_id);
		echo json_encode($moment_info);
		return;

	}

	function edit_comment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$event_id = $this->input->post('event_id');
		$moment_id = $this->input->post('moment_id');

		if(is_admin() || has_event($event_id)) {
			$this->data['moment'] = $this->moment_model->get_moment_info($moment_id);
			$output = $this->load->view('moments/edit_comment_view', $this->data);
			return $output;
		}

		return false;
	}

	/**
     * Old edit_tags_for_moment -> in modal
     */
    function edit_tags_for_moment()
	{
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		$user = $this->ion_auth->user()->row();

		$moment_id = $this->input->post('moment_id');
		$event_id = $this->input->post('event_id');

		$tagfields = $this->tagfield_model->get_tagfields_for_user($user->id);
		$event_info = $this->event_model->get_event_info_by_event_id($event_id);
		$selected_tags = $this->moment_model->get_selected_tags_for_moment($moment_id);
		$selected_players = $this->moment_model->get_selected_players_for_moment($moment_id);

		$data = array(
			'tagfields' => $tagfields,
			'event_info' => $event_info,
			'selected_tags' => $selected_tags,
			'selected_players' => $selected_players,
			'live' => false,
			'moment_id' => $moment_id
		);
		$html = $this->load->view('tags/tagfield_modal_view', $data);
		return $html;
	}

    /**
     * New edit tags for moment -> inline view
     */
    function edit_tags_inline()
    {
        if(!$this->ion_auth->logged_in()){
            set_status_header(403);
            exit("Session expired, please login again.");
        }
        $user = $this->ion_auth->user()->row();

        $moment_id = $this->input->post('moment_id');
        $event_id = $this->input->post('event_id');

        $tagfields = $this->tagfield_model->get_tagfields_for_user($user->id);
        $selected_tagfield = $this->tagfield_model->get_selected_tagfield_for_user($user->id);
        $event_info = $this->event_model->get_event_info_by_event_id($event_id);
        $selected_tags = $this->moment_model->get_selected_tags_for_moment($moment_id);
        $selected_players = $this->moment_model->get_selected_players_for_moment($moment_id);

        $data = array(
            'tagfields' => $tagfields,
            'selected_tagfield' => $selected_tagfield,
            'event_info' => $event_info,
            'selected_tags' => $selected_tags,
            'selected_players' => $selected_players,
            'live' => false,
            'moment_id' => $moment_id
        );
        $this->load->view('tags/tagfield_inline_view', $data);
    }

}
