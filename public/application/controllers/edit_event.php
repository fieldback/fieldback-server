<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_event extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('event_model');
		$this->load->model('system_model');
		$this->data['system_info'] = $this->system_model->get_system_info();
		$this->load->helper('date');

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}
	public function index()
	{
		redirect('events');
	}
	public function edit_event_info()
	{
		$event_info_id = $this->input->post('event_info_id');
		$event_info_text = $this->input->post('event_info_text');
		if($event_info_id == "event_name"){
			$data = "<div class='input-group'><input class='to_select form-control' id='event_name' type='text' value='" . trim($event_info_text) . "' /><span class='input-group-btn'><input class='edit_submit btn btn-primary' id='event_name_submit' type='submit' onclick='updateEventInfo(&apos;event_name&apos;)' value='OK' /></span></div>";
			echo $data;
		}
		if($event_info_id == "event_description"){
			$data = "<div class='input-group'><textarea class='to_select form-control' id='event_description'>" . trim($event_info_text) . "</textarea><span class='input-group-btn'><input class='edit_submit btn btn-primary' id='event_description_submit' type='submit' onclick='updateEventInfo(&apos;event_description&apos;)' value='OK' /></span></div>";
			echo $data;
		}
	}
	public function edit_team_to_event()
	{
		$team_id = $this->input->post('team_id');
		$event_id = $this->input->post('event_id');
		$team_check = $this->event_model->check_if_event_has_team($event_id);
		if($team_check == FALSE){
			$this->event_model->add_team_to_event($event_id, $team_id);
		}else{
			$this->event_model->update_team_to_event($event_id, $team_id);
		}
		return;
	}
	public function update_event_info()
	{
		$event_id = $this->input->post('event_id');
		$event_info_id = $this->input->post('event_info_id');
		$event_info_new_text = $this->input->post('event_info_new_text');
		if($event_info_new_text == ''){
			$data = "error1";
			echo json_encode($data);
		}else{
			$data = array(
				$event_info_id => $event_info_new_text
			);
			$this->event_model->update_event_information($data, $event_id);
			$event_info = $this->event_model->get_event_info_by_event_id($event_id);
			echo json_encode($event_info);
		}
	}
	
}
