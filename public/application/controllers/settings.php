<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settings extends MY_Controller
{
    private $user_id;
    private $organisation_id;

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            flash_error('not_logged_in');
            redirect('login');
        }

        $this->load->model('team_model');
        $this->load->model('system_model');
        $this->load->model('camera_model');
        $this->load->model('tagfield_model');

        $this->load->library('form_validation');

        $this->user_id = $this->data['user']->id;
        $this->organisation_id = $this->session->userdata('organisation_id');

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="bg-warning">', '</div>');

        if (!temporary_password()) {
            flash_error('change_password');
            redirect('my_settings');
        }
    }

    public function index()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'system_settings' => array(
                'name' => 'System Settings',
                'link' => 'settings/system_settings',
                'state' => 'active',
            ),
        );
        $this->template->load('templates/template_view', 'settings/settings_view', $this->data);
    }

    /* ADMIN SETTINGS */
    public function system_settings()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'system_settings' => array(
                'name' => 'System Settings',
                'link' => 'settings/system_settings',
                'state' => 'active',
            ),
        );

        /* CHECK IF ADMIN */
        if (!$this->ion_auth->is_admin()) {
            flash_error('not_admin');
            redirect('home');
        }

        if ($this->input->post('setting')) {
            $this->form_validation->set_rules('setting[system_name]', 'System name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('setting[system_information]', 'System information', 'trim|required|xss_clean');
            $this->form_validation->set_rules('setting[ffmpeg]', 'ffmpeg', 'trim|required');
            $this->form_validation->set_rules('setting[openrtsp]', 'openrtsp', 'trim|required');

            if ($this->form_validation->run()) {
                $setting = $this->input->post('setting');
                $this->system_model->update_system_info($setting);

                $cameras = $this->input->post('camera');
                foreach ($cameras as $key => &$camera) {
                    $camera['number'] = $key;
                    if (!array_key_exists('active', $camera)) {
                        $camera['active'] = '0';
                    }
                    if (!array_key_exists('copy', $camera)) {
                        $camera['copy'] = '0';
                    }
                    if (!array_key_exists('live_enabled', $camera)) {
                        $camera['live_enabled'] = '0';
                    }
                    if (!array_key_exists('is_instacam', $camera)) {
                        $camera['is_instacam'] = '0';
                    }
                }

                $this->camera_model->update_cameras($cameras);

                flash_success('system_settings_updated');

                redirect('settings/system_settings');
            } else {
                die('Not all fields filled in!'); // TODO: replace with better errors - what is the use of form validation if you don't give errors back to users...
            }
        }

        $this->data['settings'] = $this->system_model->get_system_info();
        unset($this->data['settings']['id']);

        $this->data['cameras'] = $this->camera_model->get_all_cameras();

        $this->template->load('templates/template_view', 'settings/settings/system_settings_view', $this->data);
    }

    public function delete_camera($camera_id)
    {
        if (!is_admin()) {
            flash_error('not_admin');
            redirect('home');
        }
        $this->camera_model->delete_camera($camera_id);
        $this->session->set_flashdata('success', 'The camera has been deleted');
        redirect('settings/system_settings');
    }

    public function organisations()
    {
        $this->template->load('templates/template_view', 'settings/organisations/organisations_view', $this->data);
    }

    public function users()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'users' => array(
                'name' => 'Users',
                'link' => 'settings/users',
                'state' => 'active',
            ),
        );

        // check if allowed to edit users
        if (is_admin() || is_org_admin()) {
            $this->load->helper('string');
            $this->load->library('encrypt');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('user[first_name]', 'First name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('user[last_name]', 'Last name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('user[email]', 'E-mail', 'trim|required|xss_clean|valid_email|callback_email_check');

            $this->form_validation->set_message('email_check', 'This email is already registered');

            if ($this->form_validation->run()) {

                //create user variable for users table in mysql
                $user = array();
                //create a random password
                $password = randomPassword();

                //get post data
                $user_profile = $this->input->post('user');
                $role = $this->input->post('role');
                $team = $this->input->post('team');

                //create user data for users table
                $user['username'] = $user_profile['email'];
                $user['password'] = $password;

                //insert everything in the database
                $this->user_model->add_user($user_profile, $role, $this->organisation_id, $user, $team);

                //show success
                flash_success('user_added');
                redirect('settings/users');
            }

            $this->data['roles'] = $this->system_model->get_all_roles();
            if (is_admin()) {
                $this->data['users'] = $this->user_model->get_users_for_organisation($this->organisation_id, null, true);
            } else {
                $this->data['users'] = $this->user_model->get_users_for_organisation($this->organisation_id);
            }
            $this->data['teams'] = $this->team_model->get_all_teams_for_organisation($this->organisation_id);

            $this->template->load('templates/template_view', 'settings/users/users_view', $this->data);
        } else {
            flash_error('not_admin');
            redirect('home');
        }
    }

    public function email_check()
    {
        $user = $this->input->post('user');
        $email = $user['email'];

        return !$this->ion_auth->email_check($email);
    }

    public function teams()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'teams' => array(
                'name' => lang('term_teams'),
                'link' => 'settings/teams',
                'state' => 'active',
            ),
        );

        if (is_admin() || is_org_admin()) {
            $this->load->model('team_model');

            if (is_admin()) {
                $this->data['teams'] = $this->team_model->get_all_teams_for_organisation($this->organisation_id);
            } else {
                $this->data['teams'] = $this->team_model->get_all_teams_for_user_and_organisation($this->user_id, $this->organisation_id);
            }

            $this->template->load('templates/template_view', 'settings/teams/teams_view', $this->data);
        } else {
            flash_error('not_admin');
            redirect('home');
        }
    }
    public function team($team_id)
    {
        $this->load->model('team_model');

        $this->data['team_info'] = $this->team_model->get_team_info_by_team_id($team_id);

        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'teams' => array(
                'name' => lang('term_teams'),
                'link' => 'settings/teams',
                'state' => 'none',
            ),
            'team' => array(
                'name' => $this->data['team_info']['team_name'],
                'link' => '#',
                'state' => 'active',
            ),
        );

        $this->template->load('templates/template_view', 'settings/teams/team_view', $this->data);
    }
    public function add_team()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'teams' => array(
                'name' => lang('term_teams'),
                'link' => 'settings/teams',
                'state' => 'none',
            ),
            'add_team' => array(
                'name' => 'Add team',
                'link' => '#',
                'state' => 'active',
            ),
        );

        $this->load->model('team_model');

        $this->form_validation->set_rules('team[team_name]', 'Team name', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            //team information from post
            $team_info = $this->input->post('team');

            $team_id = $this->team_model->add_team_to_organisation($team_info, $this->organisation_id);

            redirect('settings/team/'.$team_id);
        }

        $this->template->load('templates/template_view', 'settings/teams/add_team_view', $this->data);
    }

    public function account()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'account' => array(
                'name' => 'Account',
                'link' => 'settings/account',
                'state' => 'active',
            ),
        );

        $user_id = $this->session->userdata('id');

        $this->data['user_info'] = $this->user_model->get_user_by_id($user_id);

        $this->template->load('templates/template_view', 'settings/accounts/account_view', $this->data);
    }

    public function help()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'account' => array(
                'name' => 'Help',
                'link' => '',
                'state' => 'active',
            ),
        );
        $this->template->load('templates/template_view', 'settings/help/help_view', $this->data);
    }

    public function tagfields()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'tagfields' => array(
                'name' => 'Tagfields',
                'link' => 'settings/tagfields',
                'state' => 'active',
            ),
        );
        if (has_teams() || is_admin()) {
            $this->data['tagfields'] = array();

            $organisation_tagfields = $this->tagfield_model->get_tagfields_for_organisation($this->organisation_id);
            $personal_tagfields = $this->tagfield_model->get_tagfields_for_user($this->user_id);

            $this->data['tagfields']['organisation'] = $organisation_tagfields;
            $this->data['tagfields']['personal'] = $personal_tagfields;

            $this->data['create_tagfield_form'] = $this->load->view('settings/tagfields/create_tagfield_form', $this->data);

            $this->template->load('templates/template_view', 'settings/tagfields/tagfields_view', $this->data);
        } else {
            flash_error('no_permission');
            redirect('home');
        }
    }

    public function tagfield($tagfield_id = null)
    {
        if ($tagfield_id !== null) {
            $this->data['tagfield_info'] = $this->tagfield_model->get_tagfield_info($tagfield_id);
            $this->data['trigger_tags'] = $this->tagfield_model->get_trigger_tags_for_tagfield($tagfield_id);
            $this->data['tags'] = $this->tagfield_model->get_tags_for_tagfield($tagfield_id);
            $this->data['organisation_tags'] = $this->tagfield_model->get_tags_for_organisation($this->organisation_id);

            $this->data['breadcrumbs'] = array(
                'home' => array(
                    'name' => 'Home',
                    'link' => 'home',
                    'state' => 'none',
                ),
                'settings' => array(
                    'name' => 'Settings',
                    'link' => 'settings',
                    'state' => 'none',
                ),
                'tagfields' => array(
                    'name' => 'Tagfields',
                    'link' => 'settings/tagfields',
                    'state' => 'none',
                ),
                'tagfield' => array(
                    'name' => $this->data['tagfield_info']['tagfield_name'],
                    'link' => '#', // ! deze werkt niet goed?
                    'state' => 'active',
                ),
            );

            $this->template->load('templates/template_view', 'settings/tagfields/tagfield_view', $this->data);
        } else {
            flash_error('select_tagfield');
            redirect('settings/tagfields');
        }
    }
    public function modify_tags()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none',
            ),
            'settings' => array(
                'name' => 'Settings',
                'link' => 'settings',
                'state' => 'none',
            ),
            'tagfields' => array(
                'name' => 'Tagfields',
                'link' => 'settings/tagfields',
                'state' => 'none',
            ),
            'tagfield' => array(
                'name' => 'Tagfield',
                'link' => $this->session->last_page(2),
                'state' => 'none',
            ),
            'modify_tags' => array(
                'name' => 'Modify tags',
                'link' => 'settings/modify_tags',
                'state' => 'active',
            ),
        );

        if (is_admin()) {
            $this->form_validation->set_rules('tag[tag_name]', 'Tag name', 'trim|required|xss_clean');

            if ($this->form_validation->run()) {
                $tag_info = $this->input->post('tag');

                $this->tagfield_model->add_tag_to_organisation($this->organisation_id, $tag_info);
                redirect('settings/modify_tags');
            }

            $this->data['organisation_tags'] = $this->tagfield_model->get_tags_for_organisation($this->organisation_id);
            foreach ($this->data['organisation_tags'] as &$tag) {
                $tag['moments'] = $this->tagfield_model->get_moments_for_tag($tag['tag_id']);
            }

            $this->template->load('templates/template_view', 'settings/tagfields/modify_tags_view', $this->data);
        } else {
            redirect('settings/tagfields');
        }
    }

    /**
     * Delete the tagfield.
     *
     * @param int $tagfield_id The id of the tagfield
     *
     * @return redirect
     */
    public function delete_tagfield($tagfield_id = null)
    {
        if ($tagfield_id !== null) {
            $this->tagfield_model->delete_tagfield($tagfield_id);
            flash_success('tagfield_deleted');
            redirect('settings/tagfields');
        } else {
            flash_error('select_tagfield');
            redirect('settings/tagfields');
        }
    }

    /**
     * Delete tag from organisation
     * Completely delete this tag from the database
     * This could only be done if count moments  === 0.
     */
    public function delete_tag_from_organisation($tag_id = null)
    {
        if ($tag_id !== null) {
            $connected_moments = $this->tagfield_model->get_moments_for_tag($tag_id);
            if (count($connected_moments) === 0) {
                $this->tagfield_model->delete_tag_from_organisation($this->organisation_id, $tag_id);
                flash_success('tag_deleted');
                redirect('settings/modify_tags');
            }
            // This tag still has moments connected to it
            flash_error('tag_not_without_moments');
            redirect('settings/modify_tags');
        }
        flash_error();
        redirect('settings/modify_tags');
    }

    /**
     * Add new tagfield for user and organisation.
     */
    public function create_tagfield()
    {
        $this->load->helper('date');

        $this->form_validation->set_rules('tagfield[tagfield_name]', 'Tagfield name', 'trim|required|xss_clean');

        $this->form_validation->set_error_delimiters('<div class="bg-warning">', '</div>');

        if ($this->form_validation->run()) {
            $tagfield = $this->input->post('tagfield');
            $tagfield_name = $tagfield['tagfield_name'];

            $now = now();

            $tagfield_id = $this->tagfield_model->create_tagfield($this->user_id, $tagfield_name, $now);

            flash_success('tagfield_added');
            redirect('settings/tagfield/'.$tagfield_id);
        } else {
            self::tagfields();
        }
    }

    /**
     * AJAX REQUESTS.
     */
    public function get_all_users_for_organisation()
    {
        $users = $this->user_model->get_users_for_organisation($this->organisation_id);
        echo json_encode($users);
    }

    /**
     * For a search on all users in an organisation.
     *
     * @return array('value' => name, 'data' => first_name)
     */
    public function autocomplete_players()
    {
        /* get the query */
        $query = $this->input->get('query');

        /* get all the users that are in the organisation including the inactive users */
        $players = $this->user_model->get_users_for_organisation($this->organisation_id, $query, true);

        /* prepare result */
        $result = array(
            'suggestions' => array(),
        );
        foreach ($players as $player) {
            $result['suggestions'][] = array('value' => $player['first_name'].' '.$player['last_name'], 'data' => $player['first_name']);
        }
        /* the result */
        echo json_encode($result);
    }

    /**
     * Get all players for a team.
     *
     * @return array
     */
    public function get_all_players_for_team()
    {

        /* Variables */
        $team_id = $this->input->post('team_id');

        /* Get all players */
        $players = $this->team_model->get_all_players_for_team($team_id);

        /* the result */
        echo json_encode($players);
    }

    /**
     * Get all owners for a team.
     *
     * @return array
     */
    public function get_all_owners_for_team()
    {
        /* variables */
        $team_id = $this->input->post('team_id');

        /* get all owners */
        $owners = $this->team_model->get_all_owners_for_team($team_id);

        /* the result */
        echo json_encode($owners);
    }

    /**
     * Add a player to a team.
     */
    public function add_player_to_team()
    {
        /* variables */
        $output = array();
        $player_name = $this->input->post('value');
        $team_id = $this->input->post('team_id');
        $organisation_id = $this->organisation_id;

        // See if this user exists (IT SHOULD AS WE FOUND IT EARLIER AS WELL WITH JAVASCRIPT....!!!)
        $user = $this->user_model->get_user_by_full_name($organisation_id, $player_name);
        // If found: add
        if (!empty($user)) {
            // check if owner is already player of team
            if ($this->team_model->check_if_player_is_in_team($user['id'], $team_id)) {
                // already in team, don't add again error
                $output['error'] = 'This person is already in this team.';
            } else {
                // add user to team as owner
                $this->team_model->add_player_to_team($user['id'], $team_id);
            }
        } else {
            $output['error'] = 'The user should exist but SQL could not find it..';
        }
        echo json_encode($output);
    }

    /**
     * Add an owner to a team.
     */
    public function add_owner_to_team()
    {
        $output = array();
        $owner_name = $this->input->post('value');
        $team_id = $this->input->post('team_id');
        $organisation_id = $this->organisation_id;

        // See if this user exists (IT SHOULD AS WE FOUND IT EARLIER AS WELL WITH JAVASCRIPT....!!!)
        $user = $this->user_model->get_user_by_full_name($organisation_id, $owner_name);
        // If found: add
        if (!empty($user)) {
            // check if owner is already owner of team
            if ($this->team_model->check_if_owner_of_team($user['id'], $team_id)) {
                // already in team, don't add again error
                $output['error'] = 'This person is already owner of the team.';
            } else {
                // add user to team as owner
                $this->team_model->add_owner_to_team($user['id'], $team_id);
            }
        } else {
            $output['error'] = 'The user should exist but SQL could not find it..';
        }
        echo json_encode($output);
    }

    /**
     * Remove player from a team.
     */
    public function remove_player_from_team()
    {
        $player_id = $this->input->post('player_id');
        $team_id = $this->input->post('team_id');

        $output = $this->team_model->remove_player_from_team($player_id, $team_id);

        echo json_encode($output);
    }

    /**
     * Remove owner from a team.
     */
    public function remove_owner_from_team()
    {
        $user_id = $this->input->post('user_id');
        $team_id = $this->input->post('team_id');

        $output = $this->team_model->remove_owner_from_team($user_id, $team_id);

        echo json_encode($output);
    }

    /**
     * Add tags to tagfield.
     */
    public function add_tags_to_tagfield()
    {
        $this->load->helper('date');
        $now = now();

        //variables
        $tags = $this->input->post('tags');
        $trigger_tags = $this->input->post('trigger_tags');
        $tagfield_name = $this->input->post('tagfield_name');
        $tagfield_id = $this->input->post('tagfield_id');
        $show_players = $this->input->post('show_players');

        //add tags to tagfield
        $this->tagfield_model->add_tags_to_tagfield($tagfield_id, $tags);

        //add trigger tags to tagfield
        $this->tagfield_model->add_trigger_tags_to_tagfield($tagfield_id, $trigger_tags);

        //tagfield information
        if ($show_players == 'true') {
            $show_players = 1;
        } else {
            $show_players = 0;
        }
        $data = array(
            'tagfield_name' => $tagfield_name,
            'show_players' => $show_players,
            'date_saved' => $now,
        );
        $this->tagfield_model->update_tagfield_info($tagfield_id, $data);

        flash_success('tagfield_saved');
        echo json_encode('correct');
    }
}

/* End of file settings.php */
/* Location: ./application/controllers/settings.php */
