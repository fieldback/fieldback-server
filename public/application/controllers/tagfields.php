<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tagfields extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('tagfield_model');
		$this->load->model('event_model');

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}

	/**
	 * Show the tagfield view
	 * @return string html with complete view
	 */
	public function tagfields_view()
	{
		$event_id = $this->input->post('event_id');
		$this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
		$this->data['tagfields'] = $this->tagfield_model->get_tagfields_for_user($this->data['user']->id);
		
		$selected_tagfield = $this->tagfield_model->get_selected_tagfield_for_user($this->data['user']->id);

		foreach($this->data['tagfields'] as &$tagfield){
			$tagfield['selected'] = ''; 
			if($tagfield['tagfield_id'] === $selected_tagfield['selected_tagfield']){
				$tagfield['selected'] = 'selected';
			}
		}
		$tagfields = $this->load->view('tags/tagfield_view', $this->data);
		return $tagfields;
	}

	public function get_selected_tagfield_for_user()
	{
		$user_id = $this->data['user']->id;
		$tagfield_id = $this->tagfield_model->get_selected_tagfield_for_user($user_id);
		if($tagfield_id !== false) {
			echo json_encode($tagfield_id);
		}
	}

	/**
	 * Update selected tagfield for user
	 * @return json success or failure
	 */
	public function update_selected_tagfield_for_user()
	{
		$tagfield_id = $this->input->post('tagfield_id');
		$user_id = $this->data['user']->id;

		if($tagfield_id !== null && $user_id !== null){
			if($this->tagfield_model->update_selected_tagfield_for_user($tagfield_id, $user_id)){
				echo json_encode('success');
				return;
			}
		}
		return false;
	}


}

/* End of file tagfields.php */
/* Location: ./application/controllers/tagfields.php */