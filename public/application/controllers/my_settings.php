<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->user_id = $this->data['user']->id;
		$this->load->library('form_validation');
	}

	public function index()
	{
		//breadcrumbs
		$this->data['breadcrumbs'] = array(
			'home' => array(
				'name' => 'Home',
				'link' => 'home',
				'state' => 'none'
			),
			'system_settings' => array(
				'name' => 'My Settings',
				'link' => 'my_settings',
				'state' => 'active'
			)
		);

		//get the info of the user
		$this->data['user_info'] = $this->user_model->get_user_by_id($this->user_id);

		$this->form_validation->set_rules('passwords[old]', 'Old password', 'required');
		$this->form_validation->set_rules('passwords[new]', 'New password', 'required');
		$this->form_validation->set_rules('passwords[new2]', 'New password confirm', 'required|matches[passwords[new]]');

		if($this->form_validation->run())
		{
			$data = $this->input->post('passwords');

			//if password changed, change password status to chosen password instead of generated password.
			if($this->ion_auth->change_password($this->ion_auth->user()->row()->email, $data['old'], $data['new'])){

				$this->user_model->update_user_password_status($this->ion_auth->user()->row()->id);
				flash_success('password_updated');
				redirect('my_settings');

			}

		}

		//load the view
		$this->template->load('templates/template_view', 'my_settings/my_settings_view', $this->data);
	}

}

/* End of file my_settings */
/* Location: ./application/controllers/my_settings */