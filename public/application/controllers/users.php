<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}

	public function index()
	{
		redirect('settings/users');
	}

	public function user($user_id = null)
	{
		if($user_id !== null){

			$this->data['user_info'] = $this->user_model->get_user_by_id($user_id);
			$this->data['player_teams'] = $this->user_model->get_teams_for_player($user_id);
			$this->data['owner_teams'] = $this->user_model->get_teams_for_owner($user_id);
			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => ''
				),
				'users' => array(
					'name' => 'Users',
					'link' => 'users',
					'state' => ''
				),
				'user' => array(
					'name' => $this->data['user_info']['first_name'] . ' ' . $this->data['user_info']['last_name'],
					'link' => '',
					'state' => 'active'
				)
			);
			$this->template->load('templates/template_view', 'users/user_view', $this->data);
		}else{
			redirect('home');
		}
	}

	public function delete_user($user_id = null)
	{
		if($user_id !== null){
			if(is_admin()){
				if($this->ion_auth->deactivate($user_id)){
					redirect('settings/users');
				}
			}
		}
		redirect('home');
	}

	public function hard_delete_user($user_id = null){
		if($user_id !== null){
			if(is_admin()){
				$this->user_model->hard_delete_user($user_id, $this->session->userdata('organisation_id'));
				redirect('settings/users');
			}
					}
		return false;
	}

}

/* End of file users.php */
/* Location: ./application/controllers/users.php */