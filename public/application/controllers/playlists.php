<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Playlists extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    function index()
    {
        $this->data['breadcrumbs'] = array(
            'home' => array(
                'name' => 'Home',
                'link' => 'home',
                'state' => 'none'
            ),
            'playlists' => array(
                'name' => 'Playlists',
                'link' => 'playlists',
                'state' => 'active'
            )
        );
        $this->template->load('templates/template_view', 'playlists/playlists', $this->data);
    }

    function playlist($playlist_id = null)
    {
        if($playlist_id != null)
        {
            $this->load->model("playlist_model");
            $this->data['playlist'] = $this->playlist_model->get_playlist_by_id($playlist_id);
            if(!empty($this->data['playlist'])) {
                $this->data['breadcrumbs'] = array(
                    'home' => array(
                        'name' => 'Home',
                        'link' => 'home',
                        'state' => 'none'
                    ),
                    'playlists' => array(
                        'name' => 'Playlists',
                        'link' => 'playlists',
                        'state' => 'none'
                    ),
                    'playlist' => array(
                        'name' => $this->data['playlist']['title'],
                        'link' => 'playlist',
                        'state' => 'active'
                    )
                );

                // Load the javascript for video controls
                $this->data['head']['javascripts'][] = 'video.controls.js';

                $this->template->load('templates/template_view', 'playlists/playlist', $this->data);
            } else {
                set_status_header(404);
                exit("Playlist does not exist!");
            }
        } else {
            set_status_header(400);
            exit("Enter a playlist id!");
        }
    }

}

// EOF