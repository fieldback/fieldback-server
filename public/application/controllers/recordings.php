<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recordings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('recording_model');
        $this->load->library('instacam');
	}

	public function index()
	{
		if(is_admin()){
			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => ''
				),
				'recordings' => array(
					'name' => 'Recordings',
					'link' => 'recordings',
					'state' => 'active'
				)
			);
			$this->data['recordings'] = $this->recording_model->get_recordings();
            $this->data['instacordings'] = $this->instacam->get_instacordings();
			$this->template->load('templates/template_view', 'recordings/recordings_view', $this->data);
		}else{
			redirect('home');
		}
	}

}

/* End of file recordings.php */
/* Location: ./application/controllers/recordings.php */