<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('system_model');
		$this->load->model('event_model');
		$this->load->model('moment_model');
		
		$this->data['system_info'] = $this->system_model->get_system_info();
		
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}
	public function index()
	{
        // Just redirect to /events/ as this is a better homepage than the regular homepage
        redirect('/events');
        /*
		// load includes
		$this->load->model('recording_model');
		$this->load->library('../controllers/clips');

		// set the breadcrumbs
		$this->data['breadcrumbs'] = array(
			'home' => array(
				'name' => 'Home',
				'link' => 'home',
				'state' => 'active'
			)
		);

		// gather information for dashboard
		$this->data['events_player'] = array();
		$this->data['events_owner'] = array();

		$user = $this->ion_auth->user()->row();
		$this->data['latest_moments'] = $this->moment_model->get_latest_moments_by_user_id($user->id, $this->session->userdata('organisation_id'));
		
		if(is_player()){
			$this->data['events_player'] = $this->event_model->get_all_events_for_player($user->id, $this->session->userdata('organisation_id'), 5);
		}
		if(has_teams()){
			$this->data['events_owner'] = $this->event_model->get_all_events_for_owner($user->id, $this->session->userdata('organisation_id'), 5);
		}

		// load view
 		$this->template->load('templates/template_view', 'home/home_view', $this->data);
        */
	}
	
	public function choose_organisation()
	{
		$this->data['breadcrumbs'] = array(
			'home' => array(
				'name' => 'Home',
				'link' => 'home',
				'state' => 'none'
			),
			'choose_organisation' => array(
				'name' => 'Choose organisation',
				'link' => 'choose_organisation',
				'state' => 'active'
			)
		);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('organisation', 'Organisation', 'trim|required|xss_clean');

		if($this->form_validation->run())
		{
			$organisation_id = $this->input->post('organisation');
			
			// set the organisation for this session
			$this->session->set_userdata('organisation_id', $organisation_id);

			// remember the organisation for later sessions
			$this->user_model->set_organisation_for_user($organisation_id);
			redirect('home');
		}
		$this->data['organisations'] = $this->user_model->get_organisations_for_user($this->data['user']->id);
		$this->template->load('templates/template_view', 'home/choose_organisation_view', $this->data);
	}
}

// EOF
