<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Playlist_Ajx extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        // Only do something if through AJAX
        if(!$this->input->is_ajax_request())
        {
            // TURN THIS OFF DURING DEVELOPMENT IF NEEDED
            $this->output->set_status_header('400'); // BAD REQUEST -> NOT AJAX!
            exit();
        }

        // Check if logged in
        if (!$this->ion_auth->logged_in()) {
            redirect('login');
        }

        $this->load->model('playlist_model');
    }

    /**
     *  Get all playlists for the currenty logged in user
     */
    function get_all_my_playlists()
    {
        $user_id = $this->data['user']->id;
        $playlists = $this->playlist_model->get_all_playlists_for_user($user_id);
        echo json_encode(["playlists" => $playlists]);
    }

    /**
     * Get all moments of playlist based on usage rights etc using the 'search' function
     */
    function get_playlist_moments($playlist_id)
    {
        $this->load->model('moment_model');
        $options['events'] = array();
        $options['teams'] = array();
        $options['tags'] = array();
        $options['players'] = array();
        $options['playlists'] = array($playlist_id);

        $this->data['moments'] = $this->moment_model->search_moments($options);
        echo json_encode(["moments" => $this->data['moments']]);
    }

    /**
     * Create a playlist coupled to the active user through an AJAX post with 'title' only
     *
     * echo JSON response with playlist_id
     */
    function create_playlist()
    {
        $title = $this->input->post('title');
        $user_id = $this->data['user']->id;

        $playlist = $this->playlist_model->create_playlist($title, $user_id);
        echo json_encode(["playlist" => $playlist]);
    }

    /**
     * Remove a playlist if auth succeeds
     *
     * echo response
     */
    function remove_playlist()
    {
        $playlist_id = $this->input->post('playlist_id');

        self::_check_auth_and_exists($playlist_id);

        $response = $this->playlist_model->remove_playlist($playlist_id);
        echo json_encode(["response" => $response]);
    }

    /**
     * Set the title of a playlist to this new value
     *
     * echo JSON response to parse in view
     */
    function set_title_of_playlist()
    {
        $title = $this->input->post('title');
        $playlist_id = $this->input->post('playlist_id');

        self::_check_auth_and_exists($playlist_id);

        $response = $this->playlist_model->set_title_of_playlist($title, $playlist_id);
        echo json_encode(["response" => $response]);
    }

    function sync_moment_to_playlist_ids()
    {
        $moment_id = $this->input->post('moment_id');
        $playlist_ids = $this->input->post('playlist_ids');

        // Make it an empty array of no playlists_ids are given
        if(empty($playlist_ids)) $playlist_ids = [];

        // Loop through new playlists ids and check for ownership/existence (should not go wrong)
        foreach($playlist_ids as $playlist_id) {
            self::_check_auth_and_exists($playlist_id);
        }

        // Reset the playlist as we fill it with new data -> only if the auth and exists passes all playlist_ids given
        if($this->playlist_model->remove_all_playlists_from_moment($moment_id))
        {
            // Loop again through ids but now add to database
            $response = [];
            foreach($playlist_ids as $playlist_id) {
                $response[] = $this->playlist_model->add_moment_to_playlist($moment_id, $playlist_id);
            }
            echo json_encode($response);
        }
    }

    /**
     * Function to switch order between two playlist/moments in a playlist
     * This is done in the pivot table
     */
    function switch_order()
    {
        // Get inputs
        $moment_cur = $this->input->post('moment_cur');
        $moment_new = $this->input->post('moment_new');
        $playlist_id = $this->input->post('playlist_id');

        $debug = "cur:".$moment_cur." next:".$moment_new." playlist:".$playlist_id;

        $response = $this->playlist_model->switch_order($moment_cur, $moment_new, $playlist_id);
        echo json_encode(["response" => $response, "debug" => $debug]);
    }

    /**
     * Adds a moment to a playlist
     *
     * Echo's the result in JSON for the view to parse
     */
    function add_moment_to_playlists()
    {
        $moment_id = $this->input->post('moment_id');
        $playlist_id = $this->input->post('playlist_id');

        self::_check_auth_and_exists($playlist_id);

        $response = $this->playlist_model->add_moment_to_playlist($moment_id, $playlist_id);
        echo json_encode(["response" => $response]);
    }

    /**
     * Remove a moment from a playlist
     *
     * Echo's the result in JSON for the view to parse
     */
    function remove_moment_from_playlist()
    {
        $moment_id = $this->input->post('moment_id');
        $playlist_id = $this->input->post('playlist_id');

        self::_check_auth_and_exists($playlist_id);

        $response = $this->playlist_model->remove_moment_from_playlist($moment_id, $playlist_id);
        echo json_encode(["response" => $response]);
    }




    // ---------------------------------------------- PRIVATE CONTROLLER FUNCTIONS -----------------


    /**
     * Check if the playlist exists and if the user is either an admin or the owner of the playlist
     *
     * @param $playlist_id  int     The playlist ID
     * @return bool                 True on success, status header + exit on fail
     */
    private function _check_auth_and_exists($playlist_id)
    {
        // Check if playlist exists and if current user is owner
        $playlist_owner = $this->playlist_model->get_playlist_owner($playlist_id);
        // if false => playlist entry does not exist
        if($playlist_owner != false) {
            // Check if user is owner of playlist OR the admin
            if($playlist_owner == $this->data['user']->id || is_admin() || is_system_admin())
            {
                return true; // return true
            } else {
                $this->output->set_status_header('401'); // 401 -> not authorized
            }
        } else {
            $this->output->set_status_header('404'); // 404 -> playlist is not found
        }
        exit();
    }

}

// EOF