<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sensor extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('sensor_model');
        $this->load->model('part_model');
        $this->load->library('../controllers/moments');
    }


    public function register()
    {

        $this->sensor_model->insertIntoActiveList();
        exit('1');



    }

    /**
     * Create moment by sensor input
     * @return
     */
    public function create()
    {
        // get the sensor ip address
        $sensor_ip = $_SERVER['REMOTE_ADDR'];
        $sensor_info = $this->sensor_model->get_sensor_by_ip($sensor_ip);

        $data = array(
            'event_id' => $sensor_info['event_id'],
            'part_id' => $sensor_info['part_id'],
            'lead' => $sensor_info['lead'],
            'lapse' => $sensor_info['lapse']
        );

        // check if instapart or normal recording
        if($this->part_model->check_if_instapart($sensor_info['part_id'])) {
            // instapart
            $insta_response = $this->moments->insta_create($data);

            sleep($sensor_info['lapse']);

            $this->moments->insta_stop($insta_response['capture_session_ids']);
        }else{
            // normal recording
            $data['moment_id'] = $this->moments->create_moment($data);

            sleep($sensor_info['lapse']);

            $this->moments->render_moments($data);
        }



        exit();
    }

}
