<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');

		$this->load->library('form_validation');
		$this->load->library('encrypt');

		$this->form_validation->set_error_delimiters('<span class="bg-warning">', '</span>');
		$this->ion_auth->set_error_delimiters('<p class="bg-danger">', '</p>');
	}
	/**
	 * Login page
	 * @return login_view
	 */
	public function index()
	{

		// redirect user if already logged in
		if($this->ion_auth->logged_in()){
			redirect('home');
		}

		//check if form is filled in
		$this->form_validation->set_rules('login[username]', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('login[password]', 'Password', 'required|xss_clean');

		//if form is correct
		if($this->form_validation->run())
		{
			$login = $this->input->post('login');

			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			//login
			if($this->ion_auth->login($login['username'], $login['password'], $remember))
			{
				//get the user
				$user = $this->ion_auth->user()->row();

				//get the organisations of the user
				$organisations = $this->user_model->get_organisations_for_user($user->id);

				//set the organisation to the first organisation
				$this->session->set_userdata('organisation_id', $organisations['0']['organisation_id']);

				//register the login of the user and place in database
				$this->user_model->follow_login($user);

				redirect('home');	
			}else{
				//errors...
				$this->ion_auth->errors();
			}
		}
		$this->template->load('templates/template_view', 'login/login_view', $this->data);
	}

	public function forgot_password()
	{
		//form validation
		$this->form_validation->set_rules('email', 'Email Address', 'required');

		//if we are running, GO!
		if($this->form_validation->run())
		{
			//forgotten email!
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

			if ($forgotten) { //if there were no errors
				$this->session->set_flashdata('success', $this->ion_auth->messages());
				redirect("login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("login/forgot_password", 'refresh');
			}
		}
		$this->template->load('templates/template_view', 'login/forgot_password_view', $this->data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */