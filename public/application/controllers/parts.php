<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parts extends MY_Controller {

	function __construct(){
		parent::__construct();
		//$this->data['head']['stylesheets'][] = 'parts.css';
		$this->load->model('system_model');
		$this->load->model('event_model');
		$this->load->model('part_model');
		$this->load->model('clip_model');
		$this->load->model('moment_model');
		$this->load->helper('date');
		$this->load->helper('file');
		$this->data['system_info'] = $this->system_model->get_system_info();
		if(!logged_in()){
			redirect('login');
		}
		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}
	public function index()
	{
		//magic? 
	}

	/**
	 * Add part to event
	 * The name is given in this function
	 * @param 	int 		$event_id 		ID of the event
	 * @return  view
	 */
	public function add_part_to_event($event_id = null)
	{
		if($event_id !== null) {
			// default part nr
			$part_nr = 1;

			// get the existing amount of parts
			$amount_existing_parts = self::get_amount_of_parts_for_event($event_id);
			
			// set the part_nr
			if($amount_existing_parts !== null) {
				$part_nr = $amount_existing_parts + 1;
			}

			// create the data
			$data = array(
				'part_name' => "Part " . $part_nr,
				'date_created' => now()
			);

			// add the part
			$part_id = $this->part_model->add_part_to_event($data, $event_id);

			// create dir for the part
			if(!is_dir("./videos/$event_id/$part_id")){
				$old_umask = umask(0);
				mkdir("./videos/$event_id/$part_id", 0777);
				umask($old_umask);
			}

			// redirect to view
			flash_success('part_added');
			redirect('events/event/' . $event_id);
		}
		flash_error();
		redirect('home');
	}

	/**
	 * Get the amount of existing parts for the event
	 * @param  int 		$event_id 	Existing Event ID
	 * @return int           		The amount of existing parts
	 */
	public function get_amount_of_parts_for_event($event_id)
	{
		// get the existing parts
		$parts = $this->part_model->get_parts_for_event_by_event_id($event_id);

		// how many do we have?
		$amount = count($parts);

		// return
		return $amount;
	}


	/**
	 * Delete part
	 * @param  int 		$part_id  	The part id
	 * @param  int 		$event_id 	The event id
	 * @return view           		Back to the event
	 */
	public function delete_part($part_id = null, $event_id = null)
	{
		if($event_id !== null)
		{
			if($part_id !== null)
			{
				//delete from database
				$this->part_model->delete_part_by_part_id($part_id);
				
				//if part dir exists, delete everything in it
				if(is_dir('./videos/' . $event_id . '/' . $part_id))
				{
					// delete the tmp_rec folder and the contents
					if(is_dir('./videos/'.$event_id.'/'.$part_id.'/tmp_rec')){
						delete_files('./videos/'.$event_id.'/'.$part_id.'/tmp_rec');
						rmdir('./videos/'.$event_id.'/'.$part_id.'/tmp_rec/');
					}

					// delete the tmp folder and the contents
					if(is_dir('./videos/'.$event_id.'/'.$part_id.'/tmp')){
						delete_files('./videos/'.$event_id.'/'.$part_id.'/tmp');
						rmdir('./videos/'.$event_id.'/'.$part_id.'/tmp');
					}

					// get all the moments, so we can delete them
					$moments = $this->moment_model->get_all_moments_for_part($part_id);
					foreach ($moments as $moment) 
					{
						// delete the moment folder and the files in it
						if(is_dir('./videos/' . $event_id .'/'.$part_id.'/'.$moment['moment_id']))
						{
							delete_files('./videos/' . $event_id .'/'.$part_id.'/'.$moment['moment_id']);
							rmdir('./videos/' . $event_id .'/'.$part_id.'/'.$moment['moment_id']);
						}
					}
					// now the part folder can be deleted
					delete_files('./videos/' . $event_id . '/' . $part_id);
					rmdir('./videos/' . $event_id . '/' . $part_id);
				}
				//success flash
				flash_success('part_deleted');
				redirect('events/event/' . $event_id);
			}
		}
	}

	/**
	 * Tag view in a part
	 * @param  int 		$event_id 	Event ID
	 * @param  int 		$part_id  	Part ID
	 * @return view           		The tag view is returned, the user is able to create new tags in this view.
	 */
	public function tag($event_id = null, $part_id = null)
	{
		if($event_id !== null && $part_id !== null) {
			// get all the needed info
			$this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
			$this->data['part_info'] = $this->part_model->get_part_info_by_part_id($part_id);
			$this->data['clips'] = $this->clip_model->get_clips_by_part_id($part_id);
			$this->data['moments'] = $this->moment_model->get_all_moments_for_part($part_id);

			// !Download link for downloading all moments of a part (in the breadcrumb)
			$download_link = "";
			if($this->data['system_info']['downloads'] === '1'){
				$download_link = " - <a href='".base_url()."download/moments_of_part/".$this->data['part_info']['part_id']."' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-floppy-save'></span> download all moments</a>";
			}
			
			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Events',
					'link' => 'events',
					'state' => 'none'
				),
				'event' => array(
					'name' => $this->data['event_info']['event_name'],
					'link' => 'events/event/' . $event_id,
					'state' => 'none'
				),
				'tag_part' => array(
					'name' => $this->data['part_info']['part_name'].$download_link,
					'link' => '#',
					'state' => 'active'
				)
			);

			$this->template->load('templates/template_view', 'parts/tag_view', $this->data);
		}else{
			flash_error();
			redirect('events');
		}
	}
}
