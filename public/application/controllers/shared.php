<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	The Share controller generates unique links that can be send to facebook or other social media, 
	which are connected to the user who shared the moment and is tracked with analytics software to see the impact (and simply counted in the database)
	*/

class Shared extends MY_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->model('share_model');
	}
	
	
	/*
		This function generates an unique id that can be used as url: shared/moment/*unique*
		*/
	public function create($type = 'moment', $id = 0) {
		// Load the url helper
		$this->load->helper('url');
		// To create a new share it is essential to be logged in
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}

		// check if $id is numeric and not 0 - otherwise there would be nothing to share
		if(is_numeric($id) && $id != 0) {
			// ! Check for valid types (moments, etc)
			if($type == "moment" || $type == "event") {
				// Check to see if there already is a shared token
				$share = $this->share_model->get_moment_share_by_id_and_user($id, $this->data['user']->user_id);
				// does it exist?
				if(!empty($share)) {
					// set shared_token from db
					$share_token = $share['share_token'];
				} else {
					// Create a new shared token -> used to keep track in the database
					$share_token = $this->share_model->create_share("moment", $id, $this->data['user']->user_id);
				}
				// Redirect to the shared content page with the token
				redirect("/shared/token/" . $share_token);
			} else {
				die("Not a valid share type given.");
			}
		} else {
			die("ID is not numeric or not present!");
		}
	}
	
	/*
		This function looks up the shared_token in the database and displays the appropriate page according to type and id
		Also, some basic statistics are saved - this is the 'front-end' for outsiders into Fieldback (when shared through facebook/twitter/etc)
		*/
	public function token($share_token = null) {
		// Was a token given?
		if($share_token !== null) {
			// Get info from db
			$this->data['share'] = $this->share_model->get_share_by_token($share_token);
			// check for result
			if(!empty($this->data['share'])) {
				// For each type of moment we have a different view;
				switch($this->data['share']['type']) {
					case "moment":
						// !Moment type public display
						// Load moment model
						$this->load->model('moment_model');
						// Get moment info and make it available for the view
						$this->data['moment_info'] = $this->moment_model->get_moment_info($this->data['share']['id']);
						$this->data['moment_files'] = $this->moment_model->get_moment_files_by_moment_id($this->data['share']['id']);
                        $this->data['share_token'] = $share_token;
						// Load view
						$this->load->view('share/public_moment_view', $this->data);
					break;
					case "event":
						// !Event type public display page
						die("event type is not made yet.");
					break;
				}
			} else {
				// Moment not found -> but still display something!
				// !Replace with good 404 page for the public
				die("This shared content no longer exists or never existed.");
			}
			
		}
	}

}

// EOF