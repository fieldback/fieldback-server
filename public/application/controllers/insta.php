<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insta extends MY_Controller {


    function __construct()
    {
        parent::__construct();
    }

    function moment_file()
    {
        // Get the ffmpeg location
        $ffmpeg = $this->data['system_info']['ffmpeg'];

        openlog("upload attempt:", LOG_PID | LOG_PERROR, LOG_LOCAL0);
        if(isset($_POST['unique_identifier'])) {
            $moment_file_id = base64_decode($_POST['unique_identifier']);
            $this->load->model('moment_model');
            $moment_file_info = $this->moment_model->get_moment_file_info_by_moment_file_id($moment_file_id);
            // Check if this moment_file exists in database
            if(!empty($moment_file_info)) {
                // Check if this moment was not yet uploaded
                if($moment_file_info['is_uploaded'] == 0) {
                    $path = "videos/".$moment_file_info['event_id']."/".$moment_file_info['part_id']."/".$moment_file_info['moment_id'];
                    $file_location = $path."/_".$moment_file_id.".mp4";
                    syslog(LOG_NOTICE, "File uploaded, size: ".$_FILES['moment_file']['size']);
                    syslog(LOG_NOTICE, "File name is: ".$_FILES['moment_file']['tmp_name']);
                    syslog(LOG_NOTICE, "Maybe with error: ".$_FILES['moment_file']['error']);
                    syslog(LOG_NOTICE, "File unique id was: ".$_POST['unique_identifier']);
                    syslog(LOG_NOTICE, "File timestamp: ".$_POST['timestamp']." , with FPS: ".$_POST['fps']);
                    // Move the file
                    move_uploaded_file($_FILES['moment_file']['tmp_name'], $file_location);
                    closelog();

                    // Safe the FPS and the started to the database and set uploaded to true
                    $this->moment_model->set_uploaded_info_of($moment_file_id, $_POST['fps'], substr($_POST['timestamp'], 0, -6));

                    // Check if this was the last upload of this moment, if yes: trigger the _finalize function
                    $moment_files = $this->moment_model->get_moment_files_by_moment_id($moment_file_info['moment_id']);
                    $upload_complete = true;
                    $latest_start_timestamp = 0;
                    foreach($moment_files as $moment_file) {
                        // Check if the is_uploaded flag is 1
                        if($moment_file['is_uploaded'] != 1) $upload_complete = false;
                        // Get the latest timestamp by constantly comparing which is the biggest
                        if($moment_file['started_timestamp'] > $latest_start_timestamp) $latest_start_timestamp = $moment_file['started_timestamp'];
                    }

                    if($upload_complete) {
                        // Loop through the moment_files again for jpg and avconv generation
                        foreach($moment_files as $moment_file) {
                            $moment_file_path = "videos/".$moment_file_info['event_id']."/".$moment_file_info['part_id']."/".$moment_file['moment_id'];
                            $moment_file_location = $moment_file_path . "/_" . $moment_file['moment_file_id'] . ".mp4";
                            $moment_file_output = $moment_file_path . "/" . $moment_file['moment_file_id'] . ".mp4";
                            $moment_file_output_jpg = $moment_file_path . "/" . $moment_file['moment_file_id'] . ".jpg";
                            // What is the time difference of this file with the latest file
                            $difference = $latest_start_timestamp - $moment_file['started_timestamp'];
                            syslog(LOG_NOTICE, "Time difference of ".$moment_file['moment_file_id']." is: " . $difference);

                            // Cut the time difference of the front of the video to let them play more in sync
                            $cmd_options = array(
                                $ffmpeg,
                                "-ss " . milliseconds_to_time($difference),
                                "-i " . $moment_file_location,
                                "-vcodec copy",
                                "-y",
                                $moment_file_output,
                                "> /dev/null 2> $path/avconv_vid_".$moment_file['moment_file_id'].".log" // Log file output instead of giving errors in console
                            );
                            $cmd = implode($cmd_options, " ");
                            exec($cmd);
                            syslog(LOG_NOTICE, "AVConv command executed to create .mp4: $cmd");
                            // Capture a .jpg from the newly created synced received file
                            $cmd_options = array(
                                $ffmpeg,
                                "-i " . $moment_file_location,
                                "-ss ".milliseconds_to_time(1000+$difference), // 1 second + difference into the video
                                "-vframes 1", // single frame
                                "-vcodec mjpeg", // no re-encoding, this saves cpu cycles
                                "-y",
                                $moment_file_output_jpg,
                                "> /dev/null 2> $path/avconv_jpg_".$moment_file['moment_file_id'].".log" // Log file output instead of giving errors in console
                            );
                            $cmd = implode(" ", $cmd_options);
                            exec($cmd);
                            // Remove old file $moment_file_location
                            unlink($moment_file_location);

                            syslog(LOG_NOTICE, "AVConv command executed to create .jpg: $cmd");
                        }
                    } else {
                        syslog(LOG_NOTICE, "Looped through moments, but not complete yet or false data.");
                    }
                    closelog();
                    exit();
                } else {
                    set_status_header(400);
                    syslog(LOG_NOTICE, "File was already uploaded, something went wrong.");
                    closelog();
                    exit();
                }
            } else {
                set_status_header(400);
                syslog(LOG_NOTICE, "File uploaded with unique id: ".$_POST['unique_identifier'].", but this ID was not in database..");
                closelog();
                exit();
            }
        } else {
            set_status_header(403);
            syslog(LOG_NOTICE, "Page visited but no unique_identifier given.");
            closelog();
            exit();
        }
    }
}
