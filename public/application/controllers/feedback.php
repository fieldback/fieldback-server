<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {

	public function feedback_form()
	{
		//variables from input post
		$url = $this->input->post('url');

		$data = array(
			'url' => $url
		);

		//load view
		$page = $this->load->view('feedback/feedback_form_view', $data);
		echo $page;
		return;
	}

	public function send_feedback() 
	{
		$this->load->library('email');

		$title 			= $this->input->post('title');
		$message 		= $this->input->post('message');
		$url 			= $this->input->post('url');

		$user 			= $this->ion_auth->user()->row();
		$user->info 	= $this->user_model->get_user_by_id($user->id);

		$content 		= '<h1>' . $title . '</h1>';
		$content 		.= '<p>' . $message . '</p>';
		$content 		.= '<p><b>Person:</b> ' . $user->info['first_name'] . ' ' . $user->info['last_name'] . '</p>';
		$content 		.= '<p><b>E-mail:</b> ' . $user->info['email'] . '</p>';
		$content		.= '<p><b>URL:</b> ' . $url . '</p>';
		$content		.= '<p><b>Date:</b> ' . date('l jS \of F Y h:i:s A');

		$this->load->library('email');

		$this->email->from('fieldback@fieldback.net', $this->data['system_info']['system_name']);
		$this->email->to('support@fieldback.net'); 

		$this->email->subject('New feedback on Fieldback');
		$this->email->message($content);	

		$this->email->send();

		echo json_encode('correct');
	}

}

/* End of file feedback.php */
/* Location: ./application/controllers/feedback.php */