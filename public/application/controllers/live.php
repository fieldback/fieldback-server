<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Live extends MY_Controller {

	function __construct(){
		parent::__construct();
		
		// Check if logged in
		if(!$this->ion_auth->logged_in()){
			redirect('login');
		}
		
		// Load the camera model, all functions use this
		$this->load->model("camera_model");
		
	}
	
	/**
		In the live controller we offer a live view of the camera's connected to the system that are active and support a live stream.
		This can be used to watch from home or follow the game with a camera without a view finder
		*/
	public function index() {
		// Get all active cameras and put it in a cameras variable - count how many live camera's there are
		$this->data['cameras'] = $this->camera_model->get_all_live_cameras();
		$number_of_cameras = count($this->data['cameras']);
		// If there is no live camera this page should have never been visited -> error
		if($number_of_cameras == 0) { 
			flash_error('no_live_cameras');
			redirect('home');
		} else {
			// Fill the breadcrumb array		
			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Live view',
					'link' => 'live',
					'state' => 'active'
				)
			);
			// Load the template view
			$this->template->load('templates/template_view', 'live/select_camera_view', $this->data);
			return;
		}
		
	}
	
	public function camera($camera_id = '') {
		// Get the camera info for this camera
		$this->data['camera'] = $this->camera_model->get_stream_by_cam_id($camera_id);
		// See if the camera exists (it should when someone is at this page)
		if(!empty($this->data['camera'])) {
			// Fill the breadcrumb array		
			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'live' => array(
					'name' => 'Live view',
					'link' => 'live',
					'state' => 'none'
				),
				'camera' => array(
					'name' => 'Camera: '.$this->data['camera']['description'],
					'link' => 'live/camera/'.$this->data['camera']['camera_id'],
					'state' => 'active'
				)
			);
			// Load the template view
			$this->template->load('templates/template_view', 'live/camera_view', $this->data);
			return;
		} else {
			flash_error('camera_does_not_exist');
			redirect('home');
		}
	}

	public function camera_fullscreen($camera_id = '') {
		// Get the camera info for this camera
		$this->data['camera'] = $this->camera_model->get_stream_by_cam_id($camera_id);
		// See if the camera exists (it should when someone is at this page)
		if(!empty($this->data['camera'])) {
			// Load the template view
			$this->template->load('templates/template_view', 'live/camera_fullscreen_view', $this->data);
			return;
		} else {
			flash_error('camera_does_not_exist');
			redirect('home');
		}
	}

		
}


// EOF