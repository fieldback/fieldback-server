<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clips extends MY_Controller {

	function __construct(){
		parent::__construct();

		//load models
		$this->load->model('system_model');
		$this->load->model('part_model');
		$this->load->model('event_model');
		$this->load->model('clip_model');
		$this->load->model('camera_model');
		$this->load->model('recording_model');
		$this->load->model('tagfield_model');
		$this->load->model('moment_model');
		$this->load->model('sensor_model');

		//load helpers
		$this->load->helper('date');
		$this->load->helper('file');

		$this->load->library('form_validation');

		//get system information from the database
		$this->data['system_info'] = $this->system_model->get_system_info();

		//login check
		if(!logged_in()){
			redirect('login');
		}

		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}

		//variables
		$ffmpeg = $this->data['system_info']['ffmpeg'];
		$openrtsp = $this->data['system_info']['openrtsp'];
	}
	public function index()
	{
		redirect('home');
	}
	/*UPLOADS*/
	public function upload_clip_to_part($event_id, $part_id)
	{
		$this->data['clips_to_part'] = $this->clip_model->get_clips_by_part_id($part_id);
		$this->data['part_info'] = $this->part_model->get_part_info_by_part_id($part_id);
		$this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
		$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Events',
					'link' => 'events',
					'state' => 'none'
				),
				'event' => array(
					'name' => $this->data['event_info']['event_name'],
					'link' => 'events/event/' . $event_id,
					'state' => 'none'
				),
				'upload' => array(
					'name' => 'Upload',
					'link' => '',
					'state' => 'active'
				)

			);
		$this->template->load('templates/template_view', 'clips/upload_clip_view', $this->data);
	}
	public function do_upload_clip_to_part($event_id, $part_id)
	{
		$ffmpeg = $this->data['system_info']['ffmpeg'];

		//create event folder if not exists
		if(!is_dir('./videos/' . $event_id)){
			$old_umask = umask(0);
			mkdir('./videos/' . $event_id, 0777);
			umask($old_umask);
		}
		//create part folder if not exists
		if(!is_dir('./videos/' . $event_id . '/' . $part_id)){
			$old_umask = umask(0);
			mkdir('./videos/' . $event_id . '/' . $part_id, 0777);
			umask($old_umask);
		}
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			//do the upload
			$config['upload_path'] = './videos/' . $event_id . '/' . $part_id . '/';
			$config['allowed_types'] = 'mpg|mpeg|mp4|mov|avi|wmv|3gp|mts';
			$config['max_size'] = '2147483648';


			$this->load->library('upload', $config);
			if( ! $this->upload->do_upload()){
				$error = $this->upload->display_errors();
				echo $error;
			}else{
				$file_data = $this->upload->data();
				$file_data['date_created'] = now();
				$file_data['kind'] = 'upload';
				unset($file_data['is_image']);
				unset($file_data['image_width']);
				unset($file_data['image_height']);
				unset($file_data['image_type']);
				unset($file_data['image_size_str']);
				$user_id = $this->data['user']->id;
				$clip_id = $this->clip_model->add_new_clip($file_data, $user_id, $part_id);

				//create a thumb of the movie
                $ffmpeg_thumb = "$ffmpeg -i " . $file_data['full_path'] . " -y -f image2 -vcodec mjpeg -vframes 1 -s 320x180 videos/" . $event_id . "/" . $part_id . "/" . $clip_id . ".jpg";
                shell_exec($ffmpeg_thumb);
                //change the video to h264 And check if it is .MTS because than it should deinterlace the filetype.
                if($file_data['file_ext'] == '.MTS' || $file_data['file_ext'] == '.mts') {
                    $ffmpeg_command = "$ffmpeg -i " . $file_data['full_path'] . " -movflags frag_keyframe -vf yadif=0 -vcodec libx264 -pix_fmt yuv420p -strict -2 -g 50 videos/" . $event_id . "/" . $part_id . "/" . $clip_id . ".mp4 </dev/null >/dev/null 2> " . $file_data['file_path'] . "progress.txt &";
                } else {
                    $ffmpeg_command = "$ffmpeg -i " . $file_data['full_path'] . " -movflags frag_keyframe -vcodec libx264 -pix_fmt yuv420p -strict -2 -g 50 videos/" . $event_id . "/" . $part_id . "/" . $clip_id . ".mp4 </dev/null >/dev/null 2> " . $file_data['file_path'] . "progress.txt &";
                }

                shell_exec($ffmpeg_command);
                $message = 'File uploaded';
                return $message;
			}
		}
	}
	public function get_encode_progress_of_upload()
	{
		$event_id = $this->input->post('event_id');
		$part_id = $this->input->post('part_id');
		if(file_exists('./videos/' . $event_id . '/' . $part_id . '/progress.txt')){
			$content = file_get_contents('./videos/' . $event_id . '/' . $part_id . '/progress.txt');
			if($content){
			    //get duration of source
			    preg_match("/Duration: (.*?), start:/", $content, $matches);

				if(!empty($matches)){
				    $rawDuration = $matches[1];
				    //rawDuration is in 00:00:00.00 format. This converts it to seconds.
				    $ar = array_reverse(explode(":", $rawDuration));
				    $duration = floatval($ar[0]);
				    if (!empty($ar[1])) $duration += intval($ar[1]) * 60;
				    if (!empty($ar[2])) $duration += intval($ar[2]) * 60 * 60;

				    //get the time in the file that is already encoded
				    preg_match_all("/time=(.*?) bitrate/", $content, $matches);

				    $rawTime = array_pop($matches);

				    //this is needed if there is more than one match
				    if (is_array($rawTime)){$rawTime = array_pop($rawTime);}

				    //rawTime is in 00:00:00.00 format. This converts it to seconds.
				    $ar = array_reverse(explode(":", $rawTime));
				    $time = floatval($ar[0]);
				    if (!empty($ar[1])) $time += intval($ar[1]) * 60;
				    if (!empty($ar[2])) $time += intval($ar[2]) * 60 * 60;

				    //calculate the progress
				    $progress = round(($time/$duration) * 100);
				    $data = array(
				    	'progress' => $progress
				    );
				    echo json_encode($data);
				    return;
				}
			}
		}
		// if not encoding, send no encoding...
		$data = array('progress' => 'No encoding');
		echo json_encode($data);
	}
		/*
		*	This function gets all the active camera's and lets a user select which camera to use and determine the recording time.
		*	April 2015: added function to also support high speed / instant camera's when available in the system
		*/
	public function record($event_id = null, $part_id = null)
	{
		$ffmpeg = $this->data['system_info']['ffmpeg'];
		if($event_id !== null && $part_id !== null)
		{
			$this->data['event_id'] = $event_id;
			$this->data['part_id'] = $part_id;
			//select all cameras in network
			$this->data['cameras'] = $this->camera_model->get_all_active_cameras();
            if(!is_dir('./videos/' . $event_id)){
                $old_umask = umask(0);
                if (!mkdir('./videos/' . $event_id, 0777)) {
                    die('Could not create event directory. Please contact an administrator.');
                }
                umask($old_umask);
            }
            //create dirs to post to
			if(!is_dir('./videos/' . $event_id . '/' . $part_id)){
				$old_umask = umask(0);
				if (!mkdir('./videos/' . $event_id . '/' . $part_id, 0777)) {
					die('Could not create part directory. Please contact an administrator.');
				}
				umask($old_umask);
			}
			if(!is_dir('./videos/' . $event_id . '/' . $part_id . '/tmp')){
				$old_umask = umask(0);
				if (!mkdir('./videos/' . $event_id . '/' . $part_id . '/tmp', 0777)) {
					die('Could not create /tmp folder. Please contact an administrator.');
				}
				umask($old_umask);
			}
			//create thumbs
			$numcams = count($this->data['cameras']);
			$i = 1;
			foreach ($this->data['cameras'] as $camera)
			{
				$stream = $camera['stream'];
				$number = $camera['number'];

				//check if stream is open
				$check = self::rtsp_exists($stream);
				// $check = true;
				if($check == true)
				{
					// call dev/null if not last camera (for speed)
					if ($i < $numcams) {
						$devnull = '> /dev/null';
					} else {
						$devnull = '';
					}

					//create stream in ffmpeg to make picture
					exec("$ffmpeg -i $stream -f image2 -vframes 1 ./videos/$event_id/$part_id/tmp/screenshot$number.jpg $devnull 2> ./videos/$event_id/$part_id/tmp/ffmpeglog$number.txt");
				}
				$i++;
			}

			// Fill the instacams array with thumbnail data
			$this->load->library('instacam');
			// Let the library create thumbnails for all active camera's and place it at the event_id/part_id/tmp folder
			$this->data['instacams'] = $this->instacam->get_thumbnails_for_active_cameras($event_id, $part_id);

			$event = $this->event_model->get_event_info_by_event_id($event_id);

			$this->data['breadcrumbs'] = array(
				'home' => array(
					'name' => 'Home',
					'link' => 'home',
					'state' => 'none'
				),
				'events' => array(
					'name' => 'Events',
					'link' => 'events',
					'state' => 'none'
				),
				'event' => array(
					'name' => $event['event_name'],
					'link' => 'events/event/' . $event['event_id'],
					'state' => 'none'
				),
				'choose cams' => array(
					'name' => $event['event_name'] . ' - Recording settings',
					'link' => '',
					'state' => 'active'
				)
			);

			// check for wireless triggers
			$this->data['sensors'] = $this->sensor_model->get_sensors();

			$this->template->load('templates/template_view', 'clips/record_clip_view', $this->data);
		}
		else
		{
			redirect('events');
		}
	}
	public function rtsp_exists($url)
	{
	    $server = parse_url($url, PHP_URL_HOST);
	    $port = "554";
	    $hdrs = "DESCRIBE " .$url ." RTSP/1.0"."\r\n\r\n";

	    //Open connection (15s timeout)
	    $sh = @fsockopen($server, $port, $err, $err_otp, 1);

	    //Check connections
	    if($sh == false) return false;
	    fclose($sh);
	    return true;
	}
	public function clear_tmp()
	{
		$event_id = $this->input->post('event_id');
		$part_id = $this->input->post('part_id');
		if(is_dir("./videos/$event_id/$part_id/tmp")){
			$dir = "./videos/$event_id/$part_id/tmp";
			delete_files($dir);
			rmdir($dir);
		}
		return;
	}
	public function start_recording($event_id, $part_id)
	{

		$this->form_validation->set_rules('time[hours]', 'Time', 'callback_max_time');

		if( $this->form_validation->run() ){
			if($this->input->post('time'))
			{
				//variables
				$openrtsp = $this->data['system_info']['openrtsp'];
				$ffmpeg = $this->data['system_info']['ffmpeg'];

				// get cameras from form
				$cameras = $this->input->post('camera');
				$time = $this->input->post('time');

				//calculate record time to seconds
				$hours = intval($time['hours'])*3600;
				$minutes = $time['minutes']*60;
				$time_to_record = ($hours+$minutes);

				//set sensor(s) to event and part
				$sensors = $this->input->post('sensors');
				foreach ($sensors as $sensor) {
					if(isset($sensor['sensor_id'])){
						$sensor_id = $sensor['sensor_id'];
						$lead = $sensor['lead'];
						$lapse = $sensor['lapse'];

						$this->sensor_model->set_sensor_to_event($sensor_id, $event_id, $part_id, $lead, $lapse);
					}
				}


				//create temp dir for recording(s)
				if(!is_dir("./videos/$event_id/$part_id/tmp_rec"))
				{
					$old_umask = umask(0);
					mkdir("./videos/$event_id/$part_id/tmp_rec", 0777);
					umask($old_umask);
				}
				//start recording for all cameras
				foreach ($cameras as $cam_nr => $val)
				{

					foreach ($val as $cam_id => $value) {
						$camera_id = $cam_id;
					}

					//get RTSP stream
					$camera = $this->camera_model->get_stream_by_cam_id($cam_id);
					$stream = $camera['stream'];
					$cam_fps = $camera['cam_fps'];
					$cam_constante = $camera['cam_constante'];
					$cam_description = $camera['description'];
					$copy = $camera['copy'];

					$cmd_options = array(

						$ffmpeg,

						//"-rtsp_transport tcp", // via tcp protocol

						//"-stimeout 2000000", // If camera is disconnected, exit ffmpeg

						"-t ".$time_to_record, // Max rec time from config

						"-i ".$stream, // RTSP stream adress

						"-f mp4", // Safe the stream

						"-vcodec copy", // no re-encoding, this saves cpu cycles

						"-movflags frag_keyframe", // very important, safe the keyframe data to each keyframe instead of at the end

						"videos/" . $event_id . "/" . $part_id . "/tmp_rec/rec" . $cam_nr . ".mp4", // output file

						"-loglevel debug", // for debugging only

						"-y",

						"> /dev/null 2> videos/" . $event_id ."/" . $part_id . "/tmp_rec/rec_log" . $cam_nr . ".txt & echo $!" // Log file output instead of giving errors in console - return PID to kill later

					);

					// Create command line string from the options

					$cmd = implode(" ", $cmd_options);

					// dump($cmd, true);
					// Execute cmd and get the process id

					$pid = exec($cmd);

					$data = array(
						'cam_number' 		=> $cam_nr,
						'cam_description'	=> $cam_description,
						'cam_fps'			=> $cam_fps,
						'cam_constante'		=> $cam_constante,
						'stream'			=> $stream,
						'copy'				=> $copy,
						'time_to_record' 	=> $time_to_record,
						'start_time' 		=> now(),
						'process_id'		=> $pid,
						'rec_owner'			=> $this->data['user']->id,
						'event_id'			=> $event_id,
						'part_id'			=> $part_id
					);

					$recording_id = $this->recording_model->insert_new_recording($data);
				}
			}
		}
		redirect('clips/live_tagging/' . $event_id . '/' . $part_id);
	}

	public function start_instacording($event_id, $part_id)
	{
		$this->load->library('instacam');
		// Get variables
		$this->data['event_id'] = $event_id;
		$this->data['part_id'] = $part_id;
		// get the camera_ids
		$camera_ids = $this->input->post('instacam');
		// Start the session with the instacam library: this is all we need to do
		// (pass the url and camera identifier for each cam)
		$this->instacam->start_session($camera_ids, $event_id, $part_id);

        //set sensor(s) to event and part
        $sensors = $this->input->post('sensors');
        foreach ($sensors as $sensor) {
            if(isset($sensor['sensor_id'])){
                $sensor_id = $sensor['sensor_id'];
                $lead = 0;
                $lapse = $sensor['lapse'];

                $this->sensor_model->set_sensor_to_event($sensor_id, $event_id, $part_id, $lead, $lapse);
            }
        }

        redirect('clips/instacam/' . $event_id . '/' . $part_id);
	}

	public function stop_instacording($ajax = true, $data = array())
    {
        $this->load->library('instacam');
		// data can come from two different places...
		if($ajax === true){
			$event_id = $this->input->post('event_id');
			$part_id = $this->input->post('part_id');
		}
		if($ajax === false){
			$event_id = $data['event_id'];
			$part_id = $data['part_id'];
		}
		$this->instacam->stop_session_for($event_id, $part_id);
        if($ajax === true){
            echo json_encode('yes');
        }
        return;
	}

	public function max_time() {
		$time = $this->input->post('time');

		$hours = $time['hours'];
		$minutes = $time['minutes'];

		$hours = intval($hours)*3600;
		$minutes = $minutes*60;
		$time_to_record = ($hours+$minutes);

		if($time_to_record > 7200){
			return false;
		}else{
			return true;
		}

	}

	public function stop_recording($ajax = true, $data = array())
	{
		if($ajax === true){
			$event_id = $this->input->post('event_id');
			$part_id = $this->input->post('part_id');
		} else {
            $event_id = $data['event_id'];
			$part_id = $data['part_id'];
		}

        $this->load->library("recording");
        $this->recording->stop_recording($event_id, $part_id);

		if($ajax === true){
			echo json_encode('yes');
		}
		return;
	}
	public function live_tagging($event_id = null, $part_id = null)
	{
		if($event_id !== null && $part_id !== null){
			//$this->data['head']['stylesheets'][] = 'live_tagging.css';

			$this->load->helper('form');

			if($event_id && $part_id){
                if($this->part_model->check_if_instapart($part_id)) {
                    redirect('clips/instacam/' . $event_id . '/' . $part_id);
                }
				$this->data['recordings'] = $this->recording_model->get_recordings_by_event_id_and_part_id($event_id, $part_id);

				//check if recording is true
				if(!empty($this->data['recordings'])){
					// Get rest of data
					$this->data['tagfields'] = $this->tagfield_model->get_tagfields_for_user($this->data['user']->id);
					$this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
					$this->data['part_info'] = $this->part_model->get_part_info_by_part_id($part_id);
					$this->data['moments'] = $this->moment_model->get_all_moments_for_part($part_id);

					$this->data['breadcrumbs'] = array(
						'home' => array(
							'name' => 'Home',
							'link' => 'home',
							'state' => 'none'
						),
						'events' => array(
							'name' => 'Events',
							'link' => 'events',
							'state' => 'none'
						),
						'event' => array(
							'name' => $this->data['event_info']['event_name'],
							'link' => 'events/event/' . $event_id,
							'state' => 'none'
						),
						'live_tagging' => array(
							'name' => $this->data['event_info']['event_name'] . ' - Live tagging',
							'link' => '',
							'state' => 'active'
						)
					);
					$this->template->load('templates/template_view', 'clips/live_tagging_view', $this->data);
				}else{
					redirect('parts/tag/' . $event_id . '/' . $part_id);
				}
			}
		}else{
			flash_error();
			return false;
		}
	}

	public function instacam($event_id = null, $part_id = null)
	{
		if($event_id !== null && $part_id !== null)
	    {
 			// Get info
            $this->data['event_info'] = $this->event_model->get_event_info_by_event_id($event_id);
            $this->data['part_id'] = $part_id;
            // Pass the recordings info to the view: determine if recording is active there
            $this->load->library('instacam');
            $this->data['instacordings'] = $this->instacam->get_active_instacordings_for_part_id($part_id);

            $this->data['breadcrumbs'] = array(
                'home' => array(
                    'name' => 'Home',
                    'link' => 'home',
                    'state' => 'none'
                ),
                'events' => array(
                    'name' => 'Events',
                    'link' => 'events',
                    'state' => 'none'
                ),
                'event' => array(
                    'name' => $this->data['event_info']['event_name'],
                    'link' => 'events/event/' . $event_id,
                    'state' => 'none'
                ),
                'part'  => array(
                    'name' => "instant session",
                    'link' => 'clips/instacam/' . $event_id . "/" . $part_id,
                    'state' => 'active'
                )
            );

            $this->data['head']['javascripts'][] = 'video.controls.js';

            $this->template->load('templates/template_view', 'clips/instacording_view', $this->data);
		}
	}
}
