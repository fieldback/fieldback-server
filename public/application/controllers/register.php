<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
		if(!temporary_password()){
			flash_error('change_password');
			redirect('my_settings');
		}
	}

	public function index()
	{
		
	}

	public function registers(){
	  //$this->ion_auth->register($username, $password, $email, $additional_data, $group)
	    $this->ion_auth->register('tim', 'tim', 'tim@lessormore.nl', array( 'first_name' => 'Tim', 'last_name' => 'Scholten' ), array('1') );
		echo $this->ion_auth->errors();
	}

}

/* End of file register.php */
/* Location: ./application/controllers/register.php */