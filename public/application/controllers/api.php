<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class API
 *
 * The API class is for all external actions that are controlled by external scripts instead of an user
 * There is no login possible, so auth_keys are used
 */
class Api extends MY_Controller {

    public function __construct(){
        parent::__construct();

		// Load system info
		$this->load->model('system_model');
		$this->data['system_info'] = $this->system_model->get_system_info();

		// Load sync library
		$this->load->library('sync_library');

    }

    /**
     * This little helper function checks the JSON file and adds the json file to the uploads directory
     * @param $event_json
     * @return mixed
     */
    private function check_received_json($event_json) {
        // Send uploaded json to library to prepare for import
        $response = $this->sync_library->check_json($event_json);
        // Add the 'files' to the cue so that they can be checked when imported
        $import_code = $this->sync_library->add_files_to_import_cue($response['files']);
        // Add the .json to the 'uploads' directory so we can use it later
        $json_file_location = './videos/uploads/'.$import_code.'.json';
        $fp = fopen($json_file_location, 'w');
        fwrite($fp, json_encode($event_json));
        fclose($fp);
        return $response;
    }

    /**
     * This simple functions checks the database for any pending uploads and starts uploading them
     * this can be triggered by a cron job, or from AJAX from a web page.
     * After it has uploaded a file and received a 200 status code, it flags the file as uploaded so it won't upload again
     */
    public function do_uploads() {
        // Load the sync model
        $this->load->model("sync_model");
        // Get all pending exports
        $exports = $this->sync_model->get_exports();
        // Loop through the exports and upload them to the server
        foreach($exports as $export) {
            // Target url
            $target_url = $export['upload_to'];
            // Full path
            $file_name_with_full_path = realpath($export['original_file_name']);

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $MIME = finfo_file($finfo, $file_name_with_full_path);
            finfo_close($finfo);

            // Create a CURLFile object
            $cfile = curl_file_create(
                $file_name_with_full_path,
                $MIME,
                $export['unique_file_name']
            );

            // Curl options
            $curl_options = array(
                CURLOPT_URL => $target_url,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => [
                    "sync_auth_key"     => $this->data['system_info']['sync_auth_key'], // Send the auth key for authentication
                    "file"              => $cfile
                ],
                CURLOPT_HTTP_VERSION => 1.0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTPHEADER => array("Content-Type:multipart/form-data"),
                CURLOPT_INFILESIZE => filesize($file_name_with_full_path)
            );
            $ch = curl_init();
            curl_setopt_array($ch, $curl_options);
            $result = curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if($code == 200) {
                // Check if the upload was success, we can flag the db
                $result_json = json_decode($result);
                // Check if it was successful by seeing if it can be decoded
                if($result_json != NULL) {
                    // This key should exist; it has worked!
                    if(array_key_exists("last_file", $result_json)) {
                        // /switch the uploaded flag as we successfully uploaded this file
                        $this->sync_library->set_to_uploaded($export['unique_file_name']);
                        // echo result as it should have gone well
                        echo "Success: ". $result;
                    } else {
                        // Something is wrong... should not happen
                        set_status_header(500);
                        die("Something went wrong while uploading. Please contact an administrator.");
                    }
                }
            } else {
                // echo result
                echo "Error: ".$result;
            }
        }
    }

    /**
	 * The 'import' function receives a whole event or .json file in .zip format and checks if this is a correct file
	 * Through $_POST sync_auth_key the authenticity of the import is checked
	 * This can be run by another Fieldback install to upload events
	 */
	public function import_event() {
		// Check if auth_key is correct
		if($this->input->post('sync_auth_key') == $this->data['system_info']['sync_auth_key']) {
			// Check if $_POST upload is an archive, a video file or a .json file
            $event_json = $this->input->post("event_json");
            if($event_json != false) { // JSON data through POST from a slave server?
                // We do not need to json_decode this as codeigniter recognizes it as JSON and puts it in an array
				$response = self::check_received_json($event_json);
				// Respond with status response from library
                set_status_header(200);
				echo json_encode($response);
			} else { // archive or .mp4 uploaded?
                // Use the CodeIgniter upload library
                $config['upload_path'] = './videos/uploads/';
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                // Do the upload, and echo errors on failure
                if( ! $this->upload->do_upload("file")) {
                    $error = $this->upload->display_errors('', '');
                    set_status_header(400);
                    die(json_encode($error));
                } else {
                    // Set the import timestamp to 0 as default
                    $import_timestamp = 0;
                    // Get the data from this upload
                    $upload_data = $this->upload->data();

                    // Debug:
                    // var_dump($upload_data);

                    // Check what kind of upload this is (zip or image/video or the json file)
                    if($upload_data['file_ext'] == ".json") { // The JSON file -> check and respond
                        // Get the JSON file content and send it to the checking scripts
                        $event_json = file_get_contents($upload_data['full_path']);
                        // Because it is read like a string, we need to json_decode this first to be usable in PHP
                        $event_json = json_decode($event_json, true);
                        $response = self::check_received_json($event_json);
                        // Remove uploaded json file as a new one was created
                        unlink($upload_data['full_path']);
                        // Respond with status response from library
                        set_status_header(200);
                        echo json_encode($response);
                    } else {
                        if($upload_data['file_ext'] == ".fb") { // A fb zip file with videos, unpack first!
                            // Unzip this zip and loop through the files
                            $zip = new ZipArchive;
                            $zip_valid = true; // true until proven false
                            if ($zip->open($upload_data['full_path']) === true) {
                                // Loop through this archive
                                for($i = 0; $i < $zip->numFiles; $i++) {
                                    // Get the filename for this file and extract it from the Zip
                                    $filename = $zip->getNameIndex($i);
                                    $zip->extractTo($config['upload_path'], $filename);
                                    // Check if the file is correct and expected by
                                    // getting the import timestamp of this file -> it's the same for each file in the zip
                                    $import_timestamp = $this->sync_library->get_import_timestamp($filename);
                                    // Stop unpacking if import_timestamp = 0 -> SHOULD NOT HAPPEN - indicates tampering with the .zip
                                    if($import_timestamp == 0)
                                    {
                                        $zip_valid = false;
                                        // We don't need to look further
                                        break;
                                    }
                                }
                                // Close the zip
                                $zip->close();
                                // Remove the zip file as we don't need it anymore
                                if($zip_valid) { unlink($upload_data['full_path']); }
                            } else {
                                set_status_header(500);
                                die("Error while unpacking zip file.");
                            }
                        } else { // The file was a .mp4 or .jpg -> we can process it directly
                            // Check if the file is correct by checking it's import_timestamp
                            $import_timestamp = $this->sync_library->get_import_timestamp($upload_data['orig_name']);
                            // Don't remove the file, as this file is needed later!
                        }

                        // A non existing timestamp means that the uploaded file was not expected -> die!
                        // If you get errors -> check the get_import_timestamp functions and do some var_dumps to see if filenames are correct
                        if($import_timestamp == 0) {
                            set_status_header(400);
                            // Remove the uploaded file as it was wrong
                            unlink($upload_data['full_path']);
                            die("The file was not valid or already uploaded. Please upload the fb_event.json file first or choose a different file to upload.");
                        }

                        // Check if all files are uploaded after the .zip, .jpg of .mp4 is uploaded
                        if($this->sync_library->check_if_last($import_timestamp)) {
                            // Get the .json file that was uploaded from the upload directory
                            $file_location = "./videos/uploads/".$import_timestamp.".json";
                            $json = file_get_contents($file_location);
                            // Import the event with the library, again decoding the plain/text json to an array
                            $import_result = $this->sync_library->import_event(json_decode($json, true));
                            // Remove the json file
                            if(!unlink($file_location)) {
                                set_status_header(500);
                                die("The JSON file could not be removed. Please contact an administrator.");
                            }
                            // Return status that this was the last file
                            set_status_header(200);
                            // The receiving script should do something with this info -> send to the new event or something
                            echo json_encode(["last_file" => true, "event_id" => $import_result['new_event_id'], "status" => $import_result['status'], "debug" => $import_result['debug']]);
                        } else {
                            // Do nothing, wait for another file
                            set_status_header(200);
                            echo json_encode(["last_file" => false]);
                        }
                    }
                }
			}
		} else {
			// Unauthorized!
			set_status_header(403);
			die('The sync_auth_key is not correct!');
		}
	}


    /**
     * By requesting the settings this Fieldback installation acts as a 'slave', and will request settings from another
     * server defined in the system settings. Is requested through AJAX, with no login checks (for initial syncs and command line sync)
     */
    public function sync_settings() {
        // Check if this system is set up for syncing (the link should not be available if it's not so a double check)
        if($this->data['system_vars']['sync_active'] == true) {
            // Sync the settings with the library
            $response = $this->sync_library->sync_settings();
            // return output
            echo json_encode($response);
        } else {
            set_status_header(400);
            die("Sync was not active. How did you get here? Contact your administrator!");
        }
    }
	
}


// EOF