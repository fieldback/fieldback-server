<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<link rel="icon" href="<?= base_url('favicon.ico') ?>" type="image/x-icon"/>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
		<meta name="HandheldFriendly" content="true">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="apple-touch-icon" href="<?=base_url('images/ios/icon.png')?>" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('images/ios/icon-72.png')?>" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('images/ios/icon@2x.png')?>" />
		<link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('images/ios/icon-72@2x.png')?>" />
		<!-- Startup images -->
		<!-- iOS 6 & 7 iPad (retina, portrait) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-1536x2008.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: portrait)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPad (retina, landscape) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-1496x2048.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: landscape)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPad (portrait) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-768x1004.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: portrait)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPad (landscape) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-748x1024.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: landscape)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPhone 5 -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-640x1096.png')?>"
					media="(device-width: 320px) and (device-height: 568px)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPhone (retina) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-640x920.png')?>"
					media="(device-width: 320px) and (device-height: 480px)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPhone -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-320x460.png')?>"
					media="(device-width: 320px) and (device-height: 480px)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">
		
		<!-- Stylesheets & Scripts -->
		<link rel="stylesheet" type="text/css" href="<?=base_url('css/')?>/vendor/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=base_url('css/')?>/main.css" media="screen" />
		<script type="text/javascript" src="<?=base_url('js/')?>/vendor/jquery-1.9.1.js"></script>
		
		<? // Stylesheet overrides, only apply here on this public page ?>
		<style>
			html, body {
				position: relative;
				min-height: 100%;
				height: 100%;
				/* Prevent text size adjustment on orientation change. */
				-webkit-text-size-adjust: 100%;
			}
			/* Keep the first container after the top far way from the fixed navbar */
			body > .container {
				padding-top: 80px;
			}
			/* Padding settings for main container */
			.container {
			    padding-left: 0px;
			    padding-right: 0px
			}
			
			.navbar > .container {
				padding-left: 15px;
				padding-right: 15px;
			}

		</style>
					

		<!-- Facebook Open Graph parameters -->
		<meta property="og:url" content="<?=base_url('shared/token')."/".$share['share_token']?>" /> 
		<meta property="og:title" content="Fieldback - instant performance analysis" />
		<meta property="og:site_name" content="Fieldback" />
		
		<meta property="og:description" content="A moment from the event: <?=$moment_info['event_name']?>" /> 
		<meta property="og:image" content="<?=base_url('images/FieldbackLogoFacebook.png')?>" />

	</head>
	<body id="body">
		<!-- Facebook API - NEEDS TO BE DIRECTLY AFTER THE BODY TAG -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.3&appId=274762806037176";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="menu">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" target="_self" href="<?=base_url()?>">Fieldback</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a target="_self" href="http://www.fieldback.net/">Info</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-9">
					<h5><?=$moment_info['event_name']?> : <?= $moment_info['team_name']?> - Watch moment #<?=$moment_info['moment_id']?></h5>
				</div>
                <div class="col-xs-12 col-sm-3">
                    <p>
                    <div class="fb-share-button" data-href="<?=base_url("shared/token/".$share_token)?>" data-layout="button"></div>
                    </p>
                </div>
				<div id="video-container" class="col-xs-12 video_container">
					<? $amount_files = count($moment_files); // How many videos does this moment have? ?>
					<? foreach ($moment_files as $moment_file) {?>
					<?
						$event_id = $moment_file['event_id'];
						$part_id = $moment_file['part_id'];
						$moment_file_id = $moment_file['moment_file_id'];
						$path = site_url() . $moment_file['path'];
						
						$image = $path . '/' . $moment_file_id . '.jpg';
						$moment_file = $path . '/' . $moment_file['filename'] . '?' . rand(0,1000);
						$vid_width = 100 / $amount_files - 2;
					?>
						<video class="video-js vjs-default-skin edit-moment-video"
							controls preload="auto" width="<?=$vid_width?>%" height="400"
							poster="<?=$image?>">
							<source src="<?=$moment_file?>" type='video/mp4' />
							<p class="vjs-no-js">To view this video please enable JavaScript</a></p>
						</video>
					<?}?>
	   			</div>
  			</div>
  			
  			<!-- Playback speed -->
  			<div class="row">
  				<div class="col-xs-12 col-sm-3">
                    <h4 class="pull-right">Playback speed</h4>
                </div>
  				<div class="col-xs-12 col-sm-6">
  					<input type="range" name="slowmo" id="slowmo" value="10" min="1" max="10" style="height: 50px">
  				</div>
			</div>

			<!-- Comments, Tags and Extra info -->
			<div class="row"> 
	  			
				<!-- Moment Info -->
				<div id="moment_info" class="col-xs-12 col-sm-2">
					<h4>Date created:</h4>
					<p><?=date('d-m-Y - H:i', $moment_info['date_created'])?></p>
				</div>

				<!-- Comments -->
				<div id="action">
					<div id="moment_settings" class="col-xs-12 col-sm-2">
						<h4>Comment</h4>
						<div class="col-md-12 comment-container">
							<div class="row">
								<p>
									<?php if(!empty($moment_info['comment'])){ 
										echo $moment_info['comment']; 
									}else{
										echo "No comment";}
									?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<!-- Moment tags -->
				<div id="moment_tags" class="col-sm-7">
					<div class="col-sm-6">
						<div class="row">
							<h4>Tags</h4>
						</div>
						<div class="row row-tags">
							<?php if (!empty($moment_info['tags'])): ?>
								<? foreach ($moment_info['tags'] as $tag) {?>
									<label class="label label-warning label-tag"><?=$tag['tag_name']?></label>
								<?}?>
							<?php endif ?>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row">
							<h4><?=lang('term_players')?></h4>
						</div>
						<div class="row row-tags">
							<?php if (!empty($moment_info['players'])): ?>
								<? foreach ($moment_info['players'] as $player) {?>
									<label class="label label-info label-tag"><?=$player['first_name']?> <?=$player['last_name']?></label>
								<?}?>
							<?php endif ?>
						</div>
					</div>
				</div>

            </div>

		</div>


<script>
$(function() {
	/*
		Slow motion control through the input slider
		*/
	$('#slowmo').on('change', function(){
		var value = $(this).val() / 10;
		// Get each video on this page
		videos.each(function(){
		var id = $(this).attr('id');
		var video = document.getElementById(id);
		// Change playbackrate
		video.playbackRate = value;
		});
	});	
	
});
</script>

	</body>
</html>