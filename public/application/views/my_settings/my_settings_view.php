<?php // This page loads inside the template_view ?>

<div class="container">

	<!-- Show warning when temporary password is used -->
	<? if(!temporary_password()) { ?>
		<div class="row">&nbsp;</div>
		<div class="col-md-12">
			<div class="alert alert-danger alert-dismissible" role="alert">
				<h4><strong>Warning!</strong></h4>
				<p>Please change your temporary password.</p>
				<p>You are not able to use the system with the temporary password.</p>
			</div>
		</div>	
	<? } ?>
	
	<!-- Column change user info -->
	
	<div class="col-md-4">
		
		<?php if (temporary_password()): ?>
			<div class="panel panel-success">
				<div class="panel-heading">User information</div>
				<div class="panel-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-4">
								<a href="#" class="thumbnail"><img src="<?=base_url('/images/200x200.gif')?>" /></a>
							</div>
							<div class="col-md-8">
								<h4><?=$user_info['first_name']?> <?=$user_info['last_name']?></h4>
								
								<p><?=$user_info['email']?></p>
							</div>
						</div>
						<div class="alert alert-warning" role="warning">In the near future you will be able to change this information</div>
					</div>
				</div>
			</div>
		<?php endif ?>

	</div>

	<!-- Column change password -->
	<div class="col-md-4">
		
		<div class="panel <? if(temporary_password()){ echo 'panel-success';}else{ echo 'panel-warning'; }?>">
			<div class="panel-heading">Change password</div>
			<div class="panel-container">
				<form role="form" action="<?= site_url('my_settings') ?>" method="POST">
					<?= form_error('passwords[old]'); ?>
					<div class="form-group">
						<input type="password" class="form-control" value="" name="passwords[old]" placeholder="Old password">
					</div>
					<?= form_error('passwords[new]'); ?>
					<div class="form-group">
						<input type="password" class="form-control" value="" name="passwords[new]" placeholder="New password">
					</div>
					<?= form_error('passwords[new2]'); ?>
					<div class="form-group">
						<input type="password" class="form-control" value="" name="passwords[new2]" placeholder="Confirm new password">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</form>
			</div>
		</div>

	</div>
</div>