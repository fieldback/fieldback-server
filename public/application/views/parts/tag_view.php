<?php // This page loads inside the template_view ?>
<? 
	/*PHP VARIABLES*/
	$event_id = $event_info['event_id'];
	$part_id = $part_info['part_id'];
	$part_url = site_url('videos/' . $event_id . '/' . $part_id) . '/';
	$clip_count = count($clips);
?>
<div class="container-fluid no-padding">
	<div id="video_container" class="col-md-12 no-padding">
		<? $class = 12 / $clip_count ?>
		
		<? if($clip_count === 1){ ?>
			<? foreach ($clips as $key => $clip) {?>
				<div class="col-md-3">&nbsp;</div>
				<div class="col-md-6">
					<video class="tag-video" style="width: 100%;" controls>
						<source src="<?=$part_url . $clip['clip_id'] . '.mp4'; ?>" type='video/mp4' />
					</video>
				</div>
				<div class="col-md-3"></div>
			<?}?>
		<? }else{ ?>

			<!-- Load clips -->
			<? foreach ($clips as $key => $clip) {?>
				<div class="col-md-<?=$class?>">
					<video class="tag-video" style="width: 100%;" controls>
						<source src="<?=$part_url . $clip['clip_id'] . '.mp4'; ?>" type='video/mp4' />
					</video>
				</div>
			<?}?>
		<?}?>


	</div>
</div>
<div class="container-fluid">
	<div class="tagfields" style="height: 240px"></div>
</div>
<div class="container-fluid no-padding">
	<div class="moments">
	</div>
</div>

<script type="text/javascript">
	/*VARS*/
	var event_id = '<?=$event_id?>';
	var part_id = '<?=$part_id?>';
	var part_url = '<?=$part_url?>';
	var clip_count = '<?=$clip_count?>';
	var i = 0;
	
	$(document).ready(function(){
		getTagfields();
		getMoments();
		checkEncodeProgress();
		synchronize('.tag-video');
		setVideoHeight();
	});

	function setVideoHeight(){
		height = $(window).height() - 330;
		$('.tag-video').css({
			'height' : height + 'px'
		});
	}
	
	

	/*MOMENTS*/
	//get moments for this part
	function getMoments() {
		$.ajax({
			type: 'POST',
			url: '<?=site_url('moments/get_moments_for_live_tagging')?>',
			dataType: 'html',
			data: {event_id: event_id, part_id: part_id},
			success: function(data){
				$('div.moments').html(data);
			}
		})
	}
	

	//TAGFIELDS 
	function getTagfields()	{
		$.ajax({
			url: '<?=site_url('tagfields/tagfields_view')?>',
			type: 'POST',
			dataType: 'html',
			data: {event_id: event_id},
			success: function(data){
				$('.tagfields').html(data);
			}
		});
	}

	//Encode progress
	function checkEncodeProgress() {
		$.ajax({
	 		type: 'POST',
	 		url: '<?=base_url('/clips/get_encode_progress_of_upload/')?>',
	 		data: {event_id: event_id, part_id: part_id},
	 		dataType: 'json',
	 		success: function(data){
	 			console.log(data['progress']);
	 		}
	 	});
	}

	/*
	// Event Listeners
	*/

	// tag button
	$('button#tag').on('click', function() {
		$('div#tag_list').fadeIn('fast');	
	});

	// rewind button
	$('.rewind').on('click', function() {
		var sec = $(this).data('sec');
		rewind(sec);
	});


	
</script>