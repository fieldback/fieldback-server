<ul id="settings" class="navigation">
	<li>
		<a href="<?=base_url('settings/system_settings')?>">System settings</a>
	</li>
	<li>
		<a href="<?=base_url('settings/users')?>">Users</a>
	</li>
	<li>
		<a href="<?=base_url('settings/teams')?>"><?=lang('term_teams')?></a>
	</li>
	<li>
		<a href="<?=base_url('settings/tagfields')?>">Tagfields</a>
	</li>
	<li>
		<a href="<?=base_url('logout')?>">Logout</a>
	</li>
</ul>