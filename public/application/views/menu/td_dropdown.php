<ul id="settings" class="navigation">
	<li>
		<a href="<?=base_url('settings/account')?>">My Account</a>
	</li>
	<li>
		<a href="<?=base_url('settings/tagfields')?>">My Tags</a>
	</li>
	<li>
		<a href="<?=base_url('settings/organisations')?>">Organisations</a>
	</li>	
	<li>
		<a href="<?=base_url('settings/teams')?>">My <?=lang('term_teams_l')?></a>
	</li>
	<li>
		<a href="<?=base_url('settings/users')?>">My Users</a>
	</li>
	<li>
		<a href="<?=base_url('settings/tag')?>">My Tags</a>
	</li>
</ul>