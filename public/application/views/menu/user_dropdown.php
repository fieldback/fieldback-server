<ul id="settings" class="navigation">
	<li>
		<a href="<?=base_url('settings/account')?>">My Account</a>
	</li>
	<li>
		<a href="<?=base_url('settings/tag')?>">My moments</a>
	</li>
	<?php if (has_teams()): ?>
		<li>
			<a href="<?=base_url('settings/tagfields')?>">Tagfields</a>
		</li>
	<?php endif ?>
	<li>
		<a href="<?=base_url('logout');?>">Logout</a>
	</li>
</ul>