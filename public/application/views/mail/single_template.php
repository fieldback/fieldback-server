<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
</html>
<body style="margin: 0; padding: 0;">
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;">
                            <img src="<?=site_url('images/logos/fieldback_dev.png')?>" alt="Creating Email Magic" width="300" height="230" style="display: block;" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                             <table border="1" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                Row 1
                               </td>
                              </tr>
                              <tr>
                               <td>
                                Row 2
                               </td>
                              </tr>
                              <tr>
                               <td>
                                Row 3
                               </td>
                              </tr>
                             </table>
                            </td>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            3
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>