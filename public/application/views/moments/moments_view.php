<?php // This page loads inside the template_view ?>
<div class="container" id="js_playlist_container"></div>
<div class="container" id="js_page_container">
	<div class="row">
		<div class="col-sm-12 col-md-3 col-lg-2" id="filter_column">
			<div class="panel panel-info">
				<div class="panel-heading">
                    Add filter
                    <div class="btn-group btn-group-xs pull-right">
                        <button id="hide_filter_column" class="btn btn-default"><span class="glyphicon glyphicon-chevron-up"></span></button>
                    </div>
                </div>
				<div class="panel-container">
					<!-- Select event -->
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
							<select id="event" class="simple-select" data-width="100%" data-live-search="true" multiple title="Select event">
								<?foreach ($events as $event) {?>
									<option data-selector="event" value="<?=$event['event_id']?>"<?php if(isset($_GET['event']) && $_GET['event'] == $event['event_id']) echo " selected";?>><?=$event['event_name']?></option>
								<?}?>
							</select>
						</div>
                        <div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
                            <select id="team" class="simple-select" data-width="100%" data-live-search="true" multiple title="Select <?=lang('term_team_l')?>">
								<?foreach ($teams as $team) {?>
									<option data-selector="team" value="<?=$team['team_id']?>"<?php if(isset($_GET['team']) && $_GET['team'] == $team['team_id']) echo " selected";?>><?=$team['team_name']?></option>
								<?}?>
							</select>
						</div>
					</div>
					<!-- Select date range -->
					<div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
							<div class="input-group date">
								<input class="form-control" data-selector="from" type="text" id="from" name="from" placeholder="Date From">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
							<div class="input-group date">
								<input class="form-control" data-selector="to" type="text" id="to" name="to" placeholder="Date To">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>
					</div>
					<!-- Select tags -->
					<div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
							<select name="tags" id="tags-select" data-width="100%" data-live-search="true" multiple title="Select tags">
								<?php foreach ($tags as $tag): ?>
									<option data-selector="tag" value="<?=$tag['tag_id']?>"><?=$tag['tag_name']?></option>
								<?php endforeach ?>
							</select>
						</div>
                        <div class="col-xs-6 col-sm-6 col-md-12 bottom-buffer">
							<select name="tags" id="players-select" data-width="100%" data-live-search="true" multiple title="Select <?=lang('term_players_l')?>">
								<?php foreach ($players as $player): ?>
									<option data-selector="player" value="<?=$player['id']?>"<?php if(isset($_GET['player']) && $_GET['player'] == $player['id']) echo " selected";?>><?=$player['first_name']?> <?=$player['last_name']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
                    <!-- Select playlists tags -->
                    <div class="row">
                        <div class="col-xs-12">
                            <select name="playlists" id="playlist-select" data-width="100%" data-live-search="true" multiple title="Select playlists">
                                <?php foreach ($playlists as $playlist): ?>
                                    <option data-selector="playlist" value="<?=$playlist['playlist_id']?>"<?php if(isset($_GET['playlist']) && $_GET['playlist'] == $playlist['playlist_id']) echo " selected";?>><?=$playlist['title']?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-9 col-lg-10" id="result_column">
			<div class="row">
				<div class="panel panel-success">
					<div class="panel-heading">
                        <div class="btn-group btn-group-xs">
                            <button id="show_filter_column" class="btn btn-default"><span class="glyphicon glyphicon-filter"></span></button>
                        </div>
						Moments
                        <div class="pull-right">
<!-- NEEDS TO BE IMPLEMENTED FIRST
                            <div class="btn-group btn-group-xs">
                                <button id="btn_save_as_playlist" class="btn btn-default"><span class="glyphicon glyphicon-bookmark"></span> save as playlist</button>
                            </div>
-->

                            <div class="btn-group btn-group-xs">
                                <button id="refresh_moments" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span></button>
                            </div>
                            <div class="btn-group btn-group-xs" data-toggle="buttons" id="view_select">
                                <label class="btn btn-default">
                                    <input type="radio" name="js_views" value="play_all" autocomplete="off" class="btn btn-default"><span class="glyphicon glyphicon-play"></span> play mode
                                </label>
                                <!--
                                <label class="btn btn-default">
                                    <input type="radio" name="js_views" value="list_large" autocomplete="off" class="btn btn-default"><span class="glyphicon glyphicon-th-large"></span> grid
                                </label>
                                -->
                                <label class="btn btn-default">
                                    <input type="radio" name="js_views" value="list_default" autocomplete="off" class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span> list
                                </label>
                            </div>
                        </div>

					</div>
					<div class="pillbox">
						<!-- placeholder for the pills! -->
					</div>
                    <div id="play_index" class="panel-body">
                        <div class="pull-left">
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" id="btn_play_first"><span class="glyphicon glyphicon-fast-backward"></span> </button>
                            </div>
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" id="btn_play_back"><span class="glyphicon glyphicon-backward"></span> </button>
                            </div>

                        </div>
                        <div class="pull-right">
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" id="btn_play_forward"><span class="glyphicon glyphicon-forward"></span> </button>
                            </div>
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" id="btn_play_last"><span class="glyphicon glyphicon-fast-forward"></span> </button>
                            </div>
                        </div>
                        <div class="text-center">
                            <span id="js_current_play_index"></span> / <span id="js_total_play_index"></span>
                        </div>
                    </div>
					<div class="list-group" id="moment_results">
                        <p>Loading....</p>
                    </div>
                    <table class="table" id="moment_results_table">
                        <thead class="bg-empha">
                            <th>
                                Date:
                            </th>
                            <th>
                                At time:
                            </th>
                            <th>
                                Event:
                            </th>
                            <th>
                                Tags:
                            </th>
                        </thead>
                        <tbody id="moment_results_table_body">

                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {

        // PHP variables
        var initial_view = "<?php
        if(isset($_GET['initial_view'])) {
            switch($_GET['initial_view']) {
                case 'play_all':
                echo "play_all";
            break;
                case 'list_large':
                echo "list_large";
            break;
                case 'list_default':
                echo "list_default";
            break;
            }
        } else {
            echo "list_default";
        } ?>";

        // If $_GET filter_colum is set to hide, trigger a click on the hide button (this was easiest to implement)
        var filter_column = <?php
        if(isset($_GET['filter_column'])) {
            if($_GET['filter_column'] == 'hide') {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            echo "true";
        }
        ?>;

        $.fn.bootstrapDP = $.fn.datepicker.noConflict(); // return $.fn.datepicker to previously assigned value

        // Stupid thing does not work well on mobile
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            $('select').selectpicker('mobile');
        } else {
            $('select').selectpicker();
        }
        $('.date').bootstrapDP();

        var $result_list = $("#moment_results");
        var $moment_results_table = $("#moment_results_table");
        var $moment_results_table_body = $("#moment_results_table_body");
        var $play_index = $("#play_index");
        var $current_play_index = $("#js_current_play_index");
        var $total_play_index = $("#js_total_play_index");
        var $btn_play_back = $("#btn_play_back");
        var $btn_play_forward = $("#btn_play_forward");
        var $btn_play_first = $("#btn_play_first");
        var $btn_play_last = $("#btn_play_last");
        var moment_data;
        var moment_index = 0;
        // var $btn_save_as_playlist = $("#btn_save_as_playlist");
        var $view_select = $("#view_select");

        var $refresh_moments = $("#refresh_moments");

        var $filter_column = $("#filter_column");
        var $result_column = $("#result_column");
        var $hide_filter_column = $("#hide_filter_column");
        var $show_filter_column = $("#show_filter_column");

        $show_filter_column.hide();

        // Two click handlers for toggling the bootstrap col- definition classes on the result-column. As the filter is hidden, the rest should fill up the gap
        $hide_filter_column.on("click", function () {
            $filter_column.hide();
            $result_column.removeClass("col-md-9 col-lg-10");
            $show_filter_column.show();
        });
        $show_filter_column.on("click", function () {
            $filter_column.show();
            $result_column.addClass("col-md-9 col-lg-10");
            $show_filter_column.hide();
        });

        // This was set earlier with PHP -> check and trigger a click if false
        if (filter_column == false) $hide_filter_column.trigger('click');

        // Set initial view state based on $_GET
        $view_select.find("[value=" + initial_view + "]").prop("checked", true).parent().addClass("active");
        $view_select.trigger("change");

        // Hide the two play-mode views
        $play_index.hide();
        $moment_results_table.hide();

        search();

        $("#event, #team, #from, #to, #tags-select, #players-select, #playlist-select").on('change', function () {
            search();
        });

        $("#event, #team, #tags-select, #players-select, #playlist-select").on('change', function () {
            var elements = $(this).children(':selected');
            var selector = $(this).children().data('selector');
            $('.pillbox li.' + selector).remove();

            elements.each(function (index) {
                var element = $(this);
                $('.pillbox').append('<li class="btn btn-default ' + selector + '"> ' + element.text() + '</li>');
            });
        });

        function search() {
            $result_list.html('Searching...');
            var events = $('#event').val();
            var teams = $('#team').val();
            var date_from = $('#from').val();
            var date_to = $('#to').val();

            var selected_tags = $('#tags-select').val();
            var selected_players = $('#players-select').val();
            var selected_playlists = $('#playlist-select').val();

            $.ajax({
                url: '<?=base_url('moments/search')?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    events: events,
                    teams: teams,
                    date_from: date_from,
                    date_to: date_to,
                    selected_tags: selected_tags,
                    selected_players: selected_players,
                    selected_playlists: selected_playlists
                }
            }).done(function (response) {
                moment_data = response;
                // Get total count of moments
                $total_play_index.html(moment_data.length);
                moment_index = 0;
                var moment_results_table_html = $.templates({
                    markup: "#tmpl_moment-item-presentation-table",
                    allowCode: true
                }).render(moment_data);
                $moment_results_table_body.html(moment_results_table_html);
                $view_select.trigger("change");
            });

        }

        $view_select.on("change", function () {
            var template_selected = $view_select.find(":checked").val();
            var moment_html;
            if (moment_data.length > 0) {
                switch (template_selected) {
                    case 'list_large':
                        $play_index.hide();
                        $moment_results_table.hide();
                        moment_html = $.templates({
                            markup: "#tmpl_moment-item-large",
                            allowCode: true
                        }).render(moment_data);
                        $result_list.html(moment_html);
                        watch_images($result_list);
                        break;
                    case 'list_default':
                        $play_index.hide();
                        $moment_results_table.hide();
                        moment_html = $.templates({markup: "#tmpl_moment-item", allowCode: true}).render(moment_data);
                        $result_list.html(moment_html);
                        watch_images($result_list);
                        break;
                    case 'play_all':
                        $play_index.show();
                        $moment_results_table.show();
                        render_moment("#tmpl_moment-item-within-list", moment_data[moment_index]["moment_id"], $result_list, site_url);
                        break;
                }
                updatePlayIndex();
            } else {
                $play_index.hide();
                $moment_results_table.hide();
                $result_list.html("<p>There are no moments for this search!</p>");
            }
        });

        $refresh_moments.on("click", function () {
            search();
        });

        $btn_play_first.on("click", function () {
            moment_index = 0;
            $view_select.trigger("change");
        });

        $btn_play_last.on("click", function () {
            moment_index = moment_data.length - 1;
            $view_select.trigger("change");
        });

        $btn_play_back.on("click", function () {
            if (moment_index > 0) moment_index--;
            $view_select.trigger("change");
        });

        $btn_play_forward.on("click", function () {
            if (moment_index < moment_data.length - 1) moment_index++;
            $view_select.trigger("change");
        });

        $moment_results_table_body.on("click", "tr", function () {
            var $this = $(this);
            moment_index = $this.data("index");
            $view_select.trigger("change");
        });

        video_item_helper("#moment_results", ".js_moment", "<?=site_url();?>", $("#tmpl_moment-item-within-list").html());
        video_button_helper("#moment_results");

        function updatePlayIndex() {
            console.log("updating play index");
            // Highlight the current indexed moment
            var $curActive = $moment_results_table_body.find("tr.active");
            var $newActive = $moment_results_table_body.find("tr[data-index=" + moment_index + "]");
            $curActive.removeClass("active");
            $newActive.addClass("active");

            // Update the play index indicator at the top
            $current_play_index.html(moment_index + 1);
            // Disable or enable the buttons if we are at the first or last index
            if (moment_index <= 0) {
                $btn_play_back.prop('disabled', true);
                $btn_play_first.prop('disabled', true);
            } else {
                $btn_play_back.prop('disabled', false);
                $btn_play_first.prop('disabled', false);
            }
            if (moment_index >= moment_data.length - 1) {
                $btn_play_forward.prop('disabled', true);
                $btn_play_last.prop('disabled', true);
            } else {
                $btn_play_forward.prop('disabled', false);
                $btn_play_last.prop('disabled', false);
            }
        }

        // ******************************************************************************** //

        var playFirst = 122;
        var playPrev = 120;
        var playNext = 99;
        var playLast = 118;

        var refresh = 112;

        // Catch keypresses on the page for refresh and first-last-next-prev buttons
        $(document).on("keypress", function (e) {
            if(keyControlActive) {
                console.log("Key pressed: " + e.keyCode);
                switch (e.keyCode) {
                    case playFirst:
                        $btn_play_first.trigger("click");
                        break;
                    case playLast:
                        $btn_play_last.trigger("click");
                        break;
                    case playPrev:
                        $btn_play_back.trigger("click");
                        break;
                    case playNext:
                        $btn_play_forward.trigger("click");
                        break;
                    case refresh:
                        search();
                        break;
                }
            }
        });

    });
</script>

<script type="text/html" id="tmpl_moment-item">
    <?php require("js/templates/moments/tmpl_moment-item.php"); ?>
</script>

<script type="text/html" id="tmpl_moment-item-large">
    <?php require("js/templates/moments/tmpl_moment-item-large.php"); ?>
</script>

<script type="text/html" id="tmpl_moment-item-presentation-table">
    <?php require("js/templates/moments/tmpl_moment-item-presentation-table.php"); ?>
</script>
<script type="text/html" id="tmpl_moment-item-within-list">
    <?php require("js/templates/moments/tmpl_moment-item-within-list.php"); ?>
</script>