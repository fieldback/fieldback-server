<div class="amount thumbnail text-center"><?=count($moments)?> results</div>
<table class="table table-hover">
				<tbody data-link="row" class="rowlink">
					<?php foreach ($moments as $moment): ?>
						<tr class="moment col-md-6" data-moment-id="<?=$moment['moment_id']?>">
							<td class="col-md-5 hidden-xs">
								<?php if (!empty($moment['moment_files'])): ?>
								<?php $amount = count($moment['moment_files']); ?>
								<?php $class = 12 / $amount;?>
								<?foreach($moment['moment_files'] as $moment_file){?>
									<div class="image thumbnail col-md-<?=$class?>">
										<img src="<?=site_url() . $moment_file['path'] . '/' . $moment_file['moment_file_id'] . '.jpg';?>" class="img-polaroid">
									</div>
								<?}?>
								<?php else: ?>
									<div class="image"></div>
								<?php endif ?>
							</td>
							<td>
								<div class="description">
									<!-- team name -->
									<p><span class="glyphicon glyphicon-asterisk"></span> <?=$moment['team_name']?></p>
									<!-- event name -->
									<p><span class="glyphicon glyphicon-flag"></span> <?=$moment['event_name']?></p>
									<!-- date created -->
									<p><span class="glyphicon glyphicon-calendar"></span> <?=date('Y-m-d H:i', $moment['date_created']);?></p>
									
								</div>
							</td>
							<td class="col-md-3">
								<!-- tags -->
								<ul class="list-unstyled">
			 						<? 
										$i = 0; 
										// maximum amount of tags in homeview
										$max_list = 2;
									?>
				 					<?foreach ($moment['tags'] as $tag) {?>
										<? if($i < $max_list) { ?>
											<li><span class="label label-warning"><?= character_limiter($tag['tag_name'], 12)?></span></li>
										<? } elseif($i == $max_list) { ?>
											<li><span class="label label-warning">...</span></li>
										<? }else{ 
											break;
										}
										$i++;
										?>

									<?}?>
									<? $i = 0; ?>
									<?foreach ($moment['players'] as $player) {?>
										<? if($i < $max_list) { ?>
											<li><span class="label label-info"><?= $player['first_name']?> <?= character_limiter($player['last_name'], 12)?></span></li>
										<? } elseif($i == $max_list) { ?>
											<li><span class="label label-info">...</span></li>
										<? }else{ 
											break;
										}
										$i++;
										?>
									<?}?>
								</ul>

							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
<script type="text/javascript">

	$('tr.moment').on('click', function(){
		var moment_id = $(this).data('moment-id');
		show_moment(moment_id);
	});

</script>