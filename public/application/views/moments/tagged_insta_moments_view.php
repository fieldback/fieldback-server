<?php
foreach ($moments as $moment) {
    // Get variables
    $moment_id = $moment['moment_id'];
?>

    <div class="list-group-item">
        <div class="row js_video_row_item" data-moment-id="<?=$moment_id?>">
<?php
        // Calculate the size of each moment file in relation to the grid
        $amount_of_moment_files = count($moment['moment_files']);
        switch($amount_of_moment_files) {
            case 1:
            default:
                $col = 4;
                break;
            case 2:
                $col = 3;
                break;
            case 3:
                $col = 2;
                break;
            case 4:
                $col = 2;
                break;
            case 5:
                // TODO: do something about moments with more than 4 files
                $col = 1;
                break;
        }
        // How much cols do we have left?
        $remain_cols = 12 - ($amount_of_moment_files * $col);

        foreach ($moment['moment_files'] as $moment_file) {
            $moment_file_id = $moment_file['moment_file_id'];
            // Set default thumb src when no image is present (yet)
            $tmp_thumb_src = site_url('./images/icons/loader.gif');
            $thumb_rel_src = "./videos/$event_id/$part_id/$moment_id/$moment_file_id.jpg";
            $thumb_src = site_url($thumb_rel_src);
            // Check if file already exists - change src of image
            if (file_exists($thumb_rel_src)) {
?>
                <div class="col-xs-<?=$col?>">
                    <img class="img-thumbnail " src="<?=$thumb_src?>">
                </div>
<?php
            } else {
?>
                <div class="col-xs-<?=$col?>">
                    <img id="moment_file_thumb_<?=$moment_file_id?>" class="img-thumbnail " src="<?=$tmp_thumb_src?>" data-new-src="<?=$thumb_src?>">
                </div>

<?php
            }
        }
?>

            <div class="col-xs-<?=$remain_cols?>">
                <h4 class="media-heading">
                    <?=seconds_to_time($moment['time_in_clip'])?>
                </h4>
                <ul class="list-inline">
                    <?php foreach ($moment['tags'] as $tag): ?>
                        <li><h4><span class="label label-warning"><?= $tag['tag_name'] ?></span></h4></li>
                    <?php endforeach ?>
                </ul>
                <ul class="list-inline">
                    <?php foreach ($moment['players'] as $player): ?>
                        <li><h4><span class="label label-info"><?= $player['first_name']?></span></h4></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>

<?php
}
?>