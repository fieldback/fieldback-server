<div class="moments-footer hidden-xs hidden-sm">
	<div class="tagged-moment-container">
		<?php foreach ($moments as $moment): ?>
			<?
				$moment_id = $moment['moment_id'];
				$thumb_src = site_url('images/icons/loader.gif');
			?>
			<div class="tagged-moment pull-left col-md-12" data-moment-id="<?=$moment_id?>" data-content="popover" rel="popover" data-placement="top" data-original-title="Title" data-trigger="hover">
				<div class="container-fluid text-center">
					<div>
						<p class="time_in_clip"><span class="label label-default"># <?=$moment['moment_id']?></span>&nbsp; &nbsp; &nbsp; &nbsp;<small><?=seconds_to_time($moment['time_in_clip']);?></small></p>
					</div>
				</div>
				<!-- If there are no moments files, show loader -->
				<?php if (empty($moment['moment_files'])): ?>
					<div class="col-md-4"></div>
					<img class="loader thumbnail col-md-4" src="<?=site_url('images/icons/loader.gif')?>" />
				<!-- Show the moment files (with their images) -->
				<?php else: ?>
					<? $class = 12 / count($moment['moment_files']); ?>
					<?php foreach ($moment['moment_files'] as $moment_file): ?>
						<!-- Variables -->
						<?
							$moment_file_id = $moment_file['moment_file_id'];
						?>
						<!-- Only show the image if it exists -->
						<?php if (file_exists("videos/$event_id/$part_id/$moment_id/$moment_file_id.jpg")): ?>
							<? $thumb_src = site_url("videos/$event_id/$part_id/$moment_id/$moment_file_id.jpg"); ?>
						<?php endif ?>
						<!-- Set the image -->
						<img class="moment_thumb thumbnail col-md-<?=$class?>" src="<?=$thumb_src?>" alt="thumbnail" />
					<?php endforeach ?>
				<?php endif ?>
				<p class="tag_nr">
				<?php foreach ($moment['tags'] as $tag): ?>
					<span class="label label-warning"><?= character_limiter($tag['tag_name'], 23) ?></span>
				<?php endforeach ?>
				<?php foreach ($moment['players'] as $player): ?>
					<span class="label label-info"><?= $player['first_name']?></span>
				<?php endforeach ?>
				</p>
			</div>

		<?php endforeach ?>
	</div>
</div>

<script type="text/javascript">
		var amount_moments = $('.tagged-moment').length;
		var width = 200 * amount_moments;

		// window functions
		$(window).resize(function(){
			setHover();
		});

		// initial functions
		setHover();

		// set the width of the moments container
		$('.tagged-moment-container').width(width);

		/***
		** Set hover
		** The hover needs to collapse on a certain height. 
		*/
		function setHover(){
			// Get all the current heights
			var videoHeight = $('.tag-video').height();
			var menuHeight = $('.navbar').height();
			var breadcrumbHeight = $('.breadcrumb').height();
			var containerTagsHeight = $('.container.tags').height();
			var elementsHeight = videoHeight + menuHeight + breadcrumbHeight + containerTagsHeight;
			
			var windowHeight = $(window).height();

			// Do the math
			if(windowHeight - elementsHeight < 250){
				// footer collapses
				$('.moments-footer').css({
					'height' : '40px'
				});
				// on hover it shows up
				$('.moments-footer').hover(

					function(){
						$(this).stop().animate({
							'height' : '200px'
						});
					},
					function(){
						$(this).stop().animate({
							'height' : '40px'
						});
					}
				);
			}else{
				// footer shows up!
				$('.moments-footer').unbind('mouseenter mouseleave').css({
					'height' : '200px'
				});
			}
		}

		$('.moments-footer').scrollLeft($('.tagged-moment-container').width());
</script>