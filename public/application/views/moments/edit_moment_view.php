<?
	$amount_players = count($files);
?>
<div id="edit-moment-modal" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-wide">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>
					<label class="label label-warning"><?=date('d-m-Y - H:i', $moment_info['date_created'])?></label>
					<?=$moment_info['event_name']?> : <?= $moment_info['team_name']?> - Watch moment #<?=$moment_info['moment_id']?>
				</h4>
		 	</div>
			<div class="modal-body">
				<div class="container-fluid">
						<div id="video-container" class="col-md-12 video_container">
							<? $i = 0; ?>
							<? foreach ($files as $file) {?>
							<?
								$event_id = $file['event_id'];
								$part_id = $file['part_id'];
								$moment_file_id = $file['moment_file_id'];
								$path = site_url() . $file['path'];
								$image = $path . '/' . $moment_file_id . '.jpg';
								$file = $path . '/' . $file['filename'] . '?' . rand(0,1000);
								$vid_width = 100 / $amount_players - 2;
							?>
								<video id="moment<?=$i?>" class="video-js vjs-default-skin edit-moment-video"
									controls preload="auto" width="<?=$vid_width?>%" height="320"
									poster="<?=$image?>">
									<source src="<?=$file?>" type='video/mp4' />
									<p class="vjs-no-js">To view this video please enable JavaScript</a></p>
								</video>
							<? $i++; ?>
							<?}?>
			   			</div>

		  			<!-- Playback speed -->
		  			<div class="row-fluid hidden-xs">
		  				<div class="col-md-3"><h4 class="pull-right">Playback speed</h4></div>
		  				<div class="col-md-6">
		  					<input type="range" name="slowmo" id="slowmo" value="100" min="1" max="100" style="height: 50px">
		  				</div>
						<div class="col-md-3"></div>
					</div>

		  		</div>
				<div class="container-fluid">
		  			<div class="row-fluid"> 
						<div id="action">
						<!-- Comments -->
							<div id="moment_settings" class="col-md-4">
								<div class="form-group">
									<h4>Comment</h4>
									<div class="col-md-12 comment-container">
										<?php if (has_team($moment_info['team_id']) || is_admin()): ?>
											<?php if (!empty($moment_info['comment'])): ?>
												<div class="row">
													<p class="comment row-tags"><?=$moment_info['comment']?></p>
													<a id="edit-comment" class="btn btn-default">Edit comment</a>
												</div>
											<?php else: ?>
												<div class="row">
													<div class="form-group" style="min-height: 96px;">
														<textarea class="form-control" id="comment" name="comment" rows="4"></textarea>
													</div>
													<div class="form-group">
														<input type="submit" id="submit-comment" class="btn btn-primary" value="Submit" />
													</div>
												</div>
											<?php endif ?>
										<?php else: ?>
											<div class="row">
												<p>
													<?php if(!empty($moment_info['comment'])){ 
														echo $moment_info['comment']; 
													}else{
														echo "No comment";}
													?>
												</p>
											</div>
										<?php endif ?>
								  </div>
								</div>
							</div>
						</div>

						<!-- Moment tags -->
						<div id="moment_tags" class="col-md-4">
							<div class="col-md-6">
								<div class="row">
									<h4>Tags</h4>
								</div>
								<div class="row row-tags">
									<?php if (!empty($moment_info['tags'])): ?>
										<? foreach ($moment_info['tags'] as $tag) {?>
											<label class="label label-warning label-tag"><?=$tag['tag_name']?></label>
										<?}?>
									<?php endif ?>
								</div>
								<? if(is_admin() || has_team($moment_info['team_id'])) { ?> 
									<div class="row">
										<a class="btn btn-default edit-tags" href="javascript:;">Edit tags</a>
									</div>
								<? } ?>
							</div>
							<div class="col-md-6">
								<div class="row">
									<h4><?=lang('term_players')?></h4>
								</div>
								<div class="row row-tags">
									<?php if (!empty($moment_info['players'])): ?>
										<? foreach ($moment_info['players'] as $player) {?>
											<label class="label label-info label-tag"><?=$player['first_name']?> <?=$player['last_name']?></label>
										<?}?>
									<?php endif ?>
								</div>
								 <?php if( has_team($moment_info['team_id']) || is_admin() ): ?>
									<div class="row">
										<a class="btn btn-default edit-players" href="javascript:;">Edit <?=lang('term_players_l')?></a>
									</div>
								<?php endif ?>
							</div>
						</div>

						<!-- Moment Info -->
						<div id="moment_info" class="col-md-4 hidden-xs">
							<h4>Moment information</h4>
							<?php
							// check if the current user has rights for this team or is an admin
							if (has_team($moment_info['team_id']) || is_admin()): ?>
								<div class="form-group lead-lapse">
								  <div class="input-group">
									<span class="input-group-addon">Lead</span>
									<input id="lead" name="lead" type="text" class="form-control" value="<?=$moment_info['lead']?>" placeholder="Lead">
									<span class="input-group-addon">Lapse</span>
									<input id="lapse" name="lapse" type="text" class="form-control" value="<?=$moment_info['lapse']?>" placeholder="Lapse">
								  </div>
								</div>

								<?php
								// Set the Time in video to hidden; we don't want to use this anymore - it is used in server side code often so we need to keep it....... 
								?>
								<div class="form-group hidden">
								  <div class="break"></div>
								  <div class="input-group">
									<span class="input-group-addon">Time in video</span>
									 <input id="time_in_clip" name="time_in_clip" type="text" class="form-control" value="<?=$moment_info['time_in_clip']?>" placeholder="Time in video">
									<span class="input-group-addon">sec</span>
								  </div>
								</div>
								<div class="form-group">
								  <div class="input-group">
									<input type="submit" value="Change" id="change_moment" class="btn btn-default">
								  </div>
								</div>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<?php
					// TODO
					// Select playlist to add moment to
					// Under this comment you can find an old version
				?>
                <!-- <span id="playlist-loader">
                    <img src="<?=site_url('images/icons/loading_small.gif')?>">
                </span> -->


                <!-- <select class="simple-select" multiple title="Select playlist" id="playlist-select" data-live-search="true">
                <?php foreach($moment_info['playlists'] as $playlist) { ?>
                    <option value="<?=$playlist['playlist_id']?>"<?php if($playlist['has_moment']) echo " selected"; ?>>
                        <?=$playlist['title']?>
                    </option>
                <?php } ?>
                </select> -->

                <?php
                	// End TODO
                ?>

				<?php if (has_team($moment_info['team_id']) || is_admin()): ?>
					<a href="javascript:;" class="btn btn-danger pull-left" id="delete_moment"><span class="glyphicon glyphicon-trash"></span></a>
				<?php endif ?>
				<? if($system_info['downloads'] === '1'){ ?>
					<a href="<?=base_url("download/moment/".$moment_info['moment_id'])?>" class="btn btn-default" id="download_moment"><span class="glyphicon glyphicon-floppy-save"></span> Download moment</a>
				<? } ?>
                <? if($system_info['shares'] === '1'){ ?>
    				<a href="<?=base_url("shared/create/moment/".$moment_info['moment_id'])?>" target="_blanc" class="btn btn-default" id="share_moment"><span class="glyphicon glyphicon-share"></span> Share</a>
                <? } ?>



                <button class="btn btn-default close-modal" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
	  	</div>
	</div>
</div>

<script id="video_field_script" type="text/javascript">

	var moment_id 	= "<?=$moment_info['moment_id']?>";
	var part_id 	= "<?=$moment_info['part_id']?>";
	var event_id 	= "<?=$moment_info['event_id']?>";
	var recording 	= "<?=$moment_info['recording']?>";
	var videos 		= $('.video-js');

	$(document).ready(function(){
		positionVideoField();
		synchronize('.edit-moment-video');

        var i = 0;
	});

	$(window).resize(function(){
		positionVideoField();
	});

	//delete moment
	$('#delete_moment').confirm({
		text: 'Are you sure you want to delete this moment?',
		title: 'Confirmation required',
		confirm: function(){
			$.ajax({
				url: '<?=site_url('moments/delete_moment')?>',
				type: 'POST',
				dataType: 'json',
				data: {moment_id: moment_id},
				success: function(data){
					window.location.reload();
				}
			});
		},
		cancel: function(){

		},
		confirmButton: "Yes",
		cancelButton: "No",
		post: true
	});

	/* COMMENTS */
	$('#submit-comment').click( submitComment );

	$('#edit-comment').click( editComment );

	/**
	 * Submit the comment [ajax]
	 * @return {string} comment
	 */
	function submitComment() {
		var comment = $('#comment').val();
		$.ajax({
			url: '<?=site_url('moments/submit_comment')?>',
			type: 'POST',
			dataType: 'json',
			data: {moment_id: moment_id, comment: comment},
			success: function(data){
				$('.comment-container').html('<div class="row"><p class="comment row-tags">' + data.comment + '</p><a id="edit-comment" class="btn btn-default">Edit comment</a></div>');
				$('#edit-comment').click( editComment );
			}
		});
	}

	function editComment() {
		$.ajax({
			url: '<?=site_url('moments/edit_comment')?>',
			type: 'POST',
			dataType: 'html',
			data: {event_id: event_id, moment_id: moment_id},
			success: function(data){
				$('.comment-container').html(data);
				$('#submit-comment').click(	submitComment );
			}
		});
	}
	
	/**
	 * Key functions
	 */
	
	$(document).keyup(function(e) {
		//escape
		//if (e.keyCode == 27) { close_edit_moment(); } 
		//spacebar
		if (e.keyCode == 32) { 
			if($('#comment').is(':focus')){

			}else{
				e.preventDefault();
				play_video_players(); 
			}
		}
	});

	/**
	 * Page functions
	 */
	$('.modal').on('hidden.bs.modal', function () {
		close_edit_moment();
	});

	function close_edit_moment() {
		$('#edit-moment-modal, .modal-backdrop, #video_field_script').remove();
	}

	function positionVideoField(){
		var video_field = $('#video_field');
		width = video_field.width();
		height = video_field.height();
		margin_left = (width / 2) * -1;
		margin_top = (height / 2) * -1;
		video_field.css({
			'margin-left' :  margin_left + 'px',
			'margin-top' :  margin_top + 'px'
		});
	}

	if(findBootstrapEnvironment() === 'xs'){
		$('.video-js').css({
			'height' : '200px',
			'width' : '100%',
			'margin' : '0'
		});
	}

	/**
	 * EDIT MOMENT
	 */
	function editMomentTime(lead, lapse, time_in_clip) {
		$.ajax({
			url: '<?=site_url('moments/update_moment')?>',
			type: 'POST',
			dataType: 'json',
			data: {event_id: event_id, part_id: part_id, moment_id: moment_id, lead: lead, lapse: lapse, time_in_clip: time_in_clip, recording: recording},
			success: function(data){
				// !Moment is changed - reload videos - simply call load() on the video -> source is still the same
				$(".edit-moment-video").each(function() {
					// Loops through each video and calls .load() so that the source is loaded again
					$(this)[0].load();
				});
			}
		});
	}

	$('#change_moment').on('click', function() {
		var lead = $('#lead').val();
		var lapse = $('#lapse').val();
		var time_in_clip = $('#time_in_clip').val();
		editMomentTime(lead, lapse, time_in_clip);
	});

	$('.edit-tags, .edit-players').on('click', function(){
		$.ajax({
			url: '<?=site_url('moments/edit_tags_for_moment')?>',
			type: 'POST',
			dataType: 'html',
			data: {moment_id: moment_id, event_id: event_id},
			success: function(data){
				//console.log(data);
				$('.container').append(data); 
				$('#tag-modal').modal();
			}
		});
	});

	$('#slowmo').on('change', function(){
		var value = $(this).val() / 100;
		videos.each(function(){
			var id = $(this).attr('id');
			var video = document.getElementById(id);
			video.playbackRate = value;
		});
	});
</script>	