<?php // This page loads inside the template_view ?>
<div class="container">
	<div class"row">
		<div class="col-md-4">
			
		</div>
		<div class="col-md-4">
			<h3><?=$user_info['first_name']?> <?=$user_info['last_name']?><?php if(is_admin()): ?>
				<? if(is_admin()){ ?> 
				<a class="confirm" href="<?=site_url('users/delete_user/' . $user_info['id'])?>">
					<span class="glyphicon glyphicon-trash"></span>
				</a>
				<? }?>
			<?php endif ?></h3>
			<?php if (is_admin()): ?>
				<p><?=$user_info['email']?></p>
			<?php endif ?>
		</div>
		<div class="col-md-4">
			<h3><?=lang('term_teams')?></h3>
			<hr>
			<p><strong>Owner of</strong></p>
			<ul class="list list-unstyled">
				<?php foreach ($owner_teams as $team): ?>
					<li><?=$team['team_name']?></li>
				<?php endforeach ?>
			</ul>
			<hr>
			<p><strong><?=lang('term_player')?> of</strong></p>
			<ul class="list list-unstyled">
				<?php foreach ($player_teams as $team): ?>
					<li><?=$team['team_name']?></li>
				<?php endforeach ?>
			</ul>
			<?php if (is_admin()): ?>
				<small><a class="confirm" href="<?= site_url('users/hard_delete_user/' . $user_info['id']) ?>">hard delete this user</a></small>
			<?php endif ?>
		</div>
	</div>
</div>