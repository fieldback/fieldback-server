<?
	$free_space = $space['free'];
	$total_space = $space['total'];
	$used_space = $space['total'] - $space['free'];
?>

<?php // This page loads inside the template_view ?>
<section class="col-md-4">
	<h3>Events list</h3>
	<table id="events_list" class="table table-striped">
		<thead>
			<th>Event name</th>
			<th>Size</th>
			<th>Delete</th>
		</thead>
	<? 
		foreach ($events as $event) 
		{?>
			<tr class="event_container">
				<td class="event_title">
					<?=$event['event_name']?>
				</td>
				<td> 
					(<?=byteFormat($event['folder_size'])?>)
				</td>
				<td>
					<a class="pull-right delete" href="<?=base_url('events/delete_event/' . $event['event_id'])?>"><span class="glyphicon glyphicon-trash"></span></a>
				</td>
			</tr>	
		<?}
	?>
	</table>
</section>
<section class="col-md-4">
	<h3>Other info</h3>
</section>
<section class="col-md-4">
	<h3>Space usage</h3>
	<div id="stats">
		<div id="disk_space_pie" style="height: 300px; width: 100%;"></div>
		<div id="disk_space_info">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Space used</th>
						<th>Space free</th>
						<th>Total space</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?=byteFormat($used_space)?></td>
						<td><?=byteFormat($free_space)?></td>
						<td><?=byteFormat($total_space)?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>

<script type="text/javascript">
	window.onload = function () {

	$('.delete').confirm();

	CanvasJS.addColorSet("customColorSet1",
     [//colorSet Array
     "#2ac1ba",
     "#f47564",
     "#8FAABB",
     "#B08BEB",
     "#3EA0DD",
     "#F5A52A",
     "#23BFAA",
     "#FAA586",
     "#EB8CC6"
    ]); 

	var chart = new CanvasJS.Chart("disk_space_pie",
	{
		title:{
			text: "Space usage"
		},
		colorSet: "customColorSet1",
		toolTip:{
			enabled: true,
			content: function(e){
				return e.entries[0].dataPoint.label;
			}
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			dataPoints: [
				{  	y: <?=$free_space?>, 
					legendText:"Free space", 
					indexLabel: "Free space",
					label: "<?=byteFormat($free_space)?>" 
				},
				{  	y: <?=$used_space?>, 
					legendText:"Used space", 
					indexLabel: "Used space",
					label: "<?=byteFormat($used_space)?>" 
				}
			]
	 	}
	 	]
	});

	chart.render();
	}
  </script>