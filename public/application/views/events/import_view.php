<?php // This page loads inside the template_view ?>

<script type="application/javascript" src="<?= site_url("js/vendor/dropzone.js");?>"></script>
<link rel="stylesheet" href="<?=site_url("css/vendor/dropzone.css");?>">

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Import the .json file
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <form class="dropzone" id="json-dropzone" action="<?=site_url('api/import_event');?>">
                            <input type="hidden" name="sync_auth_key" value="<?=$system_info['sync_auth_key'];?>">
                        </form>
                    </div>
                </div>
                <ul class="list-group" id="json_import_status">
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Import video files
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <form class="dropzone" id="video-dropzone" action="<?=site_url('api/import_event');?>">
                            <input type="hidden" name="sync_auth_key" value="<?=$system_info['sync_auth_key'];?>">
                        </form>
                    </div>
                </div>
                <ul class="list-group" id="video_import_status">
                </ul>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
$(document).ready(function(){
    // Get some elements from the page for later
    var $json_import_status = $("#json_import_status");
    var $video_import_status = $("#video_import_status");

    // Set the options for the json-dropzone
    Dropzone.options.jsonDropzone = {
        paramName: "file",
        maxFilesize: 1,
        dictDefaultMessage: "Drag the fb_event_#.json file here or click to select from your computer.",
        addRemoveLinks: false,
        maxFiles: 1,
        acceptedFiles: "application/json",
        autoProcessQueue: true,
        init: function() { // On accepting the .json, display the status messages from the JSON response
            var thisDz = this;
            this.on("complete", function(file) {
                // Check the values of the import:
                var status = [];
                var xhrObj = file.xhr;
                var statusCode = xhrObj.status;
                // Check the status header of the request -> it should be 200 otherwise it's an error
                if(statusCode == 200) {
                    var response = $.parseJSON(xhrObj.response);
                    // Check for creator
                    if(response.creator) {
                        console.log("The creator existed and will be added as the event creator.");
                    } else {
                        console.log("The creator did not exist, you will become the creator.");
                    }
                    if(response.team) {
                        console.log("The team existed and will be added as the event's team.");
                    } else {
                        console.log("The team did not exist, the 'standard' team will be added as the team.");
                    }
                    if(response.team) {
                        console.log("The organisation existed and will be added as the event's organisation.");
                    } else {
                        console.log("The organisation did not exist, the current organisation will be added as the organisation.");
                    }
                    // Loop through tags and playertags and see if they are all true
                    var tags_oke = true;
                    var player_tags_oke = true;
                    $.each(response.tags, function(index, value) {
                        if(response.tags[index] == false) tags_oke = false;
                    });
                    $.each(response.player_tags, function(index, value) {
                        if(response.player_tags[index] == false) player_tags_oke = false;
                    });
                    if(tags_oke) {
                        console.log("All tags existed, and they will be added to the moments.");
                    } else {
                        console.log("Not all tags existed, and they will not be added to the moments.");
                    }
                    if(player_tags_oke) {
                        console.log("All playertags existed, and they will be added to the moments.");
                    } else {
                        console.log("Not all playertags existed, and they will not be added to the moments.");
                    }
                    status.push("The event JSON file was received by the server. Please upload the fb_event_videos_#.fb file below.");
                    // Append them to the json_import_status list
                    $.each(status, function(index, value) {
                        $("<li />").addClass("list-group-item").html(status[index]).appendTo($json_import_status);
                    });
                    // Disable the dropzone
                    thisDz.disable();
                } else {
                    // We have an error: display this as well and remove the file that was uploaded
                    var error = "Error ("+statusCode+"): "+  xhrObj.responseText;
                    $("<li />").addClass("list-group-item").html(error).appendTo($json_import_status);
                    // Remove all files so you can try again
                    thisDz.removeAllFiles();
                }
            });
        }
    };

    // Set the options for the video-dropzone which is used to upload video files
    Dropzone.options.videoDropzone = {
        paramName: "file",
        maxFilesize: 5000,
        dictDefaultMessage: "Drag the fb_event_video_#.fb file here or click to select from your computer.",
        addRemoveLinks: false,
        maxFiles: 10000,
        autoProcessQueue: true,
        init: function () {
            var thisDz = this;
            this.on("complete", function(file) {
                // Check the values of the import:
                var xhrObj = file.xhr;
                var statusCode = xhrObj.status;
                // Check the status header of the request -> it should be 200 otherwise it's an error
                if (statusCode == 200) {
                    var response = $.parseJSON(xhrObj.response);
                    if(response.last_file) {
                        // It was the last file (should always be true in the current state), show the status
                        $("<li />").addClass("list-group-item").html("File uploaded; all files have been received, starting import..").appendTo($video_import_status);
                        // Loop through the status we received from the server and display this
                        $.each(response.status, function(index, value) {
                            $("<li />").addClass("list-group-item").html(response.status[index]).appendTo($video_import_status);
                        });
                        thisDz.disable();
                        // Add a link to the newly created event
                        $("<li />").addClass("list-group-item").html("<a href='<?=site_url("events/event");?>/"+response.event_id+"'>Click here to go to the new event.</a>").appendTo($video_import_status);
                    } else {
                        // Not all files have been uploaded -> this should not happened in the current settings
                        $("<li />").addClass("list-group-item").html("File uploaded, but this was not the last file of the batch.").appendTo($video_import_status);
                    }
                } else {
                    // We have an error: display this as well and remove the file that was uploaded
                    var error = "Error ("+statusCode+"): "+  xhrObj.responseText;
                    $("<li />").addClass("list-group-item").html(error).appendTo($video_import_status);
                    // Remove all files so you can try again
                    thisDz.removeAllFiles();
                }
            });
        }
    };
});
</script>

<script type="text/html" id="tmpl_import_status-item">
    <tr>
        <td>
            {{:id}}
        </td>
        <td>
            {{niceDateTime:import_timestamp}}
        </td>
        <td>
            {{if uploaded==0}}
                Pending..
            {{else}}
                {{niceDateTime:upload_timestamp}}
            {{/if}}
        </td>
    </tr>
</script>