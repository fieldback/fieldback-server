<div class="modal fade" id="create_event_modal">
  	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Create new event</h4>
	 		 </div>
	  	<div class="modal-body">
			<!-- 
				
				description 

			-->

			<form role="form" action="<?=site_url('events/create_event')?>" method="post">
			  	<div class="form-group">
				    <label for="event_name">Event name</label>
				    <?= form_error('event[event_name]') ?>
				    <input type="text" class="form-control" id="event_name" name="event[event_name]" placeholder="Event name" value="<?=set_value('event[event_name]');?>">
			  	</div>
			  	<div class="form-group">
				    <label for="event_description">Event description</label>
				    <?= form_error('event[event_description]')?> 
				    <textarea class="form-control" id="event_description" name="event[event_description]" placeholder="Event description"><?=set_value('event[event_description]')?></textarea>
			  	</div>
			  	<div class="form-group">
			  		<label for="team_id"><?=lang('term_team')?></label>
			  		<select name="team_id" class="form-control" id="team_id">
				  		<?php foreach ($teams as $team): ?>
				  			<option value="<?=$team['team_id']?>" <?= set_select('team_id', $team['team_id']); ?>><?= $team['team_name'] ?></option>
				  		<?php endforeach ?>
			  		</select>
			  	</div>
			  	<div class="form-group">
				  	<? if( is_admin() ) { ?>
						<label for="event_owner">Event owner</label>
						<select name="event_owner" class="form-control" id="event_owner">
						</select>
				  	<? } ?>
				</div>
	  	</div>
	 	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<input type="submit" class="btn btn-primary" value="Create event!">
			</form>
		  </div>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->