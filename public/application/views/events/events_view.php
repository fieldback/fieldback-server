<?php // This page loads inside the template_view ?>
<?php if (has_teams() || is_admin()): ?>
	<?=$create_event_form?>
<?php endif ?>
<? if(validation_errors() && (has_teams() || is_admin())) { ?>
	<script type="text/javascript">
		$(function(){
			$('#create_event_modal').modal();
		});
	</script>
<?}?>
<div class="container">
	<? if(has_teams()) { ?>
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					Events of which you are coach/owner
					<div class="btn-group pull-right">
						<? if(has_teams() || is_admin()){?>
							<a id="create_event" class="btn btn-primary btn-xs" href="javascript:;" alt="create_event" data-toggle="modal" data-target="#create_event_modal"><span class="glyphicon glyphicon-plus"></span> Create event</a>
						<? } ?> 
						<? if(is_admin()){?>
							<a class="btn btn-default btn-xs" href="<?=base_url('events/events_list')?>" alt="events_list"><span class="glyphicon glyphicon-th-list"></span><span class="hidden-xs"> Event</span> list</a>
						<? } ?>
					</div>
				</div>
				<table class="table table-hover table-striped">
					<thead class="bg-empha">
						<tr>
							<th width="10%">Date</th>
							<th width="10%"><?=lang("term_team")?></th>
							<th width="25%;">Name</th>
							<th width="35%" class="hidden-xs">Description</th>
							<th width="20%" class="hidden-xs">Creator</th>
						</tr>
					</thead>
					<tbody data-link="row" class="rowlink">
						<?foreach($events_owner as $event){?>
							<tr>
								<td><a href="<?=base_url('events/event/' . $event['event_id']);?>"><?=date('d M Y', $event['date_created'])?></a></td>
								<td><?=$event['team_name']?></td>
								<td><?=$event['event_name']?></td>
								<td class="hidden-xs"><?=$event['event_description']?></td>
								<? if(isset($event['creator']['first_name'])) { ?>
									<td class="hidden-xs"><?=$event['creator']['first_name']?> <?=$event['creator']['last_name']?></td>
								<? }else{ ?>
									<td class="hidden-xs">Unknown</td>
								<?	} ?>
							</tr>
						<?}?>
					</tbody>
				</table>
			</div>
		</div>
	<? } ?>

	<? if(is_player()) { ?>
	<div class="col-md-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				Events of which you are <?=lang('term_player_l')?>
			</div>
			<table class="table table-hover table-striped">
				<thead class="bg-empha">
					<tr>
						<th width="10%">Date</th>
						<th width="10%"><?=lang("term_team")?></th>
						<th width="25%;">Name</th>
						<th width="35%" class="hidden-xs">Description</th>
						<th width="20%" class="hidden-xs">Creator</th>
					</tr>
				</thead>
				<tbody data-link="row" class="rowlink">
					<?foreach($events_player as $event){?>
						<tr>
							<td><a href="<?=base_url('events/event/' . $event['event_id']);?>"><?=date('d M Y', $event['date_created'])?></a></td>
							<td><?=$event['team_name']?></td>
							<td><?=$event['event_name']?></td>
							<td class="hidden-xs"><?=$event['event_description']?></td>
							<? if(isset($event['creator']['first_name'])) { ?>
								<td class="hidden-xs"><?=$event['creator']['first_name']?> <?=$event['creator']['last_name']?></td>
							<? }else{ ?>
								<td class="hidden-xs">Unknown</td>
							<?	} ?>
						</tr>
					<?}?>
				</tbody>
			</table>
		</div>
	</div>
	<? } ?> 

	<? if( is_admin() ) { ?>
		<div class="col-md-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				All events of organisation
			</div>
			<table class="table table-hover table-striped">
				<thead class="bg-empha">
					<tr>
						<th width="10%">Date</th>
						<th width="10%"><?=lang("term_team")?></th>
						<th width="25%;">Name</th>
						<th width="35%" class="hidden-xs">Description</th>
						<th width="20%" class="hidden-xs">Creator</th>
					</tr>
				</thead>
				<tbody data-link="row" class="rowlink">
					<?foreach($events_admin as $event){?>
						<tr>
							<td><a href="<?=base_url('events/event/' . $event['event_id']);?>"><?=date('d M Y', $event['date_created'])?></a></td>
							<td><?=$event['team_name']?></td>
							<td><?=$event['event_name']?></td>
							<td class="hidden-xs"><?=$event['event_description']?></td>
							<? if(isset($event['creator']['first_name'])) { ?>
								<td class="hidden-xs"><?=$event['creator']['first_name']?> <?=$event['creator']['last_name']?></td>
							<? }else{ ?>
								<td class="hidden-xs">Unknown</td>
							<?	} ?>
						</tr>
					<?}?>
				</tbody>
			</table>
		</div>
	</div>
	<? } ?>
</div>

<? if( is_admin() ) {  // only if admin! ?>
<script>
	$(document).ready(function(){
		change_owners_list();
	});

	$('#team_id').on('change', function(){
		change_owners_list();
	});

	// Functions
	
	function change_owners_list() {
		var team_id = $('#team_id').val();
		$.ajax({
			url: '<?=site_url('events/get_all_owners_for_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {team_id: team_id},
			success: function(data){
				$('#event_owner').empty();
				$.each(data, function(index, value) {
					$('#event_owner').append('<option value="'+this.user_id+'">'+this.first_name+' '+this.last_name+'</option>');
				});
			}
		});
	}
</script>
<? } ?>