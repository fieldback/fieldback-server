<?php // This page loads inside the template_view ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Export event
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h4>
                                Event: <?php echo $event_info['event_name']; ?>
                            </h4>
                            <p>
                                <span id="date_created" data-date-created="<?php echo $event_info['date_created']; ?>"></span>
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?php if($master_server_active) { ?>
                                <p>
                                    <button class="btn btn-default btn-block" id="btn_send_to_master">
                                        <span class="glyphicon glyphicon-share-alt"></span> Send to <?php echo $server_name; ?>
                                    </button>
                                </p>
                            <?php } ?>
                            <p>
                                <button class="btn btn-default btn-block" id="btn_export_to_files">
                                    <span class="glyphicon glyphicon-export"></span> Download as file(s)
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
                <ul class="list-group text-center" id="ul_download_links">

                </ul>
            </div>
        </div>

        <?php if($master_server_active) { ?>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Upload status
                    </div>
                    <table class="table">
                        <thead>
                            <th>Export ID:</th>
                            <th>Exported at:</th>
                            <th>Uploaded:</th>
                        </thead>
                        <tbody id="upload_status_body">

                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script type="application/javascript">
// Get the event_id from PHP
var event_id = <?php echo $event_id; ?>;

$(document).ready(function(){
    // Get the elements of the page
    $btn_send_to_master = $("#btn_send_to_master");
    $btn_export_to_files = $("#btn_export_to_files");
    $ul_download_links = $("#ul_download_links");
    $upload_status_body = $("#upload_status_body");


    // Make a nice date format of the date_created:
    var date_created = moment.unix($("#date_created").data("date-created"));
    $("#date_created").html(date_created.format("dddd, Do MMMM YYYY, HH:mm:ss"))

    // On click, get the links and show them as clickable links
    $btn_export_to_files.on("click", function() {
        $btn_export_to_files.prop('disabled', true);
        $btn_export_to_files.html('Generating links..');
        // Request the links to download the files from
        $.ajax({
            url: site_url + 'export/get_download_links/' + event_id,
            dataType: 'json'
        }).success(function(response){
            for(var i = 0; i < response.length; i++) {
                $("<li class='list-group-item' />")
                    .html("<a href='"+response[i]['url']+"'><span class='glyphicon glyphicon-floppy-save'></span> "+response[i]['name']+"</a>")
                    .appendTo($ul_download_links);
            }
            $btn_export_to_files.html('Ready for download.');
        }).fail(function (response) {
            console.log(response);
        });
    });

    <?php if($master_server_active) { ?>
    $btn_send_to_master.on("click", function(){
        $btn_send_to_master.prop('disabled', true);
        $btn_send_to_master.html('Exporting event..');
        // Send the command to send the event to the remote
        $.ajax({
            url: site_url + 'export/send_event_to_master/' + event_id,
            dataType: 'json'
        }).success(function(response){
            console.log(response);
            setTimeout(function(){
                update_upload_status();
            }, 1000);
            $btn_send_to_master.html('Exported event! Uploading files...');
            // Do the uploads with ajax, after the JSON was send
            $.ajax({
                url: site_url + 'api/do_uploads'
            }).success(function(response) {
                console.log(response);
            }).fail(function(error){
                console.log(error);
            });
        }).fail(function (response) {
            console.log(response);
        });
    });
    function update_upload_status() {
        console.log("Updating status..");
        $.ajax({
            url: '<?=base_url('export/get_upload_status')?>/'+event_id,
            type: 'POST',
            dataType: 'json'
        }).done(function(response){
            var status_incomplete = $.templates({markup: "#tmpl_upload_status-item", allowCode: true}).render(response['uploads_incomplete']);
            var status_complete = $.templates({markup: "#tmpl_upload_status-item", allowCode: true}).render(response['uploads_complete']);
            $upload_status_body.html(status_incomplete + status_complete);
        }).fail(function(response) {
            console.log(response);
        });
    }

    // Run once
    update_upload_status();
    // Run once every few seconds
    var updater = setInterval(function(){
        update_upload_status();
    }, 5000);

    <?php } ?>
});
</script>

<script type="text/html" id="tmpl_upload_status-item">
    <tr>
        <td>
            {{:id}}
        </td>
        <td>
            {{niceDateTime:export_timestamp}}
        </td>
        <td>
            {{if uploaded==0}}
                Pending..
            {{else}}
                {{niceDateTime:upload_timestamp}}
            {{/if}}
        </td>
    </tr>
</script>

<script>
    var usable_fields_for_template = [{
        "id": "43",
        "export_timestamp": "1441283387",
        "event_id": "9",
        "original_file_name": ".\/videos\/9\/9\/9.mp4",
        "unique_file_name": "MTQ0MTI4MzM4Ny4vdmlkZW9zLzkvOS85Lm1wNA%3D%3D.mp4",
        "upload_to": "http:\/\/localhost\/api\/import_event",
        "uploaded": "0",
        "upload_timestamp": "0",
        "event_name": "Test Name",
        "event_description": "Test Description",
        "date_created": "1440749103"
    }, {
        "id": "44",
        "export_timestamp": "1441283387",
        "event_id": "9",
        "original_file_name": ".\/videos\/9\/9\/9.jpg",
        "unique_file_name": "MTQ0MTI4MzM4Ny4vdmlkZW9zLzkvOS85LmpwZw%3D%3D.jpg",
        "upload_to": "http:\/\/localhost\/api\/import_event",
        "uploaded": "0",
        "upload_timestamp": "0",
        "event_name": "Test Name",
        "event_description": "Test Description",
        "date_created": "1440749103"
    }, {
        "id": "45",
        "export_timestamp": "1441283387",
        "event_id": "9",
        "original_file_name": ".\/videos\/9\/9\/6\/6.mp4",
        "unique_file_name": "MTQ0MTI4MzM4Ny4vdmlkZW9zLzkvOS82LzYubXA0.mp4",
        "upload_to": "http:\/\/localhost\/api\/import_event",
        "uploaded": "0",
        "upload_timestamp": "0",
        "event_name": "Test Name",
        "event_description": "Test Description",
        "date_created": "1440749103"
    }, {
        "id": "46",
        "export_timestamp": "1441283387",
        "event_id": "9",
        "original_file_name": ".\/videos\/9\/9\/6\/6.jpg",
        "unique_file_name": "MTQ0MTI4MzM4Ny4vdmlkZW9zLzkvOS82LzYuanBn.jpg",
        "upload_to": "http:\/\/localhost\/api\/import_event",
        "uploaded": "0",
        "upload_timestamp": "0",
        "event_name": "Test Name",
        "event_description": "Test Description",
        "date_created": "1440749103"
    }]
</script>
