<?
	if(empty($event_info['team'])){
		$event_info['team']['team_id'] = 0;
	}
	$max_str_length_part_name = 25;
	$recording_in_progress = recording_in_progress();
	$recording_in_progress = ($recording_in_progress) ? 'true' : 'false';
?>

<?php // This page loads inside the template_view ?>
<div class="container">
	<!-- EVENT INFO -->
	<div class="col-md-4">
		<section class="panel panel-success">
			<div class="panel-heading">Event info</div>
			<div class="panel-container">
				<ul id="event_information" class="list-unstyled">
					<li id="error"></li>
					
					<small>Event name</small>
					<?php if (is_admin() || has_event($event_info['event_id'])): ?>
						<li title="click to edit the event name" class="event_info editable" id="event_name"><p><span class="glyphicon glyphicon-edit"></span> <?=$event_info['event_name']?></p></li>
					<?php else: ?>
						<li id="event_name"><p><?=$event_info['event_name']?></p></li>
					<?php endif ?>
					
					<small>Description</small>
					<?php if (is_admin() || has_event($event_info['event_id'])): ?>
						<li title="click to edit the event description" class="event_info editable" id="event_description"><p><span class="glyphicon glyphicon-edit"></span> <?=$event_info['event_description']?></p></li>
					<?php else: ?>
						<li id="event_description"><p><?=$event_info['event_description']?></p></li>
					<?php endif ?>
					
					
					<small><?=lang("term_team")?> name</small>
					<li id="team"><p><?=$event_info['team']['team_name']?></p></li>

					<small>Organisation</small>
					<li id="organisation"><p><?=$event_info['organisation_name']?></p></li>

					<small>Date</small>
					<li id="date_created"><p><?=date('d-m-Y', $event_info['date_created'])?></p></li>

					<small>Created by:</small>
					<li id="username"><p><?=$event_info['creator']['first_name']?> <?=$event_info['creator']['last_name']?></p></li>

					<?php if (is_admin() || has_event($event_info['event_id'])): ?>
                        <div class="pull-right">
                            <a href="<?=site_url("events/export/".$event_info['event_id'])?>" class="btn btn-default">
                                <span class="glyphicon glyphicon-export"></span> Export
                            </a>
                            </button>
						    <a href="<?=site_url('events/delete_event/' . $event_info['event_id'])?>" class="btn btn-danger confirm"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                        </div>
                        <div class="clearfix"></div>
					<?php endif ?>
				</ul>
			</div>
		</section>
	</div>
	<!-- PARTS -->
	<div class="col-md-8">
		<section class="panel panel-success">
			<?php if (has_event($event_info['event_id']) || is_admin()): ?>
				<div class="panel-heading">Parts <a href="<?=site_url('parts/add_part_to_event') . '/' . $event_info['event_id']?>" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Add new part</a></div>
			<?php else: ?>
				<div class="panel-heading">Parts</div>
			<?php endif ?>
			<div class="table-responsive">
				<table class="table table-hover table-striped">
				<!-- show parts -->
				<? // !dump($parts); ?>
				<tbody>
					<?php foreach ($parts as $part):
						if( empty($part['clips']) && is_player_of_team($event_info['team']['team_id'] && !has_event($event_info['event_id']) && !is_admin())) {
							//do not show the part 
						}else{
						?>
						<tr>
							<td class="col-md-6 text-center">
							<a href="<?=base_url('clips/live_tagging/' . $event_info['event_id'] . '/' . $part['part_id'])?>"></a>
							<? if(!empty($part['recordings']) || !empty($part['instacordings'])) {?>
									<div class="clip">
										<a class="btn btn-primary btn-part" href="<?=base_url('clips/live_tagging/' . $event_info['event_id'] . '/' . $part['part_id'])?>">Recording in progress</a>
									</div>
								<?}else{
									if(!empty($part['clips'])) { ?>
										<!-- clips are there -->
										<? $col_nr = 12 / count($part['clips']);?>
										<?foreach($part['clips'] as $clip){ ?>
											<div class="clip thumbnail col-md-<?=$col_nr?>">
												<a href="<?=base_url('clips/live_tagging/' . $event_info['event_id'] . '/' . $part['part_id'])?>"><img style="max-height: 80px" src="<?=base_url('videos/' . $event_info['event_id'] . '/' . $part['part_id'] . '/' . $clip['clip_id'] . '.jpg');?>" alt="img"/></a>
											</div>
										<?}?>
										<!-- ./ clips are there -->
									<?}else{?>
										<!-- part is empty -->
										<div class="btn-group">
											<a href="<?=base_url('clips/record/' . $event_info['event_id'] . '/' . $part['part_id'])?>" class="btn btn-primary btn-part btn-loading">New recording</a>
											<a href="<?=base_url('clips/upload_clip_to_part/' . $event_info['event_id'] . '/' . $part['part_id'])?>" class="btn btn-default btn-part">Upload video</a>
										</div>
										<!-- ./ part is empty -->
									<?}?>
								<?}?>
							</td>
							<td class="col-md-5">
								<?php if (has_event($event_info['event_id']) || is_admin()): ?>
									<h4 class="part_info editable" id="part_name" data-content="<?=$part['part_name']?>" data-part="<?=$part['part_id']?>"><span class="glyphicon glyphicon-edit"></span> <? echo substr($part['part_name'], 0, $max_str_length_part_name); if(strlen($part['part_name']) > $max_str_length_part_name){echo '...';}?></h4>
								<?php else: ?>
									<h4 class="part_info" id="part_name" data-content="<?=$part['part_name']?>" data-part="<?=$part['part_id']?>"><? echo substr($part['part_name'], 0, $max_str_length_part_name); if(strlen($part['part_name']) > $max_str_length_part_name){echo '...';}?></h4>
								<?php endif ?>
								
								<p><?= date('d-m-Y', $part['date_created']) ?></p>
							</td>
							<td class="col-md-1 rowlink-skip">
								<div class="btn-group-vertical pull-right pull-bottom btn-part">
									<?if(count($part['clips']) > 0){?>
										<a class="edit_item btn btn-default btn" href="<?=base_url('clips/live_tagging/' . $event_info['event_id'] . '/' . $part['part_id'])?>" >
											<span class="glyphicon glyphicon-pushpin"></span> 
											<?php if (is_admin() || has_event($event_info['event_id'])): ?>
												Edit
											<?php else: ?>
												Show
											<?php endif ?>
										</a>
									<?}?>
									<?php if (is_admin() || has_event($event_info['event_id'] || is_system_admin())): ?>
										<? //! replaced by next line to enable removing of parts when no recording is yet running - if(!empty($part['clips']) && empty($part['recordings'])){  ?>
										<? if(empty($part['recordings'])) { ?>
										<a class="edit_item btn btn-danger btn-xs confirm" href="<?=base_url('parts/delete_part/' . $part['part_id'] . '/' . $event_info['event_id']);?>" >
											<span class="glyphicon glyphicon-trash"></span> 
										</a>
										<? } ?>
									<?php endif ?>
								</div>
							</td>
						</tr>
						<?}?>
					<?php endforeach ?>
				</tbody>
				<!-- ./ show parts -->
				</table>
			</div>
		</section>
	</div>

	<!-- INITIALIZING CAMERAS MODAL -->
	<div class="modal fade" id="modal-loading">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-body text-center">
					<img style="height: 50px; width: 50px;" src="<?=site_url('images/icons/loader.gif')?>" alt="loading..." />
					<p>Initializing cameras</p>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- RECORDING IN PROGRESS MODAL -->
	<div class="modal fade" id="modal-recording-in-progress">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-body text-center">
					<p>Recording in progress. Stop existing recording before starting a new one.</p>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>


<script type="text/javascript">
	//variables
	var event_id = '<?=$event_info['event_id']?>';

	//Loading recording
	$('a.record').on('click', function(){
		$(this).parent().parent().html('loading...');
	});

	//PARTS
	$('.part').hover(function(){
		$(this).find('.part_edit').stop().toggle();
	});
	
</script>

<? if(has_event($event_info['event_id']) || is_admin()) {?>
	
	<script>

		var max_str_length_part_name = '<?=$max_str_length_part_name?>';
		var recording_in_progress = '<?=$recording_in_progress?>';

		//Edit part info
		$('.part_info').click(function(){
			console.log($('h4.edit').length);
			if($('h4.edit').length === 0){
				if($(this).hasClass('edit')){
					$(this).removeClass('edit');
				}else{
					$(this).addClass('edit');
					if($(this).attr('class') !== 'edit'){
						var part_id = $(this).attr('data-part');
						var part_info = $(this);
						var part_info_id = $(this).attr('id');
						var part_info_text = $(this).attr('data-content');
						$.ajax({
							type: "POST",
							url: "<?=base_url('edit_part/edit_part_info')?>",
							data: {part_info_id: part_info_id, part_info_text: part_info_text, part_id: part_id},
							dataType: 'html',
							success: function(data){
								part_info.attr('class', 'edit');
								part_info.html(data);
								$('h4[data-part="' + part_id + '"]').children('input.part_name').focus();
								// Edit submit button on enter
								var parent = $('h2[data-part="' + part_id + '"]');
								submitOnEnter(parent);
							}
						})
					}
				}
			}
		});

		//Edit event info 
		$('.event_info').click(function(){
			if($(this).attr('class') !== 'edit'){
				var event_info = $(this);
				var event_info_id = $(this).attr('id');
				var event_info_text = $(this).text();
				$.ajax({
					type: "POST",
					url: "<?=base_url('edit_event/edit_event_info')?>",
					data: {event_info_id: event_info_id, event_info_text: event_info_text},
					dataType: 'html',
					success: function(data){
						event_info.attr('class', 'edit');
						event_info.html(data);
						event_info.children('.to_select').focus();
						parent = event_info;
						submitOnEnter(parent);
					}
				})
			}
		});

		//Get event info
		function updateEventInfo(event_info_id){
			var event_info_new_text = $('input#' + event_info_id).val();
			if(event_info_new_text == null){
				event_info_new_text = $('textarea#' + event_info_id).val();
			}
			$.ajax({
				type: "POST",
				url: "<?=base_url('edit_event/update_event_info')?>",
				data: {event_info_new_text: event_info_new_text, event_info_id: event_info_id, event_id: event_id},
				dataType: 'json',
				success: function(data){
					if(data == 'error1'){
						$('li#error').html('The input can not be empty');
					}else{
						$('li#error').empty();
						if(event_info_id == "event_name"){
							$('#' + event_info_id).html('<p><span class="glyphicon glyphicon-edit"></span> ' + data.event_name + '</p>');
						}
						if(event_info_id == "event_description"){
							$('#' + event_info_id).html('<p><span class="glyphicon glyphicon-edit"></span> ' + data.event_description + '</p>');
						}
						$('#' + event_info_id).removeClass('edit');
						$('#' + event_info_id).addClass('event_info editable');
					}
				}
			});
		}




		//update part info	
		function updatePartInfo(part_info_id, part_id){
			var part_info_new_text = $('input#' + part_info_id).val();
			$.ajax({
				type: "POST",
				url: "<?=base_url('edit_part/update_part_info')?>",
				data: {part_info_new_text: part_info_new_text, part_info_id: part_info_id, part_id: part_id},
				dataType: 'json',
				success: function(data){	
					if(data == 'error1'){
						$('div#error').html('The input can not be empty');
					}else{
						$('div#error').empty();
						if(part_info_id == "part_name"){
							if(data.part_name.length > max_str_length_part_name){
								var text = data.part_name.substring(0, max_str_length_part_name) + '...';
							}else {
								var text = data.part_name;
							}
							$('h4[data-part="' + part_id + '"]').html('<span class="glyphicon glyphicon-edit"></span> ' + text);
							$('h4[data-part="' + part_id + '"]').attr('data-content', data.part_name);
							$('h4[data-part="' + part_id + '"]').attr('class', 'part_info editable');
						}
					}
				}
			});
		}

		function cancelUpdatePart(part_id)
		{
			event.stopImmediatePropagation();
			var text = $('h4[data-part="' + part_id + '"]').children().children('input').data('value');
			$('h4[data-part="' + part_id + '"]').html('<span class="glyphicon glyphicon-edit"></span> ' + text);
			$('h4[data-part="' + part_id + '"]').attr('data-content', text);
			$('h4[data-part="' + part_id + '"]').attr('class', 'part_info editable');
		}

		// Edit submit button on enter
		function submitOnEnter(parent){
				$('input').keypress(function(e){
				if(e.which == 13){
					parent.children('input.edit_submit').click();
				}
			});
		}

		$('.btn-loading').on('click', function(e){
			if(recording_in_progress === 'false'){
				$('#modal-loading').modal();
			}else{
				$('#modal-recording-in-progress').modal();
				e.preventDefault();
			}
		});

	</script>
<? } ?>