<div class="jumbotron">
	<h1>Fieldback</h1>
	<h4>Instant Performance Analysis</h4>
</div>
<div class="container-fluid">
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
		<form class="form-signin" role="form" method="post" action="<?=base_url('login')?>">
			<h2 class="form-signin-heading">Please login</h2>
			<?=$this->ion_auth->errors();?>
			<!-- username -->
			<div class="form-group">
				<?=form_error('login[username]')?>
				<input class="form-control" type="text" class="centered" name="login[username]" id="login[username]" placeholder="E-mail" value="<?=set_value('login[username]')?>" required autofocus/>
			</div>
			
			<!-- password -->
			<div class="form-group">
				<?=form_error('login[password]')?>
				<input class="form-control" type="password" class="centered" name="login[password]" id="login[password]" placeholder="Password" value="<?=set_value('login[password]')?>" required/>
			</div>
			<div class="form-group">
			<!-- submit -->
				<input class="btn btn-lg btn-primary btn-block" type="submit" class="centered" name="submit" value="Login" />
			</div>
			<p class="pull-left">
			  <label>
			  <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> &nbsp;Remember
			</p>
		</form>
			<a href="login/forgot_password" class="pull-right">Forgot password</a>
		</label>
	</div>
	<div class="col-md-4">
	</div>
</div>
<footer class="footer">
	<div class="container hidden-xs">
		<p class="text-muted">Designed and developed in the smartest region of the world</p>
		<div class="promo-logos">
			<a href="http://www.spinnov.com" target="_blank"><img class="promo-logo" alt="Spinnov" src="<?= base_url('images/logos/spinnov.png'); ?>"></a>
			<a href="http://www.lessormore.nl" target="_blank"><img class="promo-logo" alt="Less or More" src="<?= base_url('images/logos/lom.png'); ?>"></a>
			<a href="http://www.tue.nl" target="_blank"><img class="promo-logo" alt="Technische Universiteit Eindhoven" src="<?= base_url('images/logos/tue.png'); ?>"></a>
			<a href="http://www.innosport.nl" target="_blank"><img class="promo-logo" alt="Innosport" src="<?= base_url('images/logos/innosport.png'); ?>"></a>
		</div>
	</div>
	<div class="container">
		<p class="text-muted">Visit our website for more information: <a href="http://www.fieldback.net" target="_blank">www.fieldback.net</a></p>
	</div>
</footer>