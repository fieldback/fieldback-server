<div class="jumbotron">
	<h1>Fieldback</h1>
	<h4>Instant Performance Analysis</h4>
</div>
<div class="container-fluid">
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
		<form class="form-signin" role="form" method="post" action="<?=base_url('login/forgot_password')?>">
			<h2 class="form-signin-heading">What's your e-mail?</h2>
			
			<?=$this->session->flashdata('message');?>
			<!-- username -->
			<div class="form-group">
				<?=form_error('email')?>
				<input class="form-control" type="text" class="centered" name="email" id="email" placeholder="E-mail" value="<?=set_value('email')?>" required autofocus/>
			</div>
			<div class="form-group">
			<!-- submit -->
				<input class="btn btn-lg btn-primary btn-block" type="submit" class="centered" name="submit" value="Let me pick a password" />
			</div>
		</form>
		<a href="<?=site_url('login')?>" class="pull-right">Login</a>
	</div>
	<div class="col-md-4">
	</div>
</div>