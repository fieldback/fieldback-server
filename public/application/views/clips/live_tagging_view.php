<?
//load template menu
// This page loads inside the template_view ;

//variables
$event_id 	= $event_info['event_id'];
$part_id 	= $part_info['part_id'];
?>
<section class="full">
	<!-- TAGFIELDS -->
	<div id="tagfield_selector" class="jumbotron">
		<div class="row">
			<div class="col-md-4">
				<button class="btn btn-default" id="stop_recording">stop recording</button>
				<h2><?=$event_info['event_name']?></h2>
				<h4><?=$part_info['part_name']?></h4>
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4 text-right">
				<div id="time-left"></div>
			</div>
		</div>
	</div>

	<!-- SHOW TAGFIELDS -->
	<div class="tagfields" style="height: 200px">
	</div>
</section>
<div class="moments">
</div>

<script type="text/javascript">
	//variables
	var event_id 		= '<?=$event_id?>';
	var part_id 		= '<?=$part_id?>';
	var part_url 		= '<?=base_url("videos/$event_id/$part_id/tmp_rec")?>/';
	var time_to_record 	= <?=$recordings[0]['time_to_record'];?>;
	var start_time	 	= <?=$recordings[0]['start_time'];?>;
	// Get timestamps to calculate the difference between local time and server time
	var server_time 	= <?=time();?>;
	var local_time		= parseInt(Math.round((new Date()).getTime() / 1000));
	var time_diff		= local_time - server_time;
	var recordTimeLeft;
	
	//doc ready
	$(document).ready(function(){
		//Start interval for checking time left to record
		intervalRecordTime();
		getMoments();
		getTagfields();
	});

	window.onresize = function(event){
		positionVideoField();
	};

	//check how much time is left
	function timeLeft(){
		// unix timestamp - minus the time_diff of server and local
		var now = Math.round((new Date()).getTime() / 1000) - time_diff;
		//difference between time started and now
		var difference = parseInt(now) - parseInt(start_time);
		//stop interval or give seconds left
		if(difference > time_to_record){
			stopIntervalRecordTime();
			//stop recording if time is over
			stopRecording();
		}else{
			var time = parseInt(time_to_record) - parseInt(difference);
			var hms = secondsToHMS(time);
			$('div#time-left').html('time left: <span class="badge">' + hms + '</span>');
		}
	}
	//seconds to h:m:s
	function secondsToHMS(s) {
	    var h = Math.floor(s/3600); //Get whole hours
	    s -= h*3600;
	    var m = Math.floor(s/60); //Get remaining minutes
	    s -= m*60;
	    return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
	}

	//start interval for time left
	function intervalRecordTime() {
		recordTimeLeft = setInterval(timeLeft, 1000);
	}

	//stop interval for time left
	function stopIntervalRecordTime() {
		clearInterval(recordTimeLeft);
	}


	//get moments for this part
	function getMoments()
	{
		$.ajax({
			type: 'POST',
			url: '<?=site_url('moments/get_moments_for_live_tagging')?>',
			dataType: 'html',
			data: {event_id: event_id, part_id: part_id},
			success: function(data){
				$('div.moments').html(data);
			}
		})
	}

	/**
	 * SHOW MOMENTS
	 */
	
	function positionVideoField(){
		var video_field = $('#video_field');
		width = video_field.width();
		height = video_field.height();
		margin_left = (width / 2) * -1;
		margin_top = (height / 2) * -1;
		video_field.css({
			'margin-left' :  margin_left + 'px',
			'margin-top' :  margin_top + 'px'
		});
	}

	function backFieldClose(){
		$('#back_field').on('click', function(){
			closeVideoField();
		});
	}

	function closeVideoField(){
		$('#video_field').hide();
		$('#back_field').hide();
	}

	//stop recordings
	$('#stop_recording').on('click', function(){
		stopRecording();
	});

	function stopRecording()
	{
		$.ajax({
			type: 'POST',
			url: '<?=site_url('clips/stop_recording/')?>',
			dataType: 'json',
			data: {event_id: event_id, part_id: part_id},
			success: function(data){
				window.location.replace('<?=site_url("events/event/")?>/' + event_id);
			}
		});
	}

	//TAGFIELDS 
	function getTagfields()
	{
		$.ajax({
			url: '<?=site_url('tagfields/tagfields_view')?>',
			type: 'POST',
			dataType: 'html',
			data: {event_id: event_id},
			success: function(data){
				$('.tagfields').html(data);
			}
		});
	}	
	
	
</script>