<?php // This page loads inside the template_view ?>

<div class="container">
	<!-- IF PART DOES NOT CONSIST ANY CLIPS -->
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Info</div>
			<div class="panel-container">
				<p>Upload to <br/> Part: <strong><?=$part_info['part_name']?></strong><br/>Event: <strong><?=$event_info['event_name']?></strong></p>
				<div id="clip_thumb">
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-success">
			<div class="panel-heading">Info</div>
			<div class="panel-container">
				<form role="form" enctype="multipart/form-data" method="post" action="<?=base_url('clips/do_upload_clip_to_part/' . $event_info['event_id'] . '/' . $part_info['part_id'])?>">
					<div class="form-group">
						<input class="fileupload" type="file" name="userfile" />
					</div>
				</form>
				<div id="uploading"></div>
				<div id="upload-message" class="error">
				</div>
				<div class="upload-results"><label>Upload progress</label></div>
				<div id="upload-progress" class="upload-progress progress">
					<div class="bar progress-bar" style="width: 0%;"></div>
				</div>
				<div class="encode-results"><label>Encoding progress</label></div>
				<div id="encode-progress" class="encode-progress progress">
					<div class="bar progress-bar" style="width: 0%;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var event_id = '<?=$event_info['event_id']?>';
	var part_id = '<?=$part_info['part_id']?>';
	/*UPLOAD PROGRESS*/
	$(function(){
		$('.fileupload').each(function(){
			$(this).fileupload({
		       dataType: 'html',
		       start: function (e){
		       		$('form').hide();
		       		$('#uploading').html('Uploading');
		       },
		       progressall: function (e, data) {
		       		var progress = parseInt(data.loaded / data.total * 100, 10);
		           	$('#upload-progress').show();
		           	$('#upload-progress').find('.bar').css(
		               'width',
		               progress + '%'
		           	);
		       },
		       done: function (e, data) {
		       		console.log(data);
		           $('#upload-message').html(data.result);
//		           //$('#upload-progress').hide();
		           $('#upload-progress').find('.bar').css(
		               'width',
		               '100%'
		           );
		           $('#uploading').html('Encoding');
		       }
		   });
		});
	});
	
	/*ENCODE PROGRESS*/
	window.setInterval(function(){
	 	$.ajax({
	 		type: 'POST',
	 		url: '<?=base_url('/clips/get_encode_progress_of_upload/')?>',
	 		data: {event_id: event_id, part_id: part_id},
	 		dataType: 'json',
	 		success: function(data){
	 			console.log(data);
	 			if(data['progress'] == 100){
	 				document.location.href = '<?=base_url()?>' + 'events/event/' + event_id 
	 			}
	 			if(data['progress'] == null){
	 			}else{
		 			$('#encode-progress').find('.bar').css({
		 				'width' : data['progress'] + '%'
		 			});
		 		}
	 		}
	 	});
	}, 5000);
</script>
