<div class="container">

    <? // Test to see if recording is active ?>
    <? if(!empty($instacordings)) { ?>

	<div class="row">
        <div class="col-xs-12">
            <section id="session_control" class="panel panel-default">
                <div class="panel-heading">
                    <button class="btn btn-default btn-xs pull-right" id="stop_instacording">stop session</button>
                    Capture control
                </div>
                <div class="panel-body">
                    <button type="button" class="btn btn-default btn-lg btn-block" id="btn-tag-moment" data-toggle="popover" data-container="body" data-placement="top" data-title="Capturing" data-content="" data-animation="false" data-trigger="manual">
                        Capture moment
                    </button>
                </div>
            </section>
        </div>
    </div>

    <? } ?>

	<div class="row">
        <div class="col-lg-12">
            <section class="panel panel-default">
                <div class="panel-heading">
                    Moments within this part.
                    <a href="<?=site_url("moments?event=".$event_info["event_id"]."&initial_view=play_all&filter_column=hide")?>" class="btn btn-xs btn-primary pull-right">
                        <span class="glyphicon glyphicon-film"></span> <span class="hidden-xs">open in </span>playmode
                    </a>
                </div>
                <div class="list-group" id="insta_moment_list"></div>
            </section>
    </div>
	</div>

</div>

<script type="text/javascript">
	//variables
	var event_id 		= '<?=$event_info['event_id'];?>';
	var part_id 		= '<?=$part_id?>';

    var interval;
    var timer = 0;
    var $new_moment;
    var capture_session_ids;
    var started_success = false;
    var minimumTimer;
    var timerExpired = false;
    var isDown = false;


    //doc ready
	$(document).ready(function(){

        var $tagbutton = $("#btn-tag-moment");
        // Create the popover that can be used to display the counter
        $tagbutton.popover();

        // On touch start or click start, start the capturing
        $tagbutton.on("touchstart mousedown", function(event){
            // Stop default stuff from happening
            event.preventDefault();
            event.stopPropagation();
            // Set the isDown value so to know that it's pressed for the body on touchup event
            isDown = true;
            // Set an interval of 2 seconds as this is the minimum capture time
            timerExpired = false;
            // After 3 seconds, set it to true
            minimumTimer = setTimeout(function() {
                timerExpired = true;
            }, 2000);
            // Show the time popover and disable the button
            $tagbutton.popover('show');
            $tagbutton.prop('disabled', true);
            $tagbutton.data('bs.popover').$tip.find(".popover-content").text("Capture initiated...");
            $tagbutton.text('Capturing moment...');
            // Do the request to start capture
            $.ajax({
                type: 'POST',
                url: '<?=site_url('moments/insta_create')?>',
                data: {
                    event_id: event_id,
                    part_id: part_id
                },
                dataType: 'json'
            }).success(function(data){
                // Reset the timer and reset the interval - used for the button - does not do anything usefull besides that
                timer = 0;
                clearInterval(interval);
                capture_start();
                // Create a new jQuery instance of the new moment and hide it
                $new_moment = $($.trim(data['moment_html'])).hide();
                // Save the instant session ids to be able to return it later
                capture_session_ids = data['capture_session_ids'];
                // Say that it started successfully - the mouseUp checks for this on mouseUp -> transmit the state of ajax
                started_success = true;
            }).fail(function(response) {
                window.alert("Not all camera's started recording, please check if they are all still connected. Try again or start a new session.");
                // Send a stop capture command as well
                console.log(response);
                endCapture();
            });
        });

        $(document).on('touchend mouseup', function(){
            if(isDown) {
                // Reset the isDown
                isDown = false;
                // Stop everything from happening
                event.preventDefault();
                event.stopPropagation();
                // Was the button release too soon or after a few seconds?
                if(timerExpired) {
                    // Reset timerExpired
                    timerExpired = false;
                    // End the capture
                    endCapture();
                } else {
                    $tagbutton.data('bs.popover').$tip.find(".popover-content").text("Please wait while finishing capture...");
                    clearInterval(interval);
                    // Wait another 3 seconds before ending capture
                    setTimeout(function() {
                        timerExpired = false;
                        endCapture();
                    }, <?=$system_info['instacording_timer_lapse'];?>);
                }
            }
        });

        var endCapture = function() {
            // Hide the time popover
            $tagbutton.popover('hide');
            // Reset the timer
            timer = 0;
            // Stop the interval as it would otherwise run endlessly
            clearInterval(interval);
            // Disable the button
            $tagbutton.text('Processing capture, please wait...');
            // Always stop capturing, even if it did not work. Some camera's might still be recording
            $.ajax({
                type: 'POST',
                url: '<?=site_url('moments/insta_stop')?>',
                data: {
                    event_id: event_id,
                    part_id: part_id,
                    capture_session_ids: capture_session_ids
                },
                dataType: 'json'
            }).success(function (data) {
                resetButton();
            }).fail(function(data) {
                window.alert("Not all camera's successfully stopped recording. Check if the recording LED's are still active, and try capturing again.");
                resetButton();
            });
        };

        var resetButton = function() {
            $tagbutton.text('Capture moment');
            $tagbutton.prop('disabled', false);
            if(started_success) {
                // Simply reload the moments and re-render the list
                getMoments();
            }
        };

        var capture_start = function() {
            interval = setInterval(updateTimer, 100);
            // Set the button content to 0
            writeTimer();
        };

        var updateTimer = function(){
            // increase the timer with 100 ms
            timer += 100;
            writeTimer();
        };

        var writeTimer = function() {
            if(timer > (<?=$system_info['max_instacord_seconds'];?> * 1000)) {
                $tagbutton.data('bs.popover').$tip.find(".popover-content").text("Max recording time reached, capture stopped. The maximum time for a recording is <?=$system_info['max_instacord_seconds'];?> seconds");
            } else {
                $tagbutton.data('bs.popover').$tip.find(".popover-content").text(formatTime(timer));
            }

        };

        $("#stop_instacording").on('click', function() {
            $.ajax({
                type: 'POST',
                url: '<?=site_url('clips/stop_instacording/')?>',
                dataType: 'json',
                data: {
                    event_id: event_id,
                    part_id: part_id
                },
                success: function(){
                    // Reload!
                    location.reload();
                }
            });
        });

        function getMoments()
        {
            $.ajax({
                url: '<?=base_url('moments/search')?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    part_ids: part_id
                }
            }).done(function(response){
                var moment_html = $.templates({markup: "#tmpl_moment-item", allowCode: true}).render(response);
                $insta_moment_list = $("#insta_moment_list");
                $insta_moment_list.html(moment_html);
                watch_images($insta_moment_list);
            });
        }

        getMoments();

        video_item_helper("#insta_moment_list", ".js_moment", "<?=site_url();?>", $("#tmpl_moment-item-within-list").html());
        video_button_helper("#insta_moment_list");

	});

</script>

<script type="text/html" id="tmpl_moment-item">
    <?php require("js/templates/moments/tmpl_moment-item.php"); ?>
</script>

<script type="text/html" id="tmpl_moment-item-within-list">
    <?php require("js/templates/moments/tmpl_moment-item-within-list.php"); ?>
</script>
