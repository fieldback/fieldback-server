<div class="modal fade" id="modal-loading">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body text-center">
				<img style="height: 50px; width: 50px;" src="<?=site_url('images/icons/loader.gif')?>" alt="loading..." />
				<p>Initializing cameras</p>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php // This page loads inside the template_view ?>

<div class="container">

<? if(!empty($cameras)) { ?>

	<form method="post" action="<?=base_url('clips/start_recording/' . $event_id . '/' . $part_id);?>">
	<div class="row">
		<!-- cameras -->
		<div class="col-md-6">
			<div id="thumbs">
				<div class="panel panel-success">
					<div class="panel-heading">
						Select streaming cameras <a href="javascript:;" class="btn btn-primary btn-xs btn-loading btn-refresh pull-right"><span class="glyphicon glyphicon-refresh"></span> Reload</a>
					</div>
                    <div class="panel-body">
					<?
					foreach($cameras as $camera){
						/*
							CAMERA VARS
								[camera_id]
							    [description]
							    [stream]
							    [number]
							    [active]
							    [cam_fps]
							    [cam_constante]
							    [copy]
						*/
						$camera_id = $camera['camera_id'];
						$description = $camera['description'];
						$number = $camera['number'];

						// random number for image unchache
						$rnd = rand(0,1000);
					?>
						<?if(is_file("./videos/$event_id/$part_id/tmp/screenshot$number.jpg")){?>
							<div class="camera">
								<p class="breadcrumb"><input data-description="<?=$description?>" type="checkbox" class="camera" value="1" name="camera[<?=$number?>][<?=$camera_id?>]" id="camera[<?=$number?>][<?=$camera_id?>]"/> <?= $camera['description']?></p>

								<label for="camera[<?=$number?>][<?=$camera_id?>]" class="thumbnail">
									<img src="<?=base_url("videos/$event_id/$part_id/tmp/screenshot$number.jpg?$rnd")?>" alt="screenshot" />
								</label>
							</div>
						<?}else{?>
                            <div class="camera">
                                <p class="breadcrumb">
                                    <?=$camera['description']?>
                                </p>
                                <div class="thumbnail">
                                    <img src="<?=base_url("images/icons/offline.png?" . $rnd)?>" alt="screenshot" />
                                </div>
                            </div>
						<?}?>
					<?}?>
                    </div>
				</div>
			</div>
		</div>
		<!-- ./ cameras -->

		<!-- settings -->
		<div class="col-md-6">
			<div id="settings" class="panel panel-success">
				<div class="panel-heading">Time</div>
				<div class="panel-container">

					<h4>Time to record</h4>
					<div class="form-group">
						<?= form_error('time[hours]'); ?>
						<small>Hours</small>
						<input type="text" class="form-control" name="time[hours]" id="hours" class="time" value="1" />
					</div>
					<div class="form-group">
						<small>Minutes</small>
						<input type="text" class="form-control" name="time[minutes]" id="minutes" class="time" value="30" />
					</div>
				</div>
			</div>
		</div>
		<!-- ./settings -->

		<!-- triggers -->
		<div class="col-md-6">
			<div id="settings" class="panel panel-success">
				<div class="panel-heading">Triggers</div>
				<div class="panel-container">

					<h4>Set automatic triggers</h4>

					<div class="form-group">
							<table class='table able-hover'>
								<thead>
									<tr>
										<td>&nbsp;</td>
										<td>Trigger name</td>
										<td>Lead</td>
                                        <td>Lapse</td>
										<td>Status</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									foreach ($sensors as $sensor): ?>
										<tr>
											<td><input type='checkbox' name='sensors[<?=$i?>][sensor_id]' value='<?= $sensor['id'] ?>'></td>
											<td> Trigger <?= $sensor['id'] ?> </td>
											<td><input type="number" name="sensors[<?=$i?>][lead]" value="5"></td>
											<td><input type="number" name="sensors[<?=$i?>][lapse]" value="5"></td>
											<td> <?= $sensor['status'] ?> </td>
										</tr>
									<?php
									$i++;
									endforeach;
									?>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-6">
			</div>
		<div class="col-md-6">
			<div id="buttons" class="panel panel-success buttons">
				<div class="panel-heading">Start</div>
				<div class="panel-container">
					<? if(!recording_in_progress()){?>
					<input id="submit" class="btn btn-primary" type="submit" name="submit" value="Start recording" disabled="disabled"/>
					<?}?>
				</div>
			</div>
		</div>
	</div>

	</div>
	</form>
	<!-- ./ settings -->


<? } ?>

    <!-- Instacameras - only when available -->
    <? if(!empty($instacams)) { ?>
	<div class="row">
		<form method="post" action="<?=base_url('clips/start_instacording/' . $event_id . '/' . $part_id);?>">
		<div class="col-md-6">
		   	<div class="panel panel-success">
				<div class="panel-heading">
					Select instant camera's <a href="javascript:;" class="btn btn-primary btn-xs btn-loading btn-refresh pull-right"><span class="glyphicon glyphicon-refresh"></span> Reload</a>
				</div>
				<div class="panel-body">
				<? $counter = 0; ?>
				<? foreach($instacams as $instacam) { ?>
					<?
					$counter++;
					// random number for image unchache
					$rnd = rand(0,1000);

					if(is_file($instacam['thumbnail']) && $instacam["online"] == true) {
					?>
						<div class="camera">
							<p class="breadcrumb">
								<input type="checkbox" class="instacam" value="<?=$instacam['camera_id']?>" name="instacam[<?=$counter?>]" id="instacam[<?=$counter?>]" /> <?=$instacam['description']?>
							</p>
							<label for="instacam[<?=$counter?>]" class="thumbnail">
								<img src="<?=base_url($instacam['thumbnail'] . "?" . $rnd)?>" alt="screenshot" />
							</label>
						</div>
					<?}else{?>
                        <div class="camera">
                            <p class="breadcrumb">
                                <?=$instacam['description']?>
                            </p>
                            <div class="thumbnail">
                                <img src="<?=base_url($instacam['thumbnail'] . "?" . $rnd)?>" alt="screenshot" />
                            </div>
                        </div>
					<?}?>
				<? } ?>
				</div>
			</div>
		</div>

        <!-- triggers -->
		<div class="col-md-6">
			<div id="settings" class="panel panel-success">
				<div class="panel-heading">Triggers</div>
				<div class="panel-container">

					<h4>Set automatic triggers</h4>

					<div class="form-group">
							<table class='table able-hover'>
								<thead>
									<tr>
										<td>&nbsp;</td>
										<td>Trigger name</td>
										<td>Lead</td>
                                        <td>Lapse</td>
										<td>Status</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									foreach ($sensors as $sensor): ?>
										<tr>
											<td><input type='checkbox' name='sensors[<?=$i?>][sensor_id]' value='<?= $sensor['id'] ?>'></td>
											<td> Trigger <?= $sensor['id'] ?> </td>
											<td><input type="number" name="sensors[<?=$i?>][lead]" value="5"></td>
											<td><input type="number" name="sensors[<?=$i?>][lapse]" value="5"></td>
											<td> <?= $sensor['status'] ?> </td>
										</tr>
									<?php
									$i++;
									endforeach;
									?>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					Settings
				</div>
				<div class="panel-body">
					<input id="submit_insta" class="btn btn-primary" type="submit" name="submit_insta" value="Start session" disabled="disabled"/>
				</div>
			</div>
		</div>

		</form>
	</div>
    <? } ?>

</div>



<script type="text/javascript">
	//variables
	var event_id = '<?=$event_id?>';
	var part_id = '<?=$part_id?>';

	//check if cams are selected when a checkbox is clicked
	$(function(){

		$('label.thumbnail').on('click', function(){
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).addClass('selected');
			}
		});

		// If someone clicks the checkbox, reflect this on the sibling container of the parent (<p>) (camera_container) as well
		$('input[type=checkbox]').change(function() {
	        if($(this).is(":checked")) {
	           	$(this).parent().siblings('label').addClass('selected');
	        }else{
	        	$(this).parent().siblings('label').removeClass('selected');
	        }
	        checkCamSel();
		});

    });

	$('input[type=text]').change(function(){
		checkCamSel();
	});

	$('.instacam').change(function() {
		var sel = false;
		$.each($('input.instacam'), function(){
			if($(this).is(':checked')){
				sel = true;
			}
		});
		if(sel) {
			$('input#submit_insta').removeAttr('disabled');
		} else {
			$('input#submit_insta').attr('disabled', 'disabled');
		}
	});

	//check if cameras are selected
	function checkCamSel()
	{
		var sel = false;
		if($('#hours').val() > 0 || $('#minutes').val() > 0){
			var time = true;
		}else{
			var time = false;
		}
		$.each($('input.camera'), function(){
			if($(this).is(':checked')){
				sel = true;
			}
		});
		if(sel === true && time === true){
			$('input#submit').removeAttr('disabled');
		}else{
			$('input#submit').attr('disabled', 'disabled');
		}
	}

	//clear temp files if page if left
	$(window).on('beforeunload', function() {
		clearTemp();
	});
	//clear temp directory
	function clearTemp() {
		$.ajax({
			type: 'POST',
			url: '<?=base_url('clips/clear_tmp/')?>',
			data: {event_id: event_id, part_id: part_id},
			dataType: 'json',
			async: false
		});
	}

	// This simply reloads the page so that the camera's are initialized again -> all happens on server side
	$('.btn-refresh').on('click', function(){
		location.reload();
	});

	$('.btn-loading').on('click', function(){
		$('#modal-loading').modal();
	});
</script>
