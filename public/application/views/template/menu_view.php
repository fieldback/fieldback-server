<?php
// only if the $user array exists, render the menu
//
if (!empty($user)) {
    ?>

    <nav class="navbar navbar-default" role="navigation" id="menu">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url('home') ?>">Fieldback</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- LEFT -->
                <?php
                if (is_player() || has_teams() || is_org_admin() || is_admin()) {
                    ?>
                    <ul class="nav navbar-nav">
                        <!-- Events -->
                        <li class=""><a href="<?= site_url('events') ?>"><span class="glyphicon glyphicon-flag"></span>
                                Events</a></li>
                        <!-- Moments -->
                        <li class=""><a href="<?= site_url('moments') ?>"><span class="glyphicon glyphicon-film"></span>
                                Moments</a></li>
                        <!-- Playlists -->
                        <li class=""><a href="<?= site_url('playlists') ?>"><span class="glyphicon glyphicon-bookmark"></span>
                                Playlists</a></li>

                        <?php
                        // Only show the live view link when there are live camera's available (this is checked at my_controller)
                        if ($live_camera_available == 1) {
                            ?>
                            <li class=""><a href="<?= site_url('live') ?>"><span
                                        class="glyphicon glyphicon-facetime-video"></span> Live view</a></li>
                        <?php

                        }
                    ?>
                    </ul>
                <?php

                }
    ?>


                <!-- RIGHT -->
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="<?= site_url('users/user/'.$user->id) ?>"><span class="glyphicon glyphicon-user"></span> <?= $user->username ?></a></li>
                    <li><a href="<?= site_url('home/choose_organisation') ?>"><span class="glyphicon glyphicon-globe"></span> <?= $user->selected_organisation['organisation_name'] ?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Settings <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php if (!$system_vars['sync_active']): ?>
                                <li>
                                    <a href="<?= site_url('my_settings') ?>">
                                    <span class="glyphicon glyphicon-user"></span>
                                    My account
                                    </a>
                                </li>
                            <?php endif ?>
                                <?php if (is_system_admin()) {
    ?>
                                <li><a href="<?= site_url('settings/system_settings') ?>"><span class="glyphicon glyphicon-cog"></span> System settings</a></li>
                            <?php

}
    ?>
                            <?php if (is_org_admin() || is_admin() && !$system_vars['sync_active']): ?>
                                <li class="divider"></li>
                                <li><a href="<?= site_url('settings/users') ?>"><span class="glyphicon glyphicon-user"></span> Users</a></li>
                                <li><a href="<?= site_url('settings/teams') ?>"><span class="glyphicon glyphicon-asterisk"></span> <?= lang('term_teams') ?></a>
                                </li>
                            <?php endif ?>

                            <?php if (has_teams() && !$system_vars['sync_active']): ?>
                                <li><a href="<?= site_url('settings/tagfields') ?>"><span class="glyphicon glyphicon-list-alt"></span> Tagfields</a></li>
                            <?php endif ?>
                            <?php if (has_teams()): ?>
                                <li><a href="<?= site_url('events/import') ?>"><span class="glyphicon glyphicon-import"></span> Import event</a></li>
                                <li class="divider"></li>
                            <?php endif ?>

                            <li><a href="<?= site_url('settings/help') ?>"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li>
                            <li><a href="<?= site_url('logout') ?>"><span class="glyphicon glyphicon-log-out"></span>
                                    Logout</a></li>
                        </ul>
                    </li>
                    <?php if ($system_info['is_online'] == 1) {  // Check if online or offline version of Fieldback -> show the bug/idea tracker? ?>
                        <li class="active"><a class="feedback" href="javascript:;"
                                              data-uri="<?= $this->uri->uri_string();
    ?>"><span
                                    class="glyphicon glyphicon-envelope"></span> Give us feedback</a></li>
                    <?php

}
    ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->

        </div>
    </nav>
    <ol class="breadcrumb">
        <?php if (has_teams() || $this->ion_auth->is_admin()): ?>
            <div class="pull-right">
                <?php if (recording_in_progress()): ?>
                    <?php if (is_admin() || is_org_admin()): ?>
                        <a href="<?= base_url('recordings') ?>"><span class="glyphicon glyphicon-record"></span>
                            <span class="hidden-xs">Recording in progress</span></a>
                    <?php else: ?>
                        <span class="glyphicon glyphicon-record"></span> <span class="hidden-xs">Recording in progress</span>
                    <?php endif ?>
                <?php else: ?>
                    <span class="glyphicon glyphicon-record"></span> <span class="hidden-xs">No active recording</span>
                <?php endif ?>
            </div>
        <?php endif ?>
        <?php
        if (!empty($breadcrumbs)) {
            foreach ($breadcrumbs as $breadcrumb) {
                if ($breadcrumb['state'] == 'active') {
                    echo "<li class='active'>".$breadcrumb['name'].'</li>';
                } else {
                    echo "<li><a href='".base_url().$breadcrumb['link']."'>".$breadcrumb['name'].'</a></li>';
                }
            }
        } else {
        }
    ?>
            <?php if ($system_vars['sync_active']) {
    ?>
                <div class="text-center">
                    <a href="#" id="sync_settings">
                        <span id="last_sync_label"></span>
                        <span id="last_sync_time" data-last-sync-time="<?php echo $system_info['last_sync'];
    ?>" data-current-server-time="<?php echo time();
    ?>"></span>
                    </a>
                </div>
            <?php

}
    ?>


    </ol>
    <script type="text/javascript">
        $(function () {
            $("ul.navigation").hide();
            $("#open-navigation").click(function () {
                $("#navigation").slideDown(200, function () {
                    $("body").click(function () {
                        $("#navigation").slideUp(200, function () {
                            $("#body").unbind("click");
                        });
                    });
                });
            });
            $("#open-settings").click(function () {
                $("#settings").slideDown(200, function () {
                    $("body").click(function () {
                        $("#settings").slideUp(200, function () {
                            $("#body").unbind("click");
                        });
                    });
                });
            });
        });
    </script>
<?php

}
?>
