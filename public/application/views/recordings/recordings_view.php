<?php // This page loads inside the template_view ?>
<div class="col-md-3">
</div>
<div class="col-md-6">
	<h3>Active recordings</h3>
	<?php if (!empty($recordings) || !empty($instacordings)): ?>
		<table class="table">
		<thead>
			<tr>
				<th>Started</th>
				<th>Owner</th>
				<th>Event</th>
				<th>Part</th>
			</tr>
		</thead>
		<? $i = 1; ?>
		<?php foreach ($recordings as $recording): ?>
			<tr>
				<td><?=date('d-m-Y h:i:s', $recording['start_time'])?></td>
				<td>
					<a href="<?= site_url('users/user/' . $recording['rec_owner']['id']) ?>"><?=$recording['rec_owner']['first_name']?> <?=$recording['rec_owner']['last_name']?></a>
				</td>
				<td>
					<? if(isset($recording['event_info']['event_id'])) {?>
						<a href="<?=base_url('events/event/' . $recording['event_info']['event_id'])?>"><?=$recording['event_info']['event_name']?></a>
					<? }else{ ?>
						Event verwijderd!
					<? } ?>
				</td>
				<td><?=$recording['part_info']['part_name']?></td>
				
			</tr>
			<? $i++ ?>
		<?php endforeach ?>
        <? $i = 1; ?>
        <?php foreach ($instacordings as $instacording): ?>
            <tr>
                <td><?=date('d-m-Y h:i:s', $instacording['session_start'])?></td>
                <td>
                    <?=$instacording['rec_owner']['first_name']?> <?=$instacording['rec_owner']['last_name']?>
                </td>
                <td>
                    <? if(isset($instacording['event_info']['event_id'])) {?>
                        <a href="<?=base_url('events/event/' . $instacording['event_info']['event_id'])?>"><?=$instacording['event_info']['event_name']?></a>
                    <? }else{ ?>
                        Event verwijderd!
                    <? } ?>
                </td>
                <td><?=$instacording['part_info']['part_name']?></td>

            </tr>
            <? $i++ ?>
        <?php endforeach ?>
		</table>
	<?php else: ?>
		<p>No recordings at the moment</p>
	<?php endif ?>

</div>
<div class="col-md-3">
</div>