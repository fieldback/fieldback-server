<?php // This page loads inside the template_view ?>
<div class="container">

	<!-- system settings -->
	<div class="col-md-4">
		<div class="panel panel-success">
		<div class="panel-heading">System settings</div>
	  		<?php if ($this->ion_auth->is_admin()): ?>
	  			<a class="btn btn-full-width" href="<?=base_url('settings/system_settings')?>">System settings</a>
                <?php if(!$system_vars['sync_active']): ?>
                <a class="btn btn-full-width" href="<?=base_url('settings/users')?>">Users</a>
                <?php endif ?>
	  		<?php endif ?>
	  	</div>
	</div>
	<!-- ./ system settings -->
    <?php if(!$system_vars['sync_active']): ?>
	<!-- settings -->
	<div class="col-md-4">
	  	<div class="panel panel-warning">
		<div class="panel-heading">Settings</div>
	  		<?php if (has_teams() || $this->ion_auth->is_admin()): ?>
	  			<a class="btn btn-full-width" href="<?=base_url('settings/teams')?>"><?=lang('term_teams')?></a>
	  			<a class="btn btn-full-width" href="<?=base_url('settings/tagfields')?>">Tagfields</a>
	  		<?php endif ?>
	  	</div>
	</div>
	<!-- ./ settings -->
    <?php endif ?>

	<!-- logout -->
	<div class="col-md-4">
		<div class="panel panel-success">
		<div class="panel-heading">Logout</div>
			<a class="btn btn-full-width" href="<?=base_url('logout')?>">Logout</a>
		</div>
	</div>
	<!-- ./ logout -->
</div>
