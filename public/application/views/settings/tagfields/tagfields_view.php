<?// This page loads inside the template_view ;?>
<?php if (has_teams() || is_admin()): ?>
	<?=$create_tagfield_form?>
<?php endif ?>
<? if(validation_errors() && (has_teams() || is_admin())) { ?>
	<script type="text/javascript">
		$('#create_tagfield_modal').modal();
	</script>
<?}?>

<div class="container">
	<div class="panel panel-success">
		<div class="panel-heading">
			Personal tagfields 
			<?if(has_teams()){?>
				<a class="btn btn-primary btn-xs pull-right hidden-xs" href="javascript:;" data-toggle="modal" data-target="#create_tagfield_modal"><span class="glyphicon glyphicon-plus"></span> Create new tagfield</a>
			<?}?>
		</div>
		<div class="panel-container">
			<!-- PERSONAL TAGFIELDS -->
			<ul class="list list-unstyled">
				<?php if (!empty($tagfields['personal'])): ?>
					<?foreach ($tagfields['personal'] as $tf) {?>
						<? if (empty($tf['tagfield_name'])): 
							$tf['tagfield_name'] = 'undefined';
						endif ?>
						<li>
							<a href="<?=base_url('settings/tagfield/' . $tf['tagfield_id'])?>"><?=$tf['tagfield_name'];?></a>
						</li>
					<?}?>
				<?php else: ?>
					<li>
						<p>No tagfields yet...</p>
					</li>
				<?php endif ?>
			</ul>
			
		</div>
	</div>
</div>