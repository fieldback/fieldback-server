<div class="modal fade" id="create_tagfield_modal">
  	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Create new tagfield</h4>
	 		 </div>
	  	<div class="modal-body">
			<!-- 
				
				description 

			-->

			<form role="form" action="<?=site_url('settings/create_tagfield')?>" method="post">
			  	<div class="form-group">
				    <label for="tagfield_name">Tagfield name</label>
				    <?= form_error('tagfield[tagfield_name]') ?>
				    <input type="text" class="form-control" id="tagfield_name" name="tagfield[tagfield_name]" placeholder="Tagfield name" value="<?=set_value('tagfield[tagfield_name]');?>">
			  	</div>
	  	</div>
	 	<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<input type="submit" class="btn btn-primary" value="Create tagfield!">
			</form>
		  </div>
		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->