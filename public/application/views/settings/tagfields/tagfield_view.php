<?// This page loads inside the template_view ;?>
<!-- first column -->
<section class="col-md-2 hidden-xs">
	<h3>All tags</h3>
	<?php if (is_admin()):  // only admins can create new tags?>
		<div class="btn-group">
			<a class="btn btn-default" href="<?=site_url('settings/modify_tags')?>">Create new tags</a>
		</div>
	<?php endif ?>
	<ul class="list-group list-unstyled big-label">
		<?foreach ($organisation_tags as $tag) { //show all tags that are here for the organisation ?>
			<li class="draggable_clone tag" data-id='<?=$tag['tag_id']?>'><span class="label label-warning"><?=$tag['tag_name']?></span></li>
		<?}?>
	</ul>
</section>
<!-- ./ first column -->

<!-- second column -->
<section class="col-md-7 hidden-xs">
	<div class="form-group">
		<h3>Tagfield</h3>
		<!-- name -->
		<div class="input-group">
			<span class="input-group-addon">Name</span>
			<input id="tagfield_name" class="form-control" type="text" placeholder="Tagfield name" value="<?if(!empty($tagfield_info['tagfield_name'])){echo $tagfield_info['tagfield_name'];}?>"/>
		</div>
	</div>
	<!-- The tagfield -->
	<div id="tagfield" class="droppable big-label">
		<?foreach ($tags as $tag) {?>
			<div class="draggable tag tag-tagfield" data-id="<?=$tag['tag_id']?>" data-original-id="<?=$tag['original_tag_id']?>" data-label="<?=$tag['label']?>" style="left: <?=$tag['left']?>%; top: <?=$tag['top'];?>%; position: absolute;"><span class="label label-warning"><?=$tag['label']?></span><a class="delete" style="font-size: 14pt;"><span class="glyphicon glyphicon-trash"></span></a></div>
		<?}?>
	</div>
	<!-- ./ the tagfield -->
	<a class="btn btn-primary" id="save" href="javascript:;">Save</a>
	<a class="btn btn-danger delete_button delete_tagfield" id="delete" href="<?=base_url('settings/delete_tagfield/' . $tagfield_info['tagfield_id'])?>">Delete <span class="glyphicon glyphicon-trash"></span></a>
	<form>
		<label class="checkbox">
		   	<input id="show_players" name="show_players" type="checkbox" value="1" <?if($tagfield_info['show_players'] == '1'){echo 'checked="checked"';}?> />Show <?=lang('term_players_l')?> 
		</label>
	</form>
</section>
<!-- ./ second column -->

<!-- third column -->
<section class="col-md-3 hidden-xs">
	<h3>Trigger tags</h3>
	<div id="trigger_tags">
		<?
			if(!empty($trigger_tags)){
				foreach ($trigger_tags as $trigger_tag) {?>
					<div class="trigger_tag">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								<input class="form-control name" type="text" placeholder="Name" value="<?=$trigger_tag['name']?>"/>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Lead</span>
								<input class="form-control lead" type="text" placeholder="Lead time (sec)" value="<?=$trigger_tag['lead']?>"/>
								<span class="input-group-addon">Lapse</span>
								<input class="form-control lapse" type="text" placeholder="Lapse time (sec)" value="<?=$trigger_tag['lapse']?>"/>
							</div>
						</div>
					</div>
					<hr>
				<?}
			}
		?>
	</div>
	<div class="clear"></div>
	<a id="add_trigger_tag" class="btn btn-default" href="javascript:;">+</a>
</section>
<!-- ./ third column -->

<section class="col-md-12 visible-xs">
	<div class="alert alert-warning">
		<b>Warning</b> It is not possible to change the tagfields on mobile devices
	</div>
</section>

<script type="text/javascript">
	//variables
	var tagfield_id = "<?=$tagfield_info['tagfield_id']?>";

	$(document).ready(function(){
		draggableClone();
		afterDropped();
		start();	
	});

	//trigger tags
	$('a#add_trigger_tag').on('click', function(){
		var trigger_tag = '<div class="trigger_tag "><div class="form-group"><div class="input-group"><span class="input-group-addon">Name</span><input class="form-control name" type="text" placeholder="Name"></div></div><div class="form-group"><div class="input-group"><span class="input-group-addon">Lead</span><input class="form-control lead" type="text" placeholder="Lead time (sec)"/><span class="input-group-addon">Lapse</span><input class="form-control lapse" type="text" placeholder="Lapse time (sec)"></div></div></div><hr>';
		$('#trigger_tags').append(trigger_tag);
	});

	//standard actions
	$('body').droppable({
		disabled : true
	});

	$('#save').on('click', function(){
		save();
	});

	$(".droppable").droppable({
		accept : ".tag",
		tolerance : "fit",
		activeClass : "active",
		drop: function(event, ui){
			var tagfieldOffset = $('#tagfield').offset();
			mousePositionX = event.pageX - tagfieldOffset.left;
			mousePositionY = event.pageY - tagfieldOffset.top;
			if(ui.draggable.hasClass('draggable_clone')){
				$( "<div class='draggable tag tag-tagfield' data-label='"+ui.draggable.text()+"' data-original-id='"+ui.draggable.data('id')+"'></div>" ).html('<span class="label label-warning">' + ui.draggable.text() + "</span>" ).append('<a class="delete"><span class="glyphicon glyphicon-trash"></span></a>').appendTo( this ).css({
					left : mousePositionX,
					top : mousePositionY,
					position: 'absolute'
				});
				ui.draggable.css({'visibility' : 'hidden'});
			}
			afterDropped();
		}
	});

	

	//messages
	$('.delete_tagfield').on('click', function(){
		return confirm('Are you sure you want to delete this tagfield?');
	});

	//functions
	
	/**
	 * This happens on loading the page
	 */
	function start(){
		$('.draggable').each(function(){
			var tag_id = $(this).data('original-id');
			$('.draggable_clone').each(function(){
				if($(this).data('id') == tag_id){
					$(this).css({
						'visibility' : 'hidden'
					});
				}
			});
		});
	}

	/**
	 * make all tags draggable
	 */
	function draggableClone()
	{
		$(".draggable_clone").draggable({
			appendTo : '#tagfield',
			helper : 'clone',
			snap : true,
			snapMode : 'outer',
			snapTolerance : 5,
			cursorAt : {left: 0, top: 0}
		});
	}

	/**
	 * after a tag is dropped in the tagfield, this happens:
	 *
	 * 1 all class = draggable items become draggable
	 * 2 all items get a delete function
	 */
	function afterDropped(){
		$(".draggable").draggable({
			containment : '#tagfield',
			cursor : 'crosshair',
			snap : true,
			snapMode : 'outer',
			snapTolerance : 5,
			cursorAt : {left: 0, top: 0}
		});
		$('a.delete').on('click', function(){
			var tag_id = $(this).parent().data('original-id');
			$('.tag[data-id='+tag_id+']').css({'visibility': 'visible'});
			$(this).parent().remove();
		});
	}

	/**
	 * save all the settings
	 * @return redirect to tagfields page.
	 */
	function save()
	{
		var tagfield_name = $('#tagfield_name');

		var checkTriggerTags = checkTriggerTags();
        //if(checkTriggerTags === true){
        if(true){
                //if tagfield has no name, select the tagfield name field
			if(tagfield_name.val() == ''){
				tagfield_name.css({
					'border' : '1px solid red'
				});
				tagfield_name.select();
				$(body).animate({scrollTop: '0px'}, 300);
			}else{
				var tags = {};
				var i = 0;
				$('#tagfield').children('.tag').each(function(){
					var tag = $(this);
					var positionLeft = tag.position().left;
					var positionTop = tag.position().top;
					
					var tagfieldPosition = $('#tagfield').offset();
					var tagfieldWidth = $('#tagfield').width();
					var tagfieldHeight = $('#tagfield').height();

					var percentageLeft = positionLeft / (tagfieldWidth / 100);
					var percentageTop = positionTop / (tagfieldHeight / 100);

					tags[i] = {};
					tags[i]['original_tag_id'] = tag.data('original-id');
					tags[i]['left'] = percentageLeft;
					tags[i]['top'] = percentageTop;
					tags[i]['label'] = tag.data('label');
					i++;
				});

				//get trigger tags + information
				var trigger_tags = {};
				var i = 0;
				$('.trigger_tag').each(function(index, key){
					var tt = $(this);
					trigger_tags[i] = {};
					trigger_tags[i]['name'] = tt.find('.name').val();
					trigger_tags[i]['lead'] = tt.find('.lead').val();
					trigger_tags[i]['lapse'] = tt.find('.lapse').val();
					i++;
				});

				if(jQuery.isEmptyObject(tags)){
					alert('You have to add tags to the tagfield');
				}else{
					var tf_name = tagfield_name.val();
					var show_players = $('#show_players').prop('checked');
					$.ajax({
						url: '<?=base_url('settings/add_tags_to_tagfield')?>',
						type: 'POST',
						dataType: 'json',
						data: {tags: tags, tagfield_name: tf_name, tagfield_id: tagfield_id, show_players: show_players, trigger_tags: trigger_tags},
						success: function(data){
							window.location.href = '<?=base_url('settings/tagfields')?>';
						}
					});
				}
			}
		}

		

		function checkTriggerTags()
		{
			var trigger_tags = $('.trigger_tag');
			var valid = true;

			if(trigger_tags.length < 1){
				return valid = false;
			}

			trigger_tags.each(function(i) {
				var trigger_tag = $(this);
				var lead = trigger_tag.find('.lead');
				var lapse = trigger_tag.find('.lapse');
				var name = trigger_tag.find('.name');

				var result = parseInt(lead.val()); + parseInt(lapse.val());
				if( name.val().length === 0 ){
					name.css({
						'border' : '1px solid red'
					});
					return valid = false;
				}
				if(result === 0 || isNaN(result) ){
					lead.css({
						'border' : '1px solid red'
					});
					lapse.css({
						'border' : '1px solid red'
					});
					return valid = false;
				}
				
			});

			return valid;
		}
	}
</script>