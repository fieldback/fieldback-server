<?// This page loads inside the template_view ;?>
<div class="col-md-3"></div>
<div class="col-md-3">
	<h3>Create tag</h3>
	<label>Create tag</label>
	<?=form_error('tag[tag_name]')?>
	<form class="form" method="post" action="<?=base_url('settings/modify_tags')?>">
		<div class="form-group">
			<input class="form-control" type="text" id="tag_name" name="tag[tag_name]" />
		</div>
		<div class="form-group">	
			<input class="btn btn-primary" type="submit" name="submit" value="Create">
		</div>
	</form>
</div>
<div class="col-md-3">
	<h3>Tags</h3>
	<ul class="list-group">
	<?php foreach ($organisation_tags as $tag): ?>
		<li class="list-group-item">
			<p><?=$tag['tag_name']?> <span class="badge"><?= count($tag['moments']) ?></span>
			<? if(count($tag['moments']) === 0){ ?>
				<span class="delete"><a class="pull-right" href="<?=site_url('settings/delete_tag_from_organisation/' . $tag['tag_id'])?>"><span class="glyphicon glyphicon-trash"></span></a></span></li>
			<? } ?>
	<?php endforeach ?>
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('input#tag_name').focus();
	});
</script>