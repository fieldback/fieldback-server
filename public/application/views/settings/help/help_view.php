<?php // This page loads inside the template_view ?>
<div class="container">
	<h3>Help pagina:</h3>
	<p>Het Fieldback Systeem is een videoanalyse systeem dat het makkelijk maakt om hele wedstrijden op te nemen en direct naast of op het veld momenten terug te kijken. Daarnaast is het een web-based systeem en kan iedereen direct vanaf thuis de momenten bekijken en terugkijken.</p> 

	<p>Het Systeem is onderverdeeld in Events en Moments. Events zijn wedstrijden/trainingen waar iets noemenswaardig gebeurd. Moments zijn interessante momenten (bv 10 seconden) binnen een event die u wilt opslaan en terug kijken.</p> 

	<p>Iedere gebruiker heeft een aparte login. Afhankelijk van de rol van de gebruiker heeft hij specifieke rechten. De rollen binnen het systeem zijn:</p>
	<ul class="list">
		<li>Technische directeur: Aanmaken van Teams / Spelers en koppelen van spelers aan teams.  Aanmaken van events en moments voor elk team.</li> 
		<li>Trainer: Aanmaken van Events en moments en persoonlijk tagfields. </li>
		<li>Speler: Bekijken van team/speler specifieke  events en moments </li>
	</ul>

	<p>Hieronder wordt het concept verder uitgelegd. </p>
	<div class="row">
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-flag"></span> Events:</strong>
			<p>Events zijn alle gebeurtenissen die je wilt opnemen en waar je lering uit wilt trekken. Deze events worden in zijn geheel opgenomen in verschillende parts. 
			Een voorbeeld van een Event is een wedstrijd. Maar ook een training van een bepaalde dag is een event. </p>
		</div>
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-adjust"></span> Parts:</strong>
			<p>Events zijn onderverdeeld in parts. Parts zijn bijvoorbeeld 1ste en 2de helft. Een part bestaat uit clips.  Als er met een camcorder een stuk film wordt opgenomen van een event kan deze later heel makkelijk worden toegevoegd als een losse part. </p>
		</div>
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-camera"></span> Clips:</strong>
				<p>Clips zijn fysieke film bestanden. Een part heeft vaak 2 fysieke film bestanden (Noord en Zuid zijde). </p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-facetime-video"></span> Moments:</strong>
			<p>Moments zijn getagte momenten in een Event. Zodra er iets noemenswaardig gebeurd in een event kan er op een knop gedruk worden en wordt er een moment aangemaakt. Dit moment wordt opgeslagen in een database en kan worden terug gezocht aan de hand van metadata. (Bijvoorbeeld: Datum, Gelinkte Speler, Gelinkte Tag, Event etc.)</p>
		</div>
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-pushpin"></span> Tags:</strong>
			<p>Om tijdens een event momenten te kunnen aanmaken zijn er tags nodig. Tags zijn labels die je aan een moment hangt. (Bijvoorbeeld doelpunt voor).  Het is mogelijk om meerdere tags aan een moment te hangen. De labels die aan het moment hangen zijn tevens zoekbaar in de momenten bibliotheek. </p>
		</div>
		<div class="col-md-4">
		<strong><span class="glyphicon glyphicon-pushpin"></span> Triggertags:</strong>
			<p>Triggertags zijn basis tags die definiëren hoelang het moment duurt. Elke triggertag heeft een Lead en Lapse time. De lead time is de tijd voor het moment van drukken en de lapse time de tijd na het moment van drukken. Dit is allemaal in seconden uitgedrukt. </p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-list-alt"></span> Tagfield:</strong>
			<p>Tagfield is een verzameling van tags en triggertags op een canvas.  Op dit canvas staan de tags en triggertags gerangschikt naar eigen indeling.  Er kunnen per gebruiker meerdere tagfields worden gemaakt. Het kan bijvoorbeeld makkelijk zijn om een tagfield te hebben voor de training en een ander tagfield voor de wedstrijd. </p>
		</div>
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-user"></span> Players:</strong>
			<p>Players zijn een speciaal soort tags.  Deze tags hebben namelijk direct betrekking op de individuele speler. Zodra een player wordt gekoppeld aan een moment dan zal deze direct (prive) zichtbaar worden voor een speler als hij inlogt.  Zo is het mogelijk voor een trainer om speciale momenten toe te wijzen aan een speler.  </p>
		</div>
		<div class="col-md-4">
			<strong><span class="glyphicon glyphicon-globe"></span> Organisation:</strong>
			<p>Organisation is de Hockey Club waar het systeem op draait. In dit geval is de organisatie HCAS. </p>
		</div>
	</div>	
</div>