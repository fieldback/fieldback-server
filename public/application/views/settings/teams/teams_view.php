<?// This page loads inside the template_view ;?>
<div class="container">
	<div class="panel panel-success">
		<div class="panel-heading">My <?=lang('term_teams_l')?> <a class="btn btn-primary btn-xs pull-right" href="<?=site_url('settings/add_team')?>"><span class="glyphicon glyphicon-plus"></span> Add new <?=lang('term_team_l')?></a></div>
		<div class="panel-container">
			<ul class="list-unstyled">
				<?foreach ($teams as $team) {?>
					<li class="team"><a href="<?=base_url('settings/team/' . $team['team_id'])?>"><?=$team['team_name']?></a></li>
				<?}?>
			</ul>
		</div>
	</div>
</div>