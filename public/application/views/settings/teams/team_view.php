<?php // This page loads inside the template_view ?>

<div class="container">
	<!-- left -->
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading"><?=lang('term_team')?>_info</div>
			<div class="panel-container">
				<small><?=lang('term_team')?> name</small>
				<p><?=$team_info['team_name']?></p>
			</div>
		</div>
	</div>

	<!-- center -->
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Owners</div>
			<div class="panel-container">
				<div id="add_team_owner">
					<h4>Search person to add</h4>
					<form role="form" action="javascript:;" method="post">
						<input class="form-control autocomplete_owners" type="text" />
					</form>
					<span class="error"></span>
				</div>
				<h4>Owners of the <?=lang('term_team_l')?></h4>
				<ul id="owners_list" class="list list-striped list-group">
					Loading owners...
				</ul>
			</div>
		</div>
	</div>

	<!-- right -->
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Add <?=lang('term_player_l')?></div>
			<div class="panel-container">
				<div id="add_player">
					<h4>Search <?=lang('term_player_l')?> to add</h4>
					<form action="javascript:;" method="post">
						<input class="form-control autocomplete_players" type="text" />
					</form>
					<span class="error"></span>
				</div>
				<h4><?=lang('term_players')?> of the <?=lang('term_team_l')?></h4>
				<ul id="players_list" class="list list-striped list-group">
					Loading <?=lang('term_players_l')?>...
				</ul>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
	var team_id = '<?=$team_info['team_id']?>';

	$(document).ready(function(){
		getAllPlayersForTeam();
		getAllOwnersForTeam();
	});

	/*AUTOCOMPLETE SEARCH*/
	//players
	$('.autocomplete_players').autocomplete({
	    serviceUrl: '<?=base_url('settings/autocomplete_players')?>',
	    onSelect: function (suggestion) {
	    	addPlayerToTeam();
	    }
	});

	$('.autocomplete_players').keypress(function(e){
		if(e.which == 13){
			addPlayerToTeam();
		}
	});

	//team_owners
	$('.autocomplete_owners').autocomplete({
	    serviceUrl: '<?=base_url('settings/autocomplete_players')?>',
	    onSelect: function (suggestion) {
	    	addOwnerToTeam();
	    }
	});

	$('.autocomplete_owners').keypress(function(e){
		if(e.which == 13){
			addOwnerToTeam();
		}
	});

	function addPlayerToTeam()
	{
		var value = $('.autocomplete_players').val();
		$.ajax({
			url: '<?=base_url('settings/add_player_to_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {value: value, team_id: team_id},
			success: function(data){
				if(data.error !== null){
					$('span.error').html(data.error);
				}
				getAllPlayersForTeam();
				$('input').val('');
			}
		});
	}

	function addOwnerToTeam()
	{
		var value = $('.autocomplete_owners').val();
		$.ajax({
			url: '<?=base_url('settings/add_owner_to_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {value: value, team_id: team_id},
			success: function(data){
				if(data.error !== null){
					$('span.error').html(data.error);
				}
				getAllOwnersForTeam();
				$('input').val('');
			}
		});
	}

	function getAllPlayersForTeam()
	{
		$.ajax({
			url: '<?=site_url('settings/get_all_players_for_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {team_id: team_id},
			success: function(data){
				$('#players_list').empty();
				$.each(data, function(){
					$('#players_list').append('<li class="list-group-item">' + this.first_name + ' ' + this.last_name + '<a type="button" href="javascript:;" class="pull-right remove-player" data-pid="'+this.player_id+'"><span class="glyphicon glyphicon-trash"></a></button></li>');
				});

				$(".remove-player").confirm({
				    text: "Are you sure you want to delete that <?=lang('term_player_l')?> from the <?=lang('term_team_l')?>?",
				    title: "Confirmation required",
				    confirm: function(button) {
				        removePlayer(button);
				    },
				    cancel: function(button) {
				        // do something
				    },
				    confirmButton: "Yes I am",
				    cancelButton: "No",
				    post: false
				});
			}
		});
	}

	function getAllOwnersForTeam()
	{
		$.ajax({
			url: '<?=site_url('settings/get_all_owners_for_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {team_id: team_id},
			success: function(data){
				$('#owners_list').empty();
				$.each(data, function(){
					$('#owners_list').append('<li class="list-group-item">' + this.first_name + ' ' + this.last_name + '<a type="button" href="javascript:;" class="pull-right remove-owner" data-uid="'+this.user_id+'"><span class="glyphicon glyphicon-trash"></a></button></li>');
				});

				$(".remove-owner").confirm({
				    text: "Are you sure you want to delete that owner from the <?=lang('term_team_l')?>?",
				    title: "Confirmation required",
				    confirm: function(button) {
				        removeOwner(button);
				    },
				    cancel: function(button) {
				        // do something
				    },
				    confirmButton: "Yes I am",
				    cancelButton: "No",
				    post: false
				});
			}
		});
	}

	function removePlayer(button)
	{
		var player_id = button.data('pid');
		$.ajax({
			url: '<?=site_url('settings/remove_player_from_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {player_id: player_id, team_id: team_id},
			success: function(data){
				getAllPlayersForTeam();
			}
		});
	}

	function removeOwner(button)
	{
		var user_id = button.data('uid');
		$.ajax({
			url: '<?=site_url('settings/remove_owner_from_team')?>',
			type: 'POST',
			dataType: 'json',
			data: {user_id: user_id, team_id: team_id},
			success: function(data){
				getAllOwnersForTeam();
			}
		});
	}
</script>