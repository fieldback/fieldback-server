<?// This page loads inside the template_view ;?>
<div class="col-md-4">
	&nbsp;
</div>
<div class="col-md-4">
	<h3>Add <?=lang('term_team_l')?></h3>
	<form role="form" id="add_team" method="post" action="<?=base_url('settings/add_team')?>">
		<div class="form-group">
			<label><?=lang('term_team_l')?> name <small>( ex.: Heren 1 )<small></label>
			<?=form_error('team[team_name]')?>
			<input class="form-control" type="text" name="team[team_name]"/>
		</div>
		<div class="form-group">
			<input class="btn btn-primary" type="submit" name="submit">
		</div>
	</form>
</div>
<div class="col-md-4">
	&nbsp;
</div>