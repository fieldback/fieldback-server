<?php // This page loads inside the template_view ?>
<div class="container">
	<form method="post" action="<?=base_url('settings/system_settings')?>">
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">System settings</div>
			<div class="panel-container">
			<?
				foreach ($settings as $key => $value) {?>
					<span><small><?=$key?></small></span>
					<div class="input-group">
						 <span class="input-group-addon"></span>
						<input type="text" class="form-control" name="setting[<?=$key?>]" value="<?=$value?>" />	
					</div>
				<?}?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<!-- CAMERA SETTINGS -->
		<div class="cameras">
			<div class="panel panel-success">
				<div class="panel-heading">Camera settings <a href="javascript:;" id="add_camera" class="btn btn-primary btn-xs pull-right">+ Add camera</a></div>
				<div class="panel-container">
					<?
					$i = 1;
					foreach ($cameras as $camera) {?>
						<input type="hidden" name="camera[<?=$i?>][camera_id]" value="<?=$camera['camera_id']?>" />
						
						<div class="input-group">
							<small>Camera <?=$i?></small>
						</div>
						<a class="btn btn-danger btn pull-right" href="<?=base_url('settings/delete_camera') . '/' . $camera['camera_id']?>/"><span class="glyphicon glyphicon-trash"></span></a>
						
						<div class="input-group">
							<span class="input-group-addon"></span>
							<input type="text" class="form-control" placeholder="name" name="camera[<?=$i?>][description]" value="<?=$camera['description']?>"/>
						</div>

						<div class="input-group">
							<span class="input-group-addon"></span>
							<input type="text" class="form-control" placeholder="stream" name="camera[<?=$i?>][stream]" value="<?=$camera['stream']?>"/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"></span>
							<input type="text" class="form-control" placeholder="stream live" name="camera[<?=$i?>][stream_live]" value="<?=$camera['stream_live']?>"/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"></span>
							<input type="text" class="form-control" placeholder="stream live preview" name="camera[<?=$i?>][stream_live_preview]" value="<?=$camera['stream_live_preview']?>"/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"></span>	
							<input type="text" class="form-control" placeholder="frames per second" name="camera[<?=$i?>][cam_fps]" value="<?=$camera['cam_fps']?>"/>
						</div>
						<div class="input-group">
							<span class="input-group-addon"></span>
							<input type="text" class="form-control" placeholder="cam constante" name="camera[<?=$i?>][cam_constante]" value="<?=$camera['cam_constante']?>"/>
						</div>
						<div class="input-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="camera[<?=$i?>][copy]" value="1" <?if($camera['copy'] == '1'){echo 'checked';}?>/> Copy
								</label>
							</div>
							<div class="checkbox">
								<label>	
									<input type="checkbox" name="camera[<?=$i?>][active]" value="1" <?if($camera['active'] == '1'){echo 'checked';}?>/> Active
								</label>
							</div>
							<div class="checkbox">
								<label>	
									<input type="checkbox" name="camera[<?=$i?>][live_enabled]" value="1" <?if($camera['live_enabled'] == '1'){echo 'checked';}?>/> Live enabled
								</label>
							</div>
							<div class="checkbox">
								<label>	
									<input type="checkbox" name="camera[<?=$i?>][is_instacam]" value="1" <?if($camera['is_instacam'] == '1'){echo 'checked';}?>/> Instant camera (high speed/no stream)
								</label>
							</div>
						</div>

						<hr>
					<?
					$i++;
					}
					$num_cams = $i;
					?>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-4">
		<h3>Action</h3>
		<a class="cancel btn btn-default" href="<?=base_url('home')?>">Cancel</a>
		<input class="btn btn-primary" type="submit" name="submit"/>
	</div>
	
	</form>
	
</div>

<script type="text/javascript">
	var num_cams = <?=$num_cams?>;

	$('#add_camera').on('click', function(){
		// A bit redundant but add another form with empty values
		var input = '' +
			'<div class="input-group">' +
			'	<small>Camera ' + num_cams + '</small>' +
			'</div>' +
			'' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>' +
			'	<input type="text" class="form-control" placeholder="name" name="camera[' + num_cams + '][description]" value=""/>' +
			'</div>' +
			'' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>' +
			'	<input type="text" class="form-control" placeholder="stream" name="camera[' + num_cams + '][stream]" value=""/>' +
			'</div>' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>' +
			'	<input type="text" class="form-control" placeholder="stream live" name="camera[' + num_cams + '][stream_live]" value=""/>' +
			'</div>' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>' +
			'	<input type="text" class="form-control" placeholder="stream live preview" name="camera[' + num_cams + '][stream_live_preview]" value=""/>' +
			'</div>' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>	' +
			'	<input type="text" class="form-control" placeholder="frames per second" name="camera[' + num_cams + '][cam_fps]" value=""/>' +
			'</div>' +
			'<div class="input-group">' +
			'	<span class="input-group-addon"></span>' +
			'	<input type="text" class="form-control" placeholder="cam constante" name="camera[' + num_cams + '][cam_constante]" value=""/>' +
			'</div>' +
			'<div class="input-group">' +
			'	<div class="checkbox">' +
			'		<label>' +
			'			<input type="checkbox" name="camera[' + num_cams + '][copy]" value="1" checked /> Copy' +
			'		</label>' +
			'	</div>' +
			'	<div class="checkbox">' +
			'		<label>	' +
			'			<input type="checkbox" name="camera[' + num_cams + '][active]" value="1" checked /> Active' +
			'		</label>' +
			'	</div>' +
			'	<div class="checkbox">' +
			'		<label>	' +
			'			<input type="checkbox" name="camera[' + num_cams + '][live_enabled]" value="1" /> Live enabled' +
			'		</label>' +
			'	</div>' +
			'	<div class="checkbox">' +
			'		<label>	' +
			'			<input type="checkbox" name="camera[' + num_cams + '][is_instacam]" value="1" /> Instant camera (high speed/no stream)' +
			'		</label>' +
			'	</div>' +
			'</div>' +
			'' +
			'<hr>' +
		'';

		$('.cameras .panel-container').append(input);
		num_cams++;
	});
</script>