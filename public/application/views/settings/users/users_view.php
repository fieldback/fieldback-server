<?// This page loads inside the template_view ;?>
<!-- container -->
<div class="container">
	
	<!-- column new user -->
	<div class="col-md-4">

		<!-- panel -->
		<div class="panel panel-success">
			<div class="panel-heading">Add new <?=lang('term_player_l')?></div>
			<!-- panel container -->
			<div class="panel-container">

				<form role="form" method="POST" action="<?=site_url('settings/users')?>">
					<?=form_error('user[first_name]')?>
					<div class="form-group">
						<small>First name</small>
						<input class="form-control" type="text" placeholder="First name" name="user[first_name]" value="<?=set_value('user[first_name]')?>" />
					</div>
					<?=form_error('user[last_name]')?>
					<div class="form-group">
						<small>Last name</small>
						<input class="form-control" type="text" placeholder="Last name" name="user[last_name]" value="<?=set_value('user[last_name]')?>" />
					</div>
					<?=form_error('user[email]')?>
					<div class="form-group">
						<small>E-mail</small>
						<input class="form-control" type="text" placeholder="E-mail" name="user[email]" value="<?=set_value('user[email]')?>"/>
					</div>
					<div class="form-group">
						<!-- ROLE -->
						<small>Role</small>
						<select class="form-control" name="role">

						<?foreach($roles as $role){?>
							<?php if ($role['level'] == 'user'): ?>
								<option value="<?=$role['user_level_id']?>" selected><?=$role['level']?></option>
							<?php else: ?>
								<option value="<?=$role['user_level_id']?>"><?=$role['level']?></option>
							<?php endif ?>
							
						<?}?>
						
						</select>
					</div>
					<div class="form-group">
						<small><?=lang('term_team')?></small>
						<select id="select-team" name="team[team_id]" class="form-control">
							<option value="0">No <?=lang('term_team_l')?></option>
							<?php foreach ($teams as $team): ?>
								<option value="<?=$team['team_id']?>"><?=$team['team_name']?></option>
							<?php endforeach ?>
						</select>
						<div class="breadcrumb select-role-team" style="display: none">
							<p><input type="checkbox" name="team[player]" value="1" /> <?=lang('term_player')?> of <?=lang('term_team_l')?></p>
							<p><input type="checkbox" name="team[owner]" value="1" /> Owner of <?=lang('term_team_l')?></p>
						</div>
					</div>
					<input class="btn btn-primary" type="submit" name="submit" id="submit" value="Add user" /> 
				</form>
			</div> <!-- ./ panel container -->
		</div> <!-- ./ panel -->
	</div> <!-- ./ column new user -->
	
	<!-- column new user -->
	<div class="col-md-4">

		<!-- panel -->
		<div class="panel panel-success">
			<div class="panel-heading">Users</div>
			<!-- panel container -->
			<div class="panel-container">
				<div id="users">
					<?foreach ($users as $user) {?>
						<div class="user">
							<?php if ($user['active'] === '1'): ?>
								<a href="<?=base_url('users/user/' . $user['id'])?>"><?=$user['first_name']?> <?=$user['last_name']?></a>
							<?php else: ?>
								<a class="user-inactive" href="<?=base_url('users/user/' . $user['id'])?>"><?=$user['first_name']?> <?=$user['last_name']?> (inactive)</a>
							<?php endif ?>
						</div>
					<?}?>
				</div>
			</div> <!-- ./ panel container -->
		</div> <!-- ./ panel -->
	</div> <!-- ./ column new user -->
</div>
<!-- ./ container -->

<script type="text/javascript">
	$(document).ready(function(){
		$('input').first().focus();
	});

	$('#select-team').on('change', function(){
		var value = $('#select-team').val();
		if(value !== '0'){
			$('.select-role-team').show();
		}else{
			$('.select-role-team').hide();
		}
	});
</script>