<div id="tag-modal" class="modal fade tags">
	<div class="modal-dialog modal-wide">
		<div class="modal-content">
			<div class="modal-header">
				<h2>Tag</h2>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<ul class="nav nav-tabs tagfields_tab" role="tablist">
					<!-- Show the tagfieldnames, so the user can switch -->
					<? $active = 'active'; ?>
					<?php foreach ($tagfields as $tagfield): ?>
						<li id="tagfield<?=$tagfield['tagfield_id']?>"><a href="#<?=$tagfield['tagfield_id']?>" role="tab" data-toggle="tab"><?=$tagfield['tagfield_name']?></a></li>
						<? $active = ''; ?>
					<?php endforeach ?>
					</ul>

					<div class="tab-content">
					<!-- Show the tags for the tagfields -->
					<? $active = 'active'; ?>
					<?php foreach ($tagfields as $tagfield): ?>
						<div class="tab-pane" id="<?=$tagfield['tagfield_id']?>">
							<div class="tags_overview col-md-8">
								<?foreach ($tagfield['tags'] as $tag) {?>
									<? $tag_added = ''; ?>
									<? if(isset($selected_tags)){
										foreach($selected_tags as $selected_tag) {
											if($selected_tag['tag_id'] === $tag['original_tag_id']){
												$tag_added = 'tag_added';
											}
										}
									}?>
									<a class="tag_button btn btn-warning <?=$tag_added?>" data-tag-id="<?=$tag['original_tag_id']?>" data-label="<?=$tag['label']?>" data-kind="tag" href="javascript:;" style="left: <?=$tag['left']?>%; top: <?=$tag['top']?>%;"><?=$tag['label']?></a>
								<?}?>
							</div>
							<div class="players col-md-4">
								<h3><?=lang('term_players')?></h3>
								<? if($tagfield['show_players'] == 1){?>
									<?foreach ($event_info['players'] as $player) {?>
										<? $tag_added = ''; ?>
										<? if(isset($selected_players)){
											foreach($selected_players as $selected_player) {
												if($selected_player['player_id'] === $player['id']){
													$tag_added = 'tag_added';
												}
											}
										}?>
										<div class="row">
										<div class="col-md-6">
											<a class="player_button btn btn-info col-md-12 <?=$tag_added?>" data-tag-id="<?=$player['player_id']?>" data-label="<?=$player['first_name'] . ' ' . $player['last_name']?>" data-kind="player" href="javascript:;"><?=$player['first_name']?> <?=$player['last_name']?></a>
										</div>
										</div>
									<?}?>
								<?}?>
							</div>
						</div>
						<? $active = ''; ?>
					<?php endforeach ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			   	<button class="btn btn-primary save-data" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div>
	</div>
</div>	



<script type="text/javascript">

	<? if ( isset($moment_id) ){ ?>
		var moment_id = '<?= $moment_id ?>';
	<? } ?>

	$(function () {
    	//$('.tagfield a:first').tab('show');
 	});

	//close the add tag screen
	$('.save-data').on('click', function(){
		//$('.trigger_tags').show();
		//$('.tags').hide();
		$('.tag_button, .player_button').removeClass('tag_added');
		<? if ( $live ){ ?>
			getMoments();		
		<? }else{ ?>
			location.reload();
		<? } ?>
		
	});
	
	// tagging to moment
	$('a.tag_button, a.player_button').on('click', function(){
		var tag = $(this);
		<? if ( !isset($moment_id) ){ ?>
			var moment_id = $('.tags').attr('data-moment-id');
		<? } ?>
		var label = tag.data('label');
		var tag_id = tag.data('tag-id');
		if( tag.hasClass('tag_button') ){
			var same_tags = $(".tag_button[data-tag-id='" + tag_id + "']");
		}
		if( tag.hasClass('player_button') ){
			var same_tags = $(".player_button[data-tag-id='" + tag_id + "']");
		}
		var kind = tag.data('kind');
		//add tag to moment
		if(tag.hasClass('tag_added') == false){
			$.ajax({
				type: 'POST',
				url: '<?=site_url('moments/add_tag_to_moment')?>',
				dataType: 'json',
				data: {moment_id: moment_id, tag_id: tag_id, label: label, kind: kind},
				success: function(data){
					same_tags.addClass('tag_added');
				}
			});
		//if moment was tagged, delete it when click again.
		}else{
			$.ajax({
				type: 'POST',
				url: '<?=site_url('moments/delete_tag_from_moment')?>',
				dataType: 'json',
				data: {moment_id: moment_id, tag_id: tag_id, kind: kind},
				success: function(data){
					same_tags.removeClass('tag_added');
				}
			})
		}
		$('.tags_overview').click();
	});




</script>
