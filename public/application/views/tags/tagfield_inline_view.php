<div class="col-xs-12">
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" id="tagfields_tab" role="tablist">
                <!-- Show the tagfieldnames in tabs, so the user can switch -->
                <?php foreach ($tagfields as $tagfield): ?>
                    <li id="tagfield<?=$tagfield['tagfield_id']?>" data-tagfield-id="<?php echo $tagfield['tagfield_id'];?>">
                        <a href="#field<?=$tagfield['tagfield_id']?>" role="tab" data-toggle="tab" data-tagfield-id="<?php echo $tagfield['tagfield_id'];?>">
                            <?=$tagfield['tagfield_name']?>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <!-- Show the tags for the tagfields -->
                <? $active = 'active'; ?>
                <?php foreach ($tagfields as $tagfield): ?>
                    <div class="tab-pane" id="field<?php echo $tagfield['tagfield_id']?>">
                        <div class="tags_overview col-md-8" style="border-right: 1px solid #E9e9e9">
                            <?foreach ($tagfield['tags'] as $tag) {?>
                                <? $tag_added = ''; ?>
                                <? if(isset($selected_tags)){
                                    foreach($selected_tags as $selected_tag) {
                                        if($selected_tag['tag_id'] === $tag['original_tag_id']){
                                            $tag_added = 'tag_added';
                                        }
                                    }
                                }?>
                                <a class="tag_button btn btn-warning <?=$tag_added?>" data-tag-id="<?=$tag['original_tag_id']?>" data-label="<?=$tag['label']?>" data-kind="tag" href="javascript:;" style="left: <?=$tag['left']?>%; top: <?=$tag['top']?>%;">
                                    <?=$tag['label']?>
                                </a>
                            <?}?>
                        </div>
                        <div class="players col-md-4">
                            <h3><?=lang('term_players')?></h3>
                            <? if($tagfield['show_players'] == 1){?>
                                <?foreach ($event_info['players'] as $player) {?>
                                    <? $tag_added = ''; ?>
                                    <? if(isset($selected_players)){
                                        foreach($selected_players as $selected_player) {
                                            if($selected_player['player_id'] === $player['id']){
                                                $tag_added = 'tag_added';
                                            }
                                        }
                                    }?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p>
                                                <a class="player_button btn btn-info btn-sm <?=$tag_added?>" data-tag-id="<?=$player['player_id']?>" data-label="<?=$player['first_name'] . ' ' . $player['last_name']?>" data-kind="player" href="javascript:"><?=$player['first_name']?> <?=$player['last_name']?></a>
                                            </p>
                                        </div>
                                    </div>
                                <?}?>
                            <?}?>
                        </div>
                    </div>
                    <? $active = ''; ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    <? if ( isset($moment_id) ){ ?>
    var moment_id = '<?= $moment_id ?>';
    <? } ?>

    // get the default selected tagfield for this user
    var selected_tagfield =
    <?php
        if(!empty($selected_tagfield))
        {
            if(array_key_exists("selected_tagfield", $selected_tagfield) && $selected_tagfield['selected_tagfield'] != NULL) {
                echo $selected_tagfield['selected_tagfield'];
            } else {
                echo 0;
            }
        } else {
            echo 0;
        };
    ?>;


    (function($){
        $('a[data-tagfield-id=' + selected_tagfield + ']').tab('show');

        $('#tagfields_tab').on('click', "li", function(){
            var tagfield_id = $(this).data('tagfield-id');
            console.log("updating selected tagfield id to "+tagfield_id);
            update_selected_tagfield_for_user(tagfield_id);
        });

        // tagging to moment
        $('a.tag_button, a.player_button').on('click', function(){
            var tag = $(this);
            <? if ( !isset($moment_id) ){ ?>
            var moment_id = $('.tags').attr('data-moment-id');
            <? } ?>
            var label = tag.data('label');
            var tag_id = tag.data('tag-id');
            if( tag.hasClass('tag_button') ){
                var same_tags = $(".tag_button[data-tag-id='" + tag_id + "']");
            }
            if( tag.hasClass('player_button') ){
                var same_tags = $(".player_button[data-tag-id='" + tag_id + "']");
            }
            var kind = tag.data('kind');
            // Give an edit status
            /*
            $btn_edit_tags = $("#btn_edit_moment");
            $btn_edit_tags.prop("disabled", true);
            $btn_edit_tags.html("<span class='glyphicon glyphicon-floppy-save'></span>" +
                " <span class='hidden-xs'>Saving tags</span>");
             */
            //add tag to moment
            if(tag.hasClass('tag_added') == false){
                $.ajax({
                    type: 'POST',
                    url: '<?=site_url('moments/add_tag_to_moment')?>',
                    dataType: 'json',
                    data: {moment_id: moment_id, tag_id: tag_id, label: label, kind: kind},
                    success: function(data){
                        same_tags.addClass('tag_added');
                        sync_tags();
                    }
                });
                //if moment was tagged, delete it when click again.
            }else{
                $.ajax({
                    type: 'POST',
                    url: '<?=site_url('moments/delete_tag_from_moment')?>',
                    dataType: 'json',
                    data: {moment_id: moment_id, tag_id: tag_id, kind: kind},
                    success: function(data){
                        same_tags.removeClass('tag_added');
                        sync_tags();
                    }
                })
            }
        });

        function sync_tags() {
            /*
            $btn_edit_tags = $("#btn_edit_moment");
            $btn_edit_tags.prop("disabled", true);
            $btn_edit_tags.html("<span class='glyphicon glyphicon-floppy-saved'></span>" +
                " <span class='hidden-xs'>Tags saved</span>");
             */
            // Save a new empty array with new tags that we can draw at the end
            var tags = "";
            var player_tags = "";
            var uniTags = [];
            // Loop through all tags and save the id's
            $(".tag_added").each(function(){
                var $this = $(this);
                // Get the data from the data DOM objects
                var tag_id = $this.data("tag-id");
                var kind = $this.data("kind");
                var label = $this.data("label");
                // Only push to tags array if the unique kind+tag_id does not exist yet
                // tags can be in multiple tagfields, so this prevents them from being added double
                if(uniTags.indexOf(kind+"_"+tag_id) == -1) {
                    uniTags.push(kind+"_"+tag_id);
                    if(kind == "tag") {
                        tags += "<span class='label label-warning'>" + label + "</span>\n";
                    } else {
                        player_tags += "<span class='label label-info'>" + label + "</span>\n";
                    }
                }
            });
            // Find all tags in the open moment and the hidden field
            $(".open_moment").find(".js_tags").html(tags);
            $(".open_moment").find(".js_player_tags").html(player_tags);
            $(".js_video_hidden").find(".js_tags").html(tags);
            $(".js_video_hidden").find(".js_player_tags").html(player_tags);
        }
    })(jQuery);
</script>
