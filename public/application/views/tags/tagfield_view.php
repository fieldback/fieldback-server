<!-- Nav tabs -->
<div class="container tags">
	<div class="row">	
	
		<!-- Choose tagfield -->
		<div class="col-md-3">
			<? $i = 0 ?>
			<select name="tagfields_select" id="tagfields_select">
				<?php foreach ($tagfields as $tagfield): ?>
					<option value="<?=$tagfield['tagfield_id']?>" <?=$tagfield['selected']?>><?= $tagfield['tagfield_name'] ?></option>
					<? $i++; ?>
				<?php endforeach ?>
			</select>
		</div>
	
		<!-- Trigger tags -->
		<div class="col-md-9">
			<?php foreach ($tagfields as $tagfield) : ?>
				<div class="trigger_tags" id="trigger<?=$tagfield['tagfield_id']?>">
					<?php foreach ($tagfield['trigger_tags'] as $trigger): ?>
						<a data-toggle="modal" href="#tag-modal"  class="trigger_button btn btn-primary" data-lead="<?=$trigger['lead']?>" data-lapse="<?=$trigger['lapse']?>"><?=$trigger['name']?> ( <?=$trigger['lead']?> , <?=$trigger['lapse']?> )</a>
					<?php endforeach ?>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>

<?
	$data = array(
		'tagfields' => $tagfields,
		'event_info' => $event_info,
		'live' => true
	);
	$this->load->view('tags/tagfield_modal_view', $data);
?>

<script>
	// init
    // Stupid thing does not work well on mobile
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        $('#tagfields_select').selectpicker('mobile');
    } else {
        $('#tagfields_select').selectpicker();
    }

	// hide all the trigger tags
	$('.trigger_tags').hide();
	$('.trigger_tags').first().show();

	// EVENT LISTENERS
	// show trigger tags on select
	$('#tagfields_select').on('change', function(){
		var val = $(this).val();
		$('.trigger_tags').hide();
		$('#trigger' + val).show();
		update_selected_tagfield_for_user(val);
	});

	//show buttons on trigger_tag
	$('.trigger_button').on('click', function(){
		var lead = $(this).data('lead');
		var lapse = $(this).data('lapse');
		createMoment(lead, lapse);
		get_selected_tagfield_for_user();
//		positionTagsContainer();
	});

	function createMoment(lead, lapse)
	{
		var seektime = null;
		if($('video').length > 0){
			seektime = $('video').get(0).currentTime;
		}

		$.ajax({
			url: '<?=site_url('moments/create_moment')?>',
			type: 'POST',
			dataType: 'json',
			data: {lead: lead, lapse: lapse, event_id: event_id, part_id: part_id, seektime: seektime},
			success: function(data){
				var moment_id = data;
				$('.tags').attr('data-moment-id', moment_id);

				var timeout = (lapse*1000);
				
				setTimeout(function(){
					renderMoments(lead, lapse, event_id, part_id, moment_id, seektime);
				}, timeout);
			}
		});
	}

	function renderMoments(lead, lapse, event_id, part_id, moment_id, seektime)
	{
		$.ajax({
			type: 'POST',
			url: '<?=site_url('moments/render_moments')?>',
			dataType: 'json',
			data: {lead: lead, lapse: lapse, event_id: event_id, part_id: part_id, moment_id: moment_id, seektime: seektime},
			success: function(data){
				getMoments();
			}
		});
	}
</script>
