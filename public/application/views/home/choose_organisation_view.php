<?php // This page loads inside the template_view ?>
<section id="logo" class="col-md-4">
	&nbsp;
</section>
<section id="home_menu" class="col-md-4">
	<h3>Choose organisation</h3>
	<form class="form" action="<?=base_url('home/choose_organisation')?>" method="post">
		<div class="form-group">
			<select class="form-control" name="organisation">
				<?
					foreach ($organisations as $organisation) 
					{
						echo '<option value="' . $organisation['organisation_id'] . '">'.$organisation['organisation_name'].'</option>';		
					}
				?>
			</select>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" value="Choose">
		</div>
	</form>
</section>
<section class="col-md-4">
	&nbsp;
</section>