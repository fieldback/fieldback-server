<?php // This page loads inside the template_view ?>
<div class="container ">
	<div class="row-fluid" >

		<!-- Not a member of any team -->

			<?php if (!is_player() && !has_teams()): ?>
				<div class="col-md-12">
					<section class="panel panel-warning">
						<div class="panel-heading">Warning</div>
						<div class="panel-container">
							<p>You are not a member of any <?=lang('term_team_l')?>. To start using Fieldback please contact your system administrator</p>
						</div>
					</section>
				</div>
			<?php endif ?>

		<div class="col-md-12 col-lg-8">

			<!-- Events if coach -->
			<?php if (has_teams()): ?>
				
				<section class="panel panel-success">
					<div class="panel-heading">
						My latest events as coach
					</div>
					<table class="table table-hover table-striped">
						<thead class="bg-empha">
							<tr>
								<th width="17%">Date</th>
								<th width="10%"><?=lang('term_team')?></th>
								<th width="20%;">Name</th>
								<th width="33%" class="hidden-xs">Description</th>
								<th width="20%" class="hidden-xs">Creator</th>
							</tr>
						</thead>
						<tbody data-link="row" class="rowlink">
							<?foreach($events_owner as $latest_event){?>
								<tr>
									<td><a href="<?=base_url('events/event/' . $latest_event['event_id']);?>"><span class="glyphicon glyphicon-calendar"></span> <?=date('d M Y', $latest_event['date_created'])?></a></td>
									<td><?=$latest_event['team_name']?></td>
									<td><?=$latest_event['event_name']?></td>
									<td class="hidden-xs"><?=$latest_event['event_description']?></td>
									<? if(isset($latest_event['creator']['first_name'])) { ?>
										<td class="hidden-xs"><span class="glyphicon glyphicon-user"></span> <?=$latest_event['creator']['first_name']?> <?=$latest_event['creator']['last_name']?></td>
									<? }else{ ?>
										<td class="hidden-xs"><span class="glyphicon glyphicon-user"></span> Unknown</td>
									<?	} ?>
								</tr>
							<?}?>
						</tbody>
					</table>
				</section>

			<?php endif ?>
			<!-- ./ events if coach -->

			<!-- Events if player -->
			<?php if (is_player()): ?>

				<section class="panel panel-success">
					<div class="panel-heading">My latest events as <?=lang('term_player_l')?></div>
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th width="17%">Date</th>
								<th width="10%"><?=lang('term_team')?></th>
								<th width="20%;">Name</th>
								<th width="33%" class="hidden-xs">Description</th>
								<th width="20%" class="hidden-xs">Creator</th>
							</tr>
						</thead>
						<tbody data-link="row" class="rowlink">
							<?foreach($events_player as $latest_event){?>
								<tr>
									<td><a href="<?=base_url('events/event/' . $latest_event['event_id']);?>"><span class="glyphicon glyphicon-calendar"></span> <?=date('d M Y', $latest_event['date_created'])?></a></td>
									<td><?=$latest_event['team_name']?></td>
									<td><?=$latest_event['event_name']?></td>
									<td class="hidden-xs"><?=$latest_event['event_description']?></td>
									<? if(isset($latest_event['creator']['first_name'])) { ?>
										<td class="hidden-xs"><span class="glyphicon glyphicon-user"></span> <?=$latest_event['creator']['first_name']?> <?=$latest_event['creator']['last_name']?></td>
									<? }else{ ?>
										<td class="hidden-xs"><span class="glyphicon glyphicon-user"></span> Unknown</td>
									<?	} ?>
								</tr>
							<?}?>
						</tbody>
					</table>
				</section>

			<?php endif ?>
			<!-- ./ Events if player -->
			
		</div>

		<div class="col-md-12 col-lg-4">

			
			<?php 
			// Do not show if is not a member of any team
			if(is_player() || has_teams()): ?>

				<section class="panel panel-success">
					<div class="panel-heading">My latest moments</div>
					<table class="table table-hover table-striped">
						<tbody data-link="row" class="rowlink">
							<?php foreach ($latest_moments as $moment): ?>
								<tr class="moment" data-moment-id="<?=$moment['moment_id']?>">
									<td class="col-md-4 col-lg-6 hidden-xs">
										<?php if (!empty($moment['moment_files'])): ?>
										<div class="image thumbnail">
											<img src="<?=$moment['thumbnail']?>" class="img-polaroid">
										</div>
										<?php else: ?>
											<div class="image"></div>
										<?php endif ?>
									</td>
									<td class="col-md-4 col-lg-6">
										<div class="description">
											<!-- team name -->
											<p><span class="glyphicon glyphicon-asterisk"></span> <?=$moment['team_name']?></p>
											<!-- event name -->
											<p><span class="glyphicon glyphicon-flag"></span> <?=$moment['event_name']?></p>
											<!-- date created -->
											<p><span class="glyphicon glyphicon-calendar"></span> <?=date('Y-m-d H:i', $moment['date_created']);?></p>
										</div>
									</td>
									<td class="col-md-4 col-lg-12">
										<!-- tags -->
										<ul class="list-unstyled">
										<? 
											$i = 0; 
											// maximum amount of tags in homeview
											$max_list = 2;
										?>
					 					<?foreach ($moment['tags'] as $tag) {?>
											<? if($i < $max_list) { ?>
												<li><span class="label label-warning"><?= character_limiter($tag['tag_name'], 12)?></span></li>
											<? } elseif($i == $max_list) { ?>
												<li><span class="label label-warning">...</span></li>
											<? }else{ 
												break;
											}
											$i++;
											?>

										<?}?>
										<? $i = 0; ?>
										<?foreach ($moment['players'] as $player) {?>
											<? if($i < $max_list) { ?>
												<li><span class="label label-info"><?=$player['first_name']?> <?=$player['last_name']?></span></li>
											<? } elseif($i == $max_list) { ?>
												<li><span class="label label-info">...</span></li>
											<? }else{ 
												break;
											}
											$i++;
											?>
										<?}?>
										</ul>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</section>

			<?php endif ?>
		</div>
		
	</div>
</div>

<script type="text/javascript">

	$('tr.moment').on('click', function(){
		var moment_id = $(this).data('moment-id');
		show_moment(moment_id);
	});

</script>