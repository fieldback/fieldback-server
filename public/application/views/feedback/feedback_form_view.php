<div id="feedback-modal" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Feedback form</h3>
            </div>

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row-fluid" class="feedback_form">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title" class="form-control" type="text" placeholder="Title" />
                                <input id="url" type="hidden" value="<?=$url?>"/>
                            </div>

                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea id="message" class="form-control" placeholder="I would like to say..." style="height: 300px;"></textarea>
                            </div>
                            <div class="feedback_error">
                            </div>
                            <input id="submit_feedback" type="submit" class="btn btn-primary" /><br><br>
                        <small>This information will be send to <?= $this->data['system_info']['support_email'] ?>. We could contact you for more information.</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default close-modal" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(function(){
        
        $('#submit_feedback').on('click', function(){
            checkFeedback();
        });

    });

    function sendFeedback()
    {
        var title = $('#title').val();
        var message = $('#message').val();
        var url = $('#url').val();

        $('.feedback_error').html('<p>Sending...</p>');

        $.ajax({
            url: '<?=base_url('feedback/send_feedback')?>',
            type: 'POST',
            dataType: 'json',
            data: {title: title, message: message, url: url},
            success: function(data){
                $('#feedback-modal').modal('hide');
            }
        });
    }

    function checkFeedback()
    {
        if( $('#title').val() && $('#message').val() ){
            sendFeedback();
        }else{
            if($('#title').val() === ''){
                $('.feedback_error').append('<p class="error">You have to fill in a title</p>');
            }
            if($('#message').val() === ''){
                $('.feedback_error').append('<p class="error">You have to fill in a message</p>');
            }
        }

    }


</script>