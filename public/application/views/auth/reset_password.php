<div class="jumbotron">
	<h1>Fieldback</h1>
	<h4>Instant Performance Analysis</h4>
</div>
<div class="container-fluid">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<div id="page_content">
		<h1><?php echo lang('reset_password_heading');?></h1>

		<div id="infoMessage"><?php echo $message;?></div>

		<?php echo form_open('auth/reset_password/' . $code);?>
			
			<div class="form-group">
				<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
				<input class="form-control" type="<?=$new_password['type']?>" name="<?=$new_password['name']?>" id="<?=$new_password['id']?>" pattern="<?=$new_password['pattern']?>" />
			</div>
			<div class="form-group">
				<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?>
				<input class="form-control" type="<?=$new_password_confirm['type']?>" name="<?=$new_password_confirm['name']?>" id="<?=$new_password_confirm['id']?>" pattern="<?=$new_password_confirm['pattern']?>" />
			</div>
			<?php echo form_input($user_id);?>
			<?php echo form_hidden($csrf); ?>

			<input type="submit" name="submit" class="btn btn-block btn-lg btn-primary" value="<?=  lang('reset_password_submit_btn') ?>" />
		<?php echo form_close();?>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>