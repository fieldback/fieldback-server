<div class="jumbotron">
	<h1>Fieldback</h1>
	<h4>Instant Performance Analysis</h4>
</div>
<div class="container-fluid">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<h2><?php echo lang('forgot_password_heading');?></h2>
		<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

		<div id="infoMessage"><?php echo $message;?></div>

		<?php echo form_open("auth/forgot_password");?>
		      	<div class="form-group">
					<input class="form-control" type="text" name="email" id="email" placeholder="e-mail" > 
				</div>	
				<input type="submit" class="btn btn-primary btn-block btn-lg" value="<?= lang('forgot_password_submit_btn') ?>" />
		<?php echo form_close();?>
	</div>
</div>