<h1>Welcome to Fieldback</h1>
<p>An account has been created for you. After the registration process you will be able to see all the important moments of your team. We hope you like it and will get better! <br/><br/> Good luck, <br/> The Fieldback Team.</p>
<br/><br/>

<h3><?php echo sprintf(lang('email_activate_heading'), $identity);?></h3>
<p><?php echo sprintf(lang('email_activate_subheading'), anchor('auth/activate/'. $id .'/'. $activation, lang('email_activate_link')));?></p>

<p> Your temporary password is: <?= $password ?></p>