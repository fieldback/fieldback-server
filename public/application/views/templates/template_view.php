<!DOCTYPE HTML>
<html>
	<head>
		<title><?=$head['title']?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
		<meta name="HandheldFriendly" content="true">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<? foreach($head['metas'] as $key => $meta){?>
			<meta name="<?=$key?>" content="<?=$meta?>" />  <? /* Meta are loaded, set in controller, defaults in application/libraries/MY_controller.php */ ?>
		<? } ?>
		
		<? // FAVICONS and APPICONS ?>		
		<? // STANDARD FAVICON IN ROOT DIR ?>
		<link rel="icon" href="<?= base_url('favicon.ico') ?>" type="image/x-icon"/>
		
		<link rel="apple-touch-icon" sizes="57x57" href="<?=base_url('/images/icons/apple-touch-icon-57x57.png')?>">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=base_url('/images/icons/apple-touch-icon-60x60.png')?>">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('/images/icons/apple-touch-icon-72x72.png')?>">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('/images/icons/apple-touch-icon-76x76.png')?>">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('/images/icons/apple-touch-icon-114x114.png')?>">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('/images/icons/apple-touch-icon-120x120.png')?>">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('/images/icons/apple-touch-icon-144x144.png')?>">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('/images/icons/apple-touch-icon-152x152.png')?>">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('/images/icons/apple-touch-icon-180x180.png')?>">
		<link rel="icon" type="image/png" href="<?=base_url('/images/icons/favicon-32x32.png')?>" sizes="32x32">
		<link rel="icon" type="image/png" href="<?=base_url('/images/icons/android-chrome-192x192.png')?>" sizes="192x192">
		<link rel="icon" type="image/png" href="<?=base_url('/images/icons/favicon-96x96.png')?>" sizes="96x96">
		<link rel="icon" type="image/png" href="<?=base_url('/images/icons/favicon-16x16.png')?>" sizes="16x16">
		<link rel="manifest" href="<?=base_url('/images/icons/manifest.json')?>">
		<meta name="msapplication-TileColor" content="#00a300">
		<meta name="msapplication-TileImage" content="<?=base_url('/images/icons/mstile-144x144.png')?>">
		<meta name="theme-color" content="#ffffff">
		
		<!-- Startup images -->
		<!-- iOS 6 & 7 iPad (retina, portrait) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-1536x2008.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: portrait)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPad (retina, landscape) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-1496x2048.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: landscape)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPad (portrait) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-768x1004.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: portrait)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPad (landscape) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-748x1024.png')?>"
					media="(device-width: 768px) and (device-height: 1024px)
						 and (orientation: landscape)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPhone 5 -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-640x1096.png')?>"
					media="(device-width: 320px) and (device-height: 568px)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 & 7 iPhone (retina) -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-640x920.png')?>"
					media="(device-width: 320px) and (device-height: 480px)
						 and (-webkit-device-pixel-ratio: 2)"
					rel="apple-touch-startup-image">

		<!-- iOS 6 iPhone -->
		<link href="<?=base_url('images/ios/apple-touch-startup-image-320x460.png')?>"
					media="(device-width: 320px) and (device-height: 480px)
						 and (-webkit-device-pixel-ratio: 1)"
					rel="apple-touch-startup-image">
		
		<!-- Stylesheets -->
		<? foreach($head['stylesheets'] as $stylesheet) { ?> <? /* Stylesheets are loaded, set in controller, defaults in application/libraries/MY_controller.php */ ?>
			<link rel="stylesheet" type="text/css" href="<?=base_url('css/' . $stylesheet)?>" media="screen" />
		<? } ?>

		<!-- Javascripts -->
		<? foreach($head['javascripts'] as $javascript) { ?> <? /* Javascripts are loaded, set in controller, defaults in application/libraries/MY_controller.php */ ?>
			<script type="text/javascript" src="<?=base_url('js/' . $javascript)?>"></script>		
		<? } ?>		

	</head>
	<body id="body">
		
		<?php
			if($system_info['is_online'] === '1'){ 
				// Add Google Analytics
				@include_once("analyticstracking.php");
			} 
		?>

		<? 
		// flashdata
		if($this->session->flashdata('success'))
		{
			echo '<div class="panel panel-success flash"><div class="panel-heading"><h3 class="panel-title">Success</h3></div><div class="panel-body">' . $this->session->flashdata('success') . '</div></div>';
		}
		?>
		<? if($this->session->flashdata('error'))
		{
			echo '<div class="panel panel-warning flash"><div class="panel-heading"><h3 class="panel-title">Warning</h3></div><div class="panel-body">' . $this->session->flashdata('error') . '</div></div>';
		}
		?>

        <?php $this->load->view('template/menu_view'); ?>

        <?=$contents?>

	</body>

	<!-- base info -->
	<script type="text/javascript">
		// We keep this for the older code, but new variables need to use the new methods down below
		var base_url = "<?= base_url() ?>";
		var site_url = "<?= site_url() ?>";
		var user_id = "<?= $this->data['user']->id ?>";

		// Make a global variable with all kinds of variables we use throughout the site
		var system_vars = <?php echo json_encode($system_vars);?>;

        var user_teams = <?php echo get_teams(); ?>;

        $.views.helpers({
            system_info: function(value) {
                return system_vars[value];
            },
            system_info_check: function(value) {
                return system_vars[value] == 1 || system_vars[value] == true;
            },
            has_team: function(team_id) {
                for(var key in user_teams) {
                    if(user_teams[key]['team_id'] == team_id) { return true; }
                }
                return false;
            },
            is_admin: function() {
                return system_vars['is_admin'];
            }
        });

		/**
		 * Template converters -> helper functions and passers of info to templates
		 */
		$.views.converters({
            niceDate: function (value) {
                return moment(moment.unix(value)).format("DD-MM-YYYY");
            },
            niceDateTime: function (value) {
                return moment(moment.unix(value)).format("DD-MM-YYYY HH:mm:ss");
            },
            niceTime: function(value) {
				return secondsToTime(value);
			},
			site_url: function (value) {
				return base_url + value;
			}
		});

		setInterval(function(){
			$('.flash').fadeOut();
		}, 4000); // Give users a little bit more time to see and read the flash message
		
	</script>

</html>