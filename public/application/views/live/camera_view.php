<?php // This page loads inside the template_view ?>

<div class="container ">
	<div class="row-fluid" >
		<div class="col-md-12">
			
			<section class="panel panel-default">
				<div class="panel-heading">
					Camera: <?=$camera['description']?>
					<div class="btn-group pull-right">
						<a class="btn btn-default btn-xs" href="<?=base_url('live')?>" alt="Live"><span class="glyphicon glyphicon-chevron-left"></span> back to camera selector</a>
					</div>

				</div>
				<div class="panel-body">
					<img src="<?=$camera['stream_live']?>" width="100%" />
				</div>
			</section>
		</div>
	</div>
</div>