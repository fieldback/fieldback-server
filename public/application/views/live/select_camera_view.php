<?php // This page loads inside the template_view ?>

<div class="container ">
	<div class="row-fluid" >
		<div class="col-md-12">
			
			<section class="panel panel-default">
				<div class="panel-heading">
					Select a camera for live view
				</div>
				
				<div class="panel-body">
<? 
// Get the number of cameras for proper row width
$number_of_cameras = count($cameras);
// go through each camera and get the live stream
foreach($cameras as $camera) { ?>
					<div class="col-md-<? echo round(12 / $number_of_cameras, PHP_ROUND_HALF_DOWN); ?>">
						<h2><?=$camera['description']?></h2>
						<p>
							<a href="<?=base_url()."live/camera/".$camera['camera_id']?>">
								<img src="<?=$camera['stream_live_preview']?>" width="100%" />
							</a>
						</p>
						<p>
							<div class="form-group">
								<a href="<?=base_url()."live/camera/".$camera['camera_id']?>" class="btn btn-primary">Open</a>
								<a href="<?=base_url()."live/camera_fullscreen/".$camera['camera_id']?>" class="btn btn-primary">Open fullscreen</a>
							</div>
						</p>
					</div>
<? } ?>
				</div>				
			</section>
		</div>
	</div>
</div>



