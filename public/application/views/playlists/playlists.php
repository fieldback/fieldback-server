
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <section class="panel panel-success">
                <div class="panel-heading">
                    Playlists
                    <div class="btn-group pull-right">
                        <button class="btn btn-xs btn-primary" id="btn_playlist-new"><span class="glyphicon glyphicon-plus"></span> Create playlist</button>
                    </div>
                </div>
                <div class="panel-body" id="pnl_playlist-new" style="display:none;">
                    <form id="frm_playlist-new" class="form-inline">
                        <div class="form-group">
                            <input class="form-control input-sm" type="text" id="inp_playlist-new-title" name="title" placeholder="title" required>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-sm btn-default" type="submit">Add new playlist</button>
                        </div>
                    </form>
                </div>
                <table class="table">
                    <thead class="bg-empha">
                    <tr>
                        <th>Number:</th>
                        <th>Title:</th>
                        <th>Number of moments:</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="playlist-list">
                    </tbody>
                </table>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        // Get jQuery elements
        var $playlist_list = $("#playlist-list");
        var playlist_data;

        var loadPlaylists = function() {
            // Get all playlists with ajax and populate field
            $.ajax({
                url: '<?=site_url("playlist_ajx/get_all_my_playlists")?>',
                type: 'get',
                dataType: 'json'
            }).done(function(response){
                playlist_data = response['playlists'];
                renderPlaylists(playlist_data);
            });
        };

        var renderPlaylists = function(data) {
            var playlists_html = $.templates("#tmpl_playlist-list-item").render(data);
            $playlist_list.html(playlists_html);
        };

        loadPlaylists();

        $("#btn_playlist-new").on("click", function(){
            $("#pnl_playlist-new").slideDown();
        });

        // On submit (so you can use enter) we send the form through ajax and add it to the list
        $("#frm_playlist-new").on("submit", function(event){
            // Don't actually submit the form!
            event.preventDefault();
            // Get value of 'title'
            var $this = $(this);
            var new_title_input = $this.find("#inp_playlist-new-title");
            var new_title = new_title_input.val();
            if(new_title != "") {
                $.ajax({
                    url: '<?=site_url("playlist_ajx/create_playlist")?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        title: new_title
                    }
                }).done(function(response){
                    loadPlaylists();
                    // Reset input
                    new_title_input.val("");
                });
            }
        });

        // Remove a playlist item from the list with ajax
        $playlist_list.on("click", "#btn_remove-playlist", function(event){
            if(window.confirm("Are you sure?")) {
                var playlist_id = $(this).data("playlist-id");
                $.ajax({
                    url: '<?=site_url("playlist_ajx/remove_playlist")?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        playlist_id: playlist_id
                    }
                }).done(function(response){
                   loadPlaylists();
                });
            }
        });

        // Edit field -> on click activate the form to edit the title
        $playlist_list.on("click", "#btn_edit-playlist", function(){
            var playlist_id = $(this).data("playlist-id");
            // Switch text with form
            var $spn_playlist_title = $("#spn_playlist-title_"+playlist_id);
            $spn_playlist_title.hide();
            var $frm_playlist_edit = $("#frm_playlist-edit_"+playlist_id);
            $frm_playlist_edit.show();
            // Add event handler for submit
            $frm_playlist_edit.on("submit", function(event){
                event.preventDefault();
                var new_title_input = $frm_playlist_edit.find("#inp_playlist-edit-title");
                var new_title = new_title_input.val();
                if(new_title != "") {
                    $.ajax({
                        url: '<?=site_url("playlist_ajx/set_title_of_playlist")?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            title: new_title,
                            playlist_id: playlist_id
                        }
                    }).done(function(response){
                        $frm_playlist_edit.hide();
                        $spn_playlist_title.html(new_title);
                        $spn_playlist_title.show();
                    }).always(function(response){
                        console.log(response);
                    });
                }
            });
        });
    });
</script>

<script type="text/html" id="tmpl_playlist-list-item">
    <tr data-playlist-id="{{:playlist_id}}">
        <td>
            {{:#index+1}}
        </td>
        <td>
            <a href="<?=site_url('playlists/playlist')?>/{{:playlist_id}}" id="spn_playlist-title_{{:playlist_id}}">{{:title}}</a>
            <form id="frm_playlist-edit_{{:playlist_id}}" class="form-inline" style="display:none;" data-playlist-id="{{:playlist_id}}">
                <div class="form-group">
                    <input class="form-control input-sm" type="text" id="inp_playlist-edit-title" name="title" placeholder="{{:title}}" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Save</button>
                </div>
            </form>

        </td>
        <td>{{:number_of_moments}}</td>
        <td align="right">
            <button type="button" class="btn btn-xs btn-default" id="btn_edit-playlist" data-playlist-id="{{:playlist_id}}"><span class="glyphicon glyphicon-edit"></span></button>
            <button type="button" class="btn btn-xs btn-danger" id="btn_remove-playlist" data-playlist-id="{{:playlist_id}}"><span class="glyphicon glyphicon-remove"></span></button>
        </td>
    </tr>
</script>