<div class="container">
    <div class="row">
        <section class="panel panel-success">
            <div class="panel-heading">
                Playlist: <?=$playlist['title']?>
                <div class="btn-group pull-right">
                    <a href="<?=site_url("moments?playlist=".$playlist["playlist_id"])?>" class="btn btn-xs btn-primary">
                        <span class="glyphicon glyphicon-film"></span> open in moment view
                    </a>
                </div>
            </div>
            <div class="panel-body" id="playlist_play_field">

            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            Date:
                        </th>
                        <th>
                            At time:
                        </th>
                        <th>
                            Event:
                        </th>
                        <th>
                            Tags:
                        </th>
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody id="tb_playlist_list"></tbody>
            </table>
        </section>
    </div>
</div>

<script type="text/javascript">
$(function(){
    $playlist_list = $("#tb_playlist_list");
    $playlist_play_field = $("#playlist_play_field");

    var playlist_data;
    var play_index;

    $.ajax({
        url: '<?=site_url("playlist_ajx/get_playlist_moments")."/".$playlist['playlist_id']?>/',
        type: 'GET',
        dataType: 'JSON'
    }).done(function(response){
        playlist_data = response['moments'];
        var playlist_html = $.templates("#tmpl_playlist-list-item").render(response['moments']);
        $playlist_list.html(playlist_html);
        play_index = 0;
    }).always(function(response){
        console.log(response['moments']);
    });

    $playlist_list.on("click", "#btn_remove-from-playlist", function(event){
        event.stopPropagation();
        if(window.confirm("Are you sure?")){
            var moment_id = $(this).data("moment-id");
            var playlist_id = $(this).data("playlist-id");
            $.ajax({
                url: '<?=site_url("playlist_ajx/remove_moment_from_playlist")?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    moment_id: moment_id,
                    playlist_id: playlist_id
                }
            }).done(function(){
                // Remove the moment_id
                $playlist_list.find("tr[data-moment-id="+moment_id+"]").remove();
            }).always(function(response){
                console.log(response);
            });
        }
    });

    /**
     * Playback controls
     */

    $playlist_list.on("click", "tr", function() {
        var $this = $(this);
        var index = $this.data("index");
        var moment_id = $this.data("moment-id");
        $this.addClass('info').siblings().removeClass('info');
        render_moment("#tmpl_playlist-play-item", moment_id, $playlist_play_field, site_url);
    });

    $playlist_list.on("click", ".btn_order", function(event){
        event.stopPropagation();
        var $this = $(this);
        var $this_row = $this.closest("tr");
        var $moment_new = {}; // defualt->empty
        // Get the moment id of the previous or next item based on direction
        if($this.data("direction") == "up") {
            $moment_new = $this_row.prev();
        } else if($this.data("direction") == "down") {
            $moment_new = $this_row.next();
        } else {
            console.log("This should not happen!");
        }
        var moment_cur = $this.data("moment-id");
        // check if this was the first or last one by looking at moment_prev
        if($moment_new.length == 1) {
            var moment_new = $moment_new.data("moment-id");
            // do an ajax call to switch order
            $.ajax({
                url: '<?=site_url("playlist_ajx/switch_order")?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    moment_new: moment_new,
                    moment_cur: moment_cur,
                    playlist_id: $this.data("playlist-id")
                }
            }).done(function(response){
                // make the switch in the DOM
                console.log(response);
                if($this.data("direction") == "up") {
                    $moment_new.before($this_row);
                } else if($this.data("direction") == "down") {
                    $moment_new.after($this_row);
                } else {
                    console.log("Switching failed!");
                }
                // Add info class to indicate current row
                $this_row.addClass('info').siblings().removeClass('info');
            }).always(function(response){
                console.log(response['response']);
            });
        } else {
            // This was the first of the list, do nothing but log
            console.log("This is the first item, can't reorder!");
        }
    });

    video_button_helper($playlist_play_field);


});
</script>

<script type="text/html" id="tmpl_playlist-list-item">
    <tr data-moment-id="{{:moment_id}}" data-index="{{:#index}}">
        <td>
            {{niceDate:date_created}}
        </td>
        <td>
            {{niceTime:time_in_clip}}
        </td>
        <td>
            {{:event_name}} - {{:part_name}}
        </td>
        <td>
            {{for tags}}
            <span class="label label-warning">{{:tag_name}}</span>
            {{/for}}
            {{for players}}
            <span class="label label-info">{{:first_name}} {{:last_name}}</span>
            {{/for}}
        </td>
        <td width="100px">
            <div class="pull-right">
                <button type="button" class="btn btn-xs btn-default btn_order" data-direction="up" data-playlist-id="<?=$playlist["playlist_id"]?>" data-moment-id="{{:moment_id}}"><span class="glyphicon glyphicon-arrow-up"></span></button>
                <button type="button" class="btn btn-xs btn-default btn_order" data-direction="down" data-playlist-id="<?=$playlist["playlist_id"]?>" data-moment-id="{{:moment_id}}"><span class="glyphicon glyphicon-arrow-down"></span></button>
                <button type="button" class="btn btn-xs btn-danger" id="btn_remove-from-playlist" data-playlist-id="<?=$playlist["playlist_id"]?>" data-moment-id="{{:moment_id}}"><span class="glyphicon glyphicon-remove"></span></button>
            </div>

        </td>
    </tr>
</script>

<script type="text/html" id="tmpl_playlist-play-item">
    <?php require("js/templates/moments/tmpl_moment-item-within-list.php"); ?>
</script>