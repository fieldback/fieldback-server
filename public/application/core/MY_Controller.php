<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	function __construct($type = 'regular')
	{
		parent::__construct();

		//get the organisation the user is logged in to
		if($this->ion_auth->logged_in())
		{
			// get all the system info
			$this->load->model('system_model');
			$this->data['system_info'] = $this->system_model->get_system_info();

			// get the user
			$this->data['user'] = $this->ion_auth->user()->row();
			
			// check if an organisation is selected in the database
			if($this->user_model->get_selected_organisation_for_user()){
				$organisation_id = $this->user_model->get_selected_organisation_for_user();
				$this->session->set_userdata('organisation_id', $organisation_id);
			}else{
				if($this->session->userdata('organisation_id')){
					$organisation_id = $this->session->userdata('organisation_id');
					$this->user_model->set_organisation_for_user($organisation_id);
				}
			}
			// set the selected organisation for this session
			$this->data['user']->selected_organisation = $this->user_model->get_organisation_by_organisation_id($organisation_id);
		}

		switch($type)
		{
			case 'regular';
			default;
				$this->data['head'] = array(
					'title'			=> 'Fieldback - Instant Performance Analysis',
					'metas'			=> array(
						'description'	=> 'Fieldback - Instant Performance Analysis',
						'robots'		=> 'noindex, nofollow',
						'keywords'		=> 'keywords'
					),
					'stylesheets'	=> array(
						'vendor/bootstrap.min.css',
						'vendor/jasny-bootstrap.min.css',
						'vendor/bootstrap-select.min.css',
						'main.css'
					),
					'javascripts'	=> array(
						'vendor/jquery-1.9.1.js',
						'vendor/modernizr.js', 
						'vendor/jquery-ui-1.10.3.custom.js',
						'vendor/jquery.confirm.min.js',
						'vendor/jquery.iframe-transport.js',
						'vendor/jquery.autocomplete.js',
						'vendor/bootstrap.min.js',
						'vendor/bootstrap-select.min.js',
						'vendor/jasny-bootstrap.min.js',
						'vendor/fileuploader.js',
                        'vendor/jsrender.min.js',
                        'vendor/moment.js',
                        'vendor/jquery.pulse.min.js', // https://github.com/jsoverson/jquery.pulse.js
						'vendor/imagesloaded.pkgd.min.js',
						// 'vendor/jwplayer-server.js', // JWPLAYER IS NOT NEEDED ANYMORE?
						'main.js',
						'video.synchronizer.js',
						'time.js'
					)
				);
			break;
		}
		
		// Check if there are camera's with a livestream, if so, enable the live view menu item
		$this->load->model('camera_model');
		$this->data['live_camera_available'] = 0;
		if(count($this->camera_model->get_all_live_cameras()) > 0) { $this->data['live_camera_available'] = 1; }
		
		// Check if the system is an online or offline install of Fieldback (from system model)
		$this->load->model('system_model');
		$this->data['system_info'] = $this->system_model->get_system_info();

		// stop overtime recordings and instacordings if they are present
		$this->load->model('recording_model');
		$overtime_recordings = $this->recording_model->get_overtime_recordings();

		if(!empty($overtime_recordings)){
            $this->load->library("recording");
			foreach ($overtime_recordings as $recording) {
				$event_id = $recording['event_id'];
				$part_id = $recording['part_id'];
                $this->recording->stop_recording($event_id, $part_id);
            }
		}

		$user_id = !empty($this->data['user']) ? $this->data['user']->id : 0;
		// System variables: this is passed to a global javascript array: system_vars
		// Usage: systems_vars.variable_name
		$this->data['system_vars'] = array(
			"base_url" 			=> base_url(),
			"site_url"			=> site_url(),
			"shares"			=> $this->data['system_info']['shares'],
			"downloads"			=> $this->data['system_info']['downloads'],
			"is_admin"			=> is_admin(),
			"is_system_admin"	=> is_system_admin(),
			"user_id"			=> $user_id,
            "sync_active"       => $this->config->item("master_server_active")
		);

        // Profiler - Set to true to enable profiling during development
        $this->output->enable_profiler(FALSE);
	}

}