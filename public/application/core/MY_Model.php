<?php

class MY_Model extends CI_Model {

    protected $table;
    protected $language_fields;

    function __construct() {
        parent::__construct();
    }

    /**
     * Get single array result matching the where
     * 
     * @param array $where all conditions
     * @param string $table optional name of the table
     * @return array
     */
    public function get(Array $where = array(), $table = '') {
        $table = self::set_table($table);
        return (array) $this->db->from($table)->where($where)->get()->row_array();
    }

    /**
     * Get multiple array results matching the where
     * 
     * @param array $where all conditions
     * @param string $table optional name of the table
     * @return array
     */
    public function get_all(Array $where = array(), $table = '') {
        $table = self::set_table($table);
        return (array) $this->db->from($table)->where($where)->get()->result_array();
    }

    /**
     * Check if a certain row exists matching the where
     * 
     * @param array $where all conditions
     * @param string $table optional name of the table
     */
    public function exists(Array $where = array(), $table = '') {
        $result = (array) self::get($where, $table);
        if (empty($result)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Count all results matching the where
     * 
     * @param array $where all conditions
     * @param string $table optional name of the table
     * @return integer
     */
    public function count(Array $where = array(), $table = '') {
        $table = self::set_table($table);
        return (int) $this->db->from($table)->where($where)->count_all_results();
    }

    /**
     * Update one or more rows matching the where
     * 
     * @param array $data
     * @param array $where all conditions
     * @param string $table optional name of the table
     */
    public function update(Array $data = array(), Array $where = array(), $table = '') {
        $table = self::set_table($table);
        return $this->db->update($table, $data, $where);
    }

    /**
     * Insert multiple rows in the default table
     *
     * @param array $data 
     * @param string $table optional name of the table
     */
    public function update_batch(Array $data = array(), $key = '', $table = '') {
        $table = self::set_table($table);
        return $this->db->update_batch($table, $data, $key);
    }

    /**
     * Insert one row
     * 
     * @param array $data
     * @param string $table optional name of the table
     * @return integer id of the inserted row 
     */
    public function insert(Array $data = array(), $table = '') {
        $table = self::set_table($table);
        $this->db->insert($table, $data);
        return (int) $this->db->insert_id();
    }

    /**
     * Insert multiple rows in the default table
     *
     * @param array $data 
     * @param string $table optional name of the table
     */
    public function insert_batch(Array $data = array(), $table = '') {
        $table = self::set_table($table);
        $this->db->insert_batch($table, $data);
    }

    /**
     * Insert or update one a row. If the primary key or a unique key exists, this function will update instead of insert.
     * 
     * @param array $data
     * @param string $table 
     * @return integer id of the inserted row 
     */
    public function replace(Array $data = array(), $table = '') {
        $table = self::set_table($table);
        $query = self::replace_string($table, $data);
        $this->db->query($query);
        return (int) $this->db->insert_id();
    }

    /**
     * Insert or update multiple rows. If the primary key or a unique key exists, this function will update instead of insert.
     * @param array $data
     * @param string $table 
     */
    public function replace_batch(Array $data = array(), $table = '') {
        $table = self::set_table($table);
        $query = self::replace_string($table, $data);
        $this->db->query($query);
    }

    /**
     * Deletes rows matching the where from the default table. If tables is set, it will delete from all tables matching the where clause
     * 
     * @param array $where all conditions
     * @param string $table optional name of the table
     * @param array $tables optional. All tables where it should match the where clause and delete the values
     * @return NULL 
     */
    public function delete(Array $where = array(), $table = '', Array $tables = array()) {
        if (empty($where)) {
            log_message('error', 'Called the model delete function without specifying a where. This could lead to an empty table! Use the db truncate method.');
            return;
        }

        if (empty($tables)) {
            $table = self::set_table($table);
            $this->db->delete($table, $where);
        } else {
            $this->db->delete($tables, $where);
        }
    }
    
    /**
     * Truncate a table
     * 
     * @param string $table 
     */
    public function truncate($table = '') {
        $table = self::set_table($table);
        $this->db->truncate($table);
    }

    /**
     * Get a single value from the default table
     * 
     * @param array $where all conditions
     * @param string $column_name name of the column
     * @param string $table optional name of the table
     * @return object 
     */
    public function get_value(Array $where = array(), $column_name = "", $table = '') {
        $table = self::set_table($table);
        $this->db->select($column_name);
        return array_pop(self::get($where, $table));
    }

    /**
     * Set the table we will be using
     * 
     * @param string $table name of the table
     * @return string the table used
     */
    private function set_table($table = '') {
        return (string) (!empty($table)) ? $table : $this->table;
    }

    // --------------------------------------------------------------------

    /**
     * Generate an replace string
     * Based on the insert_string() and update_string() functions from CI
     *
     * @access	public
     * @param	string	the table upon which the query will be performed
     * @param	array	an associative array data of key/values
     * @return	string
     */
    private function replace_string($table, $data) {
        $fields = array();
        $values = array();

        $batchValues = array();
        $batch = FALSE;

        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $batch = TRUE;
                $batchValues = array();
                foreach ($val as $k => $v) {
                    $batchValues[] = $this->db->escape($v);
                    $fields[] = $this->db->_escape_identifiers($k);
                }

                $values[] = "(" . implode(', ', $batchValues) . ")";
            } else {
                $fields[] = $this->db->_escape_identifiers($key);
                $values[] = $this->db->escape($val);
            }
        }

        if ($batch) {
            return "REPLACE INTO" . $this->db->_protect_identifiers($table, TRUE, NULL, FALSE) . "(" . implode(', ', array_unique($fields)) . ") VALUES " . implode(', ', $values) . "";
        }

        return "REPLACE INTO" . $this->db->_protect_identifiers($table, TRUE, NULL, FALSE) . "(" . implode(', ', $fields) . ") VALUES (" . implode(', ', $values) . ")";
    }

}

// EOF
