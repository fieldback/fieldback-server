function synchronize(element){

	if(mobile_device === false){

		var videos = $(element);

		var audio_index = 1;
		var volume_levels = [1.0, 1.0];
		var playing = false;
		var seeking_index = -1;
		var is_resizing = false;
		var time_controlling_index = 0;

		// var audio_selector = document.getElementById('audio-selector');
		// var audio_right = document.getElementById('audio-right');
		// var audio_left = document.getElementById('audio-left');

		// function updateAudioLevels() {
		// 	//need to adjust volume level because Chrome <= 7.0 doesn't respect muted
		// 	for (var i = 0; i < videos.length; i++) {
		// 		if (i !== audio_index) {
		// 			videos[i].muted = true;
		// 			videos[i].volume = 0.0;
		// 		} else {
		// 			videos[i].muted = false;
		// 			videos[i].volume = volume_levels[i];
		// 		}
		// 	}           
		// }

		// function selectAudio(index) {
		// 	audio_index = index;
		// 	updateAudioLevels();

		// 	if (audio_index) {
		// 		audio_right.className = 'active';
		// 		audio_left.className = '';
		// 	} else {
		// 		audio_left.className = 'active';
		// 		audio_right.className = '';
		// 	}
		// }

		// audio_right.addEventListener('click', function() {
		// 	selectAudio(1);
		// }, true);

		// audio_left.addEventListener('click', function() {
		// 	selectAudio(0);
		// }, true);

		// audio_selector.addEventListener('click', function () {
		// 	audio_index++;
		// 	audio_index %= videos.length;
			
		// 	selectAudio(audio_index);
		// },false);

		/* Things start to go a little haywire when you try to seek within a video.  That's why the code gets ugly from here on it.  */
		var seeked_counter = 0;
		function seeking(evt) {
			if (seeking_index < 0) {
				pause();
				for (var i = 0; i < videos.length; i++) {
					if (videos[i].id === evt.target.id) {
						seeking_index = i;
						break;
					}
				}
			}
		}

		function seeked(evt) {
			if (seeking_index < 0) {
				return;
			} else if (videos[seeking_index].id !== evt.target.id) {
				seeked_counter--;
			} else {
				seeking_index = -1;
			}
			if (!seeked_counter) {
				var target_time = videos[seeking_index].currentTime;
				for (var i = 0; i < videos.length; i++) {
					if (videos[i].id != evt.target.id &&
						seeking_index != i &&
						videos[i].currentTime != target_time) {

						seeked_counter++;

						videos[i].currentTime = target_time;
					}
				}
				time_controlling_index = seeking_index;

				if (!seeked_counter) {
					seeking_index = -1;
					if (playing) {
						play();
					}
				}
			}
		}

		function play(evt) {
			//first find target for setting currentTime
			//if (seeking_index <0) {
				playing = true;
				for (var i = 0; i < videos.length; i++) {
					if (videos[i].paused && videos[i] != evt.target) {
						if (Math.abs(videos[i].currentTime - evt.target.currentTime) > 2) {
							if (evt.target.currentTime <= videos[i].duration) {
								videos[i].currentTime = evt.target.currentTime;
							} else {
								videos[i].currentTime = videos[i].duration;
							}
						}
						videos[i].play();
					}
				}
			//}
		}

		function pause(evt) {
			if (seeking_index < 0) {
				playing = false;
				for (var i = 0; i < videos.length; i++) {
					if (!videos[i].paused && (!evt || videos[i] != evt.target)) {
						videos[i].pause();
					}
				}
			}
		}

		var last_time_update = 0;
		/* This is disabled for now.  Only causes trouble. */
		function timeupdate(evt) {
			var now = Date.now();
			if (now - last_time_update < 1000) {
				//don't update the time more than twice within the same half-second
				return;
			}

			/*  check in every so often to make sure the videos are still in sync */
			if (videos[time_controlling_index] === evt.target &&
				seeking_index < 0
				) {
				var target_time = videos[time_controlling_index].currentTime;
				for (var i = 0; i < videos.length; i++) {
					if (time_controlling_index != i) {
						var diff =  Math.abs(videos[i].currentTime - target_time);
						if (diff > 0.5) {
							seeking_index = time_controlling_index;

							seeked_counter++;

							videos[i].currentTime = target_time;

							last_time_update = now;
							return;
						}
					}
				}
			}
		}

		function checkTime() {
			var now = Date.now();

			if (now - last_time_update < 4000) {
				//don't update the time more than twice within the same half-second
				return;
			}

			var target_time = videos[time_controlling_index].currentTime;
			for (var i = 0; i < videos.length; i++) {
				if (audio_index != i &&
					!videos[i].paused) {
					var diff =  Math.abs(videos[i].currentTime - target_time);
					if (diff > 0.3) {
						seeking_index = audio_index;

						seeked_counter++;

						videos[i].currentTime = target_time;
						last_time_update = Date.now();
						//return;
					} else if (diff > 0.1) {
						//debug.innerHTML += 'checkTime<br/>&nbsp;&nbsp;diff = ' + diff + '<br/> ';
					}
				}
			}
			
		}

		function dataunavailable(evt) {
			pause();
		}

		function ended(evt) {
			if (videos[time_controlling_index] === evt.target) {
				for (var i = 0; i < videos.length; i++) {
					if (videos[i] !== evt.target &&
						videos[i].duration > evt.target.duration) {
						time_controlling_index = i;
						return;
					}
				}
			}
		}

		function volumechange(evt) {
			for (var i = 0; i < videos.length; i++) {
				if (videos[i] == evt.target) {
					if (evt.target.volume > 0.0) {
						volume_levels[i] = evt.target.volume;
					}
					return;
				}
			}
		}

		/* resizing interface stuff */

		var checkTime_id = 0;
		function setUpVideos(evt) {
			for (var j = 0; j < videos.length; j++) {
				if (videos[j].networkState < 2 && evt.target !== videos[j] && !videos[j].videoHeight) {
					return;
				}
			}
			//updateSizes(480);
			//selectAudio(1);

			for (var j = 0; j < videos.length; j++) {
				videos[j].removeEventListener('loadedmetadata',setUpVideos,false);
			}
			if (!checkTime_id) {
				checkTime_id = setInterval(checkTime,1000);
			}
		}

		var grab = document.getElementById('grab');
		var videoContainer = document.getElementById('video-container');
		// grab.addEventListener('mousedown', function() {
		// 	is_resizing = true;
		// }, false);
		// document.addEventListener('mouseup', function() {
		// 	is_resizing = false;
		// }, true);

		function updateSizes(pos) {
			if (pos < 100) {
				pos = 100;
			} else if (pos > 860) {
				pos = 860;
			}
			grab.style.left = pos + 1 + 'px';

			var right_width = 960 - pos;
			var left_width = pos;

			if(videos.length === 1){
				videos[0].style.width = '100%';
				videos[0].style.left = left_width / 2;
				$('#grab').remove();
			}

			if(videos.length === 2){
				videos[0].style.width = left_width + 'px';
				videos[1].style.width = right_width + 'px';
			}
		}

		document.addEventListener('mousemove', function(evt) {
			if(is_resizing) {
				var pos = evt.clientX - videoContainer.offsetLeft - 8;
				updateSizes(pos);
			}
		}, true);

		document.addEventListener('load',setUpVideos, false);

		/* set up events */
		for (var i = 0; i < videos.length; i++) {
			videos[i].load();

			volume_levels[i] = videos[i].volume;
			videos[i].addEventListener('play', play, false);
			videos[i].addEventListener('pause', pause, false);
			videos[i].addEventListener('seeking', seeking, true);
			videos[i].addEventListener('seeked', seeked, true);
			
			videos[i].addEventListener('dataunavailable', dataunavailable, true);
			videos[i].addEventListener('waiting', dataunavailable, true);
			
			videos[i].addEventListener('loadedmetadata',setUpVideos, false);
			videos[i].addEventListener('timeupdate',timeupdate, false);
		}
	}
}