function secondsToTime(seconds)
{
	var totalSec = seconds;
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + Math.round(seconds*10)/10 : Math.round(seconds*10)/10);
	return result;
}