<div class="col-xs-12 col-sm-6">
    <div class="row js_moment" data-moment-id="{{:moment_id}}">
        <div class="col-xs-6">
            <p><span class="glyphicon glyphicon-flag"></span> {{:event_name}} - {{:part_name}}</p>
            <p><span class="glyphicon glyphicon-calendar"></span> {{niceDate:date_created}}</p>
            <p><span class="glyphicon glyphicon-time"></span> {{niceTime:time_in_clip}}</p>
            <p><span class="glyphicon glyphicon-user"></span> {{:team_name}}</p>

        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    {{for tags}}
                    <span class="label label-warning">{{:tag_name}}</span>
                    {{/for}}
                </div>
                <div class="col-xs-12 col-md-6">
                    {{for players}}
                    <span class="label label-info">{{:first_name}} {{:last_name}}</span>
                    {{/for}}
                </div>
            </div>
        </div>

    </div>
</div>





<!--
{"moment_id":"234",
"date_created":"1432114629",
"time_in_clip":"128",
"event_name":"test"
,"event_id":"1",
"team_name":"Test Team",
"part_name":"Part 10",
"moment_files"
:[{
    "moment_file_id":"234",
    "moment_id":"234",
    "cam":"1",
    "filename":"234.mp4",
    "path":".\/videos\/1\/48\/234"
    ,"cam_description":"TestCam 1",
    "part_id":"48",
    "event_id":"1"}
],"tags":[],"tag_ids":[],"players":[],"player_ids":[]}
-->