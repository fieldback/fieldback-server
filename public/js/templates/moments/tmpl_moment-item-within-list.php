<div class="list-group-item open_moment">
    <div class="row bottom-buffer">
        {{for moment_files}}
            <div class="col-xs-{{:12/#parent.parent.data.number_of_moment_files}}">
                <video data-video-index="{{:#index}}" id="video_{{:moment_file_id}}" class="js_play_video video_container black_bg" src="{{site_url:path}}/{{:filename}}" controls></video>
            </div>
        {{/for}}
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 text-center bottom-buffer">
            <div class="btn-group">
                <button class="btn btn-default" id="btn_begin">
                    <span class="glyphicon glyphicon-fast-backward"></span>
                </button>
            </div>
            <div class="btn-group">
                <button class="btn btn-default" id="btn_step_backward">
                    <span class="glyphicon glyphicon-step-backward"></span>
                </button>
            </div>
            <div class="btn-group">
                <button class="btn btn-default" id="btn_play">
                    <span class="glyphicon glyphicon-play"></span>
                </button>
            </div>
            <div class="btn-group">
                <button class="btn btn-default" id="btn_pause">
                    <span class="glyphicon glyphicon-pause"></span>
                </button>
            </div>
            <div class="btn-group">
                <button class="btn btn-default" id="btn_step_forward">
                    <span class="glyphicon glyphicon-step-forward"></span>
                </button>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 text-center">
            <div class="btn-group" data-toggle="buttons" id="play_speed">
                <label class="btn btn-default">
                    <input type="radio" name="js_play_speed" value="5" autocomplete="off" class="btn btn-default" />5 %
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="js_play_speed" value="15" autocomplete="off" class="btn btn-default" />15 %
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="js_play_speed" value="35" autocomplete="off" class="btn btn-default" />35 %
                </label>
                <label class="btn btn-default active">
                    <input type="radio" name="js_play_speed" value="100" autocomplete="off" class="btn btn-default" checked />100 %
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="js_play_speed" value="130" autocomplete="off" class="btn btn-default" />130 %
                </label>
            </div>
        </div>
    </div>

    <div class="row" id="moment_info_panel">
        <div class="col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <span class="glyphicon glyphicon-flag"></span>
                            {{:event_name}} - {{:part_name}}
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <span class="glyphicon glyphicon-calendar"></span>
                            {{niceDate:date_created}}
                            {{niceTime:time_in_clip}}
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <span class="glyphicon glyphicon-user"></span>
                            {{:team_name}}
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-subtitle hidden-xs">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            Comment:
                        </div>
                        <div class="col-sm-4 hidden-xs">
                            Tags:
                        </div>
                        <div class="col-sm-4 hidden-xs">
                            Athletes:
                        </div>
                    </div>
                </div>
                <div class="panel-body" id="moment_info_body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 bottom-buffer">
                            {{if ~has_team(team_id) || ~is_admin()}}
                                <div class="form-group">
                                    <textarea class="form-control" id="comment_input" name="comment" placeholder="Comment on this moment">{{:comment}}</textarea>
                                </div>
                                <div class="btn-group pull-right">
                                    <button type="submit" disabled class="btn btn-default btn-sm" id="btn_save_comment" data-moment-id="{{:moment_id}}" data-event-id="{{:event_id}}">
                                        <span class="glyphicon glyphicon-floppy-save"></span> Save
                                    </button>
                                </div>
                            {{else}}
                                {{:comment}}
                            {{/if}}
                        </div>
                        <div class="col-xs-12 col-sm-4 js_tags bottom-buffer">
                            {{for tags}}
                                <span class="label label-warning">{{:tag_name}}</span>
                            {{/for}}
                                    <!--
                            <div class="row">
                                <div class="col-xs-12 col-md-6 bottom-buffer js_tags">
                                    {{for tags}}
                                        <span class="label label-warning">{{:tag_name}}</span>
                                    {{/for}}
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    {{if ~has_team(team_id) || ~is_admin()}}
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm" id="btn_edit_moment" data-moment-id="{{:moment_id}}" data-event-id="{{:event_id}}">
                                            <span class="glyphicon glyphicon-edit"></span> Edit <span class="hidden-md">tags</span>
                                        </button>
                                    </div>
                                    {{/if}}
                                </div>
                            </div>
                                    -->
                        </div>
                        <div class="col-xs-12 col-sm-4 js_player_tags">
                            {{for players}}
                                <span class="label label-info">{{:first_name}} {{:last_name}}</span>
                            {{/for}}
                                    <!--
                            <div class="row">
                                <div class="col-xs-12 col-md-6 bottom-buffer js_player_tags">
                                    {{for players}}
                                        <span class="label label-info">{{:first_name}} {{:last_name}}</span>
                                    {{/for}}
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    {{if ~has_team(team_id) || ~is_admin()}}
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm" id="btn_edit_moment" data-moment-id="{{:moment_id}}" data-event-id="{{:event_id}}">
                                            <span class="glyphicon glyphicon-edit"></span> Edit <span class="hidden-md">players tags</span>
                                        </button>
                                    </div>
                                    {{/if}}
                                </div>
                            </div>
                                    -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6 col-sm-4">
            <select class="simple-select" multiple title="Select playlist" id="playlist-select" data-live-search="false">
                {{for playlists}}
                <option value="{{:playlist_id}}"{{if has_moment==true}} selected{{/if}}>
                {{:title}}
                </option>
                {{/for}}
            </select>
            <span id="playlist-loader">
                <img src="/images/icons/loading_small.gif">
            </span>
        </div>
        <div class="col-xs-3 col-sm-4 text-center">
            {{if ~system_info_check("downloads")}}
            <a href="/download/moment/{{:moment_id}}" class="btn btn-default" id="download_moment">
                <span class="glyphicon glyphicon-floppy-save"></span> <span class="hidden-xs hidden-sm hidden-md">Download moment</span>
            </a>
            {{/if}}
            {{if ~system_info_check("shares")}}
            <a href="/shared/create/moment/{{:moment_id}}" target="_blanc" class="btn btn-default" id="share_moment">
                <span class="glyphicon glyphicon-share"></span> <span class="hidden-xs">Share</span>
            </a>
            {{/if}}
        </div>
        <div class="col-xs-3 col-sm-4 text-right">
            {{if ~has_team(team_id) || ~is_admin()}}
                <div class="btn-group">
                    <button class="btn btn-default" id="btn_edit_moment" data-moment-id="{{:moment_id}}" data-event-id="{{:event_id}}" data-edit-active="0">
                        <span class="glyphicon glyphicon-edit"></span> <span class="hidden-xs">Edit tags</span>
                    </button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-danger" id="btn_delete_moment" data-moment-id="{{:moment_id}}" data-event-id="{{:event_id}}">
                        <span class="glyphicon glyphicon-remove"></span> <span class="hidden-xs">Delete</span>
                    </button>
                </div>
            {{/if}}
        </div>
    </div>
    <div class="row" id="js_edit_placeholder">

    </div>

</div>