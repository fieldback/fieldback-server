<tr data-moment-id="{{:moment_id}}" data-index="{{:#index}}">
    <td>
        {{niceDate:date_created}}
    </td>
    <td>
        {{niceTime:time_in_clip}}
    </td>
    <td>
        {{:event_name}} - {{:part_name}}
    </td>
    <td>
        {{for tags}}
        <span class="label label-warning">{{:tag_name}}</span>
        {{/for}}
        {{for players}}
        <span class="label label-info">{{:first_name}} {{:last_name}}</span>
        {{/for}}
    </td>
</tr>

<!--
{"moment_id":"234",
"date_created":"1432114629",
"time_in_clip":"128",
"event_name":"test"
,"event_id":"1",
"team_name":"Test Team",
"part_name":"Part 10",
"moment_files"
:[{
    "moment_file_id":"234",
    "moment_id":"234",
    "cam":"1",
    "filename":"234.mp4",
    "path":".\/videos\/1\/48\/234"
    ,"cam_description":"TestCam 1",
    "part_id":"48",
    "event_id":"1"}
],"tags":[],"tag_ids":[],"players":[],"player_ids":[]}
-->