/**
 * Video items are inline instances of a moment -> with playback controls and some edit functionality
 * @param $videoRow
 * @param rowSelector
 */
var video_item_helper = function(videoRow, rowSelector, siteUrl, template) {
    var $videoRow = $(videoRow);

    // Bind a click handler to the parent container
    $videoRow.on("click", rowSelector, function () {
        var $this = $(this);
        // Get the moment_id from the data attribute
        var moment_id = $this.data('moment-id');
        // Remove all instances with the rowSelector from the videoRow container
        $videoRow.find(".js_active_video_row").remove();
        $videoRow.find(".js_video_hidden").removeClass("js_video_hidden").show();

        $this.addClass("js_video_hidden");
        // Load the moment in a JSON array
        $.ajax({
            url: siteUrl + "/moments/get_moment_json",
            type: 'POST',
            dataType: 'JSON',
            data: {moment_id: moment_id},
            success: function (response) {
                // create a row container and fill it with this data
                var $videoRow = $("<div />", {
                    class: "js_active_video_row"
                });
                // Create HTML from the template
                var moment_html = $.templates(template).render(response);
                $this.hide();
                // Fade this into the page
                $videoRow.html(moment_html).insertAfter($this);
                // Scroll to the newly opened video
                $('html, body').animate({
                    scrollTop: $(".js_active_video_row").offset().top
                }, 500);
                // Active the playlist functionality of the template file
                activate_playlist_selector($videoRow, moment_id);
                // Synchronize the videos
                synchronize(".js_play_video");
            }
        });
    });
};

var render_moment = function(template, moment_id, $target, siteUrl) {
    // Load the moment in a JSON array
    $.ajax({
        url: siteUrl + "/moments/get_moment_json",
        type: 'POST',
        dataType: 'JSON',
        data: {moment_id: moment_id},
        success: function (response) {
            // Create HTML from the template
            var moment_html = $.templates(template).render(response);
            // Fade this into the page
            $target.html(moment_html);
            watch_images($target);
            // Active the playlist functionality of the template file
            activate_playlist_selector($target, moment_id);
            // Synchronize the videos
            synchronize(".js_play_video");
        }
    });
};

var video_button_helper = function(videoRow) {
    var $videoRow = $(videoRow);

    // Bind click handlers to the play control buttons
    $videoRow.on("click", "#btn_begin", function() {
        event.stopPropagation();
        var videos = $(".js_play_video");
        videos.each(function(){
            this.currentTime = 0;
        });
    });

    $videoRow.on("click", "#btn_play", function(event){
        event.stopPropagation();
        var videos = $(".js_play_video");
        videos[0].play();
    });

    $videoRow.on("click", "#btn_pause", function(event){
        event.stopPropagation();
        var videos = $(".js_play_video");
        videos[0].pause();
    });

    $videoRow.on("click", "#btn_step_backward", function(event){
        event.stopPropagation();
        var videos = $(".js_play_video");
        videos.each(function(){
            this.pause();
            this.currentTime = videos[0].currentTime - 0.02;
        });
    });

    $videoRow.on("click", "#btn_step_forward", function(event){
        event.stopPropagation();
        var videos = $(".js_play_video");
        videos.each(function(){
            this.pause();
            this.currentTime = videos[0].currentTime + 0.02;
        });
    });

    $videoRow.on("change", "#play_speed", function(event){
        var new_speed = $(this).find(":checked").val() / 100;
        // Get all video elements, get their ID and then use raw javascript to set the playbackRate (works better)
        $(".js_play_video").each(function(){
            var id = $(this).attr('id');
            var video = document.getElementById(id);
            video.playbackRate = new_speed;
        });
    });

    $videoRow.on("click", "#btn_edit_moment", function(event) {
        var $this = $(this);
        var moment_id = $this.data("moment-id");
        var event_id = $this.data("event-id");

        if($this.data("edit-active") == "0") {
            $this.html('<span class="glyphicon glyphicon-chevron-up"></span> <span class="hidden-xs">Close</span>');
            $this.data("edit-active", "1");
            $('html, body').animate({
                scrollTop: $("#moment_info_panel").offset().top
            }, 500);

            $.ajax({
                url: site_url + 'moments/edit_tags_inline',
                type: 'POST',
                dataType: 'html',
                data: {moment_id: moment_id, event_id: event_id},
                success: function (data) {
                    // replace the moment info body with the tagfield editor
                    $("#js_edit_placeholder").html(data);
                }
            });
        } else {
            $this.html('<span class="glyphicon glyphicon-edit"></span> <span class="hidden-xs">Edit tags</span>');
            $this.data("edit-active", "0");
            $("#js_edit_placeholder").html("");
        }
    });

    /**
     * Once the comment field is changed by input, enable the save button
     */
    $videoRow.on("input", "#comment_input", function(event) {
        var $save_button = $("#btn_save_comment");
        $save_button.prop("disabled", false);
        $save_button.html("<span class='glyphicon glyphicon-floppy-save'></span> Save");
    });

    /**
     * On submit, change the text to 'saving' and submit the comment
     */
    $videoRow.on("click", "#btn_save_comment", function(event) {
        var $this = $(this);
        event.stopPropagation();
        event.preventDefault();
        var comment = $('#comment_input').val();
        var moment_id = $this.data("moment-id");
        console.log("changing to " + comment);
        // Disable while saving and change text
        $this.prop("disabled", true);
        $this.html("<span class='glyphicon glyphicon-floppy-save'></span> Saving...");
        $.ajax({
            url: site_url + 'moments/submit_comment',
            type: 'POST',
            dataType: 'json',
            data: {moment_id: moment_id, comment: comment},
            success: function (data) {
                $this.html("<span class='glyphicon glyphicon-floppy-saved'></span> Saved...");
            }
        });
    });

    $videoRow.on("click", "#btn_delete_moment", function(event) {
        if(window.confirm("Are you sure?")) {
            var moment_id = $(this).data("moment-id");
            $.ajax({
                url: site_url + 'moments/delete_moment',
                type: 'POST',
                dataType: 'json',
                data: {moment_id: moment_id},
                success: function (data) {
                    window.location.reload();
                }
            });
        }
    });

};



/**
 * KEYBOARD CONTROL FUNCTIONS
 */

// ********************************************************************************//
// ************************* Define keyboard numbers ******************************//
// ********************************************************************************//

var keyBegin    = 113;

var keyPlay     = 101;
var keyPause    = 114;
var keyStepB    = 119;
var keyStepF    = 116;

var speedKeys = {
    49: 5,
    50: 15,
    51: 35,
    52: 100,
    53: 130
};

var screenKeys = {
    97: 0,
    115: 1,
    100: 2,
    102: 3
};

// This boolean flag is set to false on any input field focus, to prevent funny behaviour when typing
var keyControlActive = true;

(function($){
    $(document).on("focusin", "textarea", function() {
        keyControlActive = false;
    });
    $(document).on("focusout", "textarea", function() {
        keyControlActive = true;
    });
})(jQuery);

// ********************************************************************************//

// Catch keypresses on the page
$(document).on("keypress", function(e) {
    if(keyControlActive) {
        switch(e.keyCode) {
            case keyBegin:
                $("#btn_begin").trigger("click");
                break;
            case keyPlay:
                $("#btn_play").trigger("click");
                break;
            case keyPause:
                $("#btn_pause").trigger("click");
                break;
            case keyStepB:
                $("#btn_step_backward").trigger("click");
                break;
            case keyStepF:
                $("#btn_step_forward").trigger("click");
                break;
            default:
                if(e.keyCode in speedKeys) {
                    console.log("Changing speed to: " + speedKeys[e.keyCode]);
                    // Find the play_speed button with the old and new values and change them
                    var $oldBtn = $("#play_speed").find(":checked");
                    $oldBtn.parent("label").removeClass("active");
                    $oldBtn.prop("checked", false);
                    var $newBtn = $("#play_speed").find("input[value="+ speedKeys[e.keyCode] + "]");
                    $newBtn.parent("label").addClass("active");
                    $newBtn.prop("checked", true);
                    // Trigger the change for the other scripts
                    $("#play_speed").trigger("change");
                }
                if(e.keyCode in screenKeys) {
                    console.log("Going fullscreen for video: " + screenKeys[e.keyCode]);
                    // Get the target video
                    var $targetVideo = $("video[data-video-index="+ screenKeys[e.keyCode]+"]");
                    // Check if it has the class fullscreenvideo (was it already fullscreen?)
                    var wasFullscreenVideo = $targetVideo.hasClass("fullscreenvideo");
                    // Remove fullscreen from ALL videos
                    $("video").each(function(){ $(this).removeClass("fullscreenvideo"); });
                    // Was this one closed? Then we want to open it! Otherwise, do nothing
                    if(!wasFullscreenVideo) $targetVideo.addClass("fullscreenvideo");
                }
                break;
        }
    }
});

/**
 * Playlist helpers -> triggers the selectpicker
 */
var activate_playlist_selector = function($element, moment_id) {
    var $loader = $element.find("#playlist-loader");
    $loader.hide();
    var $playlist_select = $element.find("#playlist-select");
    // Stupid thing does not work well on mobile
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        $playlist_select.selectpicker('mobile');
    } else {
        $playlist_select.selectpicker();
    }

    $playlist_select.on("change", function() {
        // Get the values as an array with val()
        var playlist_ids = $playlist_select.val();
        // Start the 'spinner' to indicate activity
        $loader.show();
        // Sync this array with the server with Ajax and do the complex math server-side (what needs to be removed or added?)
        $.ajax({
            url: '/playlist_ajx/sync_moment_to_playlist_ids',
            type: 'post',
            dataType: 'json',
            data: {
                moment_id: moment_id,
                playlist_ids: playlist_ids
            }
        }).always(function (data) {
            $loader.hide();
            //console.log(data);
        });
    });
};