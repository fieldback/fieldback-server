var mobile_device = false;

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	mobile_device = true;
}

$(function(){

	//click functions
	$('.feedback').on('click', function(){ feedbackModal($(this)); });

	$('.confirm').on('click', function(){
		if(confirm('Are you sure?')) {
			return true;
		}
		return false;
	});

    var $sync_settings = $("#sync_settings");

    function updateSyncSettings(label_text, server_time, last_sync_time) {
        $sync_settings.find("#last_sync_label").html('<span class="glyphicon glyphicon-refresh"></span> '+ label_text);
        var now = moment.unix(server_time);
        var sync_time = moment.unix(last_sync_time);
        $sync_settings.find("#last_sync_time").html(
            sync_time.from(now)
        );
        // Depending on the time, colour the text green, orange or red
        var difference = Math.abs(sync_time.diff(now, 'hours'));
        var color = "#000000"; // good color
        if(difference > 24) {
            color = "#FF9900"; // Orange
        }
        if(difference > 48) {
            color = "#FF0000"; // Red
        }
        $sync_settings.css('color', color);
    }

    // On load, the last_sync time is in the data-attribute of the last_sync_time
    var initial_last_sync = $sync_settings.find("#last_sync_time").data("last-sync-time");
    var time_now_server = $sync_settings.find("#last_sync_time").data("current-server-time");
    updateSyncSettings('Last sync: ', time_now_server, initial_last_sync);

    // sync_settigs buttons if present
    $sync_settings.on('click', function(event){
        // Don't follow the link
        event.preventDefault();
        $sync_settings.html("Syncing...");
        // Update with an ajax request and let the spinner spin during this request
        $.ajax({
            url: base_url + 'api/sync_settings',
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                if(response['error']) {
                    $sync_settings.html(response['msg']);
                    $sync_settings.css('color', "red");
                } else {
                    $sync_settings.html(response['msg']);
                    $sync_settings.css('color', "black");
                }
                console.log(response['debug']);
            },
            fail: function(response) {
                console.log(response);
            }
        });
    });

});

function feedbackModal(element) {
	var url = element.data('uri');
	$.ajax({
		url: base_url + 'feedback/feedback_form',
		type: 'POST',
		dataType: 'html',
		data: {url: url},
		success: function(data){
			$('html').prepend(data);
			$('#feedback-modal').modal('show');
		}
	});
}

/*
	This function shows a moment_modal on each page where a moment
	*/
function show_moment(moment_id) {
	// Make sure to pause all running video elements on the page
	$('video').each(function() { this.pause(); });
	$.ajax({
		url: base_url + 'moments/show_moment_files',
		type: 'POST',
		dataType: 'html',
		data: {moment_id: moment_id},
		success: function(data){
			$('html').prepend(data);
			$('#edit-moment-modal').modal('show');
			// !Automatically start playing the videos when they are loaded -> DOES NOT WORK !? Keep code commented for future reference
			// GIVES A LOT OF FUNNY BEHAVIOR
			// $('#edit-moment-modal').find('videos').each(function(){ this.oncanplaythrough = this.play(); });
		}
	});
}


$(document).ready(function(){
	// Use event propagation to monitor clicks on moments
	$('div.moments').on('click', '.tagged-moment', function(){
		var moment_id = $(this).data('moment-id');
		show_moment(moment_id);
	});
	
});

/**
 * Check in which environment bootstrap is. The different environments are XS, SM, MD and LG.
 * Can be used to define if a function has to be used or not depending on the environment
 * @return {string} environment abbreviation
 */
function findBootstrapEnvironment() {
    var envs = ['xs', 'sm', 'md', 'lg'];

    $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];

        $el.addClass('hidden-'+env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env
        }
    };
}

// Display timer in a nice format
var formatTime = function(time) {
    h = Math.floor(time / (60 * 60 * 1000));
    time = time % (60 * 60 * 1000);
    m = Math.floor(time / (60 * 1000));
    time = time % (60 * 1000);
    s = Math.floor(time / 1000);
    time = time % 1000;
    ms = Math.floor(time / 100);

    newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2) + ':' + pad(ms, 1   );
    return newTime;
};
var pad = function(num, size) {
    var s = "0000" + num;
    return s.substr(s.length - size);
};

/**
 * Update the selected tagfield for the user
 * @param  {int} 	tagfield_id Tagfield ID
 * @return {array}             	data with error or success
 */
function update_selected_tagfield_for_user(tagfield_id)
{
	$.ajax({
		type: 'POST',
		url: base_url + 'tagfields/update_selected_tagfield_for_user',
		dataType: 'json',
		data: {tagfield_id: tagfield_id},
		success: function(data){
			if(data == 'success'){
				console.log('tagfield updated');
			}else{
                console.log(data);
			}
		}
	})
}

/**
 * Get the selected tagfield for the user
 * @return {array} json: containing the selected tagfield
 */
function get_selected_tagfield_for_user()
{
	$.ajax({
		type: 'POST',
		url: base_url + 'tagfields/get_selected_tagfield_for_user',
		dataType: 'json',
		data: {user_id: user_id},
		success: function(data){
			var selected_tagfield = data.selected_tagfield;
			if(selected_tagfield !== null){
				$('#tagfield' + selected_tagfield + ' a').tab('show');
			}else{
				$('.tagfields_tab a:first').tab('show');
			}
		}
	});
}

/**
 * Extension of the JSRender templating engine: adds a {{range}} function you can use to iterate over for loops with a certain range
 * This is used for iterating only a few tags for each moment
 */
$.views.tags({
    range: {
        // Inherit from {{for}} tag
        baseTag: "for",

        // Override the render method of {{for}}
        render: function(val) {
            var array = val,
                start = this.tagCtx.props.start || 0,
                end = this.tagCtx.props.end;

            if (start || end) {
                if (!this.tagCtx.args.length) {
                    // No array argument passed from tag, so create a computed array of integers from start to end
                    array = [];
                    end = end || 0;
                    for (var i = start; i <= end; i++) {
                        array.push(i);
                    }
                } else if ($.isArray(array)) {
                    // There is an array argument and start and end properties, so render using the array truncated to the chosen range
                    array = array.slice(start, end);
                }
            }

            // Call the {{for}} baseTag render method
            return this.base(array);
        },

        // override onArrayChange of the {{for}} tag implementation
        onArrayChange: function(ev, eventArgs) {
            this.refresh();
        }
    }
});

/**
 * Function to determine if an image is loaded by checking some parameters -> if not, display a loader and attempt to load it a few times after a timeout
 * @param img			The image DOM object
 * @returns boolean		Is the image loaded or not?
 */
var isImageLoaded = function(img) {
	return img.complete && typeof img.naturalWidth != 'undefined' && img.naturalWidth != 0;
};

var watch_images = function(selector) {
	// Watch the list for images loading -> if not loaded correctly replace by a loader image and try again in a few seconds
	$(selector).imagesLoaded().progress(function(instance, image) {
		if(!image.isLoaded) {
			// Save the original src
			var original_src = image.img.src;
			// Set a loader for now until the real image is available
			image.img.src = "/images/icons/loader.gif";
			// Set a timeout to try again if the image can be loaded by sending a HEAD request, do this 3 times
			var triedTimes = 0;
			var setLoadThumb = function() {
				setTimeout(function () {
					if(fileExists(original_src)) {
						// Set new src
						image.img.src = original_src;
					} else {
						if(triedTimes < 3) {
							console.log("Tried to load image but still failed... trying again in 5 seconds");
							setLoadThumb();
						} else {
							console.log("The thumbnail is not there! Stopped trying to load.");
						}
						triedTimes++;
					}
				}, 2500);
			};
			// Activate first timeout to load
			setLoadThumb();
		}
	});
};

var fileExists = function(url) {
	if(url){
		var req = new XMLHttpRequest();
		req.open('HEAD', url, false);
		req.send();
		return req.status==200;
	} else {
		return false;
	}
};